onlineManager = new function () {

    var transport = sockJsHandler;

    var c = 0;

    var iterator = 0;

    var context = this;

    var interval;

    this.init = function () {
        $('#tagCloud1, #tagCloud2, #tagCloud3').tagcanvas({
            textColour: '#000000',
            outlineColour: '#000000',
            maxSpeed: 0.003,
            minSpeed: 0.003,
            depth: 0.1,
            zoom: 1.2,
            wheelZoom: false,
            initial: [0, 0.7],
            dragThreshold: 5,
            shape: "hcylinder",
            reverse: true,
            lock: "x",
            freezeActive: false,
            dragControl: true,
            outlineOffset: 5
        });

        _call();
    };

    var _call = function () {
        c = 0;
        transport.send({
            user_id: userManager.getCurrentUser().getUserId(),
            action: "getRandomOnlineUsers",
            onlineUserLimit: 45
        });
    };

    this.stop = function () {
        clearInterval(interval);
    };

    this.setOnlineUsers = function (data) {
        var ul1 = $('#tagCloud1 ul'), ul2 = $('#tagCloud2 ul'), ul3 = $('#tagCloud3 ul');

        ul1.empty();
        ul2.empty();
        ul3.empty();

        $(data.users).each(function (key, value) {
            if (key < 15) {
                $('#online_user_tmpl').tmpl(value).appendTo(ul1);
            }

            if (key >= 15 && key < 30) {
                $('#online_user_tmpl').tmpl(value).appendTo(ul2);
            }

            if (key >= 30) {
                $('#online_user_tmpl').tmpl(value).appendTo(ul3);
            }
        });

        $('#tagCloud1, #tagCloud2, #tagCloud3').tagcanvas('update');

        interval = setTimeout(function () {

            if (!$('.start-page').is(':visible')) {
                return;
            }

           // _call();
        }, 20000);
    }
};