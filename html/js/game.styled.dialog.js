(function ($) {

    $.fn.styledDialog = function (opts) {

        /**
         * Default options
         * @type {{}}
         */
        var defaults = {
            btn: '',
            btnLabel: '',
            type: 'male'
        };

        /**
         * Options
         * @type {extend|*}
         */
        var options = $.extend(defaults, opts);

        /**
         * Reserve this
         * @type {fn}
         */
        var context = this;

        /**
         * Make jquery dialog styled!
         */
        function make() {
            var $this = $(context);

            //add shadow to box
            $this.parent()
                .addClass('sd-box')
                .css({
                    border: 'none',
                    borderTop: '1px solid #ccc',
                    padding: 0
                })
                .find('.ui-dialog-titlebar').remove();

            //wrap styled div
            $this.css({
                padding: 0,
                overflow: 'hidden'
            }).wrap('<div class="sd-wrapper ' + options.type + '"></div>');

            //make sd button
            makeBtn();

            //show when done!
            $this.dialog('option', 'resizable', false).dialog('open');
        }

        function makeBtn() {
            if (!options.btn) return;

            $(options.btn)
                .addClass('sd-btn')
                .attr('value', options.btnLabel)
                .val(options.btnLabel)
                .text(options.btnLabel);
        }

        make();
    }

})(jQuery);