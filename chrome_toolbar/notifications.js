function notify() {

    // Create a simple text notification:
    var notification = webkitNotifications.createNotification(
        'icon.png',  // icon url - can be relative
        'Hello!',  // notification title
        'Lorem ipsum...'  // notification body text
    );


    // Then show the notification.
    notification.show();
}

notify();