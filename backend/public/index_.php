<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/css/bootstrap.css" type="text/css">

    <script src="/js/jquery-2.0.3.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/sockjs.js"></script>
    <script src="/js/transport.js"></script>
    <script src="/js/logger.js"></script>

    <style>
        pre {
            outline: 1px solid #ccc;
            padding: 5px;
            margin: 5px;
        }

        .string {
            color: green;
        }

        .number {
            color: darkorange;
        }

        .boolean {
            color: blue;
        }

        .null {
            color: magenta;
        }

        .key {
            color: red;
        }
    </style>

</head>

<body>
<div id="x"></div>

<a href="#" onclick="return false;" id="update">Обновить</a>
<script>
    apiManager = new function () {
        var registeredCallback = null, transport = sockJsHandler;

        this.requestTables = function (callback) {
            registeredCallback = callback;

            transport.send({
                user_id: 1000,
                action: 'gameGetTables',
                module: 'apiManager'
            });
        };

        this.apiResponse = function (data) {
            if (registeredCallback) registeredCallback(data);
        }
    };


    $(function () {

        $('#update').on('click', function () {
            $('#x').empty();

            apiManager.requestTables(function (data) {
                $.each(data.tables, function () {
                    var json = JSON.stringify(this);
                    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                        var cls = 'number';
                        if (/^"/.test(match)) {
                            if (/:$/.test(match)) {
                                cls = 'key';
                            } else {
                                cls = 'string';
                            }
                        } else if (/true|false/.test(match)) {
                            cls = 'boolean';
                        } else if (/null/.test(match)) {
                            cls = 'null';
                        }
                        return '<span class="' + cls + '">' + match + '</span>';
                    });

                    $('#x').append($('<pre>').html(json));
                });
            });


        });

        $(document).on('socketConnectionReady', function () {
            sockJsHandler.send({
                action: "auth",
                user_id: 1000,
            });
        });
    })
</script>
</body>
</html>