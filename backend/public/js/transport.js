var sockJsHandler = new function () {
    var context = this,
        sock,
        response = {};

    var _init = function () {
        context.url = 'http://game.vkapp-lubof.ru:8991/broadcast';
        if (!context.url) {
            throw "SockJS url is empty";
        }

        logger.log('Trying to connect');
        sock = new SockJS(context.url);

        sock.onopen = function () {
            logger.log('Socket opened');

            $(document).trigger('socketConnectionReady', ['Custom', 'Event']);

            sock.onmessage = function (e) {
                logger.log('Message recived: ' + e.data);
                var json = $.parseJSON(e.data);

                if (json.action) {
                    var a = "if (typeof " + json.module + "!='undefined'&&" + json.module + "['" + json.action + "']) {" + json.module + "['" + json.action + "'](json);}";
                    eval(a);
                }
            };

            sock.onclose = function () {

                $('.ui-dialog').each(function () {
                    $(this).css({zIndex: 1});
                });

                $('.ui-widget-overlay').remove();
            };
        };
    };

    var _isConnected = function () {
        return !!((typeof(sock.readyState) != 'undefined' && sock.readyState == 1));
    };

    var _send = function (m) {
        if (!m || typeof(m) != 'object') {
            return false;
        }

        if (!_isConnected()) {
            logger.log('Cant send data, socket is not connected');
            return false;
        }

        logger.log('Trying to send data: ' + JSON.stringify(m));

        response = JSON.stringify(m);
        sock.send(response);
        response = {};
        return true;
    };

    this.send = function (m) {
        return _send(m);
    };

    $(function () {
        _init();
    })
}();
