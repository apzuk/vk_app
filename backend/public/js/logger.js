var logger = new function () {
    var context = this;
    var strBelow, strUnder, stopped = false;

    var defaults = {
        belowChar: '*',
        underChar: '-',
        length: 25,
        wrap: false,
        clear: false,
        forceLog: false
    };

    this.stopLogging = function () {
        stopped = true;
    };

    this.startLogging = function () {
        stopped = false;
    };

    this.log = function (msg, opts) {
        //log options
        var options = $.extend({}, defaults, opts);

        //if logging stopped, do not log!
        if (stopped && !options.forceLog) {
            return;
        }

        //wrap chars
        var strBelow = strUnder = '';
        for (var iterator = 0; iterator < options.length; iterator++) {
            strBelow += options.belowChar;
            strUnder += options.underChar;
        }

        //clear consol if required
        if (options.clear) {
            _clear();
        }

        //wrap log
        if (options.wrap) {
            _log(strBelow);
        }

        //log message
        _log(msg);

        //wrap log
        if (options.wrap) {
            _log(strUnder);
        }

        return context;
    };

    var _log = function (m) {
        console.log(m);
    };

    var _clear = function () {
        console.clear();
    }
};