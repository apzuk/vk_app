var gameModel = function (m) {
    /**
     *
     * Game model
     *
     * @type {{}}
     */
    var model = {};

    /**
     *
     * This variable
     *
     * @type {gameModel}
     */
    var gModel = this;

    /**
     *
     * Game players
     *
     * @type {{}}
     */
    var players = {
        users: {},
        males: [],
        females: []
    };

    /**
     *
     * Is playing?!
     *
     * @type boolean
     */
    var playing;

    /**
     *
     * Server time
     *
     * @type Date
     */
    var serverTime;

    /**
     *
     * Model last update time
     *
     * @type Date
     */
    var dateUpdate;

    /**
     *
     * Table iterator
     *
     * @type String
     */
    var iterator;

    /**
     *
     * All game answers here
     *
     * @type {{}}
     */
    var answers = {};

    /**
     *
     * User last answers
     *
     * @type {{}}
     */
    var lastAnswers = {};

    /**
     *
     * Set user chosen
     *
     * @type {{}}
     */
    var chosen = {};

    /**
     *
     * User questions
     *
     * @type {{}}
     */
    var questions = {

    };

    var inited = false;

    /**
     *
     * Step durations
     *
     * @type {{}}
     */
    var stepDuration = {};

    /**
     *
     * Game step
     *
     * @type int
     */
    var step;

    this.updateMe = function (m) {
        logger.log('Game model updating...');

        if (m.playing && !inited) {
            inited = true;
            _setModel(m);
            return;
        }

        model = m;

        // Game step
        step = model.step;

        // Server time
        serverTime = model.serverTime;

        // Model last update time
        dateUpdate = model.dateUpdate;

        // Is playing?!
        playing = model.playing;
    };

    this.stopPlaying = function () {
        playing = false;
    };

    this.setAnswers = function () {
        _parseAnswers();
    };

    this.setChosen = function () {
        _parseChosen();
    };

    this.dump = function () {
        logger.log(gModel);
    };

    this.setLastAnswers = function () {
        _parseLastAnswers();
    };

    this.modelSource = function () {
        return model;
    };

    this.players = function () {
        return players.users;
    };

    this.getUserById = function (u_id) {
        var users = this.players();
        return users[u_id];
    };

    this.getUserEnd = function (user_id) {
        var gender = gModel.getUserById(user_id);

        return gender == 1 ? 'а' : '';
    };

    this.males = function () {
        return players.males;
    };

    this.females = function () {
        return players.females;
    };

    this.opposite = function () {
        var gender = userManager.getCurrentUser().getGender();

        if (gender == 1) {
            return players.males;
        }

        if (gender == 2) {
            return players.females;
        }

        return {};
    };

    this.onesided = function () {
        var gender = userManager.getCurrentUser().getGender();

        if (gender == 2) {
            return players.males;
        }

        if (gender == 1) {
            return players.females;
        }

        return {};
    };

    this.getUserIndexById = function (user_id) {
        var gender = userManager.getCurrentUser().getGender();

        if (gender == 1) {
            return players.males.indexOf(user_id);
        }

        if (gender == 2) {
            return players.females.indexOf(user_id);
        }

        return -1;
    };

    this.getUserByIndexFromOpposite = function (index) {
        var gender = userManager.getCurrentUser().getGender();

        if (gender == 1) {
            return players.males[index];
        }

        if (gender == 2) {
            return players.females[index];
        }

        return {};
    };

    this.step = function () {
        return parseInt(step);
    };

    this.getUserQuestion = function (user_id) {
        return questions[user_id];
    };

    this.getMyQuestion = function () {
        var myId = userManager.getCurrentUser().getUserId();
        return gModel.getUserQuestion(myId);
    };

    this.getUserAnswers = function (user_id) {
        return answers['u_' + user_id]['answers'];
    };

    this.hasUserAnswers = function (user_id) {
        if ($.isEmptyObject(answers['u_' + user_id]['answers'])) {
            return false;
        }

        var has = false;
        $.each(answers['u_' + user_id]['answers'], function () {
            if ($.trim(this.answer)) has = true;
        });

        return has;
    };

    this.getUserComments = function (user_id) {
        return lastAnswers['u_' + user_id];
    };

    this.getMyComments = function () {
        var uId = userManager.getCurrentUser().getUserId();
        return gModel.getUserComments(uId);
    };

    this.hasUserChosen = function (user_id) {
        return chosen[user_id] != undefined;
    };

    this.getChosenUser = function (user_id) {
        return gModel.getUserById(chosen[user_id]);
    };

    this.hasOverlap = function (currentUserId) {
        if (!currentUserId) currentUserId = userManager.getCurrentUser().getUserId();

        var overlap = false;
        $.each(chosen, function (key, uId) {
            if (currentUserId == uId) {
                if (gModel.hasUserChosen(currentUserId) &&
                    gModel.getChosenUser(currentUserId).getUserId() == key) {
                    overlap = key;
                }
            }
        });

        if (!overlap) return false;

        return gModel.getUserById(overlap);
    };

    this.getChosenSocId = function () {
        return gModel.modelSource()['vk_id'] != undefined ? gModel.modelSource()['vk_id']['soc_id'] : false;
    };

    this.getMyKey = function () {
        var user_id = userManager.getCurrentUser().getUserId();
        return model['keys'][user_id.toString()] != undefined ? model['keys'][user_id.toString()] : false;
    };

    this.getUserVKLink = function (success_callback, error_callback) {
        var key = gModel.getMyKey();

        if (!key) return false;

        $.post('/mvc.php?c=User&do=getVkLink', {
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid(),
            key: key
        }, function (data) {
            if (data.result) success_callback(data.link);
            else error_callback();
        }, 'json');

        return true;
    };

    this.isPlaying = function () {
        return playing;
    };

    this.serverTime = function () {
        return serverTime;
    };

    this.iterator = function () {
        return iterator;
    };

    this.dateUpdate = function () {
        return dateUpdate;
    };

    this.stepDuration = function () {
        var step = gModel.step() + 1;
        return stepDuration['step' + step];
    };
    // ======================================================
    var _setModel = function (m) {
        model = m;

        // Set game step
        step = model.step;

        // Is playing?!
        playing = model.playing;

        // Server time
        serverTime = model.serverTime;

        // Model last update date
        dateUpdate = model.dateUpdate;

        // Table iterator
        iterator = model.iterator;

        // Set step durations
        stepDuration = model.stepDuration;

        // Parse players data
        $(['male', 'female']).each(function () {
            var gender = this;

            // Each males and females
            $.each(model[gender], function (index, user) {
                user['gender'] = gender == 'female' ? 1 : 2;

                var current_user = new userModel(user), current_user_id = current_user.getUserId();

                if (gModel.isPlaying()) {
                    // Add new user to array
                    _addUser(current_user);
                }

                // Add user question to question list
                _addQuestion(current_user_id, model[gender][current_user_id]['question']);
            });
        });

        // Model is initialed!
        inited = true;
    };

    var _addUser = function (user) {
        if (user.getGender() == 1) {
            players.females.push(user);
        }
        if (user.getGender() == 2) {
            players.males.push(user);
        }

        players.users[user.getUserId()] = user;
    };

    var _addQuestion = function (user_id, question) {
        questions[user_id] = question;
    };

    var _parseAnswers = function () {
        var users = gModel.players();

        // Initialize object
        $.each(users, function (user_id, user) {
            answers['u_' + user.getUserId()] = {
                answers: {},
                got: {}
            }
        });

        $(gModel.males()).each(function () {
            var current_user = this,
                current_user_id = current_user.getUserId(),
                user_answer = model['male'][current_user_id.toString()]['answer'],
                g = answers['u_' + current_user_id]['got'],
                a = answers['u_' + current_user_id]['answers'];

            // set user got answers
            if (!$.isEmptyObject(user_answer)) {
                $.each(user_answer, function () {
                    g[this.user_id] = {
                        user: gModel.getUserById(this.user_id),
                        answer: this.answer
                    }
                });
            }

            // set user answers
            $(gModel.females()).each(function () {
                var c_user = this,
                    c_user_id = c_user.getUserId(),
                    question = gModel.getUserQuestion(c_user_id),
                    answer = model['female'][c_user_id];

                if (answer['answer'] != undefined && answer['answer'][current_user_id] != undefined) {
                    a[c_user_id] = {
                        question: question,
                        answer: answer['answer'][current_user_id]['answer']
                    }
                }
            })
        });

        $(gModel.females()).each(function () {
            var current_user = this,
                current_user_id = current_user.getUserId(),
                user_answer = model['female'][current_user_id.toString()]['answer'],
                g = answers['u_' + current_user_id]['got'],
                a = answers['u_' + current_user_id]['answers'];

            // set user got answers
            if (!$.isEmptyObject(user_answer)) {
                $.each(user_answer, function () {
                    g[this.user_id] = {
                        user: gModel.getUserById(this.user_id),
                        answer: this.answer
                    }
                });
            }

            // set user answers
            $(gModel.males()).each(function () {
                var c_user = this,
                    c_user_id = c_user.getUserId(),
                    question = gModel.getUserQuestion(c_user_id),
                    answer = model['male'][c_user_id];

                if (answer['answer'] != undefined && answer['answer'][current_user_id] != undefined) {
                    a[c_user_id] = {
                        question: question,
                        answer: answer['answer'][current_user_id]['answer']
                    }
                }
            })
        });
    };

    var _parseLastAnswers = function () {
        $(gModel.males()).each(function () {
            var current_user = this,
                current_user_id = current_user.getUserId();

            if (model['male'][current_user_id]['lastAnswer'] == undefined) model['male'][current_user_id]['lastAnswer'] = {};

            lastAnswers['u_' + current_user_id] = model['male'][current_user_id]['lastAnswer'];
        });
        $(gModel.females()).each(function () {
            var current_user = this,
                current_user_id = current_user.getUserId();

            if (model['female'][current_user_id]['lastAnswer'] == undefined) model['female'][current_user_id]['lastAnswer'] = {};

            lastAnswers['u_' + current_user_id] = model['female'][current_user_id]['lastAnswer'];
        });
    };

    var _parseChosen = function () {
        chosen = model['chosen'];
    };

    if (m) {
        _setModel(m);
    }
};