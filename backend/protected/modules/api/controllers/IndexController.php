<?php

class IndexController extends Controller
{

    public function actionAddQuestion()
    {
        $question = new SysQuestions();
        $question->attributes = $_POST;


        $answers = array();
        foreach (app()->request->getPost('answers') as $key => $answer) {
            if (!trim($answer)) {
                continue;
            }

            $answers['answer' . ($key + 1)] = $answer;
        }
        $question->answers = CJSON::encode($answers);

        $question->save();

        dump($question->getErrors());

        $this->renderPartial('addQuestion');
    }

}