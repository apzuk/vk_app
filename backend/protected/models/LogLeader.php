<?php

/**
 * This is the model class for table "log_leader".
 *
 * The followings are the available columns in table 'log_leader':
 * @property integer $log_id
 * @property integer $user_id
 * @property integer $city_id
 * @property integer $current_bid
 * @property string $message
 * @property string $update_date
 * @property string $status
 */
class LogLeader extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_leader';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('update_date', 'required'),
            array('user_id, city_id, current_bid', 'numerical', 'integerOnly' => true),
            array('message', 'length', 'max' => 255),
            array('status', 'length', 'max' => 6),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('log_id, user_id, city_id, current_bid, message, update_date, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
            'city' => array(self::BELONGS_TO, 'SysCities', 'city_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'log_id' => 'Log',
            'user_id' => 'User',
            'city_id' => 'City',
            'current_bid' => 'Current Bid',
            'message' => 'Message',
            'update_date' => 'Update Date',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('log_id', $this->log_id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('city_id', $this->city_id);
        $criteria->compare('current_bid', $this->current_bid);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('status', $this->status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LogLeader the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
