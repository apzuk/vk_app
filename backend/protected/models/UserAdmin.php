<?php

/**
 * This is the model class for table "user_admin".
 *
 * The followings are the available columns in table 'user_admin':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $role
 * @property integer $active
 */
class UserAdmin extends CActiveRecord
{
    const ACCESS_ADMIN = 2;

    const PWD_DEFAULT_LEN = 20;

    public $rememberMe;
    public $passwordNew;

    private $identity;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_admin';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, password', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('login, password, role', 'length', 'max' => 255),
            array('password', 'authenticate', 'on' => 'login'),
            // The following rule is used by search().
            array('id, login, password, role, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'role' => 'Role',
            'active' => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserAdmin the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function authenticate()
    {
        if ($this->hasErrors()) {
            return false;
        }

        $this->identity = new UserIdentity($this->login, $this->password);

        if ($this->identity->authenticate()) {
            return true;
        } else {
            return $this->addError('password', 'Неправильный пароль!');
        }
    }

    public function authorize()
    {
        $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
        Yii::app()->user->login($this->identity, $duration);
    }


    public function getIdentity()
    {
        return $this->identity;
    }
}
