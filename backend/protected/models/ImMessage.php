<?php

/**
 * This is the model class for table "im_message".
 *
 * The followings are the available columns in table 'im_message':
 * @property integer $message_id
 * @property integer $sender_user_id
 * @property integer $receiver_user_id
 * @property string $message
 * @property string $sender_status
 * @property string $receiver_status
 * @property string $attachment
 * @property string $create_ts
 * @property string $conv_id
 *
 * The followings are the available model relations:
 * @property ImComplain[] $imComplains
 * @property UserAccount $receiverUser
 */
class ImMessage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'im_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_ts', 'required'),
			array('sender_user_id, receiver_user_id', 'numerical', 'integerOnly'=>true),
			array('sender_status, receiver_status', 'length', 'max'=>6),
			array('conv_id', 'length', 'max'=>125),
			array('message, attachment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('message_id, sender_user_id, receiver_user_id, message, sender_status, receiver_status, attachment, create_ts, conv_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imComplains' => array(self::HAS_MANY, 'ImComplain', 'message_id'),
			'receiverUser' => array(self::BELONGS_TO, 'UserAccount', 'receiver_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message_id' => 'Message',
			'sender_user_id' => 'Sender User',
			'receiver_user_id' => 'Receiver User',
			'message' => 'Message',
			'sender_status' => 'Sender Status',
			'receiver_status' => 'Receiver Status',
			'attachment' => 'Attachment',
			'create_ts' => 'Create Ts',
			'conv_id' => 'Conv',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('sender_user_id',$this->sender_user_id);
		$criteria->compare('receiver_user_id',$this->receiver_user_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('sender_status',$this->sender_status,true);
		$criteria->compare('receiver_status',$this->receiver_status,true);
		$criteria->compare('attachment',$this->attachment,true);
		$criteria->compare('create_ts',$this->create_ts,true);
		$criteria->compare('conv_id',$this->conv_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ImMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
