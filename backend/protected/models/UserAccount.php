<?php

class UserAccount extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_account';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('vip_end, create_ts', 'required'),
            array(
                'gender, online, city_id, visibility, cost, show_profile, ref_id, continuously_visit, is_ready, invitations',
                'numerical',
                'integerOnly' => true
            ),
            array('username, soc_id, sid', 'length', 'max' => 255),
            array('avatar, avatar_orig', 'length', 'max' => 50),
            array('balance', 'length', 'max' => 5),
            array('color', 'length', 'max' => 100),
            array('hash', 'length', 'max' => 45),
            array('birth_date, last_visit_date', 'safe'),
            array(
                'user_id, username, avatar, avatar_orig, gender, birth_date, soc_id, online, city_id, balance, sid, color, visibility, cost, show_profile, vip_end, create_ts, ref_id, last_visit_date, continuously_visit, is_ready, invitations, hash',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'imComplains' => array(self::HAS_MANY, 'ImComplain', 'user_id'),
            'imMessages' => array(self::HAS_MANY, 'ImMessage', 'receiver_user_id'),
            'logUserRefs' => array(self::HAS_MANY, 'LogUserRef', 'ref_user_id'),
            'logUserRefs1' => array(self::HAS_MANY, 'LogUserRef', 'user_id'),
            'logUserVips' => array(self::HAS_MANY, 'LogUserVip', 'user_id'),
            'logVkNotifs' => array(self::HAS_MANY, 'LogVkNotif', 'user_id'),
            'notifications' => array(self::HAS_MANY, 'Notification', 'owner_user_id'),
            'notifications1' => array(self::HAS_MANY, 'Notification', 'user_id'),
            'ref' => array(self::BELONGS_TO, 'UserAccount', 'ref_id'),
            'userAccounts' => array(self::HAS_MANY, 'UserAccount', 'ref_id'),
            'city' => array(self::BELONGS_TO, 'SysCities', 'city_id'),
            'userHasGroups' => array(self::HAS_MANY, 'UserHasGroup', 'user_id'),
            'userLeaders' => array(self::HAS_MANY, 'UserLeader', 'user_id'),
            'userLikeLogs' => array(self::HAS_MANY, 'UserLikeLog', 'target_user_id'),
            'userLikeLogs1' => array(self::HAS_MANY, 'UserLikeLog', 'user_id'),
            'userRibbons' => array(self::HAS_MANY, 'UserRibbon', 'user_id'),
            'userSponsors' => array(self::HAS_MANY, 'UserSponsor', 'sponsor_user_id'),
            'userSponsors1' => array(self::HAS_MANY, 'UserSponsor', 'target_user_id'),
            'userStatistic' => array(self::HAS_ONE, 'UserStatistic', 'user_id')
        );
    }

    public function scopes()
    {
        return array(
            'real' => array(
                'condition' => 'is_fake = 0'
            ),
            'ready' => array(
                'condition' => 'is_ready = 1',
            ),
            'online' => array(
                'condition' => 'online = 1'
            ),
            'vip' => array(
                'condition' => 'vip_end > NOW()'
            ),
            'todayVisitor' => array(
                'condition' => 'DATE(last_visit_date) = DATE(NOW())'
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'username' => 'Username',
            'avatar' => 'Avatar',
            'avatar_orig' => 'Avatar Orig',
            'gender' => 'Gender',
            'birth_date' => 'Birth Date',
            'soc_id' => 'Soc',
            'online' => 'Online',
            'city_id' => 'City',
            'balance' => 'Balance',
            'sid' => 'Sid',
            'color' => 'Color',
            'visibility' => 'Visibility',
            'cost' => 'Cost',
            'show_profile' => 'Show Profile',
            'vip_end' => 'Vip End',
            'create_ts' => 'Create Ts',
            'ref_id' => 'Ref',
            'last_visit_date' => 'Last Visit Date',
            'continuously_visit' => 'Continuously Visit',
            'is_ready' => 'Is Ready',
            'invitations' => 'Invitations',
            'hash' => 'Hash',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('avatar', $this->avatar, true);
        $criteria->compare('avatar_orig', $this->avatar_orig, true);
        $criteria->compare('gender', $this->gender);
        $criteria->compare('birth_date', $this->birth_date, true);
        $criteria->compare('soc_id', $this->soc_id, true);
        $criteria->compare('online', $this->online);
        $criteria->compare('city_id', $this->city_id);
        $criteria->compare('balance', $this->balance, true);
        $criteria->compare('sid', $this->sid, true);
        $criteria->compare('color', $this->color, true);
        $criteria->compare('visibility', $this->visibility);
        $criteria->compare('cost', $this->cost);
        $criteria->compare('show_profile', $this->show_profile);
        $criteria->compare('vip_end', $this->vip_end, true);
        $criteria->compare('create_ts', $this->create_ts, true);
        $criteria->compare('ref_id', $this->ref_id);
        $criteria->compare('last_visit_date', $this->last_visit_date, true);
        $criteria->compare('continuously_visit', $this->continuously_visit);
        $criteria->compare('is_ready', $this->is_ready);
        $criteria->compare('invitations', $this->invitations);
        $criteria->compare('hash', $this->hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserAccount the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function hashPassword($pwd)
    {
        return md5($pwd);
    }

    public static function userGetLast($count)
    {
        $criteria = new CDbCriteria;
        $criteria->limit = $count;
        $criteria->order = 'user_id DESC';

        return self::model()->findAll($criteria);
    }

    public static function getUsersCount()
    {
        return self::model()->count();
    }

    public function getAvatar()
    {
        return 'http://dev.vkapp-lubof.ru/data/avatars/100/' . $this->avatar;
    }

    public function createDate()
    {
        return date('Y-m-d', strtotime($this->create_ts));
    }

    public function vk()
    {
        return 'http://vk.com/id' . $this->soc_id;
    }

    public function isVip()
    {
        return strtotime($this->vip_end) > strtotime(Date::now());
    }

    public function genderLabel()
    {
        $genders = array(
            1 => 'Девушка',
            2 => 'Парень'
        );

        return $genders[$this->gender];
    }
}
