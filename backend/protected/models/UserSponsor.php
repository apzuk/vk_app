<?php

/**
 * This is the model class for table "user_sponsor".
 *
 * The followings are the available columns in table 'user_sponsor':
 * @property integer $id
 * @property integer $sponsor_user_id
 * @property integer $target_user_id
 *
 * The followings are the available model relations:
 * @property UserAccount $sponsorUser
 * @property UserAccount $targetUser
 */
class UserSponsor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_sponsor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sponsor_user_id, target_user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sponsor_user_id, target_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sponsorUser' => array(self::BELONGS_TO, 'UserAccount', 'sponsor_user_id'),
			'targetUser' => array(self::BELONGS_TO, 'UserAccount', 'target_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sponsor_user_id' => 'Sponsor User',
			'target_user_id' => 'Target User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sponsor_user_id',$this->sponsor_user_id);
		$criteria->compare('target_user_id',$this->target_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserSponsor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
