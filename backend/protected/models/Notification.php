<?php

/**
 * This is the model class for table "notification".
 *
 * The followings are the available columns in table 'notification':
 * @property integer $id
 * @property integer $tmpl_id
 * @property string $create_ts
 * @property string $status
 * @property integer $user_id
 * @property integer $owner_user_id
 * @property string $data
 *
 * The followings are the available model relations:
 * @property UserAccount $ownerUser
 * @property UserAccount $user
 */
class Notification extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_ts', 'required'),
			array('tmpl_id, user_id, owner_user_id', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>7),
			array('data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tmpl_id, create_ts, status, user_id, owner_user_id, data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ownerUser' => array(self::BELONGS_TO, 'UserAccount', 'owner_user_id'),
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tmpl_id' => 'Tmpl',
			'create_ts' => 'Create Ts',
			'status' => 'Status',
			'user_id' => 'User',
			'owner_user_id' => 'Owner User',
			'data' => 'Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tmpl_id',$this->tmpl_id);
		$criteria->compare('create_ts',$this->create_ts,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('owner_user_id',$this->owner_user_id);
		$criteria->compare('data',$this->data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
