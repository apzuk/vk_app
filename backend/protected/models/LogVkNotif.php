<?php

/**
 * This is the model class for table "log_vk_notif".
 *
 * The followings are the available columns in table 'log_vk_notif':
 * @property integer $log_id
 * @property integer $user_id
 * @property string $msg
 * @property string $status
 * @property string $date
 * @property integer $notif_id
 *
 * The followings are the available model relations:
 * @property UserAccount $user
 */
class LogVkNotif extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_vk_notif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, notif_id', 'numerical', 'integerOnly'=>true),
			array('msg', 'length', 'max'=>254),
			array('status', 'length', 'max'=>4),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('log_id, user_id, msg, status, date, notif_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'user_id' => 'User',
			'msg' => 'Msg',
			'status' => 'Status',
			'date' => 'Date',
			'notif_id' => 'Notif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('log_id',$this->log_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('notif_id',$this->notif_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogVkNotif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
