<?php

$basePath = __DIR__ . '/../../protected';

/* Set aliases
-------------------------------------------------- */

return array(
    'id'                => '3qYcuvw7qhtRMnpfQHVVGpnfpZwyBi',
    'name'              => 'VK приложение',
    'basePath'          => $basePath,
    'defaultController' => 'index',
    'sourceLanguage'    => 'en',
    'language'          => 'ru',

    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
    ),

    'preload' => array('log'),

    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
            'class'          => 'WebUser',
            'loginUrl'       => '/index/login',

            'identityCookie' => array(
                'httpOnly' => true
            ),
        ),

        'db'=>array(
            'class'=>'CDbConnection',
            'connectionString'=>'mysql:host=localhost;dbname=lubof',
            'username'=>'root',
            'password'=>'prch123',
            'emulatePrepare'=>true,
            'charset' => 'utf8',
        ),

        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),

        'errorHandler' => array(
            'errorAction' => YII_DEBUG ? null : 'index/error',
        ),

        'widgetFactory' => array(
            'widgets' => array(
                'CJuiAutoComplete' => array(
                    'themeUrl' => 'public/css/admin',
                    'options' => array(
                        'minLength' => '2',
                    ),
                ),
            ),
        ),

        'urlManager' => array(
            'urlFormat'      => 'path',
            'showScriptName' => false,
        ),

        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'              => 'CDbLogRoute',
                    'except'             => 'exception.CHttpException.404',
                    'levels'             => 'warning, error, info',
                    'logTableName'       => 'logs',
                    'connectionID'       => 'db',
                    'autoCreateLogTable' => YII_DEBUG
                ),
            ),
        ),

        'authManager' => array(
            'class'           => 'CDbAuthManager',
            'connectionID'    => 'db',
            'assignmentTable' => 'authAssignment',
            'itemChildTable'  => 'authItemChild',
            'itemTable'       => 'authItem'
        ),
    ),

    /* Modules settings
    -------------------------------------------------- */

    'modules' => array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'prch123',
            'ipFilters'=>array('91.210.40.62', '46.162.198.169'),
            // 'newFileMode'=>0666,
            // 'newDirMode'=>0777,
        ),

        'api',
        'admin'
    ),

    /* Global settings
    -------------------------------------------------- */

    'params' => array(
    )
)?>