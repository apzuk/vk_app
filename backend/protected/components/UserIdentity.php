<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $id;

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate($identityField = 'login')
    {
        $user = UserAdmin::model()->find(
            $identityField . '=:loginField',
            array(
                ':loginField' => strtolower(
                    $this->username
                )
            )
        );

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (UserAccount::hashPassword($this->password) !== $user->password) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode == self::ERROR_NONE;
    }

    /**
     * @return integer the ID of the user record
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer the ID of the user record
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}