<?php

class Controller extends CController
{
    public $pageTitle = 'Админ панель приложения ВК';

    public function loadOwner()
    {
        $userId = app()->request->getPost('user_id');
        $user = UserAccount::model()->findByPk($userId);

        if ($user === null) {
            $this->invalidUser();
        }

        if ($user->sid != app()->user->getPost('sid')) {
            $this->invalidUser();
        }

        return $user;
    }

    public function response($data, $isModel = false)
    {
        echo $isModel ? CJSON::encode($data) : json_encode($data);
        app()->end();
    }

    /**
     * Response no enough balance
     */
    public function noEnoughBalance()
    {
        $this->response(
            array(
                'result' => false,
                'reason' => 'no.balance'
            )
        );
    }

    public function invalidUser()
    {
        $this->response(
            array(
                'result' => false,
                'reason' => 'invalid.user'
            )
        );
    }

}