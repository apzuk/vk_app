<?php

class WebUser extends CWebUser
{
    private $model = null;

    public function getModel()
    {
        if (!$this->isGuest && $this->model === null) {
            $this->model = UserAdmin::model()->findByPk($this->id);

            if ($this->model == false) {
                Yii::app()->user->logout();
                Yii::app()->user->loginRequired();
            }
        }

        return $this->model;
    }
}