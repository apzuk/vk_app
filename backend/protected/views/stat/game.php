<style>
    pre {
        outline: 1px solid #ccc;
        padding: 5px;
        margin: 5px;
    }

    .string {
        color: green;
    }

    .number {
        color: darkorange;
    }

    .boolean {
        color: blue;
    }

    .null {
        color: magenta;
    }

    .key {
        color: red;
    }
</style>

<a href="#" onclick="return false;" id="update">Обновить</a>
<table class="table table-bordered table-striped table-condensed">
    <tr>
        <th>Игра номер</th>
        <th>Пользовател 1</th>
        <th>Пользовател 2</th>
        <th>Пользовател 3</th>
        <th>Пользовател 4</th>
        <th>Пользовател 5</th>
        <th>Пользовател 6</th>
        <th>Статус</th>
    </tr>
</table>

<script>
    apiManager = new function () {
        var registeredCallback = null, transport = sockJsHandler;

        this.requestTables = function (callback) {
            registeredCallback = callback;

            transport.send({
                user_id: 1000,
                action: 'gameGetTables',
                module: 'apiManager'
            });
        };

        this.apiResponse = function (data) {
            if (registeredCallback) registeredCallback(data);
        }
    };


    $(function () {

        $('#update').on('click', function () {

            apiManager.requestTables(function (data) {

                $('.table tr:eq(0)').nextAll().remove();

                $.each(data.tables, function () {

                    var htmlData = {
                        uName_1: '--',
                        uName_2: '--',
                        uName_3: '--',
                        uName_4: '--',
                        uName_5: '--',
                        uName_6: '--',
                        user_id_1: '',
                        user_id_2: '',
                        user_id_3: '',
                        user_id_4: '',
                        user_id_5: '',
                        user_id_6: ''
                    };

                    var json = JSON.stringify(this);
                    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                        var cls = 'number';
                        if (/^"/.test(match)) {
                            if (/:$/.test(match)) {
                                cls = 'key';
                            } else {
                                cls = 'string';
                            }
                        } else if (/true|false/.test(match)) {
                            cls = 'boolean';
                        } else if (/null/.test(match)) {
                            cls = 'null';
                        }
                        return '<span class="' + cls + '">' + match + '</span>';
                    });

                    htmlData['iterator'] = this.iterator;
                    htmlData['status'] = this.playing ? 'Играют' : 'Ожидание';
                    htmlData['className'] = this.playing ? 'success' : 'warning';

                    var table = this;
                    $(this.users).each(function (index) {
                        htmlData['uName_' + (index + 1)] = userName(table, this);
                        htmlData['user_id_' + (index + 1)] = this;
                    });

                    function userName(table, user_id) {
                        if (table['male'].hasOwnProperty(user_id)) {
                            return table['male'][user_id]['username'];
                        }
                        else {
                            return table['female'][user_id]['username'];
                        }
                    }

                    $('#table_row').tmpl(htmlData).appendTo($('.table'));

                    $('.player').each(function () {
                        var userId = $(this).attr('class').split(' ')[1];

                        if ($('.' + userId).length > 1) {
                            $('.' + userId).wrap('<b>');
                        }
                    });
                });
            });
        }).trigger('click');
    })
</script>

<script id="table_row" type="text/template">
    <tr>
        <td><a href="#" onclick="return false;">${iterator}</a></td>
        <td class="center">
            <a href="/user/view/${user_id_1}" target="_blank">
                <span class="player ${user_id_1}">${uName_1}</span>
            </a>
        </td>
        <td class="center">
            <a href="/user/view/${user_id_2}" target="_blank">
                <span class="player ${user_id_2}">${uName_2}</span>
            </a>
        </td>
        <td class="center">
            <a href="/user/view/${user_id_3}" target="_blank">
                <span class="player ${user_id_3}">${uName_3}</span>
            </a>
        </td>
        <td class="center">
            <a href="/user/view/${user_id_4}" target="_blank">
                <span class="player ${user_id_4}">${uName_4}</span>
            </a>
        </td>

        <td class="center">
            <a href="/user/view/${user_id_5}" target="_blank">
                <span class="player ${user_id_5}">${uName_5}</span>
            </a>
        </td>
        <td class="center">
            <a href="/user/view/${user_id_6}" target="_blank">
                <span class="player ${user_id_6}">${uName_6}</span>
            </a>
        </td>

        <td class="center">
            <span class="label label-${className} xright">${status}</span>
        </td>
    </tr>
</script>