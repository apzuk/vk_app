<?php foreach ($leaders as $leader) : ?>
    <tr>
        <td style="width: 220px;">
            <img src="<?= $leader->user->getAvatar(); ?>" width="30" class="xleft"/>

            <div class="xleft info">
                <?= $leader->user->username; ?><br/>
            </div>
        </td>
        <td class="center" style="width: 150px;">
            <?= $leader->city->city; ?>
        </td>
        <td class="center" style="width: 280px;">
            <?= $leader->message ?>
        </td>
        <td class="center"><?= $leader->current_bid; ?> голос.</td>
    </tr>
<?php endforeach; ?>