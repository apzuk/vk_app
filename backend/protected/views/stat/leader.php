<style>
    .xleft {
        float: left;
    }

    .info {
        margin: -4px 6px 0;
    }
</style>

<div class="row-fluid sortable center">
    <div class="box span6">
        <div class="box-header">
            <h2><i class="icon-align-justify"></i><span class="break"></span>Текущие лидеры</h2>
        </div>
        <div class="box-content">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Имя/Фото</th>
                    <th>Город</th>
                    <th>Текст</th>
                    <th>Текущая ставка</th>
                </tr>
                </thead>
                <tbody>
                <?php $this->renderPartial('_leaderList', array('leaders' => $leaders)) ?>
                </tbody>
            </table>
        </div>
    </div>
    <!--/span-->
</div><!--/row-->

<div class="row-fluid sortable">
    <div class="box span6">
        <div class="box-header">
            <h2><i class="icon-align-justify"></i><span class="break"></span>История лидеров</h2>
        </div>
        <div class="box-content">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Имя/Фото</th>
                    <th>Город</th>
                    <th>Текст</th>
                    <th>Текущая ставка</th>
                </tr>
                </thead>
                <tbody>
                <?php $this->renderPartial('_leaderList', array('leaders' => $leaderItems)) ?>
                </tbody>
            </table>

            <div class="pagination pagination-centered">
                <?$this->widget(
                    'CLinkPager',
                    array(
                        'pages' => $pages,
                        'maxButtonCount' => 6,
                        'selectedPageCssClass' => 'active',
                        'nextPageLabel' => 'След &gt;',
                        'prevPageLabel' => 'Пред &gt;',
                        'header' => '',
                        'hiddenPageCssClass' => true,
                        'cssFile' => false,
                        'htmlOptions' => array(
                            'class' => 'p'
                        ),
                    )
                )?>
            </div>
        </div>
    </div>
    <!--/span-->
</div><!--/row-->
