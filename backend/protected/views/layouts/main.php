<!DOCTYPE html>
<html lang="en">
<head>

    <!-- start: Meta -->
    <meta charset="utf-8">
    <title><?= $this->pageTitle; ?></title>
    <!-- end: Mobile Specific -->

    <!-- start: CSS -->
    <link id="bootstrap-style" href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link id="base-style" href="/css/style.css" rel="stylesheet">
    <link id="base-style-responsive" href="/css/style-responsive.css" rel="stylesheet">

    <!--[if lt IE 7 ]>
    <link id="ie-style" href="/css/style-ie.css" rel="stylesheet">
    <![endif]-->
    <!--[if IE 8 ]>
    <link id="ie-style" href="/css/style-ie.css" rel="stylesheet">
    <![endif]-->
    <!--[if IE 9 ]>
    <![endif]-->

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- start: Favicon -->
    <link rel="shortcut icon" href="/img/favicon.ico">
    <!-- end: Favicon -->


    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/jquery-migrate-1.0.0.min.js"></script>

    <script src="/js/jquery-ui-1.10.0.custom.min.js"></script>

    <script src="/js/jquery.ui.touch-punch.js"></script>

    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/jquery.cookie.js"></script>

    <script src='/js/fullcalendar.min.js'></script>

    <script src='/js/jquery.dataTables.min.js'></script>

    <script src="/js/excanvas.js"></script>
    <script src="/js/jquery.flot.min.js"></script>
    <script src="/js/jquery.flot.pie.min.js"></script>
    <script src="/js/jquery.flot.stack.js"></script>
    <script src="/js/jquery.flot.resize.min.js"></script>

    <script src="/js/jquery.chosen.min.js"></script>

    <script src="/js/jquery.uniform.min.js"></script>

    <script src="/js/jquery.cleditor.min.js"></script>

    <script src="/js/jquery.noty.js"></script>

    <script src="/js/jquery.elfinder.min.js"></script>

    <script src="/js/jquery.raty.min.js"></script>

    <script src="/js/jquery.iphone.toggle.js"></script>

    <script src="/js/jquery.uploadify-3.1.min.js"></script>

    <script src="/js/jquery.gritter.min.js"></script>

    <script src="/js/jquery.imagesloaded.js"></script>

    <script src="/js/jquery.masonry.min.js"></script>

    <script src="/js/jquery.knob.js"></script>

    <script src="/js/jquery.sparkline.min.js"></script>

    <script src="/js/custom.js"></script>

    <script src="/js/sockjs.js"></script>

    <script src="/js/transport.js"></script>

    <script src="/js/logger.js"></script>

    <script src="/js/user.js"></script>

    <script src="/js/game.js"></script>

    <script src="/js/jquery-tmpl-1.0.0.min.js"></script>

</head>

<body>
<div id="overlay">
    <ul>
        <li class="li1"></li>
        <li class="li2"></li>
        <li class="li3"></li>
        <li class="li4"></li>
        <li class="li5"></li>
        <li class="li6"></li>
    </ul>
</div>
<!-- start: Header -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse"
               data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/"> <img alt="Perfectum Dashboard" src="/img/logo20.png"/> <span
                    class="hidden-phone">Perfectum Dashboard</span></a>

        </div>
    </div>
</div>
<!-- start: Header -->

<div class="container-fluid">
    <div class="row-fluid">

        <!-- start: Main Menu -->
        <div class="span2 main-menu-span">
            <div class="nav-collapse sidebar-nav">
                <ul class="nav nav-tabs nav-stacked main-menu">
                    <li>
                        <a href="/"><i class="icon-home icon-white"></i>
                            <span class="hidden-tablet"> Главная</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= app()->createUrl('user') ?>"><i class="icon-eye-open icon-white"></i>
                            <span class="hidden-tablet"> Пользователи</span>
                        </a>
                    </li>

                    <li>
                        <a class="dropmenu" href="#"><i class="fa-icon-folder-close-alt"></i><span
                                class="hidden-tablet"> Статистика</span></a>
                        <ul>
                            <li><a class="submenu" href="<?= app()->createUrl('stat/leader'); ?>">
                                    <i class="fa-icon-file-alt"></i>
                                    <span class="hidden-tablet"> Лидеры</span></a>
                            </li>
                            <li><a class="submenu" href="<?= app()->createUrl('stat/tape'); ?>">
                                    <i class="fa-icon-file-alt"></i>
                                    <span class="hidden-tablet"> Лента</span></a>
                            </li>
                            <li><a class="submenu" href="<?= app()->createUrl('stat/game'); ?>">
                                    <i class="fa-icon-file-alt"></i>
                                    <span class="hidden-tablet"> Игры</span></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="dropmenu" href="#"><i class="fa-icon-folder-close-alt"></i><span
                                class="hidden-tablet"> Инструменты</span></a>
                        <ul>
                            <li><a class="submenu" href="<?= app()->createUrl('tools/imitation'); ?>">
                                    <i class="fa-icon-file-alt"></i>
                                    <span class="hidden-tablet"> Имитация игры</span></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="http://pmadbaccess.vkapp-lubof.ru" target="_blank"><i
                                class="icon-eye-open icon-white"></i>
                            <span class="hidden-tablet"> База данных</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!--/.well -->
        </div>
        <!--/span-->
        <!-- end: Main Menu -->


        <div id="content" class="span10">
            <!-- start: Content -->

            <?= $content ?>
            <!-- end: Content -->
        </div>
    </div>
    <!--/fluid-row-->

    <div class="modal hide fade" id="myModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here settings can be configured...</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Close</a>
            <a href="#" class="btn btn-primary">Save changes</a>
        </div>
    </div>

    <div class="clearfix"></div>

    <footer>
        <p>
            <span style="text-align:left;float:left">&copy; <a href="http://clabs.co" target="_blank">creativeLabs</a> 2013</span>
            <span style="text-align:right;float:right">Powered by: <a href="#">Perfectum Dashboard</a></span>
        </p>

    </footer>

</div>
<!--/.fluid-container-->

<!-- start: JavaScript-->
<script type="text/javascript">


    <?php /*  function message_welcome1() {
            var unique_id = $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Welcome on Perfectum Dashboard',
                // (string | mandatory) the text inside the notification
                text: 'I hope you like this template',
                // (string | optional) the image to display on the left
                image: '/img/avatar.jpg',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false,
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'my-sticky-class'
            });
        }

        function message_welcome2() {
            var unique_id = $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Perfectum is Amazing Theme',
                // (string | mandatory) the text inside the notification
                text: 'Perfectum works on all devices, computers, tablets and smartphones. Perfectum has lots of great features. Try It!',
                // (string | optional) the image to display on the left
                image: '/img/avatar.jpg',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false,
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'my-sticky-class'
            });
        }

        function message_welcome3() {
            var unique_id = $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Buy Perfectum!',
                // (string | mandatory) the text inside the notification
                text: 'This great template can be yours today.',
                // (string | optional) the image to display on the left
                image: '/img/avatar.jpg',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false,
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'gritter-light'
            });
        }

        $(document).ready(function () {

            setTimeout("message_welcome1()", 5000);
            setTimeout("message_welcome2()", 10000);
            setTimeout("message_welcome3()", 15000);

        });*/ ?>

    $(document).on('socketConnectionReady', function () {
        sockJsHandler.send({
            action: "auth",
            user_id: 1000
        });
    });
</script>
<!-- end: JavaScript-->

</body>
</html>
