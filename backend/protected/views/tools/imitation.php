<style>
    .vkLink {
        float: right;
        margin: -1px 2px 0;
    }

    .vkLink img {
        width: 16px;
    }

    .vip {
        margin: 1px 2px 0;
    }

    .xright {
        float: right;
    }

    .xleft {
        float: left;
    }

    .balance {
        font-size: 12px;
    }

    table.table tr td {
        text-align: center;
        vertical-align: middle;
    }

    table.table tr th {
        text-align: center;
    }
</style>


<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header">
            <h2><i class="icon-align-justify"></i><span class="break"></span>Все пользователи
                (<?= UserAccount::getUsersCount(); ?>)</h2>
        </div>
        <div class="box-content">

            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>Фото</th>
                    <th>Имя</th>
                    <th>Пол</th>
                    <th>Вопрос</th>
                    <th>Действие</th>
                </tr>

                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td style="width: 35px;"><img src="<?= $user->getAvatar() ?>" width="50"/></td>
                        <td style="width: 280px; text-align: left;">
                            <div class="xleft">
                                <a href="<?= app()->createUrl('user/view', array('id' => $user->user_id)); ?>">
                                    <?= $user->username ?>
                                </a>
                                <br/>
                                <span class="balance">Баланс: <b><?= $user->balance ?></b> голос</span>
                            </div>

                            <?php if ($user->soc_id) : ?>
                                <a href="<?= $user->vk(); ?>" class="vkLink" target="_blank">
                                    <img src="/img/vk.png"/>
                                </a>
                            <?php endif; ?>

                            <?php if ($user->isVip()) : ?>
                                <span class="label vip xright">vip</span>
                            <?php endif; ?>

                            <span class="label label-success xright"><?= $user->status ?></span>
                        </td>

                        <td><?= $user->genderLabel(); ?></td>
                        <td><input type="text" value="" style="width: 95%" name="txt"/></td>
                        <td>
                            <input type="checkbox" class="vipPlaying">
                            <a href="#"
                               id="<?= $user->user_id ?>"
                               data-gender="<?= $user->gender ?>"
                               data-id="<?= $user->user_id ?>"
                               class="btn btn-info btn-s">Играть</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div class="pagination pagination-centered">
                <?$this->widget(
                    'CLinkPager',
                    array(
                        'pages' => $pages,
                        'maxButtonCount' => 6,
                        'selectedPageCssClass' => 'active',
                        'nextPageLabel' => 'След &gt;',
                        'prevPageLabel' => 'Пред &gt;',
                        'header' => '',
                        'hiddenPageCssClass' => true,
                        'cssFile' => false,
                        'htmlOptions' => array(
                            'class' => 'p'
                        ),
                    )
                )?>
            </div>
        </div>
    </div>
    <!--/span-->
</div><!--/row-->

<script>
    var game = new function () {
        this.login = function (user_id, gender, text, chk) {
            var action = chk ? 'playWithoutQueue' : 'play';

            sockJsHandler.send({
                action: action,
                gender: gender,
                question: text ? text : "user" + user_id + " question",
                userId: user_id,
                user_id: 1000
            });
        };

        this.placed = function (data) {
            $('#' + data.userId).parent().parent().find('td:eq(3)').html(data.iterator);
            $('#' + data.userId).remove();
        };
    };


    $(function () {
        $(document).on('socketConnectionReady', function (event) {
            $('.btn-s').each(function () {
                $(this).on('click', function () {
                    var chk = $(this).parent().find('.vipPlaying').attr('checked');

                    game.login($(this).data('id'), $(this).data('gender'), $('[name="txt"]').val(), chk);
                });//.trigger('click');
            });
        });
    });
</script>
