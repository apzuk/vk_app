<style>
    .vkLink {
        float: right;
        margin: -1px 2px 0;
    }

    .vkLink img {
        width: 16px;
    }

    .vip {
        margin: 1px 2px 0;
    }

    .xright {
        float: right;
    }

    .xleft {
        float: left;
    }

    .balance {
        font-size: 12px;
    }

    table.table tr td {
        text-align: center;
        vertical-align: middle;
    }

    table.table tr th {
        text-align: center;
    }
</style>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header">
            <h2><i class="icon-align-justify"></i><span class="break"></span>Все пользователи
                (<?= UserAccount::getUsersCount(); ?>)</h2>
        </div>
        <div class="box-content">

            <form method="get">
                <input type="text" name="username" value="<?= $username ?>" class="username"
                       placeholder="Имя пользователя"/>

                <select name="gender">
                    <option value="0">Поиск по полу (все)</option>
                    <option <?= $gender == 1 ? 'selected' : '' ?> value="1">Женщины</option>
                    <option <?= $gender == 2 ? 'selected' : '' ?> value="2">Мужчины</option>
                </select>

                <select name="status">
                    <?php foreach ($statuses as $status) : ?>
                        <?php $selected = $status == $st ? 'selected' : ''; ?>
                        <option <?= $selected; ?> value="<?= $status ?>"><?= $status == -1 ? 0 : $status; ?></option>
                    <?php endforeach; ?>
                </select>

                <label for="vipOnly">
                    Только vip
                    <input type="checkbox" name="vipOnly" id="vipOnly" <?= $vipOnly ? 'checked' : ''; ?>/>
                </label>

                <br/>
                <button type="submit" class="btn btn-primary">Поиск</button>
            </form>

            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>Фото</th>
                    <th>Имя</th>
                    <th>Рейтинг</th>
                    <th>Симпатии</th>
                    <th>Сыграл</th>
                    <th>Дата регистрации</th>
                    <th>Действии</th>
                </tr>

                <?php foreach ($users as $user) : ?>
                    <?php if (!$user->userStatistic) {
                        $user->userStatistic = new UserStatistic();
                    }
                    ?>
                    <tr>
                        <td style="width: 35px;"><img src="<?= $user->getAvatar() ?>" width="50"/></td>
                        <td style="width: 280px; text-align: left;">
                            <div class="xleft">
                                <a href="<?= app()->createUrl('user/view', array('id' => $user->user_id)); ?>">
                                    <?= $user->username ?>
                                </a>
                                <br/>
                                <span class="balance">Баланс: <b><?= $user->balance ?></b> голос</span>
                            </div>

                            <?php if ($user->soc_id) : ?>
                                <a href="<?= $user->vk(); ?>" class="vkLink" target="_blank">
                                    <img src="/img/vk.png"/>
                                </a>
                            <?php endif; ?>

                            <?php if ($user->isVip()) : ?>
                                <span class="label vip xright">vip</span>
                            <?php endif; ?>

                            <span class="label label-success xright"><?= $user->status ?></span>
                        </td>
                        <td style="width: 110px;">
                            <b><?= $user->userStatistic->rating; ?></b> рейтинг
                        </td>
                        <td style="width: 130px;">
                            <b><?= $user->userStatistic->likes; ?></b> симпатии
                        </td>
                        <td style="width: 80px;">
                            <b><?= $user->userStatistic->game_count ?></b> игр
                        </td>
                        <td style="width: 250px;"><?= $user->create_ts ?></td>
                        <td class="center ">
                            <a class="btn btn-success" href="#">
                                <i class="icon-zoom-in icon-white"></i>
                            </a>
                            <a class="btn btn-info" href="#">
                                <i class="icon-edit icon-white"></i>
                            </a>
                            <a class="btn btn-danger" href="#">
                                <i class="icon-trash icon-white"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div class="pagination pagination-centered">
                <?$this->widget(
                    'CLinkPager',
                    array(
                        'pages' => $pages,
                        'maxButtonCount' => 6,
                        'selectedPageCssClass' => 'active',
                        'nextPageLabel' => 'След &gt;',
                        'prevPageLabel' => 'Пред &gt;',
                        'header' => '',
                        'hiddenPageCssClass' => true,
                        'cssFile' => false,
                        'htmlOptions' => array(
                            'class' => 'p'
                        ),
                    )
                )?>
            </div>
        </div>
    </div>
    <!--/span-->
</div><!--/row-->