/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50532
 Source Host           : 10.0.0.104
 Source Database       : geo

 Target Server Type    : MySQL
 Target Server Version : 50532
 File Encoding         : utf-8

 Date: 09/16/2013 12:59:29 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `firm`
-- ----------------------------
DROP TABLE IF EXISTS `firm`;
CREATE TABLE `firm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `geometry_name` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `geometry` varchar(255) NOT NULL,
  `lon` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `insert_date` datetime NOT NULL,
  `external_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `external_id` (`external_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17738 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `firm_to_subcategory`
-- ----------------------------
DROP TABLE IF EXISTS `firm_to_subcategory`;
CREATE TABLE `firm_to_subcategory` (
  `firm_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  KEY `firm_id` (`firm_id`,`subcategory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_admin`
-- ----------------------------
DROP TABLE IF EXISTS `geo_admin`;
CREATE TABLE `geo_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Админы системы';

-- ----------------------------
--  Table structure for `geo_advert_click_count`
-- ----------------------------
DROP TABLE IF EXISTS `geo_advert_click_count`;
CREATE TABLE `geo_advert_click_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commercial_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `refer` int(11) DEFAULT NULL,
  `charge` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_click_count_commercial` (`commercial_id`),
  CONSTRAINT `fk_click_count_commercial` FOREIGN KEY (`commercial_id`) REFERENCES `geo_commercial` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Счетчик кликов рекламы';

-- ----------------------------
--  Table structure for `geo_advert_view_count`
-- ----------------------------
DROP TABLE IF EXISTS `geo_advert_view_count`;
CREATE TABLE `geo_advert_view_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commercial_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `refer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_view_count_commercial` (`commercial_id`),
  CONSTRAINT `fk_view_count_commercial` FOREIGN KEY (`commercial_id`) REFERENCES `geo_commercial` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Счетчик показов рекламы';

-- ----------------------------
--  Table structure for `geo_album`
-- ----------------------------
DROP TABLE IF EXISTS `geo_album`;
CREATE TABLE `geo_album` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `place_id` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `order` int(10) DEFAULT '0',
  `date` date DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order` (`order`),
  KEY `FK_geo_album_geo_place` (`place_id`),
  KEY `GA_user_id_FK_user_id` (`user_id`),
  CONSTRAINT `FK_geo_album_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GA_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_album_photo`
-- ----------------------------
DROP TABLE IF EXISTS `geo_album_photo`;
CREATE TABLE `geo_album_photo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_id` int(10) DEFAULT NULL,
  `place_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_geo_album_photo_geo_album` (`album_id`),
  KEY `FK_geo_album_photo_geo_place` (`place_id`),
  KEY `GAP_user_id_FK_user_id` (`user_id`),
  CONSTRAINT `fk_albumPhoto_album` FOREIGN KEY (`album_id`) REFERENCES `geo_album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_album_photo_geo_album` FOREIGN KEY (`album_id`) REFERENCES `geo_album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_album_photo_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GAP_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_album_photo_comment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_album_photo_comment`;
CREATE TABLE `geo_album_photo_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_photo_comment_user` (`user_id`),
  KEY `fk_photo_comment_photo` (`photo_id`),
  CONSTRAINT `fk_photo_comment_photo` FOREIGN KEY (`photo_id`) REFERENCES `geo_album_photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_photo_comment_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Комментарии для картинок';

-- ----------------------------
--  Table structure for `geo_album_photo_comment_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_album_photo_comment_like`;
CREATE TABLE `geo_album_photo_comment_like` (
  `comment_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`comment_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `geo_album_photo_comment_like_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `geo_album_photo_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_album_photo_comment_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_app`
-- ----------------------------
DROP TABLE IF EXISTS `geo_app`;
CREATE TABLE `geo_app` (
  `client_id` varchar(50) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `user_id` int(10) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `cdate` datetime NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `url` (`url`),
  KEY `client_id_client_secret` (`client_id`,`client_secret`),
  KEY `FK_geo_app_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_app_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_app_oauth_access_token`
-- ----------------------------
DROP TABLE IF EXISTS `geo_app_oauth_access_token`;
CREATE TABLE `geo_app_oauth_access_token` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_app_oauth_authorization_code`
-- ----------------------------
DROP TABLE IF EXISTS `geo_app_oauth_authorization_code`;
CREATE TABLE `geo_app_oauth_authorization_code` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_app_oauth_refresh_token`
-- ----------------------------
DROP TABLE IF EXISTS `geo_app_oauth_refresh_token`;
CREATE TABLE `geo_app_oauth_refresh_token` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_auth_assignment`;
CREATE TABLE `geo_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(10) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `FK_geo_auth_assignment_geo_user` (`userid`),
  CONSTRAINT `FK_geo_auth_assignment_geo_user` FOREIGN KEY (`userid`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `geo_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `geo_auth_item`;
CREATE TABLE `geo_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `geo_auth_item_child`;
CREATE TABLE `geo_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `t_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `geo_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `geo_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_category`
-- ----------------------------
DROP TABLE IF EXISTS `geo_category`;
CREATE TABLE `geo_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `create_place_title` varchar(255) DEFAULT NULL,
  `create_place_description` text,
  `order` int(11) DEFAULT '0',
  `date_update` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_category_geo_category` (`parent_id`),
  KEY `order` (`order`),
  KEY `GC_user_id_FK_user_id` (`user_id`),
  KEY `GC_parent_id_FK_cat_id` (`parent_id`),
  CONSTRAINT `GC_parent_id_FK_cat_id` FOREIGN KEY (`parent_id`) REFERENCES `geo_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GC_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_city`
-- ----------------------------
DROP TABLE IF EXISTS `geo_city`;
CREATE TABLE `geo_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `name_ru` varchar(50) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`),
  KEY `country_id` (`country_id`),
  KEY `region_id` (`region_id`),
  KEY `name_ru` (`name_ru`),
  KEY `name_en` (`name_en`),
  KEY `GC_city_id_FK_city_id` (`country_id`),
  KEY `GC_region_id_FK_region_id` (`region_id`),
  CONSTRAINT `GC_city_id_FK_city_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GC_region_id_FK_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12492 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_commercial`
-- ----------------------------
DROP TABLE IF EXISTS `geo_commercial`;
CREATE TABLE `geo_commercial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `item_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `place_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `slide1` varchar(255) DEFAULT NULL,
  `slide2` varchar(255) DEFAULT NULL,
  `slide3` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `day_limit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `spend_today` decimal(10,2) NOT NULL DEFAULT '0.00',
  `spend_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_commercial_user` (`user_id`),
  KEY `fk_commercial_place` (`place_id`),
  KEY `fk_commercial_event` (`event_id`),
  CONSTRAINT `fk_commercial_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Реклама';

-- ----------------------------
--  Table structure for `geo_conv`
-- ----------------------------
DROP TABLE IF EXISTS `geo_conv`;
CREATE TABLE `geo_conv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_user_id` int(11) DEFAULT NULL,
  `receiver_user_id` int(11) DEFAULT NULL,
  `status_creator` enum('message','removed','trash') DEFAULT 'message',
  `status_receiver` enum('message','removed','trash') DEFAULT 'message',
  PRIMARY KEY (`id`),
  KEY `creator_user_id` (`creator_user_id`),
  KEY `receiver_user_id` (`receiver_user_id`),
  CONSTRAINT `FK_creator_user_id_user_id` FOREIGN KEY (`creator_user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_receiver_user_id_user_id` FOREIGN KEY (`receiver_user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_conv_message`
-- ----------------------------
DROP TABLE IF EXISTS `geo_conv_message`;
CREATE TABLE `geo_conv_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_user_id` int(11) DEFAULT NULL,
  `receiver_user_id` int(11) DEFAULT NULL,
  `sender_status` enum('read','unread','removed') DEFAULT 'unread',
  `receiver_status` enum('read','unread','removed') DEFAULT 'unread',
  `message_body` text,
  `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conv_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_user_id` (`sender_user_id`),
  KEY `receiver_user_id` (`receiver_user_id`),
  KEY `conv_id` (`conv_id`),
  KEY `GCM_conv_id_FK_conv_id` (`conv_id`),
  CONSTRAINT `GCM_conv_id_FK_conv_id` FOREIGN KEY (`conv_id`) REFERENCES `geo_conv` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `receiver_user_id_FK_user_id` FOREIGN KEY (`receiver_user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sender_user_id_FK_user_id` FOREIGN KEY (`sender_user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_cost`
-- ----------------------------
DROP TABLE IF EXISTS `geo_cost`;
CREATE TABLE `geo_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT '0',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_cost_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_cost_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_country`
-- ----------------------------
DROP TABLE IF EXISTS `geo_country`;
CREATE TABLE `geo_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ru` varchar(50) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `code` varchar(5) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`),
  KEY `name_ru` (`name_ru`),
  KEY `name_en` (`name_en`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_event`
-- ----------------------------
DROP TABLE IF EXISTS `geo_event`;
CREATE TABLE `geo_event` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `place_id` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `short` text,
  `description` text,
  `date_add` date DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_event_geo_place` (`place_id`),
  KEY `FK_geo_event_user_id` (`user_id`),
  CONSTRAINT `FK_geo_event_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_event_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_event_comment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_event_comment`;
CREATE TABLE `geo_event_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `event_id` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` int(2) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_event_comment_user` (`user_id`),
  KEY `FK_geo_event_comment_geo_event` (`event_id`),
  CONSTRAINT `FK_geo_event_comment_geo_event` FOREIGN KEY (`event_id`) REFERENCES `geo_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_event_comment_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_event_comment_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_event_comment_like`;
CREATE TABLE `geo_event_comment_like` (
  `comment_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`comment_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `geo_event_comment_like_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `geo_event_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_event_comment_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_event_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_event_like`;
CREATE TABLE `geo_event_like` (
  `event_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `geo_event_like_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `geo_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_event_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_message`
-- ----------------------------
DROP TABLE IF EXISTS `geo_message`;
CREATE TABLE `geo_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) DEFAULT NULL,
  `receiver_id` int(10) DEFAULT NULL,
  `subject` varchar(256) NOT NULL DEFAULT '',
  `body` text,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `cdate` datetime NOT NULL,
  `is_email_send` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_geo_message_geo_user` (`sender_id`),
  KEY `FK_geo_message_geo_user_2` (`receiver_id`),
  CONSTRAINT `FK_geo_message_geo_user` FOREIGN KEY (`sender_id`) REFERENCES `geo_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_message_geo_user_2` FOREIGN KEY (`receiver_id`) REFERENCES `geo_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_notification`
-- ----------------------------
DROP TABLE IF EXISTS `geo_notification`;
CREATE TABLE `geo_notification` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `type` int(2) NOT NULL,
  `data` text,
  `text` text,
  `is_read` tinyint(1) DEFAULT '0',
  `cdate` datetime DEFAULT NULL,
  `is_email_send` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_geo_notification_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_notification_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_password_recovery`
-- ----------------------------
DROP TABLE IF EXISTS `geo_password_recovery`;
CREATE TABLE `geo_password_recovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_send` tinyint(1) DEFAULT '0' COMMENT 'send password recovery email with recovery link',
  `is_recovery` tinyint(1) DEFAULT '0' COMMENT 'password was recovered',
  `recover_url` varchar(100) NOT NULL,
  `send_date` timestamp NULL DEFAULT NULL,
  `recover_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TPR_user_id_FK_user_id` (`user_id`),
  CONSTRAINT `TPR_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_payment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_payment`;
CREATE TABLE `geo_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `paySystem` int(2) DEFAULT NULL,
  `status` enum('unfinished','failed','succeed') DEFAULT 'unfinished',
  `amount` float DEFAULT '0',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_result` (`paySystem`,`status`),
  KEY `FK_geo_payment_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_payment_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place`;
CREATE TABLE `geo_place` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `coordX` float DEFAULT NULL,
  `coordY` float DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `without_avatar` int(1) DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_date` date DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('pending','approved') DEFAULT 'pending',
  `worktime` text,
  `short_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_place_geo_category` (`category_id`),
  KEY `FK_geo_place_geo_geo_country` (`country_id`),
  KEY `FK_geo_place_geo_geo_region` (`region_id`),
  KEY `FK_geo_place_geo_geo_city` (`city_id`),
  KEY `GP_user_id_FK_user_id` (`user_id`),
  KEY `GP_category_id_FK_sys_category_id` (`category_id`),
  KEY `GP_country_id` (`country_id`),
  KEY `GP_region_id_FK_sys_region_id` (`region_id`),
  KEY `GP_city_id_FK_sys_city_id` (`city_id`),
  KEY `without_avatar_title` (`without_avatar`,`title`),
  KEY `title` (`title`),
  KEY `short_name_INDEX` (`short_name`) USING BTREE,
  CONSTRAINT `GP_category_id_FK_sys_category_id` FOREIGN KEY (`category_id`) REFERENCES `geo_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GP_city_id_FK_sys_city_id` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `GP_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `GP_region_id_FK_sys_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `GP_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41556 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_admin`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_admin`;
CREATE TABLE `geo_place_admin` (
  `place_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `assign_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`place_id`,`user_id`),
  KEY `FK_geo_place_admin_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_place_admin_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_place_admin_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_contact`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_contact`;
CREATE TABLE `geo_place_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `additional` text,
  `place_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PC_place_id_UNIQUE` (`place_id`),
  KEY `PC_place_id_FK_place_id` (`place_id`),
  CONSTRAINT `PC_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_guest`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_guest`;
CREATE TABLE `geo_place_guest` (
  `place_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  UNIQUE KEY `place_id_user_id` (`place_id`,`user_id`),
  KEY `FK_geo_place_guest_geo_user` (`user_id`),
  CONSTRAINT `FK_geo_place_guest_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_place_guest_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_left_menu_link`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_left_menu_link`;
CREATE TABLE `geo_place_left_menu_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `place_id` int(10) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `params` text,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_geo_place_left_menu_link_geo_place` (`place_id`),
  CONSTRAINT `FK_geo_place_left_menu_link_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_like`;
CREATE TABLE `geo_place_like` (
  `place_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`place_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `FK_geo_place_like_geo_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_place_like_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_link`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_link`;
CREATE TABLE `geo_place_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `post` text,
  `attachment` text,
  `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `likes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `place_id` (`place_id`),
  KEY `PL_user_id_FK_user_id` (`user_id`),
  CONSTRAINT `PL_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PL_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_link_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_link_like`;
CREATE TABLE `geo_place_link_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `link_id` (`link_id`),
  CONSTRAINT `PLL_link_id_FK_link_id` FOREIGN KEY (`link_id`) REFERENCES `geo_place_link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PLL_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_post`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_post`;
CREATE TABLE `geo_place_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post` text,
  `attachment` text,
  `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `place_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `likes` int(11) DEFAULT '0',
  `is_admin_post` enum('yes','no') DEFAULT 'no',
  `priority` enum('normal','high') DEFAULT 'normal',
  PRIMARY KEY (`id`),
  KEY `survey_id` (`survey_id`),
  KEY `GPP_place_id_FK_place_id` (`place_id`),
  KEY `GPP_creator_user_id_FK_user_id` (`user_id`),
  KEY `GPP_survey_id_FK_survey_id` (`survey_id`),
  CONSTRAINT `GPP_creator_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GPP_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GPP_survey_id_FK_survey_id` FOREIGN KEY (`survey_id`) REFERENCES `geo_wall_survey` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_post_comment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_post_comment`;
CREATE TABLE `geo_place_post_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `user_id` int(11) DEFAULT NULL,
  `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  KEY `FK_post_id` (`id`),
  CONSTRAINT `FK_post_id` FOREIGN KEY (`post_id`) REFERENCES `geo_place_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_post_comment_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_post_comment_like`;
CREATE TABLE `geo_place_post_comment_like` (
  `comment_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`comment_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `geo_post_comment_like_copy_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `geo_place_post_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_post_comment_like_copy_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_post_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_post_like`;
CREATE TABLE `geo_place_post_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PL_user_id_FK_user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_tag`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_tag`;
CREATE TABLE `geo_place_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id_tag_id_UNIQUE` (`place_id`,`tag_id`),
  KEY `CT_tag_id_FK_tag_id` (`tag_id`),
  KEY `GPT_place_id_FK_place_id` (`place_id`),
  CONSTRAINT `FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `geo_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5221 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_video`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_video`;
CREATE TABLE `geo_place_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `screenshot` varchar(100) DEFAULT NULL,
  `embedcode` text,
  `url` varchar(100) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `GPV_geo_place_id_FK_place_id` (`place_id`),
  CONSTRAINT `GPV_geo_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_video_comment`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_video_comment`;
CREATE TABLE `geo_place_video_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_photo_comment_user` (`user_id`),
  KEY `fk_photo_comment_photo` (`video_id`),
  CONSTRAINT `geo_place_video_comment_ibfk_1` FOREIGN KEY (`video_id`) REFERENCES `geo_place_video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_place_video_comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_video_comment_like`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_video_comment_like`;
CREATE TABLE `geo_place_video_comment_like` (
  `comment_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `like` int(1) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`comment_id`,`user_id`),
  KEY `FK_geo_place_like_geo_user` (`user_id`),
  KEY `like` (`like`),
  CONSTRAINT `geo_place_video_comment_like_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `geo_place_video_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `geo_place_video_comment_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_view`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_view`;
CREATE TABLE `geo_place_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `hosts` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_view_log`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_view_log`;
CREATE TABLE `geo_place_view_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `last_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=375 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_visitor`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_visitor`;
CREATE TABLE `geo_place_visitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `place_id_user_id` (`place_id`,`user_id`),
  KEY `FK_place_visitor_user` (`user_id`) USING BTREE,
  CONSTRAINT `FK_place_visitor_place` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_place_visitor_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_place_visitor_statistic`
-- ----------------------------
DROP TABLE IF EXISTS `geo_place_visitor_statistic`;
CREATE TABLE `geo_place_visitor_statistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscribes` int(11) DEFAULT NULL,
  `unsubscribes` int(11) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `create_ts` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PVC_place_id_FK_place_id` (`place_id`),
  CONSTRAINT `PVC_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_region`
-- ----------------------------
DROP TABLE IF EXISTS `geo_region`;
CREATE TABLE `geo_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name_ru` varchar(50) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`),
  KEY `country_id` (`country_id`),
  KEY `name_ru` (`name_ru`),
  KEY `name_en` (`name_en`),
  KEY `GR_country_id_FK_country_id` (`country_id`),
  CONSTRAINT `GR_country_id_FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1094 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_spam_report`
-- ----------------------------
DROP TABLE IF EXISTS `geo_spam_report`;
CREATE TABLE `geo_spam_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `is_email_send` tinyint(1) NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `item_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_spam_user` (`user_id`),
  CONSTRAINT `fk_spam_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Жалобы на спам';

-- ----------------------------
--  Table structure for `geo_statistics_data`
-- ----------------------------
DROP TABLE IF EXISTS `geo_statistics_data`;
CREATE TABLE `geo_statistics_data` (
  `type` text,
  `value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_tag`
-- ----------------------------
DROP TABLE IF EXISTS `geo_tag`;
CREATE TABLE `geo_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_name_UNIQUE` (`tag_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1111114 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_tag_category`
-- ----------------------------
DROP TABLE IF EXISTS `geo_tag_category`;
CREATE TABLE `geo_tag_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id_tag_id_UNIQUE` (`category_id`,`tag_id`) USING BTREE,
  KEY `category_id` (`category_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `TG_category_id_FK_category_id` FOREIGN KEY (`category_id`) REFERENCES `geo_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `TG_tag_id_FK_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `geo_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1702 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_tag_counter`
-- ----------------------------
DROP TABLE IF EXISTS `geo_tag_counter`;
CREATE TABLE `geo_tag_counter` (
  `tag_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `counter` int(10) DEFAULT '0',
  UNIQUE KEY `tag_id_city_id` (`tag_id`,`city_id`),
  KEY `FK_geo_tag_counter_geo_city` (`city_id`),
  CONSTRAINT `FK_geo_tag_counter_geo_city` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_tag_counter_geo_tag` FOREIGN KEY (`tag_id`) REFERENCES `geo_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_timezone`
-- ----------------------------
DROP TABLE IF EXISTS `geo_timezone`;
CREATE TABLE `geo_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(45) DEFAULT NULL,
  `msk` int(11) DEFAULT NULL,
  `utc` int(11) DEFAULT NULL,
  `default` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_user`
-- ----------------------------
DROP TABLE IF EXISTS `geo_user`;
CREATE TABLE `geo_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `email_verifed` tinyint(1) DEFAULT '0',
  `last_activity` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `do_not_send_message` tinyint(1) NOT NULL DEFAULT '0',
  `do_not_send_notification` tinyint(1) NOT NULL DEFAULT '0',
  `timezone` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `sid` (`sid`),
  KEY `FK_geo_user_geo_country` (`country_id`),
  KEY `FK_geo_user_geo_region` (`region_id`),
  KEY `FK_geo_user_geo_city` (`city_id`),
  KEY `name` (`name`),
  CONSTRAINT `FK_geo_user_geo_city` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_user_geo_country` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_geo_user_geo_region` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8450 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_user_change_password_confimation_code`
-- ----------------------------
DROP TABLE IF EXISTS `geo_user_change_password_confimation_code`;
CREATE TABLE `geo_user_change_password_confimation_code` (
  `key` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`key`),
  KEY `FK_geo_user_email_activation_code_geo_user` (`user_id`),
  CONSTRAINT `geo_user_change_password_confimation_code_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_user_email_activation_code`
-- ----------------------------
DROP TABLE IF EXISTS `geo_user_email_activation_code`;
CREATE TABLE `geo_user_email_activation_code` (
  `key` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`key`),
  KEY `FK_geo_user_email_activation_code_geo_user` (`user_id`),
  KEY `email` (`email`),
  CONSTRAINT `FK_geo_user_email_activation_code_geo_user` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_user_email_changes`
-- ----------------------------
DROP TABLE IF EXISTS `geo_user_email_changes`;
CREATE TABLE `geo_user_email_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT 'activate',
  `activate_url` varchar(255) DEFAULT NULL COMMENT 'activation code',
  `change_date` timestamp NULL DEFAULT NULL,
  `activate_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_user_identity`
-- ----------------------------
DROP TABLE IF EXISTS `geo_user_identity`;
CREATE TABLE `geo_user_identity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `identity` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_wall_survey`
-- ----------------------------
DROP TABLE IF EXISTS `geo_wall_survey`;
CREATE TABLE `geo_wall_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) DEFAULT NULL,
  `creator_user_id` int(11) DEFAULT NULL,
  `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `GWS_place_id_FK_place_id` (`place_id`),
  KEY `GWS_creator_user_id_FK_user_id` (`creator_user_id`),
  CONSTRAINT `GWS_creator_user_id_FK_user_id` FOREIGN KEY (`creator_user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GWS_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_wall_survey_question`
-- ----------------------------
DROP TABLE IF EXISTS `geo_wall_survey_question`;
CREATE TABLE `geo_wall_survey_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_question` varchar(255) DEFAULT '200',
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_wall_survey_question_answer`
-- ----------------------------
DROP TABLE IF EXISTS `geo_wall_survey_question_answer`;
CREATE TABLE `geo_wall_survey_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answers` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_question_id` (`question_id`),
  KEY `SQA_question_id_FK_survey_question_id` (`question_id`),
  CONSTRAINT `SQA_question_id_FK_survey_question_id` FOREIGN KEY (`question_id`) REFERENCES `geo_wall_survey_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `geo_wall_survey_user_answer`
-- ----------------------------
DROP TABLE IF EXISTS `geo_wall_survey_user_answer`;
CREATE TABLE `geo_wall_survey_user_answer` (
  `id` int(11) NOT NULL DEFAULT '0',
  `answer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_answer_id_UNIQUE` (`answer_id`,`user_id`) USING BTREE,
  KEY `survey_answer_id` (`answer_id`),
  KEY `SUA_user_id_FK_user_id` (`user_id`),
  KEY `SUA_answer_id_FK_answer_id` (`answer_id`),
  CONSTRAINT `SUA_answer_id_FK_answer_id` FOREIGN KEY (`answer_id`) REFERENCES `geo_wall_survey_question_answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SUA_user_id_FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `geo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `subcategory`
-- ----------------------------
DROP TABLE IF EXISTS `subcategory`;
CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `external_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `external_id` (`external_id`)
) ENGINE=MyISAM AUTO_INCREMENT=958 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_migration`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tmp`
-- ----------------------------
DROP TABLE IF EXISTS `tmp`;
CREATE TABLE `tmp` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `region_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `coordX` float DEFAULT NULL,
  `coordY` float DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `title_2` (`title`),
  KEY `FK_geo_place_geo_category` (`category_id`),
  KEY `FK_geo_place_geo_geo_country` (`country_id`),
  KEY `FK_geo_place_geo_geo_region` (`region_id`),
  KEY `FK_geo_place_geo_geo_city` (`city_id`),
  KEY `GP_user_id_FK_user_id` (`user_id`),
  KEY `GP_category_id_FK_sys_category_id` (`category_id`),
  KEY `GP_country_id` (`country_id`),
  KEY `GP_region_id_FK_sys_region_id` (`region_id`),
  KEY `GP_city_id_FK_sys_city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41215 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Procedure structure for `event_get_available`
-- ----------------------------
DROP PROCEDURE IF EXISTS `event_get_available`;
delimiter ;;
CREATE DEFINER=`aramp`@`%` PROCEDURE `event_get_available`(IN dateCurrent VARCHAR(255), IN idsList VARCHAR(255), IN uId INT, IN `limit` int, IN guid varchar(255))
    DETERMINISTIC
BEGIN
	/* Found date iterator */
	DECLARE dateCurrentAvailable VARCHAR(255);

	/* Declare end of rows */
    DECLARE done INT DEFAULT FALSE;

	/* Select 7 available days start from passed current date! */
	DECLARE datesAvailable CURSOR FOR 
		SELECT date_start
		FROM geo_event
		WHERE date_start >= dateCurrent
		GROUP BY date_start
		ORDER BY date_start 
		LIMIT 7;

	/* Handel if results end */
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	/* Create tmpl table to store selected data */
	/*
	SET @sqlDelete = "DROP TABLE IF EXISTS temp_table;";
	SET @sqlInsertion = CONCAT(@sqlDelete, "CREATE TEMPORARY TABLE temp_table (id INT, title VARCHAR(255), image VARCHAR(255), short TEXT, description TEXT, `date_add` DATE, date_start DATE, date_end DATE, pTitle VARCHAR(255), pId INT(11));");
		
	PREPARE stmt FROM @sqlInsertion;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;*/

	DROP TABLE IF EXISTS temp_table;
	CREATE TEMPORARY TABLE temp_table (id INT, title VARCHAR(255), image VARCHAR(255), short TEXT, description TEXT, `date_add` DATE, date_start DATE, date_end DATE, pTitle VARCHAR(255), pId INT(11));
	
	/*Open cursor to start loop dates*/
	OPEN datesAvailable;

	/*Loop through found dates*/
	loopThroughDates: LOOP 

		IF done = TRUE THEN
			LEAVE loopThroughDates;
		END IF;

		FETCH datesAvailable INTO dateCurrentAvailable;

		SET @sqlQuery = CONCAT('
			SELECT * 
			FROM ( 
				SELECT e.id, e.title AS title, e.image, e.short, e.description, e.date_add, e.date_start, e.date_end, 
					place.title AS pTitle, place.id AS pId 
				FROM geo_event_like el 
				LEFT JOIN `geo_event` e ON e.id = el.event_id 
				LEFT JOIN `geo_place` `place` ON (e.`place_id`=`place`.`id`) AND (is_deleted = 0) 
				WHERE ((date_start<="' , dateCurrent , '") AND (date_end>="' ,  dateCurrent , '")) AND el.user_id = ' , uId , '
				UNION 
				SELECT e.id, e.title AS event, e.image, e.short, e.description, e.date_add, e.date_start, e.date_end, 
					place.title AS pTitle, place.id AS pId 
				FROM `geo_event` e 
				LEFT JOIN `geo_place` `place` ON (e.`place_id`=`place`.`id`) AND (is_deleted = 0) 
				WHERE (((place.id IN (' ,  idsList , ')) AND (date_start<="' ,   dateCurrent , '")) AND (date_end>="' ,  dateCurrent , '")) 
			) as x 
			LIMIT 7');
		
		SET @sqlInsertion = CONCAT("INSERT INTO temp_table (", @sqlQuery, ")");
		
		PREPARE stmt FROM @sqlInsertion;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;       

	END LOOP;

	SELECT * FROM temp_table;

	/* Remove tmpl table */
	DROP TABLE IF EXISTS temp_table;

	/*process done!*/
	CLOSE datesAvailable;
END
 ;;
delimiter ;

-- ----------------------------
--  Procedure structure for `event_get_current_by_date`
-- ----------------------------
DROP PROCEDURE IF EXISTS `event_get_current_by_date`;
delimiter ;;
CREATE DEFINER=`aramp`@`%` PROCEDURE `event_get_current_by_date`(IN dateCurrent VARCHAR(50), IN idsList VARCHAR(255), IN uId INT)
BEGIN
	SET @sqlQuery = CONCAT('
		SELECT * 
		FROM ( 
			SELECT e.id, e.title AS title, e.image, e.short, e.description, e.date_add, e.date_start, e.date_end, 
				place.title AS pTitle, place.id AS pId 
			FROM geo_event_like el 
			LEFT JOIN `geo_event` e ON e.id = el.event_id 
			LEFT JOIN `geo_place` `place` ON (e.`place_id`=`place`.`id`) AND (is_deleted = 0) 
			WHERE ((date_start<="' , dateCurrent , '") AND (date_end>="' ,  dateCurrent , '")) AND el.user_id = ' , uId , '
			UNION 
			SELECT e.id, e.title AS event, e.image, e.short, e.description, e.date_add, e.date_start, e.date_end, 
				place.title AS pTitle, place.id AS pId 
			FROM `geo_event` e 
			LEFT JOIN `geo_place` `place` ON (e.`place_id`=`place`.`id`) AND (is_deleted = 0) 
			WHERE (((place.id IN (' ,  idsList , ')) AND (date_start<="' ,  dateCurrent , '")) AND (date_end>="' ,  dateCurrent , '")) 
		) as x 
		LIMIT 7;');

	PREPARE stmt FROM @sqlQuery;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
