/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50532
 Source Host           : 10.0.0.104
 Source Database       : geo

 Target Server Type    : MySQL
 Target Server Version : 50532
 File Encoding         : utf-8

 Date: 09/23/2013 15:40:03 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `geo_app_demo_log`
-- ----------------------------
DROP TABLE IF EXISTS `geo_app_demo_log`;
CREATE TABLE `geo_app_demo_log` (
  `place_id` int(11) NOT NULL DEFAULT '0',
  `app_id` int(11) NOT NULL DEFAULT '0',
  `usage_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`place_id`,`app_id`),
  KEY `app_id` (`app_id`),
  CONSTRAINT `ADL_place_id_FK_place_id` FOREIGN KEY (`place_id`) REFERENCES `geo_place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ADL_app_id_FK_client_id` FOREIGN KEY (`app_id`) REFERENCES `geo_app` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
