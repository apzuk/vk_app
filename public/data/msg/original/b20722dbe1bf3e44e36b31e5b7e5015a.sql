/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50532
 Source Host           : 10.0.0.104
 Source Database       : bron

 Target Server Type    : MySQL
 Target Server Version : 50532
 File Encoding         : utf-8

 Date: 10/03/2013 09:53:23 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `module_settings`
-- ----------------------------
DROP TABLE IF EXISTS `module_settings`;
CREATE TABLE `module_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `booking_time_from` time DEFAULT NULL,
  `booking_time_to` time DEFAULT NULL,
  `timezone` varchar(45) DEFAULT NULL,
  `tz_mks` varchar(45) DEFAULT NULL,
  `admin_tel` varchar(20) DEFAULT NULL,
  `is_auto_renewal` tinyint(1) NOT NULL DEFAULT '0',
  `worktime` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `module_settings`
-- ----------------------------
BEGIN;
INSERT INTO `module_settings` VALUES ('1', '1', '1', '00:00:00', '00:00:00', '+8', '+4', '+7 (999) 999-95-55', '1', null), ('2', '41554', '1', null, null, null, null, null, '0', null), ('3', '35827', '1', null, null, null, null, null, '0', null), ('4', '41555', '1', null, null, null, null, null, '0', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
