<?php

// Is IE?!
if ($msie = strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE') ? true : false) {
    include 'ie.php';
}

// Defined constants
include '../app/bootstrap.php';
// Register loader
include $basedir . '/../app/library/Loader.php';
Loader::register($basedir . '/../app/library/');

// Renderer object
$renderer = new \library\View\Renderer();

// Get current user all needed data.
$uData = \library\App::run(\library\IFrameParams::get('viewer_id'));
if (!$uData->is_ready) {
    include 'complete.php';
}

// Current user city leader
$leader = new \library\Object($uData->leader);

// All chosen types
$chosenTypes = \library\Chosen::types();

\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/header');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/randomUser');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/messages');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/game');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/content');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/notif');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/profile');
\library\View\GraphicLoader::register(dirname(__FILE__) . '/img/vip-buttons');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/icons.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/online.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/newGame.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/leader-text.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/leader-text-line.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/become-vip-icons.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/camera-male.png');
\library\View\GraphicLoader::appendFile(dirname(__FILE__) . '/img/camera-female.png');

$minifier = new \library\Components\Minifier\Minify(array(
    'public_dir' => $basedir . '/../public',
    'cache_dir_public_path' => 'data/cache',
    'noMinify' => true
));

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/css/font/webfontkit-20131127-072243/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="/css/font/webfontkit-20131127-074021/stylesheet.css" type="text/css">

    <?php
    // -------  css libs ------->
    $minifier->appendCssFile("/css/jquery-ui-1.10.3.custom.css")
        ->appendCssFile("/css/jquery.jscrollpane.css")
        ->appendCssFile("/css/reset.css")
        ->appendCssFile("/css/main.css")
        ->appendCssFile("/css/mosaic.css")
        ->appendCssFile("/css/jquery-impromptu.css");
    ?>
    <?php if (\library\App::isMacOs()) : ?>
        <?php $minifier->appendCssFile("/css/macos_trimming.css"); ?>
    <?php endif; ?>

    <!------- external js libs ------->
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?98"></script>
    <script src="https://vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>

    <?php
    // standard js libs
    $minifier
        ->appendJsFile("/js_v2/standard/jquery-2.0.3.min.js")
        ->appendJsFile("/js_v2/standard/jquery-ui-1.10.3.custom.min.js")
        ->appendJsFile("/js_v2/standard/jquery-tmpl-1.0.0.min.js")
        ->appendJsFile("/js_v2/standard/jquery.mousewheel.js")
        ->appendJsFile("/js_v2/standard/jquery.jscrollpane.min.js")
        ->appendJsFile("/js_v2/standard/jquery.tagcanvas.js")
        ->appendJsFile("/js_v2/standard/jquery.blockUI.js")
        ->appendJsFile("/js_v2/standard/jquery.cookie.js")
        ->appendJsFile("/js_v2/standard/jquery.limit-1.2.source.js")
        ->appendJsFile("/js_v2/standard/jquery-impromptu.js")
        ->appendJsFile("/js_v2/standard/mosaic.1.0.1.min.js")
        ->appendJsFile("/js_v2/standard/sockjs.js")
        ->appendJsFile("/js_v2/standard/modernizr.custom.js");
    ?>

    <?php
    // js models libs
    $minifier->appendJsFile("/js_v2/models/user.js")
        ->appendJsFile("/js_v2/models/message.js")
        ->appendJsFile("/js_v2/models/leader.js")
        ->appendJsFile("/js_v2/models/game.js"); ?>

    <?php
    // js managers libs
    $minifier->appendJsFile("/js_v2/managers/transport.js")
        ->appendJsFile("/js_v2/managers/main.js")
        ->appendJsFile("/js_v2/managers/logger.js")
        ->appendJsFile("/js_v2/managers/notif.js")
        ->appendJsFile("/js_v2/managers/user.js")
        ->appendJsFile("/js_v2/managers/ribbon.js")
        ->appendJsFile("/js_v2/managers/online.js")
        ->appendJsFile("/js_v2/managers/message.js")
        ->appendJsFile("/js_v2/managers/game.js")
        ->appendJsFile("/js_v2/managers/app.js")
        ->appendJsFile("/js_v2/managers/leader.js"); ?>

    <?php
    // js plugins libs
    $minifier->appendJsFile("/js_v2/plugins/game.make.vip.js")
        ->appendJsFile("/js_v2/plugins/game.styled.dialog.js")
        ->appendJsFile("/js_v2/plugins/game.question.dialog.js")
        ->appendJsFile("/js_v2/plugins/game.model.dialog.js")
        ->appendJsFile("/js_v2/plugins/game.tab.js")
        ->appendJsFile("/js_v2/plugins/game.counter.down.js")
        ->appendJsFile("/js_v2/plugins/game.file.uploader.js")
        ->appendJsFile("/js_v2/plugins/game.help.text.js")
        ->appendJsFile("/js_v2/plugins/game.combobox.js")
        ->appendJsFile("/js_v2/plugins/game.checkbox.js"); ?>

    <!------- output ------->
    <?= $minifier ?>
</head>
<body>

<?= $renderer->render('template', array('uData' => $uData)); ?>
<?= $renderer->render('popups', array('uData' => $uData)); ?>

<div id="wrapper">
    <header>
        <ul class="menu">
            <li class="heart active">
                <div class="content">
                    <div class="heart-ico">
                        <span class="remaining">00:00</span>
                    </div>
                    <span class="label">Игра</span>
                </div>
            </li>
            <li class="message">
                <div class="content">
                    <div class="msg-ico">
                        <?php $countType = $uData->unreadCount < 10 ? 'single' : 'many'; ?>
                        <div class="badge <?= $countType ?> <?= $uData->unreadCount == 0 ? 'zero' : '' ?>">
                            <?= $uData->unreadCount; ?>
                        </div>
                    </div>
                    <span class="label">Сообщения</span>
                </div>
            </li>
            <li class="notification">
                <div class="content">
                    <div class="notif-ico">
                        <div class="badge single zero">0</div>
                    </div>
                    <span class="label">Уведомления</span>
                </div>
            </li>
        </ul>

        <?php $gender = $uData->gender == 1 ? 'female' : 'male' ?>
        <div class="user-profile <?= $gender ?>">
            <?php $modeClass = $uData->isvip && $uData->color ? ' clr-b-' . $uData->color : ''; ?>
            <div class="avatar <?= $gender ?>-avatar <?= $modeClass ?> me">
                <a href="#" onclick="userManager.showProfile(<?= $uData->user_id ?>)">
                    <img src="<?= \library\Storage\User::getPath($uData->avatar); ?>"/>
                </a>

                <?php if ($uData->isvip) : ?>
                    <div class="vip <?= $gender ?>-vip"></div>
                <?php endif; ?>

                <?php if (!$uData->isvip) : ?>
                    <a href="#" class="buy-button" onclick="userManager.vip();">Стать VIP</a>
                <?php endif; ?>
            </div>

            <div class="info-bar">
                <div class="user-name">
                    <?php $uName = preg_split('/\s+/', $uData->username); ?>
                    <?= $uName[0]; ?>
                    (<a class="view_count hasToolTip" onclick="userManager.showUserGuests()" title="Гости">0</a>)
                </div>
                <div class="rating">
                    <span class="ico ico-rating hasToolTip" title="Рейтинг"></span>
                    <span class="position"><?= (int)$uData->rating ?></span><span class="pos-label"> рейтинг</span>
                </div>
                <div class="money">
                    <a class="ico ico-money hasToolTip" onclick="userManager.payment()" title="Пополнить счет"></a>

                    <a class="balance" href="#"
                       onclick="userManager.payment();return false;"><?= $uData->balance * EXCHANGE; ?></a>
                    <span class="balance-label"> голосов</span>
                </div>

                <div class="user-vip <?= $uData->isvip ? '' : 'xhidden'; ?>">
                    <a class="ico ico-vip hasToolTip" onclick="return false;"></a>
                    <a href="#" class="already-vip hasToolTip"
                       onclick="userManager.vipCapabilities(); return false;" title="Дата окончания VIP статус">
                        <?= \library\Date::dateWithlabel($uData->vip_end); ?>
                    </a>
                </div>
            </div>
        </div>
    </header>
</div>
<div id="sound"></div>
<div id="content">
    <div class="game-tab1">
        <?=
        $renderer->render(
            'game-tab1',
            array(
                'renderer' => $renderer,
                'gender' => $uData->gender,
                'chosenTypes' => $chosenTypes
            )
        ); ?>
    </div>

    <div class="game-tab2 xhidden">
        <?=
        $renderer->render(
            'game-tab2',
            array(
                'conversations' => $uData->conversations,
                'user_id' => $uData->user_id,
                'isVip' => $uData->isvip,
                'meBlocked' => $uData->meBlocked,
                'myBlocked' => $uData->myBlocked
            )
        ); ?>
    </div>

    <div class="game-tab3 xhidden">
        <?= $renderer->render('game-tab3'); ?>
    </div>
</div>

<footer>
    <ul class="menu">
        <li>
            <div class="add-to-menu-block" onclick="VK.callMethod('showSettingsBox', 256)">
                <a href="#" class="ico ico-to-menu"></a>
                <span>Добавить в левое меню</span>
            </div>
        </li>
        <li>
            <div class="sound-switcher" onclick="userManager.switchSound();">
                <a href="#" class="ico ico-sound"></a>
                <span class="sound-label">Выкл.</span>
            </div>
        </li>

        <li>
            <div class="ico ico-share" onclick="g.shareWith()"></div>
        </li>
        <li>
            <div class="ico ico-faq"></div>
        </li>
        <li>
            <div class="ico ico-privacy"></div>
        </li>
    </ul>
</footer>
<script>
    $(function () {
        var currentUser = new userModel();
        currentUser.setUser({
            user_id: <?= $uData->user_id; ?>,
            username: '<?= $uData->username ?>',
            avatar: '<?= $uData->avatar ?>',
            avatar_orig: '<?= $uData->avatar_orig ?>',
            gender: '<?= $uData->gender ?>',
            balance: '<?= $uData->balance ?>',
            city_id: '<?= $uData->city_id ?>',
            city_name: '<?= $uData->city_name ?>',
            sid: '<?= $uData->sid ?>',
            show_profile: "<?= $uData->show_profile; ?>",
            vip: <?= (int) $uData->isvip ?>,
            rating: <?= (int) $uData->rating; ?>,
            cost: <?= (int) $uData->cost; ?>,
            visibility: <?= (int) $uData->visibility; ?>,
            color: <?= (int) $uData->color; ?>,
            continuously_visit: <?= (int) $uData->continuously_visit; ?>,
            hash: '<?= $uData->hash; ?>',
            online: <?= (int) $uData->online; ?>
        });
        userManager.setCurrentUser(currentUser);

        <?php if($leader->user_id) : ?>
        leaderManager.setLeader({
            user_id: <?= (int) $leader->user_id; ?>,
            username: "<?= $leader->username ?>",
            gender: <?= (int) $leader->gender ?>,
            avatar: "<?= $leader->avatar ?>",
            bid: <?= (int) $leader->current_bid ?>,
            vip: <?= (int) $leader->isvip; ?>,
            color: <?= (int) $leader->color; ?>,
            message: '<?= $leader->message ?>'
        });
        <?php else : ?>
        leaderManager.setLeader(false);
        <?php endif; ?>

        g.setExchange(<?= EXCHANGE; ?>).loadImages();

        <?php if($uData->isvip) : ?>
        <?php $secondDiff = round((strtotime($uData->vip_end) - strtotime(date('Y-m-d H:i:s'))), 1); ?>

        <?php if($secondDiff <= 5 * 60 * 60) : ?>
        userManager.runVipEndListening(<?= $secondDiff ?>);
        <?php endif; ?>

        <?php endif; ?>

        $(document).on('socketConnectionReady', function (event) {
            <?php \library\View\ScriptOnRun::getInstance()->output(); ?>

            <?php if (!$uData->updated) : ?>
            g.isUpdated = false;
            <?php else : ?>
            g.isUpdated = true;
            <?php endif; ?>
        });
    });
</script>
</body>
</html>