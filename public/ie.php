<html
    class=" js canvas canvastext draganddrop websockets rgba borderradius boxshadow textshadow opacity cssanimations cssgradients csstransitions fontface generatedcontent video audio webworkers applicationcache"
    style="">
<head>
    <link rel="stylesheet" href="/css/font/webfontkit-20131127-072243/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="/css/font/webfontkit-20131127-074021/stylesheet.css" type="text/css">

    <!------- output ------->
    <link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.10.3.custom.css">
    <link type="text/css" rel="stylesheet" href="/css/reset.css">
    <link type="text/css" rel="stylesheet" href="/css/main.css">
    <link type="text/css" rel="stylesheet" href="/css/mosaic.css">
    <link type="text/css" rel="stylesheet" href="/css/macos_trimming.css">
</head>
<body class="cap" style="">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front sd-box" tabindex="-1" role="dialog"
     aria-describedby="popup" aria-labelledby="ui-id-1"
     style="border-style: solid none none; border-top-width: 1px; border-top-color: rgb(204, 204, 204); padding: 0px; height: auto; width: 407px; top: 226px; left: 194px; display: block;">
    <div class="sd-wrapper female">
        <div id="popup" class="ui-dialog-content ui-widget-content"
             style="padding: 0px; overflow: hidden; width: auto; min-height: 0px; max-height: none; height: 286px;"><a
                class="download-chrome sd-btn" href="https://www.google.ru/intl/ru/chrome/browser/" target="_blank"
                value="Сказать Google Chrome">Сказать Google Chrome</a><a
                class="html5-ico ico-no-html-heart-left"></a><a class="html5-ico ico-no-html-heart-right"></a>

            <p class="text">К сожалению, ваш браузер не поддерживает хтмл5, либо ваш браузер уже устарел... :(<br><br>
                ...но вы можете поправить это скачав браузер :)</p><span class="html5-ico ico-chrome"></span></div>
    </div>
</div>
</body>
</html>

<?php exit;?>