<?php

ini_set('display_errors', 'on');
error_reporting(E_ALL);

$basedir = dirname(__FILE__);
include $basedir . '/../app/library/Loader.php';
Loader::register($basedir . '/../app/library/');

include "../app/bootstrap.php";

\library\Config::init(CONFIG_PATH);

$controller = \library\Request::get('c');
$action = \library\Request::get('do') . 'Action';

$path = dirname(__FILE__) . '/../app/mvc/Controller/' . $controller . '.php';

if (!(file_exists($path) && !is_dir($path))) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

include $path;

session_start();

$renderer = new \library\View\Renderer($controller);

if (class_exists($controller)) {
    $controllerObject = new $controller($renderer);
    if (!method_exists($controllerObject, $action)) {
        header("HTTP/1.0 404 Not Found");
    }

    call_user_func_array(array($controllerObject, $action), array());
}
?>