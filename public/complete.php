<?php
$minifier = new \library\Components\Minifier\Minify(array(
    'public_dir' => $basedir . '/../public',
    'cache_dir_public_path' => 'data/cache',
    'noMinify' => true
));

?>
    <!DOCTYPE html>
    <html>
    <head>
        <link rel="stylesheet" href="/css/font/webfontkit-20131127-072243/stylesheet.css" type="text/css">
        <link rel="stylesheet" href="/css/font/webfontkit-20131127-074021/stylesheet.css" type="text/css">

        <?php
        // -------  css libs ------->
        $minifier->appendCssFile("/css/jquery-ui-1.10.3.custom.css")
            ->appendCssFile("/css/jquery.jscrollpane.css")
            ->appendCssFile("/css/reset.css")
            ->appendCssFile("/css/main.css")
            ->appendCssFile("/css/mosaic.css")
            ->appendCssFile("/css/jquery-impromptu.css");
        ?>
        <?php if (\library\App::isMacOs()) : ?>
            <?php $minifier->appendCssFile("/css/macos_trimming.css"); ?>
        <?php endif; ?>

        <!------- external js libs ------->
        <script type="text/javascript" src="https://vk.com/js/api/openapi.js?98"></script>
        <script src="https://vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>

        <?php
        // standard js libs
        $minifier
            ->appendJsFile("/js_v2/standard/jquery-2.0.3.min.js")
            ->appendJsFile("/js_v2/standard/jquery-ui-1.10.3.custom.min.js")
            ->appendJsFile("/js_v2/standard/jquery-tmpl-1.0.0.min.js")
            ->appendJsFile("/js_v2/standard/jquery.mousewheel.js")
            ->appendJsFile("/js_v2/standard/jquery.jscrollpane.min.js")
            ->appendJsFile("/js_v2/standard/jquery.tagcanvas.js")
            ->appendJsFile("/js_v2/standard/jquery.blockUI.js")
            ->appendJsFile("/js_v2/standard/jquery.cookie.js")
            ->appendJsFile("/js_v2/standard/jquery.limit-1.2.source.js")
            ->appendJsFile("/js_v2/standard/jquery-impromptu.js")
            ->appendJsFile("/js_v2/standard/mosaic.1.0.1.min.js")
            ->appendJsFile("/js_v2/standard/sockjs.js")
            ->appendJsFile("/js_v2/standard/modernizr.custom.js");
        ?>

        <?php
        // js models libs
        $minifier->appendJsFile("/js_v2/models/user.js")
            ->appendJsFile("/js_v2/models/message.js")
            ->appendJsFile("/js_v2/models/leader.js")
            ->appendJsFile("/js_v2/models/game.js"); ?>

        <?php
        // js managers libs
        $minifier->appendJsFile("/js_v2/managers/transport.js")
            ->appendJsFile("/js_v2/managers/main.js")
            ->appendJsFile("/js_v2/managers/logger.js")
            ->appendJsFile("/js_v2/managers/notif.js")
            ->appendJsFile("/js_v2/managers/user.js")
            ->appendJsFile("/js_v2/managers/ribbon.js")
            ->appendJsFile("/js_v2/managers/online.js")
            ->appendJsFile("/js_v2/managers/message.js")
            ->appendJsFile("/js_v2/managers/game.js")
            ->appendJsFile("/js_v2/managers/app.js")
            ->appendJsFile("/js_v2/managers/leader.js"); ?>

        <?php
        // js plugins libs
        $minifier->appendJsFile("/js_v2/plugins/game.make.vip.js")
            ->appendJsFile("/js_v2/plugins/game.styled.dialog.js")
            ->appendJsFile("/js_v2/plugins/game.question.dialog.js")
            ->appendJsFile("/js_v2/plugins/game.model.dialog.js")
            ->appendJsFile("/js_v2/plugins/game.tab.js")
            ->appendJsFile("/js_v2/plugins/game.counter.down.js")
            ->appendJsFile("/js_v2/plugins/game.file.uploader.js")
            ->appendJsFile("/js_v2/plugins/game.help.text.js")
            ->appendJsFile("/js_v2/plugins/game.combobox.js")
            ->appendJsFile("/js_v2/plugins/game.checkbox.js"); ?>

        <!------- output ------->
        <?= $minifier ?>
    </head>
    <body class="cud">

    <div id="complete-dlg" title="Заполнить данные">
        <form>
            <?php if (!$uData->avatar) : ?>
                <div class="form-row">
                    <b>Аватар</b><br/>

                    <input type="button" name="avatar" value="Поставить аватар ВК" id="vkAvatar"/>

                    <div class="mosaic-backdrop avatar" style="display: block;">
                        <img width="80" height="80"/>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!$uData->gender) : ?>
                <hr/>
                <div class="form-row">
                    <b>Ваш пол?
                        <span style="font-size: 11px;color: #FA603D;">
                            (ВНИМАНИЕ! Вы не можете больше изменить свой пол. Пожалуйста будьте крайне внимательно с определением Вашего пола)
                        </span>
                    </b><br/>

                    <label for="g_male">
                        Женский
                        <input type="radio" name="gender" value="1" id="g_male"/>
                    </label>
                    <label for="g_female">
                        Мужской
                        <input type="radio" name="gender" value="2" id="g_female"/>
                    </label>
                </div>
            <?php endif; ?>

            <?php if (!$uData->birth_date) : ?>
                <hr/>
                <div class="form-row">
                    <b>Дата рождения <span>(yyyy-mm-dd)</span></b><br/>

                    <input type="text" name="bdate" placeholder="Например: 1988-02-02"/>
                </div>
            <?php endif; ?>
        </form>
    </div>

    <script>
        $(function () {
            g.isProfileReady = false;

            $('#complete-dlg').dialog({
                width: 252,
                height: 'auto',
                draggable: false,
                resizable: false,
                modal: true,
                closeOnEscape: false,
                autoOpen: true,
                open: function () {
                    // Dialog to front
                    $(this)
                        .parent()
                        .css('z-index', 100000000)
                        .find('.ico-close')
                        .remove();

                    $('.ui-widget-overlay').remove();

                    var vkAvatar = function (context) {
                        if ($(context).data('isLoading')) {
                            return;
                        }

                        $(context).data('isLoading', 1);

                        $.post('/mvc.php?c=User&do=getAvatar', {
                            user_id: <?= (int) $uData->user_id; ?>,
                            sid: '<?= $uData->sid; ?>'
                        }, function (response) {
                            if (!response.result) {
                                alert('ЗАгрузите пожалуйста аватар в профиле ВК и повторите ее раз');
                                return false;
                            }

                            $(context).data('isLoading', 0);
                            $('.mosaic-backdrop.avatar img').attr('src', response.avatar)
                        }, 'json');
                    };
                    $('#vkAvatar').on('click', function () {
                        vkAvatar(this);
                    });
                },
                buttons: {
                    сохранить: function () {
                        $.post('/mvc.php?c=User&do=complete', {
                            data: $(this).find('form').serialize(),
                            user_id: <?= (int) $uData->user_id; ?>,
                            sid: '<?= $uData->sid; ?>'
                        }, function (data) {
                            if (data.result) {
                                location.reload();
                            }
                        }, 'json');
                    }
                }
            });
        });
    </script>
    </body>
    </html>
<?php exit; ?>