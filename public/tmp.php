INSERT INTO sys_questions SET question="Бывали у тебя встречи в реале с людьми, с которыми вы были знакомы только по интернету? Какие впечатления?";
INSERT INTO sys_questions SET question="Как ты думаешь, где ты будешь лет через 10, если всё сложится идеально?";
INSERT INTO sys_questions SET question="У тебя когда-нибудь были прозвища?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь говорили, что ты просто копия какой-нибудь знаменитости?";
INSERT INTO sys_questions SET question="Чем интересны твои друзья, если рассматривать каждого из них в отдельности?";
INSERT INTO sys_questions SET question="Ты ходишь иногда на митинги, стадионные концерты и всякие другие массовые мероприятия?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь хотелось уехать в Новую Зеландию и начать там новую жизнь?";
INSERT INTO sys_questions SET question="Есть такая вещь, без которой ты не представляешь своей жизни?";
INSERT INTO sys_questions SET question="Какая у вас в семье коронная история про тебя, которую всем рассказывают и которой тебя уже немножко достали?";
INSERT INTO sys_questions SET question="Бывало у тебя, что вы с кем-то друг друга ненавидели, а потом вдруг раз – и подружились?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь снились вещие сны";
INSERT INTO sys_questions SET question="У тебя был когда-нибудь такой яркий сон, который запомнился бы тебе на всю жизнь?";
INSERT INTO sys_questions SET question="Как ты думаешь, на что можно было бы потратить миллиард?";
INSERT INTO sys_questions SET question="Тебе бы хотелось иметь какую-нибудь сверхспособность?";
INSERT INTO sys_questions SET question="Какая эпоха подошла бы тебе лучше, чем современность?";
INSERT INTO sys_questions SET question="У тебя в доме когда-нибудь жили кошки или собаки?";
INSERT INTO sys_questions SET question="Если бы в мире был человек – точная копия тебя, вы бы дружили и сотрудничали или воевали и соперничали?";
INSERT INTO sys_questions SET question="Ты боишься высоты?";
INSERT INTO sys_questions SET question="Каким было твоё самое важное сознательно принятое решение, повлиявшее на твою жизнь?";
INSERT INTO sys_questions SET question="Много людей в твоей компании, тусовке?";
INSERT INTO sys_questions SET question="Ты разделяешь для себя работу и личную жизнь?";
INSERT INTO sys_questions SET question="У тебя в жизни было когда-нибудь такое, чтобы казалось, что всё плохо, а потом выяснилось, что всё было к лучшему?";
INSERT INTO sys_questions SET question="Что ты думаешь об участии в каких-нибудь благотворительных волонтёрских организациях?";
INSERT INTO sys_questions SET question="Какое самое красивое место на Земле, в котором тебе доводилось побывать?";
INSERT INTO sys_questions SET question="Бывало, что у тебя слёзы лились ручьём от книги, или фильма, или какой-нибудь телепрограммы?";
INSERT INTO sys_questions SET question="Что самое сложное в жизни, что тебе приходилось делать?";
INSERT INTO sys_questions SET question="Можешь вспомнить какое-нибудь событие, которое сделало тебя сильнее?";
INSERT INTO sys_questions SET question="На что ушли твои первые заработанные деньги?";
INSERT INTO sys_questions SET question="Когда последний раз тебе пришлось сильно нервничать?";
INSERT INTO sys_questions SET question="В каком возрасте к тебе пришло понимание, что ты уже взрослый человек?";
INSERT INTO sys_questions SET question="Какое твоё самое раннее сильное переживание, которое ты помнишь?";
INSERT INTO sys_questions SET question="Какое впечатление у тебя было после первого взгляда на меня?";
INSERT INTO sys_questions SET question="У тебя были приводы в отделение полиции?";
INSERT INTO sys_questions SET question="Какой твой идеальный дом?";
INSERT INTO sys_questions SET question="Какой подарок из всех, которые тебе дарили, вызвал у тебя самую большую радость?";
INSERT INTO sys_questions SET question="Какой у тебя в жизни самый отмороженный и дикий поступок?";
INSERT INTO sys_questions SET question="Если тебя обо мне спросит твоя мама, как ты меня ей опишешь?";
INSERT INTO sys_questions SET question="Ты любишь валяться на пляже?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь случалось влипнуть в любовный треугольник?";
INSERT INTO sys_questions SET question="Ты умеешь хранить секреты?";
INSERT INTO sys_questions SET question="Ты ревнивый человек?";
INSERT INTO sys_questions SET question="Тебе хотелось бы быть такой знаменитостью, чтобы за тобой стаями бегали папарацци?";
INSERT INTO sys_questions SET question="Боишься ли ты, что в кого-нибудь влюбишься и потеряешь самоконтроль?";
INSERT INTO sys_questions SET question="Ты любишь ходить по магазинам просто так – не для покупок, а просто поглазеть?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь хотелось кого-нибудь придушить от злости?";
INSERT INTO sys_questions SET question="Есть у тебя сила воли и самодисциплина?";
INSERT INTO sys_questions SET question="Ты любишь танцевать?";
INSERT INTO sys_questions SET question="Какой поступок в твоей жизни был самым смелым?";
INSERT INTO sys_questions SET question="Тебе нравится твоё имя? Какое имя тебе бы хотелось?";
INSERT INTO sys_questions SET question="А бывало такое – тебе кто-нибудь даст хороший совет, а ты последуешь этому совету и потом думаешь – ёлки-палки, как только меня могли развести на такую глупость?";
INSERT INTO sys_questions SET question="Бывает, что к тебе прилипает какая-нибудь навязчивая идея?";
INSERT INTO sys_questions SET question="Какие песенки ты поёшь, когда принимаешь душ или ванную?";
INSERT INTO sys_questions SET question="Представь, что в мире вообще нет такой вещи, как деньги. Не нужно их зарабатывать. Чем ты тогда будешь заниматься?";
INSERT INTO sys_questions SET question="Если бы тебе предложили вечную молодость и кучу денег с условием, что у тебя в жизни никогда не будет интима, какая была бы твоя реакция?";
INSERT INTO sys_questions SET question="Ты любишь мультики?";
INSERT INTO sys_questions SET question="Какая самая дорогая из купленных тобой вещей впоследствии оказалась тебе совершенно не нужна?";
INSERT INTO sys_questions SET question="Какое у тебя было в жизни самое большое приключение?";
INSERT INTO sys_questions SET question="У тебя в семье есть человек, которого ты хронически не выносишь?";
INSERT INTO sys_questions SET question="Если бы у тебя была возможность навсегда остаться в каком-то возрасте, какой это был бы возраст?";
INSERT INTO sys_questions SET question="Какое самое экзотическое блюдо тебе приходилось пробовать?";
INSERT INTO sys_questions SET question="У тебя в телефоне есть такая песня, которую ты всё время слушаешь?";
INSERT INTO sys_questions SET question="У тебя бывало такое странное ощущение, что это с тобой уже когда-то было? Дежа-вю?";
INSERT INTO sys_questions SET question="Если бы у тебя была возможность за полчаса научиться чему угодно, то чему бы тебе захотелось научиться?";
INSERT INTO sys_questions SET question="Какие твои три самые большие слабости?";
INSERT INTO sys_questions SET question="У тебя когда-нибудь было хобби?";
INSERT INTO sys_questions SET question="Какая у тебя самая вредная привычка?";
INSERT INTO sys_questions SET question="Как ты относишься к травке?";
INSERT INTO sys_questions SET question="Тебе когда-нибудь предлагали пожениться?";
INSERT INTO sys_questions SET question="Кого ты считаешь самым великим человеком в истории?";
INSERT INTO sys_questions SET question="Ты следишь за политикой?";
INSERT INTO sys_questions SET question="Кто тебе нравится из наших политиков?";
INSERT INTO sys_questions SET question="Сколько ты собираешься заработать денег за всю жизнь?";
INSERT INTO sys_questions SET question="Бывали ли у тебя в жизни смертельно опасные ситуации?";
INSERT INTO sys_questions SET question="Тебе нравится риск и азарт?";
INSERT INTO sys_questions SET question="Бывает, что ты с кем-то разговариваешь, и вдруг в разговоре возникает пауза, и вы оба чувствуете дикую неловкость?";
INSERT INTO sys_questions SET question="У тебя есть знакомые, у которых вдруг появились проблемы с алкоголем или наркотиками?";
INSERT INTO sys_questions SET question="Возникает ли у тебя иногда мысль, что ты слишком часто ввязываешься в авантюры?";
INSERT INTO sys_questions SET question="Ты играешь на каком-нибудь музыкальном инструменте?";
INSERT INTO sys_questions SET question="Ты собираешься получать какое-то дополнительное образование?";
INSERT INTO sys_questions SET question="На тебе висел когда-нибудь большой долг или кредит?";
INSERT INTO sys_questions SET question="У тебя когда-нибудь было желание писать стихи?";
INSERT INTO sys_questions SET question="Твоя первая симпатия была взаимной?";
INSERT INTO sys_questions SET question="Какой вид спорта тебе нравится?";
INSERT INTO sys_questions SET question="Что тебя в последний раз насмешило так, чтобы ржать в голос?";
INSERT INTO sys_questions SET question="У тебя никогда не возникало досадного ощущения, что твои друзья тебя просто используют?";
INSERT INTO sys_questions SET question="Ты умеешь готовить? Какие у тебя коронные блюда?";
INSERT INTO sys_questions SET question="Как ты переносишь состояние похмелья?";
INSERT INTO sys_questions SET question="Как ты думаешь, человек сам управляет своей судьбой или в итоге всё решают обстоятельства?";
INSERT INTO sys_questions SET question="В каком климате тебе комфортнее всего?";
INSERT INTO sys_questions SET question="Тебе приходилось когда-нибудь бывать в пустыне или в таком месте, где нет никаких следов человека – ни домов, ни дорог, ни телеграфных столбов – ничего?";
INSERT INTO sys_questions SET question="Где тебе было бы комфортнее жить, в городе или на природе?";
INSERT INTO sys_questions SET question="Какую самую крупную сумму денег тебе приходилось держать в руках?";
INSERT INTO sys_questions SET question="Сколько у тебя аккаунтов в соцсетях?";
INSERT INTO sys_questions SET question="Какая компьютерная игра заставляла тебя надолго <<залипнуть>> на ней?";
INSERT INTO sys_questions SET question="Чем ты в себе гордишься?";
INSERT INTO sys_questions SET question="Какие языки тебе хотелось бы выучить?";
INSERT INTO sys_questions SET question="Когда ты вспоминаешь свою жизнь, что тебя больше всего удивляет?";
INSERT INTO sys_questions SET question="Ты часто бываешь в ударе, на кураже, на подъёме?";
INSERT INTO sys_questions SET question="В какое время суток у тебя пик бодрости? Ты сова или жаворонок?";
INSERT INTO sys_questions SET question="Тебе в жизни нравится экспериментировать?";