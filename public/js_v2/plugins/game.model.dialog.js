(function ($) {

    $.fn.modalDialog = function (onOpen, img) {
        var context = $(this);

        $('<a>')
            .attr('href', '#')
            .attr('onclick', 'return false;')
            .addClass('modal-dlg-left-header')
            .addClass('ico')
            .appendTo($(this));

        $('<a>')
            .attr('href', '#')
            .attr('onclick', 'return false;')
            .addClass('modal-dlg-right-header')
            .addClass('ico')
            .appendTo($(this));

        $('<img />')
            .attr('src', img ? img : userManager.getCurrentUser().getAvatar())
            .addClass('circleBase')
            .addClass('me-avatar')
            .appendTo($(this));

        $(this).dialog({
            open: function () {
                var image = $('img.me-avatar', context),
                    dialog = $(this),
                    leftPos = dialog.width() - image.width();

                image.css({
                    left: (leftPos / 2)
                });

                var u = context.data('forUser') ? context.data('forUser') : userManager.getCurrentUser();
                context
                    .parent()
                    .removeClass(u.getGenderF())
                    .addClass(u.getGenderT());

                image
                    .find('.' + u.getGenderF() + '-avatar')
                    .removeClass(u.getGenderF() + '-avatar');

                image.addClass(u.getGenderT() + '-avatar');
                image.attr('src', u.getAvatar());

                if ($(this).hasClass('toFront')) {
                    $(this).parents('.ui-dialog').css({
                        zIndex: 101
                    });
                }

                if (onOpen) {
                    onOpen(this);
                }

                g.activeToolTip();
            }
        });
    }
})(jQuery);