(function ($) {

    $.fn.checkbox = function (opts) {

        if ($(this).data('made') == 1) {
            return;
        }

        var defaults = {
            className: 'chk'
        };

        var options = $.extend(defaults, opts);

        var context = this;

        $(this).wrap('<div class="custom-checkbox"></div>');
        $(this).hide().data('made', 1);

        $(this).parent().append('<span id="id' + $(this).attr('id') + '" class="' + options.className + '"></span>');

        $(this).on('change', function () {
            if ($(this).is(':checked')) {
                $('#id' + $(this).attr('id')).addClass('checked')
            } else {
                $('#id' + $(this).attr('id')).removeClass('checked')
            }
        });

        $('#id' + $(this).attr('id')).on('click', function () {
            if (options.onClick) {
                options.onClick();
            }

            $(context).trigger('click');
        })
    }

})($);