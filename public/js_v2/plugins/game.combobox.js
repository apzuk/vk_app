(function ($) {
    $.widget("ui.combobox", {
        _create: function () {
            var input,
                that = this,
                wasOpen = false,
                select = this.element.hide(),
                selected = select.children(":selected"),
                value = (selected.val() || (selected.text() || "")),
                textToDisplay = (selected.text() || ""),
                placeholder = select.attr('placeholder'),
                wrapper = this.wrapper = $("<span>")
                    .addClass("ui-combobox input")
                    .insertAfter(select),
                inputDefaultTitle = 'Начните вводить название города.';

            var citiesRefreshTimeout, prevOptions, prevValue, prevText, isInputChanged = false;

            textToDisplay = textToDisplay.split('***');
            textToDisplay = $.trim(textToDisplay[0]);

            function hilightResults(response, select, request, matcher, data) {
                response(select.children("option").map(function () {
                    var text = $(this).text();
                    text = g.optionFormatting(text);

                    if (this.value && ($.trim(text) && !$.trim(request.term) || matcher.test(text) )) {

                        var newExp = new RegExp(
                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                $.ui.autocomplete.escapeRegex(request.term) +
                                ")(?![^<>]*>)(?![^&;]+;)", "gi"
                        );

                        return {
                            label: text.replace(newExp, "<strong>$1</strong>"),
                            value: text,
                            option: this
                        };
                    }
                }));


                return response;
            }

            input = $("<input>")
                .appendTo(wrapper)
                .attr("placeholder", placeholder)
                .attr('id', 'city-select')
                .val(textToDisplay)
                .addClass("ui-state-default ui-combobox-input")
                .addClass("hasToolTip")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    focus: function (event, ui) {
                        /*  var parts = ui.item.option.label.split('***');
                         $('#city-select').val(parts[0]);*/

                        return false;
                    },
                    source: function (request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        if (request.term == '' && typeof(firstCities) != 'undefined') {
                            var html = '';
                            html += '<option></option>';
                            $.each(firstCities, function () {
                                html += '<option value="' + this.value + '">' + this.label + ' *** ' + this.desc + ' *** ' + this.additional + '</option>';
                            });

                            select.html(html);

                            hilightResults(response, select, request, matcher);
                            return false;
                        }

                        var oldTerm = $.data(select, 'prevTerm');
                        if (request.term == oldTerm && request.term) {
                            hilightResults(response, select, request, matcher);
                            return false;
                        }

                        $.data(select, 'prevTerm', request.term);

                        clearTimeout(citiesRefreshTimeout);

                        citiesRefreshTimeout = setTimeout(function () {
                            $.ajax({
                                url: select.data('href'),
                                data: {term: request.term, id: select.attr('data-id')},
                                type: 'post',
                                dataType: 'json',
                                beforeSend: function () {
                                    prevOptions = select.html();
                                    prevValue = select.val();
                                    prevText = select.children('option:selected').text();
                                    isInputChanged = false;

                                    input.autocomplete('widget')
                                        .empty()
                                        .addClass('loading')
                                        .addClass('fillWhite');
                                },
                                success: function (info) {
                                    if (info['status'] == true) {
                                        input.autocomplete('widget')
                                            .removeClass('loading')
                                            .removeClass('fillWhite');

                                        var html = '';
                                        html += '<option></option>';
                                        $.each(info['data'], function () {
                                            var city = this;

                                            if (!city.label) {
                                                return;
                                            }

                                            html += '<option value="' + city.value + '">' + city.label + ' *** ' + city.desc + " *** " + city.additional + '</option>';
                                        });
                                        select.html(html);

                                        hilightResults(response, select, request, matcher);
                                    }

                                }
                            });
                        }, 200);

                    },
                    select: function (event, ui) {
                        select.val(ui.item.option.value);

                        that._trigger("selected", event, {
                            item: ui.item.option
                        });

                        select.trigger('change');

                        isInputChanged = true;

                        setTimeout(function () {
                            $('#city-select').blur();
                        }, 1);
                    },
                    change: function (event, ui) {
                        if (!ui.item) {

                            /*  olf removeIfInvalid function   */

                            var element = this;
                            var value = $(element).val(),
                                matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                                valid = false;

                            select.children("option").each(function () {
                                if ($(this).text().match(matcher)) {
                                    this.selected = valid = true;
                                    return false;
                                }
                            });

                            if (!valid) {
                                select.val("");

                                setTimeout(function () {
                                    input.tooltip("close");
                                    input.attr('title', inputDefaultTitle);
                                }, 2500);
                                input.data("ui-autocomplete").term = "";
                            }
                        }
                    }
                })
                .addClass("ui-widget ui-widget-content ui-corner-left");

            input.on("autocompleteopen", function (event, ui) {
                    $('.ui-autocomplete').addClass(userManager.getCurrentUser().getGenderT());

                    input.tooltip('close');
                    $(this).autocomplete('widget').position({
                        my: 'left top-5',
                        at: 'left bottom',
                        of: $(this).closest('.profile.city')
                    });
                    //$(this).autocomplete('widget').width($(this).closest('.select').width() - 4);
                }
            );

            input.on("autocompleteclose", function (event, ui) {
                    try {
                        var str = $(this.value).eq(0).text();
                        if (str) this.value = str;
                    }
                    catch (e) {
                    }

                    if (!isInputChanged) {
                        select.html(prevOptions);
                        select.find('option[value=' + prevValue + ']').attr('selected', 'selected');
                    }
                }
            );

            input.data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
            };

            $("<a>")
                .attr("tabIndex", -1)
                .appendTo(wrapper)
                .button({
                    icons: {
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("ui-corner-right ui-combobox-toggle")
                .mousedown(function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .click(function () {
                    input.focus();

                    // close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // pass empty string as value to search for, displaying all results
                    input.autocomplete("search", '');
                    input.autocomplete('widget').removeClass('ui-corner-all');
                });

            input.attr('title', inputDefaultTitle);
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
})(jQuery);
