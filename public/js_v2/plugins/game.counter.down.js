(function ($) {

    var interval;

    $.fn.counterDown = function (opts) {
        var defaults = {
            callback: function () {
            },
            count: 80,
            stop: false
        };

        var options = $.extend(defaults, opts);

        if (options.stop) {
            clearInterval(interval);
            return;
        }

        var context = $(this);
        if (opts.count <= 0) {
            options.callback();
            return;
        }

        context.data('count', opts.count);

        context.text(opts.count);

        if (interval) {
            clearInterval(interval);
        }

        interval = setInterval(function () {

            if ($(context['selector']).length == 0) {
                clearInterval(interval);
                return;
            }

            if (!g.isConnected()) {
                clearInterval(interval);
            }

            if (!context.length) {
                clearInterval(interval);
            }

            var sec = parseInt(context.data('count')) - 1;


            if (sec <= 0) {
                clearInterval(interval);
                options.callback();
                return;
            }

            context.text(sec).data('count', sec);
        }, 1000);
    }
})(jQuery);

(function ($) {

    $.fn.timeCounterDown = function (timeInSeconds) {

        if ($(this).attr('hasInterval') == 1) {
            return;
        }

        var interval = setInterval(setTime, 1000), timeInt = parseInt(timeInSeconds), context = this;

        $(this).attr('hasInterval', 1);

        function setTime() {

            var minutes = Math.floor(timeInt / 60);
            var seconds = timeInt - minutes * 60;

            if (seconds < 10) {
                seconds = '0' + seconds;
            }

            var timeStr = minutes + ':' + seconds;
            $(context).text(timeStr);

            timeInt--;

            if (timeInt == 0) {
                $(context).attr('hasInterval', 0);
                clearInterval(interval);
            }
        }

        setTime();
    }

})(jQuery);