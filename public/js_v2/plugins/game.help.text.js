(function ($) {

    $.fn.helpText = function (text) {
        $(this).text(text).fadeIn();
    }

})(jQuery);