(function ($) {

    $.fn.styledDialog = function (opts) {

        /**
         * Default options
         * @type {{}}
         */
        var defaults = {
            btn: '',
            btnLabel: '',
            type: 'male',
            addClass: '',
            openDlg: true,
            zIndex: false,
            btns: []
        };

        /**
         * Options
         * @type {extend|*}
         */
        var options = $.extend(defaults, opts);

        /**
         * Reserve this
         * @type {fn}
         */
        var context = this;

        if (options.zIndex) {
            $(this)
                .parents('.ui-dialog')
                .css({
                    zIndex: options.zIndex
                });
        }

        /**
         * Make jquery dialog styled!
         */
        function make() {
            var $this = $(context);

            //already styled
            $(this).data('styled', 1);

            //add shadow to box
            $this.parent()
                .addClass('sd-box')
                .css({
                    border: 'none',
                    borderTop: '1px solid #ccc',
                    padding: 0
                })
                .find('.ui-dialog-titlebar').remove();

            //wrap styled div
            $this.css({
                padding: 0,
                overflow: 'hidden'
            }).wrap('<div class="sd-wrapper ' + options.type + '"></div>');

            if (options.addClass) {
                $this.parents('.sd-box').addClass(options.addClass);
            }

            //make sd button
            makeBtn();


            $this
                .find('[class*=close]')
                .unbind('click')
                .on('click', function () {
                    $this.dialog('close');
                });

            //show when done!
            $this.dialog('option', 'resizable', false);

            if (options.openDlg) {
                $this.dialog('open');
            }

            if ($this.find('.c').length) {
                $this.dialog('option', 'height', $this.find('.c').height() + 75);

                if (!options.btn) {
                    $this.dialog('option', 'height', $this.find('.c').height() + 30);
                }
            }
        }

        function makeBtn() {
            if (!options.btn && options.btns.length == 0) return;

            if (options.btn) {
                options.btns.push({
                    btn: options.btn,
                    btnLabel: options.btnLabel
                });
            }

            $.each(options.btns, function (index) {
                $(this.btn)
                    .addClass('sd-btn')
                    .attr('value', this.btnLabel)
                    .val(this.btnLabel)
                    .text(this.btnLabel);

                var btnWith = $(context).dialog("option", "width") / 2;

                if (options.btns.length == 2) {
                    $(this.btn).css({
                        width: btnWith + 'px',
                        height: 37,
                        left: 0
                    });

                    //
                    var borderColor = userManager.getCurrentUser().isMale() ? '#7ACFE9' : '#d87b9b';

                    if (index == 0) {
                        $(this.btn).addClass('rBorder');

                        //$(this.btn).width($(this.btn).width() - 1);
                    }

                    if (index == 1) {
                        $(this.btn).css({
                            left: '50%'
                        });
                        $(this.btn).addClass('lBorder');

                        // $(this.btn).width($(this.btn).width() + 1);
                    }

                    $(this.btn).css({
                        width: '-=1'
                    })
                }
            });
        }

        if (!$(this).data('styled')) {
            make();
        }
    }

})(jQuery);