(function ($) {

    $.fn.gameTab = function (opts) {

        var defaults = {
            defaultPage: 1,
            prefix: 'profile'
        };

        var options = $.extend(defaults, opts);

        var context = $(this);

        context.wrap('<div style="overflow: none;" />');

        _hide();
        _show(options.defaultPage);

        $(this).find('ul li').each(function (i, e) {
            $(e).on('click', function (evt) {
                evt.preventDefault();

                var index = $('li.active', context).index();
                var blurEvent = options.prefix + 'TabItem' + (index + 1) + 'blur';

                $('li.active', context).removeClass('active').trigger(blurEvent, ['Custom', 'Event']);
                $(e).addClass('active');

                _show(i + 1);

                var eventName = options.prefix + 'TabItem' + (i + 1) + 'click';
                $(e).trigger(eventName, ['Custom', 'Event']);
            });
        });

        function _show(which) {
            _hide();
            $('.' + options.prefix + '-tab' + which).show();
        }

        function _hide() {
            $("div[class*='" + options.prefix + "-tab']").hide();
        }

    };
})(jQuery);