(function ($) {

    $.fn.questionDialog = function (opts) {
        var defaults = {
            call: false,
            params: {}
        };

        var transport = sockJsHandler;

        var context = $(this);

        var options = $.extend(defaults, opts);

        var button = $('.ask-question', context);

        var question = $('textarea', context);

        if (!options.call) {
            _init();
        } else {
            eval(options.call + '(options.params)');
        }

        function _init() {
            var questionDialog = context.dialog({
                autoOpen: false,
                width: 407,
                height: 203,
                closeOnEscape: false,
                position: {
                    my: "left top",
                    at: "left+380 top+42",
                    of: $("#content")
                },
                open: function () {
                    $(this).parent().find('.ui-dialog-titlebar .icon-close').hide();
                }
            }).styledDialog({
                    btn: '.ask-question',
                    btnLabel: "Задать вопрос",
                    type: userManager.getCurrentUser().getGenderT(),
                    zIndex: 99
                });

            //set gender style to image
            $('img.circleBase', questionDialog)
                .addClass('avatar')
                .addClass(userManager.getCurrentUser().getGenderT() + '-avatar')
                .removeClass(function (index, css) {
                    return (css.match(/clr-b-\d+/g) || []).join(' ');
                }).addClass('clr-b-' + userManager.getCurrentUser().getSelectedColor());

            //set gender color icons
            $('.icons .females', questionDialog)
                .find('div:first')
                .addClass('ico')
                .addClass('ico-female-' + userManager.getCurrentUser().getGenderT())
                .addClass('hasToolTip');

            $('.icons .males', questionDialog)
                .find('div:first')
                .addClass('ico')
                .addClass('ico-male-' + userManager.getCurrentUser().getGenderT())
                .addClass('hasToolTip');

            //textarea border
            $('textarea', questionDialog)
                .addClass(userManager.getCurrentUser().getGenderT());

            g.activeToolTip();

            _controlDisplay();
            _checkbox()

            button.unbind('click').on('click', function (evt) {
                evt.preventDefault();

                if (context.data('busy')) {
                    _undo();
                } else {
                    _play();
                }
            });
        }

        function _controlDisplay() {
            $('header ul li:eq(0)').
                on('gameTabItem1blur', function () {
                    context.dialog('close');
                })
                .on('gameTabItem1click', function () {
                    if (!game.isPlaying()) {
                        context.dialog('open');
                    }

                    $(this).find('time').hide();
                });
        }

        function _checkbox() {
            var pu = $('#play-unlimit');

            pu.hide();

            $('<a>')
                .attr('href', '#')
                .attr('onclick', 'return false;')
                .addClass('ico')
                .addClass('check-play-unlimit')
                .addClass('ico-play-check')
                .prependTo($('.play-unlimit', context));

            $('.check-play-unlimit').on('click', function () {
                if (game.getCurrentIterator()) {
                    return false;
                }

                pu.trigger('click');

                if (pu.is(':checked')) {
                    $('.check-play-unlimit ').addClass('check');
                } else {
                    $('.check-play-unlimit ').removeClass('check');
                }
            });
        }

        function _setGameParams(params) {
            /*if (params.maleCount < 300) {
             params.maleCount = Math.floor(Math.random() * (300 - 10)) + 10
             }
             if (params.femaleCount < 300) {
             params.femaleCount = Math.floor(Math.random() * (300 - 10)) + 10
             }*/

            $('div.males .count', context).text(params.maleCount);
            $('div.females .count', context).text(params.femaleCount);
        }

        function _play() {
            if (context.data('busy')) {
                return;
            }
            var questionTxt = $.trim(question.val());
            if (!questionTxt) {
                question.val('').focus();
                return;
            }
            _setBusyStyle(questionTxt);

            context.data('busy', true);

            button.text('Отмена');
        }

        function _setBusyStyle(questionTxt) {
            var gender = context.parents('.sd-wrapper').attr('class').split(' ')[1],
                colors = {
                    female: "#E898B0",
                    male: "#78B9EC"
                };

            game.play(questionTxt);
            question.prop('disabled', true).css('position', 'absolute');

            $('<div>').css({
                position: 'relative',
                background: colors[gender],
                opacity: 0.4,
                height: question.outerHeight(),
                width: question.outerWidth(),
                marginLeft: 6
            }).addClass('ta_block').insertAfter(question);

            var block = $('.ta_block', context);

            $('<img />')
                .attr('src', '/img/loading6.gif')
                .css({
                    position: "absolute",
                    left: block.outerWidth() / 2 - 21,
                    top: block.outerHeight() / 2 - 5,
                    width: 43,
                    height: 11
                })
                .wrap("<div class='loading'></div>")
                .appendTo(block);
        }

        function _undo(params) {
            if (!params) params = {};

            button.text('Задать вопрос');

            $('div.ta_block', context).remove();
            question.removeAttr('disabled').css('position', 'relative');

            if (params.noStopGame) {
            } else {
                game.unplay();
            }

            if (params.emptyTextarea) {
                question.val('');
            }

            context.data('busy', false);
        }

        function _showBusy() {
        }
    }

})(jQuery);