(function ($) {

    $.fn.makeVip = function (userObj) {

        $('.vip', $(this)).remove();

        if (!userObj.isVip()) {
            return $(this);
        }

        $(this).css({
            position: 'relative'
        });

        $('<div>')
            .addClass('vip')
            .addClass(userObj.getGenderT() + '-vip')
            .appendTo($(this));

        return $(this);
    }

})(jQuery);