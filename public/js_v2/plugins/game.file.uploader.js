(function ($) {

    $.fn.fileUploader = function (opts) {

        var defaults = {
            uploadUrl: '',
            validation: {
                size: {
                    min: {
                        width: 150,
                        height: 150
                    },

                    max: {
                        width: 600,
                        height: 400
                    }
                },

                type: /image.*/
            }
        };

        var options = $.extend({}, defaults, opts);

        $(this).change(function () {

            if (this.files.length == 0) {
                return false;
            }

            _validate(this.files[0]);

            return true;
        });

        var _validate = function (file) {
            if (!file.type.match(options.type)) {
                return 1;
            }

            uploadFile(file);
        };

        var uploadFile = function (file) {

        }

    }

})(jQuery);