leader = function (u, b, m) {
    var user, bid, message, isTrueLeader;

    // Leader user
    user = u ? new userModel(u) : userManager.getCurrentUser();

    // Is it true leader ot ad user
    isTrueLeader = u ? true : false;

    // Leader message
    message = $.trim(m) ? $.trim(m) : 'Вы можете стать лидером вашего города';

    // Current bid
    bid = b ? b : 0;

    //############## public methods ###############
    this.isTrueLeader = function () {
        return isTrueLeader;
    };

    this.isFakeLeader = function () {
        return !isTrueLeader;
    };

    this.leader = function () {
        return user;
    };

    this.bid = function () {
        return bid;
    };

    this.message = function () {
        return message;
    };
};