userModel = function (uData) {
    var user_id, firstname, lastname, avatar, avatar_orig, gender,
        genderT, genderF, city_id, city_name, balance, sid, cost, birth_date,
        age, show_profile, vip, patron, retinues, rating, country_id, visibility,
        color, online, continuously_visit, isLeader, hash, gameIterator;

    var context = this;

    var defaults = {
        user_id: 0,
        firstname: '',
        lastname: '',
        avatar: '',
        avatar_orig: '',
        gender: 0,
        genderT: '',
        genderF: '',
        birth_date: '',
        age: 0,
        city_id: 1,
        city_name: 'Москва',
        balance: 0,
        cost: 0,
        show_profile: 0,
        sid: null,
        vip: false,
        rating: 0,
        country_id: 0,
        visibility: 0,
        color: 0,
        online: 0,
        continuously_visit: 0
    };

    this.dump = function () {
        logger.log('******************************');
        logger.log('user_id = ' + user_id);
        logger.log('firstname = ' + firstname);
        logger.log('lastname = ' + lastname);
        logger.log('avatar = ' + avatar);
        logger.log('birth_date = ' + birth_date);
        logger.log('age = ' + age);
        logger.log('gender = ' + gender);
        logger.log('genderT = ' + genderT);
        logger.log('genderF = ' + genderF);
        logger.log('city_id = ' + city_id);
        logger.log('city_name = ' + city_name);
        logger.log('balance = ' + balance);
        logger.log('show_profile = ' + show_profile);
        logger.log('sid = ' + sid);
        logger.log('vip = ' + vip);
        logger.log('rating = ' + rating);
        logger.log('country_id = ' + country_id);
        logger.log('visibility = ' + visibility);
        logger.log('color = ' + color);
        logger.log('online = ' + online);
        logger.log('continuously_visit = ' + continuously_visit);
        logger.log('hash = ' + hash);
        logger.log('gameIterator = ' + gameIterator);
        logger.log('******************************');
    };

    this.getUserFields = function (f) {
        /*var fields = f.split(',');

         if (fields.length == 0)
         return {};

         var returnObject = {};
         $.each(fields, function (index, value) {
         returnObject = $.extend
         });*/
    };

    this.setUser = function (u) {
        if (!u.username) u.username = '';

        var user = $.extend({}, defaults, u), uName = u.username.split(' ');

        if (typeof uName[0] != 'undefined') {
            firstname = uName[0];
        }

        if (typeof uName[1] != 'undefined') {
            lastname = uName[1];
        } else {
            lastname = '';
        }

        function calcAge(dateString) {
            var birthday = +new Date(dateString);

            if (!birthday) {
                return false;
            }

            return ~~((Date.now() - birthday) / (31557600000));
        }

        user_id = user.user_id;
        avatar = user.avatar;
        avatar_orig = user.avatar_orig;
        gender = user.gender;
        genderT = user.gender == 1 ? 'female' : 'male';
        genderF = user.gender == 1 ? 'male' : 'female';
        age = calcAge(user.birth_date);
        city_id = user.city_id;
        city_name = user.city_name;
        birth_date = user.birth_date;
        cost = user.cost;
        show_profile = user.show_profile;
        vip = user.vip;
        country_id = user.country_id;
        rating = user.rating;
        visibility = user.visibility;
        color = user.color;
        online = parseInt(user.is_online);
        continuously_visit = user.continuously_visit;
        gameIterator = user.gameIterator;

        if (!sid) {
            sid = user.sid;
        }

        if (!balance) {
            balance = user.balance;
        }

        if (!hash) {
            hash = user.hash;
        }

        return context;
    };

    if (uData) {
        context.setUser(uData);
    }

    this.reload = function (callback) {
        context.load(user_id, callback);
    };

    this.load = function (user_id, callback) {
        g.getJson('/mvc.php?c=User&do=getProfile', {
            ids: user_id,
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        }, function (data) {
            context.setUser(data[0]);

            if (callback) {
                callback(data);

            }
        }, function () {

        });
    };

    this.getColorHex = function () {
        var colors = {
            male: [
                '21a4d5',
                '00ffb7',
                'ff4f3a',
                '00b8ff',
                '002d41'
            ],

            female: [
                'ff92a9',
                'ff1546',
                'e400ff',
                '00ff2a',
                'e2ff29'
            ]
        };

        var sColor = context.getSelectedColor();
        logger.log('CONTEXTSDSADASDASDASDASD:' + sColor);
        if (!sColor) {
            return context.getGenderT() == "female" ? 'ff92a9' : '21A4D5';
        }

        return colors[context.getGenderT()][sColor - 1]
    };

    this.loadSponsorData = function (callback) {
        retinues = {};

        $.post('/mvc.php?c=User&do=getSponsorInfo', {user_id: user_id}, function (data) {
            var count = 0;
            if (data.sponsor) {
                patron = new userModel();
                patron.setUser(data.sponsor);
            }

            $(data.retinue).each(function (i, e) {
                var r = new userModel(e);
                retinues['user_' + i] = r;

                count++;
            });

            callback(context, count);
        });
    };

    this.getUser = function () {
        return {
            user_id: user_id,
            username: firstname + ' ' + lastname,
            firstname: firstname,
            lastname: lastname,
            avatar: context.getAvatar(),
            avatar_orig: context.getOriginalAvatar(),
            gender: gender,
            genderT: genderT,
            genderF: genderF,
            age: context.getAge(),
            birth_date: birth_date,
            birth_month: '',
            birth_year: '',
            city_id: city_id,
            city_name: city_name,
            balance: balance,
            cost: cost,
            show_profile: show_profile,
            sid: sid,
            vip: vip,
            rating: rating,
            country_id: country_id,
            visibility: context.visibilityMode(),
            color: context.getSelectedColor(),
            online: context.isOnline(),
            continuously_visit: context.continuouslyVisit(),
            iterator: gameIterator
        };
    };

    this.getIterator = function () {
        return gameIterator;
    };

    this.vkUrlFromWall = function (type) {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        function guid() {
            return s4() + s4() + '-' + s4() + '-' + s4();
        }

        if (type != 1 && type != 2) {
            type = 1;
        }

        return g.appUrl() + '#hash_code=' + hash + '&ref_type=' + type + '&&guid=' + guid();
    };

    this.data = function () {
        return {
            user_id: user_id,
            username: firstname + ' ' + lastname,
            firstname: firstname,
            lastname: lastname,
            avatar: avatar,
            avatar_orig: avatar_orig,
            gender: gender,
            genderT: genderT,
            genderF: genderF,
            age: context.getAge(),
            birth_date: birth_date,
            city_id: city_id,
            city_name: city_name,
            balance: balance,
            cost: cost,
            show_profile: show_profile,
            sid: sid,
            vip: vip,
            rating: rating,
            country_id: country_id,
            visibility: context.visibilityMode(),
            color: context.getSelectedColor(),
            online: context.isOnline(),
            continuously_visit: context.continuouslyVisit()
        };
    };

    this.update = function (data) {
        if (data.cost != undefined) cost = data.cost;
        if (data.balance != undefined) balance = data.balance;
        if (data.rating != undefined) rating = data.rating;
        if (data.vip != undefined) vip = data.vip;
        if (data.color != undefined) color = data.color;
        if (data.visibility != undefined) visibility = parseInt(data.visibility);

        sockJsHandler.send({
            action: 'updateMe',
            isLeader: context.isLeader(),
            isVip: context.isVip(),
            module: 'userManager'
        });
    };

    this.getOriginalAvatar = function (cache) {
        if (!cache) {
            return '/data/avatars/original/' + avatar_orig;
        }

        return '/data/avatars/original/cache/80/' + avatar_orig;
    };

    this.getFullName = function (type) {
        switch (type) {
            case 'fname':
                return firstname;
                break;
            case 'lname':
                return lastname;
                break;
            default:
                return firstname + ' ' + lastname;
        }
    };

    this.getAvatar = function (size) {
        if (!size) size = 100;

        return '/data/avatars/100/' + avatar;
    };

    this.getPatron = function () {
        return patron;
    };

    this.getRetinues = function () {
        return retinues;
    };

    this.getUserId = function () {
        return user_id;
    };

    this.getCityId = function () {
        return city_id;
    };

    this.getCityName = function () {
        return city_name;
    };

    this.getGender = function () {
        return gender;
    };

    this.getGenderT = function () {
        return genderT;
    };

    this.getGenderF = function () {
        return genderF;
    };

    this.isMale = function () {
        return genderT == 'male';
    };

    this.isFemale = function () {
        return genderT == 'female';
    };

    this.getUserName = function () {
        return firstname + ' ' + lastname;
    };

    this.sid = function () {
        return sid;
    };

    this.cost = function () {
        return cost;
    };

    this.getCost = function () {
        return g.getBalance(cost);
    };


    this.bDate = function (type) {
        if (!birth_date) birth_date = '--';

        var dArray = birth_date.split('-');

        if (type == 'day') {
            if (dArray[2]) {
                return dArray[2];
            }

            return '';
        }

        if (type == 'month') {
            if (dArray[1]) {
                return dArray[1];
            }

            return '';
        }

        if (type == 'year') {
            if (dArray[0]) {
                return dArray[0];
            }

            return '';
        }

        return birth_date;
    };

    this.getAge = function () {
        if (age == 0) return '';

        return age;
    };

    this.isPublicProfile = function () {
        return show_profile == 'all';
    };

    this.isOnlyVip = function () {
        return show_profile == 'vip';
    };

    this.isVip = function () {
        return vip == 1;
    };

    this.balance = function () {
        return g.getBalance(balance);
    };

    this.rating = function () {
        return rating;
    };

    this.getCountryId = function () {
        return country_id;
    };

    this.visibilityMode = function () {
        return context.isVip() && parseInt(visibility);
    };

    this.isLeader = function () {
        return user_id == leaderManager.getCityLeader().getUserId() && leaderManager.isTrueLeader();
    };

    this.getSelectedColor = function () {
        if (!context.isVip()) {
            return 0;
        }
        return parseInt(color);
    };

    this.isOnline = function () {
        return online;
    };

    this.continuouslyVisit = function () {
        return parseInt(continuously_visit) ? parseInt(continuously_visit) : 0;
    }
};