messageModel = function () {
    var message_id, message, sender_status, receiver_status, msg_date, conv_id,
        receiver, sender, self, attachment;

    var context = this;

    var defaults = {
        message_id: 0,
        sender_user_id: 0,
        receiver_user_id: 0,
        message: '',
        sender_status: 'unread',
        receiver_status: 'unread',
        create_ts: '2013-01-01 00:00:00',
        conv_id: 0,
        attachment: ''
    };

    this.dump = function () {
        logger.log('******************************');
        logger.log('message_id = ' + message_id);
        logger.log('message = ' + message);
        logger.log('sender_status = ' + sender_status);
        logger.log('receiver_status = ' + receiver_status);
        logger.log('create_ts = ' + msg_date);
        logger.log('conv_id = ' + conv_id);
        if (receiver) {
            logger.log('============receiver===========');
            receiver.dump();
        }

        if (sender) {
            logger.log('============sender===========');
            sender.dump();
        }
        logger.log('******************************');
    };

    this.setMessage = function (m) {
        var msg = $.extend({}, defaults, m);

        message_id = msg.message_id;
        message = msg.message;
        msg_date = _getMessageFormat(msg.msg_date);
        conv_id = msg.conv_id;
        sender_status = msg.sender_status;
        receiver_status = msg.receiver_status;
        self = userManager.getCurrentUser().getUserId() == msg.sender_user_id;
        attachment = msg.attachment;

        if (attachment) {
            attachment = $.parseJSON(attachment);
        }

        if (self) {
            sender = userManager.getCurrentUser();

            receiver = new userModel();
            receiver.setUser(m);
        }
        else {
            sender = new userModel();
            sender.setUser(m);

            receiver = userManager.getCurrentUser();
        }
    };

    this.getMessageView = function () {
        return {
            sender_user_id: sender.getUserId(),
            receiver_user_id: receiver.getUserId(),
            receiver_status: receiver_status,
            username: sender.getUserName(),
            gender: sender.getGenderT(),
            message_id: message_id,
            message: message,
            date: msg_date,
            avatar: sender.getAvatar(),
            self: self,
            hasAttachment: attachment ? true : false,
            img: attachment ? '/data/msg/cache/' + attachment[1] : null
        };
    };

    this.receiver = function () {
        var d = receiver.getUser();
        d["receiver_status"] = receiver_status;

        return d;
    };

    this.sender = function () {
        var d = sender.getUser();
        d["sender_status"] = sender_status;

        return d;
    };

    this.isSelf = function () {
        return self;
    };

    this.attachment = function () {
        return attachment;
    };

    this.getConvId = function () {
        return conv_id;
    };
//=======================================================================================
    var _getMessageFormat = function (c_ts) {
        var monthNames = {
            January: 'Январь',
            February: 'Февраль',
            March: 'Март',
            April: 'Апрель',
            May: 'Май',
            June: 'Июнь',
            July: 'Июль',
            August: 'Август',
            September: 'Сентябрь',
            October: 'Октябрь',
            November: 'Ноябрь',
            December: 'Декабрь'
        };

        logger.log('Create timestamp: ' + c_ts);

        var d = new Date(g.getDateFromFormat($.trim(c_ts), 'yyyy-MM-dd HH:mm:ss'));

        //logger.log()
        c_ts = g.formatDate(d, 'dd, MMM yyyy HH:mm');

        logger.log('Create timestamp: ' + c_ts + '; d: ' + d);

        $.each(monthNames, function (en_name, ru_name) {
            c_ts = c_ts.replace(en_name, ru_name);
        });

        return c_ts;
    }
}
;