notifManager = new function () {

    var context = this, notifBlock, notifItems, ratingBlock, ratingItems,
        transport = sockJsHandler, types, confirmNotifRemoval;

    this.initManager = function () {
        // Jquery selectors
        notifBlock = $('.notif .notif-side');
        notifItems = notifBlock.find('.items');
        ratingBlock = $('.notif .notif-rating-side');
        ratingItems = ratingBlock.find('.ratings');

        types = {
            '+': 'positive',
            '-': 'negative'
        };

        // Load notifications for new news
        _updateNotifications(true, true);

        g.activeToolTip();

        // Popups
        confirmNotifRemoval = $('#confirmNotifRemoval').dialog({
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 420,
            height: 'auto',
            buttons: {
                'Да': function () {
                    _removeNotification($(this).data('id'));

                    $(this).dialog('close');
                },
                'Нет': function () {
                    $(this).dialog('close');
                }
            }
        });
    };

    /**
     * @param user_id
     */
    this.sayUserToUpdate = function (user_id) {
        transport.send({
            to_user_id: user_id,
            module: 'notificationManager',
            action: 'sayUserToUpdateNotifications'
        });
    };

    /**
     * @param id
     * @param c
     */
    this.makeAsGot = function (id, c) {
        if (!$(c).hasClass('new')) {
            return;
        }

        if ($(c).data('inProcess')) {
            return;
        }
        $(c).data('inProcess', 1);

        transport.send({
            id: id,
            module: 'notificationManager',
            action: 'makeASRead'
        });
    };

    /**
     * @param notif_id
     */
    this.confirmRemoveNotif = function (notif_id) {
        confirmNotifRemoval.data('id', notif_id);
        confirmNotifRemoval.dialog('open');
    };

    /**
     * @param data
     */
    this.changeNotifStatus = function (data) {
        var id = '#notif_' + data.id;
        _changeStatus(id);
    };

    this.updateNotif = function () {
        var lastIdArr = notifItems.find('.notif-item:first').attr('id').split('_'), lastId;
        lastId = lastIdArr.length > 1 ? lastIdArr[1] : 0;

        $.post('/mvc.php?c=User&do=getNotificationUnread', {
            lastId: lastId,
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        }, function (data) {

            var api = notifItems.data('jsp');

            $(data.notif.data).each(function () {
                var user = new userModel(this),
                    pane = notifItems.data('jsp') != undefined ? notifItems.data('jsp') : notifItems;

                this.create_ts = g.getDateWithTimezone(this.create_ts);

                var html = $('#notif_tmpl').tmpl($.extend({}, this, user.getUser()));
                pane.getContentPane().prepend(html.wrap('<div>').parent().html());
            });

            _badgeUpgrade(data.notif.unreadCount);
        });
    };

    this.updateRating = function () {
        var lastIdArr = ratingItems.find('.item:first').attr('id').split('_'), lastId;
        lastId = lastIdArr.length > 1 ? lastIdArr[1] : 0;

        $.post('/mvc.php?c=User&do=getRatingsUnread', {
            lastId: lastId,
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        }, function (data) {

            $(data.rating.data).each(function () {
                var obj = {
                    type: types[this.type],
                    t: this.type,
                    value: parseInt(this.count),
                    create_ts: this.create_ts,
                    label: this.label,
                    rating_id: this.id
                };
                var pane = ratingItems.data('jsp') != undefined ? ratingItems.data('jsp').getContentPane() : ratingItems;

                $('#rating_log_item_tmpl').tmpl(obj).prependTo(pane);
            });
        });
    };

    this.badgeUpgrade = function (count, add) {
        _badgeUpgrade(count, add);
    };

    this.showMessage = function (user_id) {
        messageManager.selectDialog(user_id, $('.left-side .item[data-user-id="' + user_id + '"]').get(0));
    };
    //===========================================================================
    var _changeStatus = function (object) {
        $(object).animate({opacity: 0}, 400, function () {
            $(this)
                .removeClass('new')
                .removeClass('last')
                .unbind('click')
                .animate({opacity: 1});

            _badgeDowngrade();
        });
    };

    var _removeNotification = function (notif_id) {
        $.post('/mvc.php?c=User&do=removeUserNotif', {
            id: notif_id,
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        }, function (data) {
            if (data.result) {
                $('#notif_' + notif_id).slideUp();
            }
        }, 'json');
    };

    var _badgeUpgrade = function (count, add) {
        var badgeMenu = $('header ul.menu li:eq(2) .badge');

        // Update top menu badge
        var badgeMenuNum = parseInt(badgeMenu.text().trim());

        if (count > badgeMenuNum) {
            _shake();
        }

        if (count > 0) {
            badgeMenu.text(count).removeClass('zero');
        } else {
            badgeMenu.text(count).addClass('zero');
        }

        if (count.toString().length > 1) {
            badgeMenu.removeClass('single').addClass('many');
        } else {
            badgeMenu.removeClass('many').addClass('single');
        }
    };

    var _badgeDowngrade = function () {
        var badgeMenu = $('header ul.menu li:eq(2) .badge');

        // Update top menu badge
        var badgeMenuNum = parseInt(badgeMenu.text().trim());
        badgeMenu.text(--badgeMenuNum < 0 ? 0 : badgeMenuNum);
        if (badgeMenuNum == 0) {
            badgeMenu.removeClass('many').addClass('single').addClass('zero');
        }
        if (badgeMenuNum.length > 1) {
            badgeMenu.removeClass('single').addClass('many');
        } else {
            badgeMenu.removeClass('many').addClass('single');
        }
    };

    var _shake = function () {
        $('header .menu li:eq(2) .badge').removeClass('zero').effect("shake");
    };

    var _updateNotifications = function (is_notif, is_rating) {

        // Pages
        var n_offset = notifItems.data('page') ? notifItems.data('page') : 0;
        var r_offset = ratingItems.data('page') ? ratingItems.data('page') : 0;

        if (is_notif && !is_rating && n_offset == -1) {
            return;
        }
        if (is_rating && !is_notif && r_offset == -1) {
            return;
        }

        // Load latest notifications
        $.post('/mvc.php?c=User&do=getNotification', {
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid(),
            n_offset: n_offset,
            r_offset: r_offset,
            is_notif: is_notif,
            is_rating: is_rating
        }, function (data) {

            if (is_notif) {
                $(data.notif.data).each(function () {
                    var user = new userModel(this),
                        pane = notifItems.data('jsp') != undefined ? notifItems.data('jsp').getContentPane() : notifItems;

                    this.create_ts = g.getDateWithTimezone(this.create_ts);

                    $('#notif_tmpl').tmpl($.extend({}, this, user.getUser())).appendTo(pane);
                });

                if (data.notif.data.length == 0) {
                    notifItems.data('page', -1);
                } else {
                    notifItems.data('page', ++n_offset);
                }
                notifItems.data('inProcess', false);
                notifItems.data('is_notif', true);

                _scroll(notifItems);
                _badgeUpgrade(data.notif.unreadCount);
            }

            if (is_rating) {
                $(data.rating.data).each(function () {
                    var obj = {
                        type: types[this.type],
                        t: this.type,
                        value: parseInt(this.count),
                        create_ts: this.create_ts,
                        label: this.label,
                        rating_id: this.id
                    };
                    var pane = ratingItems.data('jsp') != undefined ? ratingItems.data('jsp').getContentPane() : ratingItems;

                    $('#rating_log_item_tmpl').tmpl(obj).appendTo(pane);
                });

                if (data.rating.data.length == 0) {
                    ratingItems.data('page', -1);
                } else {
                    ratingItems.data('page', ++r_offset);
                }

                ratingItems.data('inProcess', false);
                ratingItems.data('is_rating', true);

                _scroll(ratingItems);
            }


        }, 'json');
    };

    var _scroll = function (selector) {
        if (!selector.data('hasScroll')) {
            var jPane = selector.jScrollPane({
                autoReinitialise: true
            });

            selector.data('hasScroll', true);
        }

        selector.bind('jsp-scroll-y', function (event, scrollPositionY, isAtTop, isAtBottom) {
            if ($(this).data('inProcess')) {
                return;
            }

            if ($(this).data('page') == -1) {
                $(this).unbind('jsp-scroll-y');
                return;
            }

            if (isAtBottom) {
                $(this).data('inProcess', true);
                _updateNotifications($(this).data('is_notif'), $(this).data('is_rating'));
            }
        });
    };
};

$(function () {
});