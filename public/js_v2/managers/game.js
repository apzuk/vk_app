game = new function () {

    var transport = sockJsHandler;

    var context = this;

    var isAuth = false;

    var isPlayersDataSet = false;

    var checkFor = 1;

    var currentIterator = null;

    var autoStartDone = false;

    var playing = false;

    var autoplaying = false;

    var gameTab1;

    var gModel;

    this.auth = function () {
        transport.send({
            action: "auth",
        });
    };

    this.playSound = function (type) {
        if ($.cookie('noPlay') != "1") {
            return;
        }

        var filename;
        if (type == 'GAME_START') {
            filename = '/sounds/Glass.mp3';
        }

        $('#sound').html("<audio src=\"" + filename + "\" autoplay></audio>");
    };

    this.getPlaying = function () {
        return playing;
    };

    this.get = function () {
        return gModel;
    };

    this.isUserAuth = function () {
        return isAuth;
    };

    this.authenticated = function () {
        isAuth = true;
        if (!autoStartDone) {
            autoStartDone = true;
            if (autoplaying) {
                //autoplaying
                var user_id = userManager.getCurrentUser().getUserId();
                var question = $('#question_dialog textarea');
                question.val('user' + user_id + ' question');
                $('.ask-question').trigger('click');
                ///end autoplaying
            }
        }
    };

    this.play = function (questionTxt) {
        var action = $('#play-unlimit').is(':checked') ? 'playWithoutQueue' : 'play';

        return transport.send({
            action: action,
            gender: userManager.getCurrentUser().getGenderT(),
            question: questionTxt
        });
    };

    this.unplay = function () {
        if (!currentIterator) return;

        transport.send({
            action: "undoPlay",
            gender: userManager.getCurrentUser().getGenderT(),
            iterator: currentIterator
        });

        currentIterator = null;
        playing = false;
    };

    this.placed = function (data) {
        currentIterator = data.iterator;
    };

    this.gameResponse = function (data) {
        if (!gModel) {
            gModel = new gameModel(data);
        } else {
            gModel.updateMe(data);
        }

        currentIterator = data.iterator;

        if (!gModel.isPlaying()) {
            isAuth = true;
            var myQuestion = gModel.getMyQuestion();

            $('#question_dialog textarea').val(myQuestion);
            $('#question_dialog .ask-question').trigger('click');

            _removeModel();

            return;
        }

        _gameLoadFromModel();
    };

    this.switchToGamePage = function () {
        var gameTab1 = $('.game-tab1');
        $('.start-page', gameTab1).animate({
            opacity: 0
        }, 350, function () {
            $(this)
                .removeAttr('style')
                .addClass('xhidden');
        });

        $('.gamePage', gameTab1).removeClass('xhidden');

        $('.leader').dialog('close');
        $('#question_dialog').dialog('close');
    };

    this.setRandomQuestion = function () {
        if (currentIterator) {
            return false;
        }

        $.post('/mvc.php?c=Sys&do=getRandomQuestion', {}, function (data) {
            $('#question_dialog textarea').val(data.question);
        }, 'json');

        return true;
    };

    this.setDoneStatus = function (data) {
        _setDoneStatus(data.user_id, 'Ответил' + gModel.getUserEnd(data.user_id) + ' на все вопросы!');
    };

    this.chosenDone = function (data) {
        _setDoneStatus(data.user_id, 'Сделал' + gModel.getUserEnd(data.user_id) + ' свой выбор!');
    };

    this.getCurrentIterator = function () {
        return currentIterator;
    };

    this.isPlaying = function () {
        if (!gModel) return false;

        return gModel.isPlaying();
    };
    /***********************************************************/
    var _gameLoadFromModel = function () {
        isAuth = true;

        // close all jquery dialogs
        $(".ui-dialog-content").dialog("close");

        // Set players data
        _gameSetPlayersData();

        // Call current step
        eval('_step' + (gModel.step() + 1) + '()');

        // Make sure game page is shown
        $('#wrapper header ul.menu li:eq(0)').trigger('click');

        // Play some sound
        context.playSound('GAME_START');

        // Activate tooltips
        g.activeToolTip();
    };

    var _step1 = function () {
        // Set help text
        $('.g-center .help').helpText('Отвечайте на все вопросы и подождите других участников!');

        // Append step1 html
        $('#gameStep1_tmpl').tmpl({
            genderF: userManager.getCurrentUser().getGenderF()
        }).appendTo($('.gamePage .g-center'));

        // Initial step 1
        _stepInit(1);

        // Set dialog positions
        _animateDialog(0);

        var dlg = $('.dlg .c'), step = 0;

        // Set current question to box
        var _setText = function () {
            var user = gModel.getUserByIndexFromOpposite(step),
                text = gModel.getUserQuestion(user.getUserId());

            $('textarea', dlg).focus().val('');
            $('.question', dlg).text(text);
        };

        // Set first question
        _setText();

        // Answer button click listener
        $('.dlg .sd-btn').unbind('click').on('click', function (evt) {
            var uId = gModel.getUserByIndexFromOpposite(step).getUserId();

            // Send answer to current question
            transport.send({
                action: "setAnswerToQuestion",
                iterator: gModel.iterator(),
                answer: $('textarea', $('.dlg')).val(),
                gender: userManager.getCurrentUser().getGenderF(),
                to: uId
            });

            // Go to next question
            step++;

            // Is it last?!
            if (step == 3) {
                _endUserActions();
            } else {
                _setText();
                _animateDialog(step);
            }

        });

        _startCounterDown(gModel.stepDuration());

        //auto-playing code block!
        /*for (var user_id in gModel.modelSource()[userManager.getCurrentUser().getGenderF()]) {
         var item = gModel.modelSource()[userManager.getCurrentUser().getGenderF()][user_id];
         $('.dlg textarea').val(userManager.getCurrentUser().getUserId() + ' answer to ' + item['question']);
         $('.dlg .dlg-btn').trigger('click');
         }*/
    };

    var _step2 = function () {
        // Set help text
        $('.g-center .help').helpText('Оставляйте комментаий ответом других участников!');

        // Append step1 html
        $('#gameStep2_tmpl').tmpl({
            genderF: userManager.getCurrentUser().getGenderF()
        }).appendTo($('.gamePage .g-center'));

        // Parse user answers
        gModel.setAnswers();

        // Initial step 2
        _stepInit(2);

        var tmpl_obj = {}, step = 0, answers, iterator, user, dlg = $('.dlg .c');

        var _setText = function () {

            // Current user
            user = gModel.getUserByIndexFromOpposite(step);

            // empty object
            tmpl_obj.length = 0;

            // Iterator from 1 :)
            iterator = 1;

            // fill it defaults
            tmpl_obj = {
                hasAnswers: gModel.hasUserAnswers(user.getUserId()),
                has_answer_1: false,
                has_answer_2: false,
                has_answer_3: false
            };

            // Append user answers
            answers = gModel.getUserAnswers(user.getUserId());
            $.each(answers, function (user_id, answer) {
                var u = gModel.getUserById(user_id);

                tmpl_obj['username_' + iterator] = u.getFullName('fname');
                tmpl_obj['question_' + iterator] = answer.question;
                tmpl_obj['answer_' + iterator] = answer.answer;
                tmpl_obj['has_answer_' + iterator] = $.trim(answer.answer) ? true : false;

                iterator++;
            });

            // remove old content
            if (!tmpl_obj.hasAnswers) {
                $('.no-answers', dlg).remove();
            }

            // Append answers tmpl
            dlg.find('div:not(.header)').remove();
            $('#gameStep2Content').tmpl(tmpl_obj).appendTo(dlg);

            // Focus in textarea
            $('textarea', dlg).focus().val('');

            // Correct dialog height
            $('.dlg').dialog('option', 'height', dlg.height() + 75);

            // Set answerer name!
            $('.header .question-label').text(user.getUserName() + ' отвечает...');

            //activate tooltips
            g.activeToolTip();

            // make it jsp
            $('.items').jScrollPane();


            if (tmpl_obj.hasAnswers) {
                // Activate answer estimate
                _activeEstimate();
            }
        };

        $('.dlg .dlg-btn').unbind('click').on('click', function (evt) {
            var ta = $('.dlg textarea'), g = userManager.getCurrentUser().getGenderF();
            transport.send({
                action: "setLastAnswer",
                iterator: gModel.iterator(),
                lastAnswer: ta.val(),
                gender: g,
                to: user.getUserId(),
                estimate: _estimate()
            });

            // Next user answers
            step++;

            // Is it last?!
            if (step == 3) {
                _endUserActions();
            } else {
                _setText();
                var pntikSelector = userManager.getCurrentUser().getGenderF() + '-pntik-right';
                _animatePntik($('.' + pntikSelector), step);
            }
        });

        _setText();

        _startCounterDown(gModel.stepDuration());

        //auto-playing code block!
        /*for (var user_id in gModel.modelSource()[userManager.getCurrentUser().getGenderF()]) {
         var estimate = $('.estimate .ico').eq(Math.floor(Math.random() * 5));

         $('.dlg textarea').val(userManager.getCurrentUser().getUserId() + ' comment to ' + user_id + ' answer');
         estimate.trigger('click');
         $('.dlg .dlg-btn').trigger('click');
         }*/

        return true;
    };

    var _step3 = function () {
        // Set help text
        $('.g-center .help').helpText('Выберите понравившегося Вам участника и подождите пока остальные сделают свой выбор!');

        // Set last answer
        gModel.setLastAnswers();

        var tmpl_obj = {}, iterator = 1, comments = gModel.getMyComments();

        // Tmpl default object
        tmpl_obj = {
            hasComments: false,
            hasComment_1: 0,
            hasComment_2: 0,
            hasComment_3: 0,
            genderF: userManager.getCurrentUser().getGenderF()
        };

        // Append my comment
        $.each(comments, function (user_id, comment) {
            var uId = comment['user_id'],
                commentText = comment['answer'],
                username = gModel.getUserById(uId).getFullName();

            if ($.trim(commentText)) {
                tmpl_obj['hasComment_' + iterator] = 1;
                tmpl_obj['username_' + iterator] = username;
                tmpl_obj['comment_' + iterator] = commentText;
                tmpl_obj['hasComments'] = true;
            }
            else {
                tmpl_obj['hasComment_' + iterator] = 0;
            }

            iterator++;
        });

        // Participants
        iterator = 1;
        $.each(gModel.opposite(), function () {
            tmpl_obj['avatar_' + iterator] = this.getAvatar();
            tmpl_obj['user_id_' + iterator] = this.getUserId();

            iterator++;
        });

        // Append step3 html
        $('#gameStep3_tmpl').tmpl(tmpl_obj).appendTo($('.gamePage .g-center'));

        // Init step
        _stepInit(3);

        // Activate persons to choose
        _activateChoose();

        // Dlg position
        var uiDlg = $('.dlg').parents('.ui-dialog'), uiDlgPos = uiDlg.position();
        $('.participants').css({
            top: uiDlgPos['top'] + uiDlg.height() + 30
        });

        _startCounterDown(gModel.stepDuration());

        //$('.person-item:eq(' + (Math.floor(Math.random() * 2)) + ')').trigger('click');
        //$('.person-item:eq(' + (Math.floor(Math.random() * 2)) + ')').trigger('click');
        //$('.person-item:eq(' + (Math.floor(Math.random() * 2)) + ')').trigger('click');
    };

    var _step4 = function () {
        // Append help text
        $('.g-center .help').helpText('Спасибо за игру, нажмите кнопку чтоб начать новую игру!');

        // Init last step
        _stepInit(4);

        // Parse users chosen
        gModel.setChosen();

        var g = userManager.getCurrentUser().getGenderF(),
            tmpl_obj = {
                genderF: userManager.getCurrentUser().getGenderF(),
                genderT: userManager.getCurrentUser().getGenderT()
            },
            iterator = 1;

        $.each(gModel.onesided(), function () {
            var currentUser = this, current_user_id = currentUser.getUserId();

            if (gModel.hasUserChosen(current_user_id)) {
                var chosenUser = gModel.getChosenUser(current_user_id);

                tmpl_obj['has_left_' + iterator] = 1;
                tmpl_obj['avatar_left_' + iterator] = chosenUser.getAvatar();
            } else {
                tmpl_obj['has_left_' + iterator] = 0;
                tmpl_obj['avatar_left_' + iterator] = '/img/no-patron.png';
            }

            iterator++;
        });

        iterator = 1;
        $.each(gModel.opposite(), function () {
            var currentUser = this, current_user_id = currentUser.getUserId();

            if (gModel.hasUserChosen(current_user_id)) {
                var chosenUser = gModel.getChosenUser(current_user_id);

                tmpl_obj['has_right_' + iterator] = 1;
                tmpl_obj['avatar_right_' + iterator] = chosenUser.getAvatar();
            } else {
                tmpl_obj['has_right_' + iterator] = 0;
                tmpl_obj['avatar_right_' + iterator] = '/img/no-patron.png';
            }

            iterator++;
        });

        // Append last step html
        $('#gameStep4').tmpl(tmpl_obj).appendTo($('.gamePage .g-center'));

        _showAnimation(function () {
            _showBingo();
            _showStartNewGame();

            userManager.updateSelfInfo();
        });
    };

    var _removeModel = function () {
        delete gModel;
        gModel = null;
    };

    var _showAnimation = function (callback) {
        var re = new RegExp(/[ -(a-z A-Z)]/g), b = false;
        $('.chosen.left').each(function () {
            var index = $(this).attr('class').replace(re, '');
            $(this).position({
                my: 'left top',
                at: 'left top+' + ((index - 1) * 180 + 40),
                of: $('.gamePage .g-center')
            });
        });

        $('.chosen.right').each(function () {
            var index = $(this).attr('class').replace(re, '');

            $(this).position({
                my: 'left top',
                at: 'right-200 top+' + ((index - 1) * 180 + 40),
                of: $('.gamePage .g-center')
            });
        });

        $('.right .points').each(function () {
            var p = this;
            var interval = setInterval(function () {
                var i = $(p).find('.ico:not(.active):last');
                if (i.length != 0) {
                    i.addClass('active');
                } else {
                    clearInterval(interval);
                    $(p).parent().find('.participants .person-item').fadeIn(function () {
                        setTimeout(function () {
                            if (b) return;
                            b = true;

                            callback();
                        }, 250);
                    });
                }
            }, 1500);
        });
        $('.left .points').each(function () {
            var p = this;
            var interval = setInterval(function () {
                var i = $(p).find('.ico:not(.active):first');
                if (i.length != 0) {
                    i.addClass('active');
                } else {
                    clearInterval(interval);
                    $(p).parent().find('.participants .person-item').fadeIn();
                }
            }, 1500);
        });
    };

    var _activateChoose = function () {
        $('.person-item').each(function () {
            $(this).mousemove(function () {
                $('.person-item .person .heart.active').removeClass('active');
                $(this).find('.heart').addClass('active');
            });

            $(this).mouseout(function () {
                $(this).find('.heart').removeClass('active');
            });

            $(this).on('click', function (evt) {
                var uId = $(this).attr('id').split('_')[1];

                $('.person-item').unbind('click').unbind('mousemove').unbind('mouseout');

                transport.send({
                    action: "setChosen",
                    iterator: gModel.iterator(),
                    lastAnswer: '',
                    gender: userManager.getCurrentUser().getGenderF(),
                    chosen: uId
                });
            });
        });
    };

    var _activeEstimate = function () {
        $('.estimate .ico-star').each(function () {
            $(this).mousemove(function () {
                $(this).addClass('ico-star-selected').prevAll('.ico-star').addClass('ico-star-selected');
                $(this).nextAll('.ico-star:not(.sel)').removeClass('ico-star-selected');
            });

            $(this).mouseout(function () {
                $('.ico-star-selected:not(.sel)').removeClass('ico-star-selected');
                //              if ($(this).hasClass('sel')) return;

//                $(this).removeClass('ico-star-selected');
            });

            $(this).on('click', function () {
                $(this)
                    .addClass('ico-star-selected')
                    .addClass('sel')
                    .prevAll()
                    .addClass('sel');

                $(this)
                    .nextAll()
                    .removeClass('sel')
                    .removeClass('ico-star-selected');
            });
        });
    };

    var _estimate = function () {
        var selected = $('.estimate .sel').last().attr('class'),
            re = new RegExp(/[ -(a-z A-Z)]/g);

        if (selected == undefined) {
            return 0;
        }

        return parseInt(selected.replace(re, ''));
    };

    var _showStartNewGame = function () {
        // Create button
        $('<a>')
            .attr('href', '#')
            .attr('onclick', 'return false;')
            .addClass('newGame')
            .addClass(userManager.getCurrentUser().getGenderT())
            .hide()
            .unbind('click')
            .on('click', function (event) {
                $('#bingo').empty();

                $('.leader').dialog('open');
                $('#question_dialog').dialog('open').questionDialog({
                    call: '_undo',
                    params: {
                        noStopGame: true,
                        emptyTextarea: true
                    }
                });

                _removeModel();

                isPlayersDataSet = false;
                currentIterator = null;

                $('#bingo').data('openit', 0);

                $('.gamePage').empty().addClass('xhidden');
                $('.start-page').removeClass('xhidden');
            })
            .appendTo($('.g-center'));

        setTimeout(function () {
            $('.points').fadeOut(function () {
                $('.g-center .newGame').fadeIn();
            });

            $('.chosen.left').animate({
                left: -86
            });
            $('.chosen.right').animate({
                left: 312
            });

        }, 450);
    };

    var _showBingo = function () {
        var vk_id = gModel.getChosenSocId(),
            overlap = gModel.hasOverlap();

        if (overlap) {
            var bingo = $('#bingo');
            $('#bingo_tmpl').tmpl({
                genderT: userManager.getCurrentUser().getGenderT(),
                genderF: userManager.getCurrentUser().getGenderF(),
                my_avatar: userManager.getCurrentUser().getAvatar(),
                target_avatar: overlap.getAvatar(),
                target_user_id: overlap.getUserId()
            }).appendTo(bingo);

            bingo.dialog({
                autoOpen: true,
                width: 436,
                height: 370,
                resizable: false,
                draggable: false,
                title: "Бинго!",
                modal: true,
                closeOnEscape: false,
                open: function () {
                    var dialog = this;

                    $(this)
                        .find('.bg .second .bingo-btn.vk')
                        .attr('href', 'http://vk.com/id' + vk_id)
                        .attr('target', '_blank');

                    $('.bingo-btn:not(.vk)').unbind('click').click(function () {
                        $(dialog).dialog('close').data('openit', 1);
                    });
                }
            });
        }
    };

    var _gameSetPlayersData = function () {
        // Initial
        _initial();

        // If already done , stop function execution
        if (isPlayersDataSet) return;
        isPlayersDataSet = true;

        // Players data
        var tmpl_obj = {
            genderT: userManager.getCurrentUser().getGenderT(),
            genderF: userManager.getCurrentUser().getGenderF()
        };

        $([1, 2]).each(function () {
            var i = this,
                users = i == 1 ? gModel.onesided() : gModel.opposite();

            $.each(users, function (r) {
                var user = this, i1 = user.getIterator();

                tmpl_obj['avatar_' + i + '_' + i1] = user.getAvatar();
                tmpl_obj['user_id_' + i + '_' + i1] = user.getUserId();
                tmpl_obj['name_' + i + '_' + i1] = user.getFullName('fname');
                tmpl_obj['age_' + i + '_' + i1] = user.getAge();

                if (user.getSelectedColor()) {
                    tmpl_obj['clr_' + i + '_' + i1] = 'clr-b-' + user.getSelectedColor();
                } else {
                    tmpl_obj['clr_' + i + '_' + i1] = 'clr-b';
                }
            });
        });

        $('#gamePage_tmpl').tmpl(tmpl_obj).appendTo($('.gamePage'));
        _makePlayersVip();
    };

    var _makePlayersVip = function () {
        $([1, 2]).each(function () {
            var i = this,
                gamePage = $('.gamePage'),
                users = i == 1 ? gModel.onesided() : gModel.opposite();

            $.each(users, function (index, user) {
                $('.' + user.getGenderT() + '-item', gamePage)
                    .eq(user.getIterator() - 1)
                    .find('img')
                    .parent()
                    .parent()
                    .makeVip(user);
            });
        });
    };

    var _initial = function () {
        // Set playing is true
        playing = true;

        // Game tab selector
        gameTab1 = $('.game-tab1');

        // Remove vk block
        $('#vk_groups').remove();

        // remove dialog with content
        $('.dlg').parents('.ui-dialog').empty();
        $('.dlg').remove();

        // stop counter donw
        $(document).counterDown({stop: true});

        // Set waiting statuses for all users
        _setWaitingStatus();
    };

    var _stepInit = function (step) {
        var sdParams;

        switch (step) {
            case 1:
                sdParams = {
                    type: userManager.getCurrentUser().getGenderF(),
                    btn: '.dlg-btn',
                    btnLabel: 'Ответить',
                    addClass: 'g-sd',
                    openDlg: true
                };

                break;
            case 2:
                sdParams = {
                    type: userManager.getCurrentUser().getGenderF(),
                    btn: '.dlg-btn',
                    btnLabel: 'Комментировать',
                    addClass: 'g-sd'
                };
                break;
            case 3:
                sdParams = {
                    type: userManager.getCurrentUser().getGenderF()
                };
                break;
            case 4:
                $('.participants').remove();
                playing = false;
                $('.menu .remaining').removeClass('visible');
                break;
        }

        // Dlg
        $('.dlg').dialog({
            autoOpen: false,
            width: 350,
            height: 'auto',
            resizable: false,
            draggable: false,
            position: [227, 120]
        }).styledDialog(sdParams);

        // Switch to game page
        context.switchToGamePage();
    };

    var _animateDialog = function (step) {
        var dlg = $('.dlg');
        dlg.parents('.ui-dialog').animate({
            top: (120 + step * 180),
            left: 227
        }, 500, function () {
            dlg.dialog('open');
        });
    };

    var _animatePntik = function (context, step) {
        var top;

        if (step == 1) top = 235;
        if (step == 2) top = 435;

        context.animate({
            top: top
        });
    };

    var _startCounterDown = function (duration) {
        var serverTime = new Date(gModel.serverTime());
        var dateUpdate = new Date(gModel.dateUpdate());
        var passed = parseInt((serverTime.getTime() - dateUpdate.getTime()) / 1000);

        $('.dlg .header .time b').counterDown({
            callback: function () {
                transport.send({
                    action: 'checkStepTimeExpired',
                    iterator: gModel.iterator(),
                    step: gModel.step()
                })
            },
            count: (duration - passed)
        });
        checkFor++;
    };

    var _setDoneStatus = function (user_id, text) {
        $('#u_' + user_id)
            .find('.ico-wait')
            .removeClass('ico-wait')
            .removeAttr('title')
            .attr('title', text)
            .addClass('ico-ok');
    };

    var _setWaitingStatus = function () {
        if (gModel.step() == 3) {
            return;
        }

        $('.ico-ok')
            .removeClass('ico-ok')
            .addClass('ico-wait')
            .removeAttr('title')
            .attr('title', 'Ожидание');
    };

    var _endUserActions = function () {
        var dlg = $('.dlg');

        $('.sd-btn', dlg).remove();
        $('.female-pntik-right', dlg).remove();
        $('.female-pntik-left', dlg).remove();
        $('.male-pntik-right', dlg).remove();
        $('.male-pntik-left', dlg).remove();
        $('.c > :not(.header)', dlg).remove();

        dlg.dialog('option', 'height', 52);

        dlg.parents('.ui-dialog')
            .css({
                left: 225
            })
            .animate({
                top: 599,
                height: 50
            });

        $('.gamePage .g-center .help').hide();
        $('.gamePage .g-center').append($('#vk_public_tmpl').tmpl({}));
        g.makeVk('vk_group_content');
    }
};

$(function () {
    var interval;
    $('header ul li.heart').on('gameTabItem1blur',function () {
        if (!game.isPlaying() || !game.getPlaying()) {
            $(this).find('.remaining').removeClass('visible');
            return;
        }

        interval = setInterval(aaa, 1000);

        function aaa() {
            var jqSelector = $('.dlg .header .time b').text().trim(),
                time = parseInt(jqSelector),
                minutes = Math.floor(time / 60),
                seconds = time - minutes * 60;

            if (!jqSelector) {
                clearInterval(interval);
                return;
            }

            if (parseInt(seconds) < 10) seconds = '0' + seconds;
            $('.heart .remaining').text('0' + minutes + ':' + seconds);
        }

        aaa();

        $(this).find('.remaining').addClass('visible');
        $('.dlg').parents('.ui-dialog').hide();


    }).on('gameTabItem1click', function () {
            $(this).find('.remaining').removeClass('visible');
            $('.dlg').parents('.ui-dialog').show();

            clearInterval(interval);

            var dlg = $('#bingo');
            if (dlg.data('openit') == 1) {
                dlg.dialog('open');
            }
        });
});

$(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }

        return true;
    });

    $(document).on('tourStart', function () {
        g.startTour();
    });
});