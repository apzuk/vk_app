userManager = new function () {

    /**
     *  User who run the application!
     */
    var currentUser;

    /**
     * Players container type of userModel
     * @type {{}}
     */
    var users = {};

    /**
     * Current loaded user by function showProfile
     */
    var loadedUser;

    /**
     * Current object
     * @type {userManager}
     */
    var context = this;

    /**
     * User profile dialog
     */
    var profileDialog;

    /**
     * Make vip dialog
     */
    var vipDialog;

    /**
     * jQuery dialog
     */
    var donateMoneyDialog;

    /**
     * Vip capabilities dialog
     */
    var vipCapabilities;

    /**
     * Like confirmation Dialog
     */
    var likeConfirmDialog;

    /**
     * Become leader dialog
     */
    var becomeLeaderConfirmDialog;

    /**
     * Payment Jquery dialog
     */
    var paymentDialog;

    /**
     * Confirm kiss dialog
     */
    var kissConfirmDialog;

    /**
     * Transport protocol
     * @type {sockJsHandler|*}
     */
    var transport = sockJsHandler;

    /**
     * Set current user data , who run the application
     * @param user
     */
    this.setCurrentUser = function (user) {
        currentUser = user;
        $(document).trigger('currentUserReady', ['Custom', 'Event']);
    };

    /**
     * Get current user who run  the application
     * @returns {*}
     */
    this.getCurrentUser = function () {
        return currentUser;
    };

    /**
     * Initialize manager
     */
    this.init = function () {
        // jQuery selectors
        var guests = $('#guests'),
            vip = $('#vip'),
            list = $('.list'),
            viewCount = $('.guest', list).length,
            uProfile = $('#userProfile'),
            vc = $('#vipCapabilities');

        /**
         * Create VIP dialog
         * @type {dialog|*}
         */
        vipDialog = vip.dialog({
            autoOpen: false,
            width: 410,
            height: 565,
            title: "Стать VIP пользователем",
            modal: true,
            resizable: false,
            draggable: false,
            close: function () {
                vipDialog.data("forUser", false);
            }
        });
        vip.styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btn: '.close-vip',
            btnLabel: 'Закрыть окно'
        });
        vip.modalDialog();

        /**
         * Create user guests dialog
         * @type {dialog|*}
         */
        guests.dialog({
            autoOpen: false,
            width: 420,
            height: 350,
            title: "Мои гости (" + viewCount + ")",
            modal: true,
            resizable: false,
            draggable: false,
            open: function () {
                // Remove new status
                $('.view_count').removeClass('new');
            }
        });

        /**
         * Create user profile dialog
         * @type {dialog|*}
         */
        profileDialog = uProfile.dialog({
            autoOpen: false,
            width: 600,
            height: 620,
            resizable: false,
            draggable: false,
            closeOnEscape: false,
            show: {
                effect: "blind",
                duration: 380
            },
            hide: {
                effect: "blind",
                duration: 380
            },
            modal: true,
            open: function () {
                // Run some plugins
                g.make();

                // Make retinues list scrollable
                $('.retinue-wrap', $(this)).jScrollPane();
            },
            close: function () {
                loadedUser = null;
            }
        });

        /**
         * Vip capabilities dialog
         * @type {dialog|*|jQuery}
         */
        vipCapabilities = vc.dialog({
            autoOpen: false,
            width: 410,
            height: 296,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Мои настройки",
            closeOnEscape: false,
            close: function () {
            }
        });
        vc.styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btns: [
                {
                    btn: '.save',
                    btnLabel: 'Сохарнить'
                },
                {
                    btn: '.close-vc',
                    btnLabel: 'Закрыть'
                }
            ]
        });
        vc.modalDialog(function () {
            $('.close-vc', vc).click(function (e) {
                e.stopImmediatePropagation();
                $('.tapeToolTip').tooltip('close');

                vipCapabilities.dialog('close');
            });

            _vipSettings();
        });

        /**
         * Confirm kiss dialog
         * @type {dialog|*|dialog|jQuery}
         */
        kissConfirmDialog = $('#confirmKiss').dialog({
            autoOpen: false,
            width: 500,
            height: 150,
            modal: true,
            resizable: false,
            draggable: false
        });

        /**
         * Donate mooney dialog
         * @type {dialog|*|jQuery}
         */
        donateMoneyDialog = $('#donateMoney').dialog({
            autoOpen: false,
            width: 352,
            height: 235,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Передать деньги"
        });

        donateMoneyDialog.styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btns: [
                {
                    btn: '.send-money-btn',
                    btnLabel: 'Отправить'
                },
                {
                    btn: '.close-sm-btn',
                    btnLabel: 'Закрыть'
                }
            ]
        });

        /**
         * Like confirmation dialog
         * @type {dialog|*|dialog|jQuery}
         */
        likeConfirmDialog = $('#confirm-user-like').dialog({
            autoOpen: false,
            width: 450,
            height: 'auto',
            resizable: false,
            draggable: false,
            modal: true
        });

        /**
         * become leader confirm dialog
         * @type {dialog|*|dialog|jQuery}
         */
        becomeLeaderConfirmDialog = $('#confirmBecomeSponsor').dialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            width: 400,
            height: 155,
            show: {
                effect: "blind",
                duration: 150
            },
            hide: {
                effect: "blind",
                duration: 150
            },
            modal: false,
            open: function () {
                var tmpl = {
                    username: loadedUser.getUserName(),
                    cost: g.getBalance(loadedUser.cost() + 1),
                    currency_label: g.getCurrencyLabel()
                };
                $('#become_sponsor_tmpl').tmpl(tmpl).appendTo($(this));
            },
            close: function () {
                $(this).empty();
            }
        });

        /**
         * Payment dialog
         * @type {dialog|*|dialog|jQuery}
         */

        paymentDialog = $('#purse')
            .dialog({
                autoOpen: false,
                width: 620,
                height: 580,
                title: "Ваш кошелек",
                modal: true,
                draggable: false,
                resizable: false,
                closeOnEscape: false
            });

        $('#purse').styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btn: '.close-purse',
            btnLabel: 'Закрыть кошелек'
        });

        $('#purse')
            .addClass(userManager.getCurrentUser().getGenderT())
            .modalDialog(function () {
                $('.wallet-info .value').text(userManager.getCurrentUser().balance());
            });

        /**
         * Load and show user profile
         * @param id
         */
        this.showProfile = function (id) {
            if ($('.tapeToolTip.ui-tooltip').length > 0) {
                $('.tapeToolTip').tooltip('close');
            }

            var tmpl = '#profile_tmpl', uProfile = $('#userProfile');
            loadedUser = new userModel();
            loadedUser.load(id, function (response) {
                // Profile template
                if (id == userManager.getCurrentUser().getUserId()) {
                    tmpl = '#my_profile_tmpl';
                }

                // Set content
                $(tmpl).tmpl($.extend({}, loadedUser.getUser(), {
                    vip: userManager.getCurrentUser().isVip(),
                    link: response[0].link,
                    self: response[0].self,
                    liked: response[0].liked,
                    kissed: response[0].kissed,
                    kiss: response[0].kiss,
                    sponsor: response[0].sponsor,
                    status: loadedUser.isOnline() ? 'online' : 'offline',
                    status_title: loadedUser.isOnline() ? 'Пользователь находится онлайн' : 'Пользователь находится оффлайн'
                })).appendTo(uProfile.empty());

                $('#profile_menu_tmpl')
                    .tmpl($.extend({}, loadedUser.getUser(), {
                        self: response[0].self,
                        likes: response[0].likes,
                        game_count: response[0].game_count,
                        retinue_count: response[0].retinue_count
                    }))
                    .appendTo($('.menu-icons'));

                // Load loaded user sponsor data
                loadedUser.loadSponsorData(function (user, userCount) {
                    _showSponsorData(user, userCount);
                });

                // Fill form loaded user data and make some initial
                _fillFormAndInit();

                // Make mosaic block
                $('.mosaic-block').mosaic({
                    animation: 'slide',
                    hover_y: 0
                });

                // Set dialog title
                var userTitle = loadedUser.getUserName() + ', ' + loadedUser.getAge() + ' лет, ' + loadedUser.getCityName();
                profileDialog.dialog('option', 'title', userTitle);

                // Set show log for this user
                _setViewProfile(id);

                // Show dialog
                profileDialog.dialog('open');
            });
        };

        this.toVK = function () {
            game.get().getUserVKLink(function (link) {
                window.open(link, '_blank');
                window.focus();
            });
        };

        this.sendMoney = function (user_id) {
            donateMoneyDialog.dialog({
                open: function () {
                    var u = loadedUser ? loadedUser : userManager.getCurrentUser();

                    $(this)
                        .parent()
                        .removeClass(u.getGenderF())
                        .addClass(u.getGenderT());

                    $(this)
                        .find('.send-money-btn')
                        .unbind('click')
                        .on('click', function () {
                            _sendMoney(user_id);
                        }
                    );
                }
            }).dialog('open');
        };

        // Update notifications
        this.lostLeaderness = function (data) {
            notifManager.updateNotif();
            leaderManager.setLeader(data);
        };

        // Update notifications
        this.updateProfileInfo = function () {
            notifManager.updateNotif();
            userManager.updateSelfInfo();
        };

        /**
         *
         * @param user_id
         * @param username
         * @param context
         */
        this.like = function (user_id, username, context) {
            if (user_id == userManager.getCurrentUser().getUserId()) {
                return;
            }

            if ($(context).parent().hasClass('dsbl')) {
                return;
            }

            likeConfirmDialog.dialog({
                open: function () {
                    $('#like_user_tmpl').tmpl({
                        name: username,
                        cost: g.getBalance(0.5)
                    }).appendTo($(this));
                },
                close: function () {
                    $(this).empty();
                },
                buttons: {
                    Да: function () {
                        _like(user_id);
                    },
                    Нет: function () {
                        $(this).dialog('close');
                    }
                }
            }).dialog('open');
        };


        /**
         *
         * Kiss to user_id
         *
         * @param user_id
         * @param context
         */
        this.kiss = function (user_id, context) {
            if (user_id == userManager.getCurrentUser().getUserId()) {
                return;
            }

            if ($(context).parent().hasClass('dsbl')) {
                return;
            }

            kissConfirmDialog.dialog({
                autoOpen: true,
                buttons: {
                    Да: function () {
                        _kiss(user_id, context);
                    },
                    Нет: function () {
                        $(this).dialog('close');
                    }
                }
            });
        };

        /**
         * Open payment dialog
         */
        this.payment = function () {
            paymentDialog
                .dialog('open');
        };

        /**
         * Update rating and money info in user block
         */
        this.updateSelfInfo = function () {
            var queryUrl = '/mvc.php?c=User&do=getShortInfo';
            var isOldVip = userManager.getCurrentUser().isVip();

            g.getJson(queryUrl, {},
                function (data) {
                    userManager.getCurrentUser().update({
                        balance: data.user.balance,
                        rating: data.user.rating,
                        vip: parseInt(data.user.vip)
                    });

                    context.showBalance(userManager.getCurrentUser().balance());
                    context.showRating(userManager.getCurrentUser().rating());

                    if (userManager.getCurrentUser().isVip()) {
                        // Remove buy button, replace with vip end date
                        $('.user-profile .buy-button').remove();

                        $('.user-vip')
                            .removeClass('xhidden')
                            .find('.already-vip')
                            .text(data.user.vip_end);


                        $('.vipPanel .play-unlimit').removeClass('xhidden');

                        // Make user vip
                        $('.user-profile .avatar img')
                            .parent()
                            .parent()
                            .makeVip(userManager.getCurrentUser());
                    } else {
                        var avatar = $('.user-profile .avatar');
                        $('.user-vip').addClass('xhidden');

                        $('<a>')
                            .attr('onclick', 'userManager.vip();')
                            .attr('href', '#')
                            .addClass('buy-button')
                            .text('Стать VIP')
                            .appendTo(avatar);

                        avatar.find('.vip').remove();

                        $('.avatar.me').removeClass(function (index, css) {
                            return (css.match(/clr-b-\d+/g) || []).join(' ');
                        });

                        if (isOldVip && leaderManager.isTrueLeader()) {
                            transport.send({
                                action: 'updateCityLeader',
                                module: 'leaderManager',
                                city_id: userManager.getCurrentUser().getCityId()
                            });

                            $('.vipPanel .play-unlimit').addClass('xhidden');
                        }
                    }
                },
                function (data) {

                }
            );
        };

        this.unblockUser = function (user_id) {
            g.getJson('/mvc.php?c=Msg&do=unBlock',
                {
                    unblock_user_id: user_id
                },
                function (data) {

                    var userItem = $('.item[data-user-id="' + user_id + '"]');
                    userItem.data('myBlocked', 0);
                    userItem.trigger('click');

                    transport.send({
                        to_user_id: user_id,
                        module: 'userManager',
                        action: 'unBlocked'
                    });

                }, function (data) {

                });
        };

        /**
         *
         * @param noUser
         */
        this.vip = function (noUser) {
            if (noUser) {
                vipDialog.dialog('open');
                return;
            }

            if (loadedUser && loadedUser.getUserId() != userManager.getCurrentUser().getUserId()) {
                vipDialog.data('forUser', loadedUser);
            }
            vipDialog.dialog('open');
        };

        /**
         *
         * @param data
         */
        this.blocked = function (data) {
            var from_user_id = data.from_user_id;
            $('.left-side .item[data-user-id="' + from_user_id + '"]').data('meBlocked', 1);
            notifManager.updateNotif();
        };

        /**
         *
         * @param data
         */
        this.unBlocked = function (data) {
            var from_user_id = data.from_user_id;
            $('.left-side .item[data-user-id="' + from_user_id + '"]').data('meBlocked', 0);
            notifManager.updateNotif();
        };

        /**
         * @param balance
         */
        this.showBalance = function (balance) {
            var cBalance = $('.user-profile .balance'), currentBalance = cBalance.text().trim();

            if (currentBalance != balance) {
                cBalance.fadeOut(850, function () {
                    $(this).text(balance).fadeIn();
                });
            }
        };

        /**
         * @param rating
         */
        this.showRating = function (rating) {
            var cRating = $('.user-profile .position'), currentRating = cRating.text().trim();

            if (currentRating != rating) {
                cRating.fadeOut(350, function () {
                    $(this).text(rating).fadeIn();
                });
            }
        };

        /**
         * @param data
         */
        this.showViewed = function (data) {
            // Remove no guest label
            $('.no-guest').remove();

            _showViewed(data);
        };

        /**
         *
         * Become to current user sponsor
         *
         * @param user_id
         * @param context
         * @returns {boolean}
         */
        this.becomeSponsor = function (user_id, context) {
            if (!loadedUser) {
                return false;
            }

            if ($(context).hasClass('m-icon-sponsor-selected')) {
                return false;
            }

            becomeLeaderConfirmDialog.dialog({
                buttons: {
                    'Да': function () {
                        _becomeLeader(user_id);
                    },
                    'Нет': function () {
                        becomeLeaderConfirmDialog.dialog('close');
                    }
                }
            }).dialog('open');

            return true;
        };

        /**
         *
         * @param rate
         */
        this.selectVipRate = function (rate) {
            var buttons = $('#vip .buttons');

            $('.buy-button', buttons).hide();
            $('.l', buttons).addClass('a');

            var params = {
                user_id: userManager.getCurrentUser().getUserId(),
                sid: userManager.getCurrentUser().sid(),
                duration: rate
            };

            if (vipDialog.data('forUser')) {
                params['to_user_id'] = vipDialog.data('forUser').getUserId();
            }

            g.getJson('/mvc.php?c=User&do=becomeVip', params,
                function () {
                    // Close vip dialog

                    if (vipDialog.data('forUser') &&
                        vipDialog.data('forUser').getUserId() != userManager.getCurrentUser().getUserId()) {
                        transport.send({
                            action: 'donateVIP',
                            module: 'userManager',
                            to_user_id: vipDialog.data('forUser').getUserId()
                        });
                    }

                    if (!vipDialog.data('forUser')) {
                        // Update rating and money info
                        userManager.updateSelfInfo();
                    }

                    var cityId = vipDialog.data('forUser')
                        ? vipDialog.data('forUser').getCityId()
                        : userManager.getCurrentUser().getCityId();

                    transport.send({
                        action: 'updateCityLeader',
                        module: 'leaderManager',
                        city_id: cityId
                    });

                    $('.buy-button', buttons).show();
                    vipDialog.dialog('close');

                    $(document).trigger('becomeVip', ['Custom', 'Event']);
                },
                function () {
                    $('.buy-button', buttons).show();
                }
            );
        };

        /**
         * Open up user gusts dialog
         */
        this.showUserGuests = function () {
            $('#guests').dialog('open');
        };

        /**
         * Show current user form
         */
        this.openForm = function () {
            // jQuery selectors
            var rightSide = $('.rightSide'),
                sponsorBlock = $('.block.sponsor', rightSide),
                formBlock = $('.block.form');

            // Hide sponsor page
            sponsorBlock.removeClass('active');

            // Show form page
            formBlock.addClass('active');
        };

        /**
         * Close current user form and show sponsor page
         */
        this.closeForm = function () {
            // jQuery selectors
            var rightSide = $('.rightSide'),
                sponsorBlock = $('.block.sponsor', rightSide),
                formBlock = $('.block.form');

            // Hide sponsor page
            formBlock.removeClass('active');

            // Show form page
            sponsorBlock.addClass('active');
        };

        this.runVipEndListening = function (left) {
            var l = (parseInt(left) + 5) * 1000;
            setTimeout(function () {
                _runVipChecking();
            }, l);
        };

        this.selectCountry = function (country_id) {
            $('.form .formRow img.selected').removeClass('selected');
            $('.mosaic-block').trigger('mousemove');
            $('#country_' + country_id).addClass('selected');
            $('#city-select').val('');

            _loadCountryData(country_id);
        };

        this.vipCapabilities = function () {
            var vc = $('#vipCapabilities'), cUser = userManager.getCurrentUser();

            vc.find('button.save').unbind('click').on('click', function (evt) {
                var regexp = RegExp(/[ -(a-z A-Z)]/g);
                var color = vc.parent().find('.clr.selected').attr('class').replace(regexp, '');

                $.post('/mvc.php?c=User&do=setSettings', {
                    color: color,
                    visibility: vc.find('input[type="checkbox"]').is(":checked"),
                    user_id: cUser.getUserId(),
                    sid: cUser.sid()
                }, function (data) {

                    if (!data.result) {
                        return false;
                    }

                    userManager.getCurrentUser().update({
                        color: data.params.color,
                        visibility: data.params.visibility
                    });

                    if (leaderManager.isTrueLeader()) {
                        transport.send({
                            action: 'updateCityLeader',
                            module: 'leaderManager',
                            city_id: userManager.getCurrentUser().getCityId()
                        });

                        $('.vipPanel .play-unlimit').addClass('xhidden');
                    }

                    $('.tapeToolTip').tooltip('close');
                    vipCapabilities.dialog('close');
                    _changeUserColor();

                    return true;
                }, 'json');
            });

            vipCapabilities.dialog('open');

            vc.find('.clr').each(function () {
                $(this).unbind('click').on('click', function () {
                    vc.find('.clr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    var index = $(this).attr('class').replace(new RegExp(/[ -(a-z A-Z)]/g), '');
                    vc.find('.preview .avatar')
                        .removeClass(function (index, css) {
                            return (css.match(/clr-b-\d+/g) || []).join(' ');
                        }).addClass('clr-b-' + index);

                    $('#vipCapabilities .circleBase').removeClass(function (index, css) {
                        return (css.match(/clr-b-\d+/g) || []).join(' ');
                    }).addClass('clr-b-' + index);

                    $('.ui-tooltip.tTooltip').removeClass(function (index, css) {
                        return (css.match(/clr-b-\d+/g) || []).join(' ');
                    }).addClass('clr-b-' + index);
                });
            });

            // Add classes for tooltip

            $('span.tapeToolTip')
                .on('mouseleave', function (event) {
                    event.stopImmediatePropagation();
                })
                .addClass('tVip')
                .addClass(userManager.getCurrentUser().getGenderT() + '-avatar');

            // Make tooltip
            g.activeToolTip('tapeToolTip');

            // Open tooltips
            $('span.tapeToolTip').tooltip('open');

            vc.find('.clr' + userManager.getCurrentUser().getSelectedColor()).trigger('click');
        };

        this.switchSound = function () {
            if ($.cookie('noPlay') == "1") {
                $.cookie('noPlay', 0);
            } else {
                $.cookie('noPlay', 1);
            }

            k.soundLabel();
        };

        this.needVip = function () {
            app.addCallback('becomeVip', function () {

                g.getJson('/mvc.php?c=User&do=vkLink', {
                    target_user_id: loadedUser.getUserId()
                }, function (response) {

                    $('.vkLinkFromProfile')
                        .attr('href', response.link)
                        .removeClass('o04')
                        .removeAttr('onclick');

                }, function () {

                });

            });

            context.vip(true);
        };
        /*===============================================================*/
        var _becomeLeader = function (user_id) {
            $.post('/mvc.php?c=User&do=becomeSponsor', {
                user_id: userManager.getCurrentUser().getUserId(),
                sid: userManager.getCurrentUser().sid(),
                target_user_id: user_id
            }, function (data) {
                if (!data.result) {

                    if (data.reason == "no.balance") {
                        userManager.payment();
                    }

                    return;
                }

                transport.send({
                    action: 'becomeSponsor',
                    target_user_id: user_id,
                    patron_id: data.patronId,
                    module: 'userManager'
                });

                _updateSponsorData(user_id);

                becomeLeaderConfirmDialog.dialog('close');
            }, 'json');
        };

        var _kiss = function (user_id, context) {
            var params = {
                target_user_id: user_id
            };

            g.getJson('/mvc.php?c=User&do=kiss', params,
                function (response) {
                    kissConfirmDialog.dialog('close');
                    $(context)
                        .attr('title', 'Вы уже поцеловали')
                        .parent()
                        .addClass('dsbl')
                        .removeClass('o07')
                        .addClass('o05');

                    var k = parseInt($('.ico-kiss-div span').text());
                    if (!k) k = 0;
                    $('.ico-kiss-div span').text(++k);

                    notifManager.sayUserToUpdate(user_id);
                    userManager.updateSelfInfo();
                }, function () {

                }
            );
        };

        var _like = function (user_id) {
            $.post('/mvc.php?c=User&do=like', {
                user_id: userManager.getCurrentUser().getUserId(),
                sid: userManager.getCurrentUser().sid(),
                target_user_id: user_id
            }, function (data) {

                if (!data.result) {
                    if (data.reason == 'no.balance') {
                        userManager.payment();
                    }

                    return false;
                }

                transport.send({
                    action: 'likeUser',
                    likedUserId: user_id,
                    likesCount: data.likes,
                    module: 'userManager'
                });

                likeConfirmDialog.dialog('close');
                userManager.updateSelfInfo();
                _liked();

                return true;

            }, 'json');
        };


        var _updateSponsorData = function (user_id) {
            if (!loadedUser || user_id != loadedUser.getUserId() || !$('#userProfile').dialog('isOpen')) {
                return;
            }

            userManager.updateSelfInfo();

            loadedUser.update({
                cost: loadedUser.cost() + 1
            });

            loadedUser.loadSponsorData(function (user) {
                _showSponsorData(user);
            });
        };

        var _changeUserColor = function () {
            var cUser = userManager.getCurrentUser();

            // Change profile color
            $('.user-profile .avatar').removeClass(function (index, css) {
                return (css.match(/clr-b-\d+/g) || []).join(' ');
            }).addClass('clr-b-' + cUser.getSelectedColor());

            // Change leader color
            $('#leader_' + cUser.getUserId())
                .find('.avatar')
                .removeClass(function (index, css) {
                    return (css.match(/clr-b-\d+/g) || []).join(' ');
                }).addClass('clr-b-' + cUser.getSelectedColor());

            // Change border color in question dialog
            $('#question_dialog img').removeClass(function (index, css) {
                return (css.match(/clr-b-\d+/g) || []).join(' ');
            }).addClass('clr-b-' + cUser.getSelectedColor());
        };

        var _loadCountryData = function (country_id) {
            $('#user-profile-form .city .combobox').attr('data-id', country_id);
        };

        var _liked = function () {
            $('.ico-like-to', $('#userProfile'))
                .parent()
                .addClass('dsbl')
                .removeClass('o07')
                .addClass('o05');
        };

        var _setViewProfile = function (user_id) {
            if (user_id == userManager.getCurrentUser().getUserId() ||
                userManager.getCurrentUser().visibilityMode()) {
                return;
            }

            transport.send({
                action: 'setViewProfile',
                viewed_id: user_id
            });
        };

        var _showViewed = function (data) {
            var tmpl_obj, sTime, api,
                guests = $('#guests'),
                target = $('.view_count'),
                count = parseInt(target.text().trim()),
                user = new userModel(),
                list = $('.list', guests);

            // Make scroll on list
            if (!list.data('jsp')) {
                list.jScrollPane({
                    autoReinitialise: true
                });

                api = list.data('jsp');
            } else {
                api = list.data('jsp');
            }

            // Set view count
            target.text(++count);

            // Set user
            user.setUser(data);

            // Time calculation
            sTime = data['serverTime'].toString().
                replace(/T/, ' ').
                replace(/\..+/, '');

            // Needed object for template
            var d = new Date(g.getDateFromFormat(sTime, 'yyyy-MM-dd HH:mm:ss'));
            tmpl_obj = user.getUser();
            tmpl_obj['time'] = g.formatDate(d, 'HH:mm');
            tmpl_obj['gender'] = user.getGenderT();
            tmpl_obj['end'] = user.getGenderT() == 'male' ? '' : 'а';

            // Append viewed item
            $('#guest_tmpl').tmpl(tmpl_obj).prependTo(api.getContentPane());
            guests.dialog('option', 'title', 'Мои гости (' + count + ')');

            if (!guests.dialog('isOpen')) {
                target.addClass('new');
            }
        };

        var _saveButton = function () {
            var uProfile = $('#userProfile'),
                saveUrl = '/mvc.php?c=User&do=save';

            $('.save-btn', uProfile).on('click', function (evt) {
                $('#cityChangeAttention').removeClass('error').addClass('xhidden');

                evt.preventDefault();

                g.getJson(saveUrl, {
                    data: $('#user-profile-form').serialize()
                }, function (data) {
                    $('.rightSide .no-error')
                        .removeClass('no-error')
                        .addClass('success')
                        .find('p')
                        .text('Изменения успешно сохранены.');

                    setTimeout(function () {
                        $('.rightSide .success').fadeOut(400, function () {
                            $(this)
                                .removeClass('success')
                                .addClass('no-error')
                                .removeAttr('style');
                        });
                    }, 3500);

                    userManager.getCurrentUser().reload();
                    userManager.updateSelfInfo();
                    var user = userManager.getCurrentUser();
                    if (user.isLeader()) {
                        transport.send({
                            action: 'updateCityLeader',
                            module: 'leaderManager',
                            city_id: userManager.getCurrentUser().getCityId()
                        });
                    }

                }, function (data) {
                    $('.rightSide .no-error')
                        .removeClass('no-error')
                        .addClass('error')
                        .find('p')
                        .text(data.reason);

                    setTimeout(function () {
                        $('.rightSide .error').fadeOut(400, function () {
                            $(this)
                                .removeClass('error')
                                .addClass('no-error')
                                .removeAttr('style');
                        });
                    }, 3500);
                });
            });
        };

        var _showSponsorData = function (user, userCount) {
            // jQuery selectors
            var t = $('#userProfile'),
                p = $('.patron-wrap', t),
                r = $('.retinue-wrap', t),
                tmpl_obj = {}, jsp;

            // Patron tmpl object generation
            if (user.getPatron() && user.getPatron().getUserId() != user.getUserId()) {
                tmpl_obj = user.getPatron().getUser();
                tmpl_obj['has_patron'] = 1;
            }
            else {
                tmpl_obj['has_patron'] = 0;
            }

            tmpl_obj['self'] = user.getUserId() == userManager.getCurrentUser().getUserId() ? 1 : 0;
            tmpl_obj['cost'] = g.getBalance(loadedUser.cost() + 1);
            tmpl_obj['rating'] = loadedUser.cost() + 1;
            tmpl_obj['user_id'] = user.getUserId();
            if (user.getPatron()) {
                tmpl_obj['selfPatron'] = user.getPatron().getUserId() == userManager.getCurrentUser().getUserId() ? 1 : 0;
                tmpl_obj['patron_id'] = user.getPatron().getUserId();
            }
            else {
                tmpl_obj['selfPatron'] = 0;
                tmpl_obj['patron_id'] = null;
            }

            // Append patron
            p.empty();
            $('#patron_tmpl')
                .tmpl(tmpl_obj)
                .appendTo(p);

            // Make retinues scrollable
            r.jScrollPane({
                autoReinitialise: true
            });

            jsp = r.data('jsp');

            if (userCount > 0) {
                jsp.getContentPane().empty();
                $('.no-retinue').removeClass('no-retinue');
            } else {
                $('.retinue-wrap').addClass('no-retinue');
            }

            // Append retinues
            $.each(user.getRetinues(), function (i, e) {
                $('#retinue_tmpl')
                    .tmpl(e.getUser())
                    .appendTo(jsp.getContentPane());
            });

            // Activate patron tooltip
            g
                .activeToolTip()
                .activeToolTip('.hasToolTip.pw', {
                    my: "left center",
                    at: "right+10 center",
                    collision: "fit flip"
                })
                .activeToolTip('.hasToolTip.rw', {
                    my: "center top",
                    at: "center top",
                    collision: "fit flip"
                });
        };

        var _fillFormAndInit = function () {
            var sProfile = $('#show_profile');
            var sProfileOnlyVip = $('#show_profile_only_vip');

            $('#name').val(loadedUser.getFullName());
            $('#day').val(loadedUser.bDate('day'));
            $('#month').val(loadedUser.bDate('month'));
            $('#year').val(loadedUser.bDate('year'));

            _saveButton();

            sProfile.checkbox({
                onClick: function () {
                    if (!sProfile.is(':checked')) {
                        sProfileOnlyVip.parent().parent().slideUp();
                    } else {
                        sProfileOnlyVip.parent().parent().slideDown();
                    }
                }
            });
            sProfileOnlyVip.checkbox();

            if (loadedUser.isPublicProfile()) {
                sProfile.trigger('click');
                sProfileOnlyVip.parent().parent().slideUp();
            } else if (loadedUser.isOnlyVip()) {
                sProfileOnlyVip.trigger('click');
                sProfileOnlyVip.parent().parent().slideDown();
            }

            context.selectCountry(loadedUser.getCountryId());

            $('#user_city_id').change(function () {
                var selectedCity = $(this).val(), user = userManager.getCurrentUser();

                if (selectedCity == user.getCityId()) {
                    $('#cityChangeAttention')
                        .removeClass('error')
                        .addClass('xhidden');

                    return;
                }

                if (user.isLeader()) {
                    $('#cityChangeAttention')
                        .removeClass('xhidden')
                        .addClass('error')
                        .find('p')
                        .text('ВНИМАНИЕ! Вы являетесь лидером готода "' + user.getCityName() + '". Если вы измените свой город потеряете лидерство')
                        .unbind('click')
                        .on('click', function () {
                            $('#cityChangeAttention').fadeOut(function () {
                                $(this).removeAttr('style').addClass('xhidden').removeClass('error');
                            });
                        });
                }
            });
        };

        var _runVipChecking = function () {
            userManager.updateSelfInfo();
            notifManager.updateNotif();
        };

        var _sendMoney = function (user_id) {
            if (user_id == userManager.getCurrentUser().getUserId()) {
                return false;
            }

            var count = $('#donateMoney .count').val();
            var params = {
                user_id: userManager.getCurrentUser().getUserId(),
                sid: userManager.getCurrentUser().sid(),
                to_user_id: user_id,
                count: count
            };

            g.getJson('/mvc.php?c=User&do=sendMoney', params,
                function () {
                    userManager.updateSelfInfo();
                    donateMoneyDialog.dialog('close');

                    transport.send({
                        action: 'sendMoney',
                        module: 'userManager',
                        to_user_id: user_id
                    });
                },
                function () {
                }
            );
            return true;
        };

        var _vipSettings = function () {
            var cUser = userManager.getCurrentUser(),
                vc = $('#vipCapabilities'),
                chk = vc.find('input[type="checkbox"]'),
                icoBtn = vc.find('.row:eq(1) .ico');

            if (cUser.visibilityMode())
                chk.attr('checked', 1);
            else {
            }

            vc.find('.clr.selected').removeClass('selected');
            vc.find('.clr' + cUser.getSelectedColor()).addClass('selected');

            icoBtn.unbind('click').on('click', function () {
                chk.trigger('click');
            });

            chk.unbind('change').change(function () {
                if ($(this).is(':checked')) {
                    icoBtn
                        .removeClass('ico-mode-turn-off')
                        .addClass('ico-mode-turn-on');

                    icoBtn.parent().find('span b').text('Вклюден');
                }
                else {
                    icoBtn
                        .removeClass('ico-mode-turn-on')
                        .addClass('ico-mode-turn-off');

                    icoBtn.parent().find('span b').text('Выклюден');
                }
            }).trigger('change');
        }

    };
};

$(function () {
    $(document).on('currentUserReady', function () {
        userManager.init();
    });
});