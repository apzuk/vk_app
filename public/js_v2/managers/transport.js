var sockJsHandler = new function () {
    var context = this,
        sock,
        response = {},
        reconnection = false;

    var _init = function () {
        context.url = 'http://game.vkapp-lubof.ru:8991/broadcast';
        if (!context.url) {
            throw "SockJS url is empty";
        }

        logger.log('Trying to connect');
        sock = new SockJS(context.url);

        sock.onopen = function () {
            logger.log('Socket opened');

            g.setConnectionStatus(true);

            sock.onmessage = function (e) {
                logger.log('Message recived: ' + e.data);
                var json = $.parseJSON(e.data);

                if (json.action) {
                    var a = "if (" + json.module + "['" + json.action + "']) {" + json.module + "['" + json.action + "'](json);}";
                    eval(a);
                }
            };

            sock.onclose = function () {
                _reConnect();
            };

            //initial all components!
            if (!reconnection) {
                main.init();
            }
        };
    };

    var _isConnected = function () {
        return !!((typeof(sock.readyState) != 'undefined' && sock.readyState == 1));
    };

    var _send = function (m) {
        if (!m || typeof(m) != 'object') {
            return false;
        }

        if (!_isConnected()) {
            logger.log('Cant send data, socket is not connected');
            return false;
        }

        logger.log('Trying to send data: ' + JSON.stringify(m));

        response = JSON.stringify(m);
        sock.send(response);
        response = {};
        return true;
    };

    this.stopConnection = function () {

    };

    this.send = function (m) {
        m['user_id'] = userManager.getCurrentUser().getUserId();
        m['sid'] = userManager.getCurrentUser().sid();
        return _send(m);
    };

    _reConnect = function () {
        _init();
        reconnection = true;

        setTimeout(function () {
            if (_isConnected()) {
                return;
            }

            // close all jquery dialogs
            $(".ui-dialog > .ui-dialog-content").dialog("close");

            $('.ui-widget-overlay').remove();
            g.makeBlockUI('Соеденение потеряно...');
            g.setConnectionStatus(false);
        }, 2000);
    };

    $(function () {
        _init();
    })
}();
