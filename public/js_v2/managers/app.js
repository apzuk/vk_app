app = new function () {

    var callbacks = {};

    this.addCallback = function (method, callable) {
        if (!callbacks.hasOwnProperty(method)) {
            callbacks[method] = [];
        }

        callbacks[method].push(callable);

        $(document).unbind(method).on(method, function () {
            callbacks[method].forEach(function (callback) {
                callback();

                callbacks[method].pop();
            });
        });
    };
};

var k = {};

$(function () {
    k.soundLabel();
});

k.soundLabel = function () {
    if ($.cookie('noPlay') == "1") {
        $('footer ul.menu .sound-switcher .sound-label').text('Вкл.');
    } else {
        $('footer ul.menu .sound-switcher .sound-label').text('Выкл.');
    }
};