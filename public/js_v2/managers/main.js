$(function () {

    if (!g.isModern()) {
        $.holdReady();
        return g.showCap();
    }

    $.blockUI({
            message: '<h1>Подключение к серверу...</h1>',
            title: null,
            draggable: true,
            css: {
                border: 'none',
                padding: '25px',
                paddingTop: '28px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '56%',
                top: '45%',
                left: '22%',
                fontSize: 25,
                fontFamily: 'ralev001'
            },

            themedCSS: {
            }
        }
    );

    if (!g.isCanvasSupported()) {
        $('body').empty();
    }
});

var g = {};
g.exchange = null;
g.graphicReady = false;
g.connecttionStatus = false;
g.vkAppUrl = 'http://vk.com/app3761544';
g.isProfileReady = true;
g.isUpdated = false;

g.appUrl = function () {
    return g.vkAppUrl;
};

g.isCanvasSupported = function () {
    var elem = document.createElement('canvas');
    return !!(elem.getContext && elem.getContext('2d'));
};


g.setConnectionStatus = function (status) {
    g.connecttionStatus = status;
    $(document).trigger('socketConnectionReady', ['Custom', 'Event']);
};

g.isConnected = function () {
    return g.connecttionStatus;
};

g.setExchange = function (value) {
    g.exchange = value;
    return g;
};

g.show_continuously_visit = function () {
    var cv = userManager.getCurrentUser().continuouslyVisit(),
        cvDialog = $('#continuously_visit');

    if (cvDialog.length == 0) return;
    cvDialog.dialog({
        autoOpen: false,
        width: 400,
        height: 250,
        resizable: false,
        draggable: false,
        show: {
            effect: "blind",
            duration: 380
        },
        hide: {
            effect: "blind",
            duration: 380
        },
        modal: true,
        close: function () {
            $(document).trigger("tourStart", [ "Custom", "Event" ]);
        }
    });

    cvDialog.styledDialog({
        type: userManager.getCurrentUser().getGenderT(),
        btn: '.close-cv-btn',
        btnLabel: 'Спасибо',
        openDlg: false
    });

    cvDialog.modalDialog(function (dialog) {
        if (cv > 0) {
            $('.days .ico-day', cvDialog)
                .eq(cv - 1)
                .addClass('selected')
        }
    });
    cvDialog.dialog('open');
};

g.loadImages = function () {
    var imgs = $('.app-graphic img'), len = imgs.length, loadedLen = 0, b = false;
    imgs.each(function () {
        b = true;
        $('<img />').load(function () {
            loadedLen++;

            if (len == loadedLen) {
                g.graphicReady = true;
                $(document).trigger('graphicReady', ['Custom', 'Event']);
            }

            var proc = (loadedLen / len) * 100;

            $('#loadedProcentage').html(' ' + parseInt(proc) + '%');
        }).attr('src', $(this).attr('src'));
    });

    if (!b) $(document).trigger('graphicReady', ['Custom', 'Event']);
};

g.get_time_zone_offset = function () {
    var current_date = new Date();
    return (parseInt(-current_date.getTimezoneOffset() / 60) - 4) * 60 * 60 * 1000;
};

g.isModern = function () {
    if (jQuery == undefined || $ == undefined) {
        return false;
    }

    var supported = true;

    for (var feature in Modernizr) {
        if (typeof Modernizr[feature] === "boolean" && Modernizr[feature] == false) {
            supported = false;
            break;
        }
    }

    return supported && !g.isInternetExplorer();
};

g.isInternetExplorer = function () {
    return /MSIE\s([\d.]+)/.test(navigator.userAgent);
};

g.showCap = function () {
    $('.blockUI').remove();
    $('body')
        .empty()
        .addClass('cap')
        .html('<div id="popup"><a class="download-chrome" href="https://www.google.ru/intl/ru/chrome/browser/" target="_blank"></a></div>');

    var popup = $('#popup');
    popup.dialog({
        width: 407,
        height: 287,
        draggable: false,
        resizable: false,
        autoOpen: false,
        closeOnEscape: false,
        open: function () {
            $('<a>')
                .addClass('html5-ico')
                .addClass('ico-no-html-heart-left')
                .appendTo($(this));

            $('<a>')
                .addClass('html5-ico')
                .addClass('ico-no-html-heart-right')
                .appendTo($(this));

            $('<p>')
                .addClass('text')
                .html('К сожалению, ваш браузер не поддерживает хтмл5, либо ваш браузер уже устарел... :(<br /><br /> ...но вы можете поправить это скачав браузер :)')
                .appendTo($(this));

            $('<span>')
                .addClass('html5-ico')
                .addClass('ico-chrome')
                .appendTo($(this));
        }
    });

    popup.styledDialog({type: 'female', btn: '.download-chrome', btnLabel: 'Сказать Google Chrome'});
    popup.dialog('open');
};

g.isGraphicReady = function () {
    return g.graphicReady || !g.isProfileReady;
};

g.updateSelfTimeout = function () {
    setTimeout(function () {
        userManager.updateSelfInfo();
    }, 15 * 60 * 1000)
};

g.optionFormatting = function (text, opt) {
    var newText = text;
    var parts = text.split('***');

    if (parts.length == 0) return false;

    if (typeof(parts[1]) != 'undefined') {
        newText = '<span class="ui-item-header">' + $.trim(parts[0]) + '</span>';

        if ($.trim(parts[1])) {
            newText += '<span class="ui-item-content">' + $.trim(parts[1]) + ', </span>';
        }

        if ($.trim(parts[2])) {
            newText += '<span class="ui-item-content">' + $.trim(parts[2]) + '</span>';
        }
    } else {
        newText = '<span class="ui-item-header">' + $.trim(parts[0]) + '</span>';
    }

    return newText;
};

g.trimMessage = function (str) {
    var regexp = new RegExp(/^(<br>)+|(<br>)+$|(<\/?div>)+/gi);
    return str.replace(regexp, '');
};

g.getExchange = function () {
    return g.exchange;
};

g.makeVk = function (id) {
    VK.Widgets.Group(id, {mode: 2, width: "350", height: "450"}, 57284494);
    VK.Observer.subscribe('widgets.groups.joined', function () {
        $.post('/mvc.php?c=User&do=checkSubscription', {}, function (data) {
        });
    });
};

g.getDateFromFormat = function (val, format) {
    var MONTH_NAMES = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    var DAY_NAMES = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

    function _isInteger(val) {
        var digits = "1234567890";
        for (var i = 0; i < val.length; i++) {
            if (digits.indexOf(val.charAt(i)) == -1) {
                return false;
            }
        }
        return true;
    }

    function _getInt(str, i, minlength, maxlength) {
        for (var x = maxlength; x >= minlength; x--) {
            var token = str.substring(i, i + x);
            if (token.length < minlength) {
                return null;
            }
            if (_isInteger(token)) {
                return token;
            }
        }
        return null;
    }

    val = val + "";
    format = format + "";
    var i_val = 0;
    var i_format = 0;
    var c = "";
    var token = "";
    var token2 = "";
    var x, y;
    var now = new Date();
    var year = now.getYear();
    var month = now.getMonth() + 1;
    var date = 1;
    var hh = now.getHours();
    var mm = now.getMinutes();
    var ss = now.getSeconds();
    var ampm = "";

    while (i_format < format.length) {
        // Get next token from format string
        c = format.charAt(i_format);
        token = "";
        while ((format.charAt(i_format) == c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        // Extract contents of value based on format token
        if (token == "yyyy" || token == "yy" || token == "y") {
            if (token == "yyyy") {
                x = 4;
                y = 4;
            }
            if (token == "yy") {
                x = 2;
                y = 2;
            }
            if (token == "y") {
                x = 2;
                y = 4;
            }
            year = _getInt(val, i_val, x, y);
            if (year == null) {
                return 0;
            }
            i_val += year.length;
            if (year.length == 2) {
                if (year > 70) {
                    year = 1900 + (year - 0);
                }
                else {
                    year = 2000 + (year - 0);
                }
            }
        }
        else if (token == "MMM" || token == "NNN") {
            month = 0;
            for (var i = 0; i < MONTH_NAMES.length; i++) {
                var month_name = MONTH_NAMES[i];
                if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                    if (token == "MMM" || (token == "NNN" && i > 11)) {
                        month = i + 1;
                        if (month > 12) {
                            month -= 12;
                        }
                        i_val += month_name.length;
                        break;
                    }
                }
            }
            if ((month < 1) || (month > 12)) {
                return 0;
            }
        }
        else if (token == "EE" || token == "E") {
            for (var i = 0; i < DAY_NAMES.length; i++) {
                var day_name = DAY_NAMES[i];
                if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                    i_val += day_name.length;
                    break;
                }
            }
        }
        else if (token == "MM" || token == "M") {
            month = _getInt(val, i_val, token.length, 2);
            if (month == null || (month < 1) || (month > 12)) {
                return 0;
            }
            i_val += month.length;
        }
        else if (token == "dd" || token == "d") {
            date = _getInt(val, i_val, token.length, 2);
            if (date == null || (date < 1) || (date > 31)) {
                return 0;
            }
            i_val += date.length;
        }
        else if (token == "hh" || token == "h") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 1) || (hh > 12)) {
                return 0;
            }
            i_val += hh.length;
        }
        else if (token == "HH" || token == "H") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 0) || (hh > 23)) {
                return 0;
            }
            i_val += hh.length;
        }
        else if (token == "KK" || token == "K") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 0) || (hh > 11)) {
                return 0;
            }
            i_val += hh.length;
        }
        else if (token == "kk" || token == "k") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 1) || (hh > 24)) {
                return 0;
            }
            i_val += hh.length;
            hh--;
        }
        else if (token == "mm" || token == "m") {
            mm = _getInt(val, i_val, token.length, 2);
            if (mm == null || (mm < 0) || (mm > 59)) {
                return 0;
            }
            i_val += mm.length;
        }
        else if (token == "ss" || token == "s") {
            ss = _getInt(val, i_val, token.length, 2);
            if (ss == null || (ss < 0) || (ss > 59)) {
                return 0;
            }
            i_val += ss.length;
        }
        else if (token == "a") {
            if (val.substring(i_val, i_val + 2).toLowerCase() == "am") {
                ampm = "AM";
            }
            else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") {
                ampm = "PM";
            }
            else {
                return 0;
            }
            i_val += 2;
        }
        else {
            if (val.substring(i_val, i_val + token.length) != token) {
                return 0;
            }
            else {
                i_val += token.length;
            }
        }
    }
    // If there are any trailing characters left in the value, it doesn't match
    if (i_val != val.length) {
        return 0;
    }
    // Is date valid for month?
    if (month == 2) {
        // Check for leap year
        if (( (year % 4 == 0) && (year % 100 != 0) ) || (year % 400 == 0)) { // leap year
            if (date > 29) {
                return 0;
            }
        }
        else {
            if (date > 28) {
                return 0;
            }
        }
    }
    if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
        if (date > 30) {
            return 0;
        }
    }
    // Correct hours value
    if (hh < 12 && ampm == "PM") {
        hh = hh - 0 + 12;
    }
    else if (hh > 11 && ampm == "AM") {
        hh -= 12;
    }
    var newdate = new Date(year, month - 1, date, hh, mm, ss);
    return (newdate.getTime() + g.get_time_zone_offset());
};

g.getDateWithTimezone = function (create_ts) {
    var d = new Date(g.getDateFromFormat($.trim(create_ts), 'yyyy-MM-dd HH:mm:ss'));

    var monthNames = {
        January: 'Январь',
        February: 'Февраль',
        March: 'Март',
        April: 'Апрель',
        May: 'Май',
        June: 'Июнь',
        July: 'Июль',
        August: 'Август',
        September: 'Сентябрь',
        October: 'Октябрь',
        November: 'Ноябрь',
        December: 'Декабрь'
    };

    var c_ts = g.formatDate(d, 'dd, MMM yyyy HH:mm');

    $.each(monthNames, function (en_name, ru_name) {
        c_ts = c_ts.replace(en_name, ru_name);
    });

    return c_ts;
};

g.formatDate = function (date, format) {
    var MONTH_NAMES = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    var DAY_NAMES = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

    function LZ(x) {
        return(x < 0 || x > 9 ? "" : "0") + x
    }

    format = format + "";
    var result = "";
    var i_format = 0;
    var c = "";
    var token = "";
    var y = date.getYear() + "";
    var M = date.getMonth() + 1;
    var d = date.getDate();
    var E = date.getDay();
    var H = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var yyyy, yy, MMM, MM, dd, hh, h, mm, ss, ampm, HH, H, KK, K, kk, k;
    // Convert real date parts into formatted versions
    var value = new Object();
    if (y.length < 4) {
        y = "" + (y - 0 + 1900);
    }
    value["y"] = "" + y;
    value["yyyy"] = y;
    value["yy"] = y.substring(2, 4);
    value["M"] = M;
    value["MM"] = LZ(M);
    value["MMM"] = MONTH_NAMES[M - 1];
    value["NNN"] = MONTH_NAMES[M + 11];
    value["d"] = d;
    value["dd"] = LZ(d);
    value["E"] = DAY_NAMES[E + 7];
    value["EE"] = DAY_NAMES[E];
    value["H"] = H;
    value["HH"] = LZ(H);
    if (H == 0) {
        value["h"] = 12;
    }
    else if (H > 12) {
        value["h"] = H - 12;
    }
    else {
        value["h"] = H;
    }
    value["hh"] = LZ(value["h"]);
    if (H > 11) {
        value["K"] = H - 12;
    } else {
        value["K"] = H;
    }
    value["k"] = H + 1;
    value["KK"] = LZ(value["K"]);
    value["kk"] = LZ(value["k"]);
    if (H > 11) {
        value["a"] = "PM";
    }
    else {
        value["a"] = "AM";
    }
    value["m"] = m;
    value["mm"] = LZ(m);
    value["s"] = s;
    value["ss"] = LZ(s);
    while (i_format < format.length) {
        c = format.charAt(i_format);
        token = "";
        while ((format.charAt(i_format) == c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (value[token] != null) {
            result = result + value[token];
        }
        else {
            result = result + token;
        }
    }
    return result;
};

g.ready = function () {
    $('.tab2').on('open', function () {
        $('.user-item:first').trigger('click');
    });
};

g.shareWith = function () {
    var share = $('#share-with-friends');
    share.dialog({
        autoOpen: false,
        width: 407,
        height: 419,
        draggable: false,
        resizable: false,
        modal: true
    });
    share.styledDialog({
        type: userManager.getCurrentUser().getGenderT(),
        btn: '.close-share',
        btnLabel: 'Закрыть',
        openDlg: false
    });
    share.modalDialog();
    share.dialog('open');
};

g.friendList = function () {
    $('#share-with-friends').dialog('close');

    var friendList = $('#friends-list');
    friendList.dialog({
        draggable: false,
        resizable: false,
        width: 550,
        height: 400,
        autoOpen: false,
        modal: true,
        closeOnEscape: false
    });
    friendList.styledDialog({
        type: userManager.getCurrentUser().getGenderT(),
        btn: '.close-friends',
        btnLabel: 'Закрыть',
        openDlg: false
    });
    friendList.modalDialog(function (self) {
        $('.friends').jScrollPane({
            autoReinitialise: true
        });

        $('#friends-list').css('padding', '60px 13px 0px 5px');

        $(self).find('.search input[type="text"]').keyup(function () {
            _c();
        }).change(function () {
                _c();
            });

        $(self).find('img.circleBase').attr('src', '/img/friend-dlg-' + userManager.getCurrentUser().getGenderT() + '.png');

        var _c = function () {
            var val = $(this).val().trim();

            $(self).find('.f .friend').each(function () {
                var d = $(this).data('username');
                if (d.indexOf(val) == -1 && val != '') {
                    $(this).addClass('xhidden');
                } else {
                    $(this).removeClass('xhidden');
                }
            });
        }
    }, '/img/friend-dlg-' + userManager.getCurrentUser().getGenderT() + '.png');
    friendList.dialog('open');
};

g.postOnFriendWall = function (user_id, type) {
    g.postOnWall(user_id, type);
};
/* friend-dlg-female.png */
g.postOnWall = function (user_id, type) {
    $('#friends-list .l4').removeClass('xhidden');

    VK.api('photos.getWallUploadServer', {uid: user_id}, function (r) {
        var userParams = {
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        };

        $.post('/mvc.php?c=Game&do=uploadPostPhotoToProfile',
            $.extend({}, r.response, userParams),
            function (response) {
                VK.api('photos.saveWallPhoto', response, function (r) {
                    $('#friends-list .l4').addClass('xhidden');

                    var media_object_id = r.response[0].id,
                        msg = 'Друзья приглашаю Вас в новую замечательную игру.Переходи по ссылке и получи подарок от меня!\n';

                    if (user_id != userParams.user_id) {
                        msg = 'Товарищ! Приглашаю тебя в замечательную игру!\n'
                    }

                    VK.api('wall.post', {
                        message: msg + userManager.getCurrentUser().vkUrlFromWall(parseInt(type)),
                        attachments: media_object_id,
                        owner_id: user_id
                    }, function (r) {
                        if (r.error) {
                            return false;
                        }

                        g.logShare('myWall');
                        $('#share-with-friends').dialog('close');

                        return true;
                    });
                })

            }, 'json')
    });
};

g.logShare = function (type) {
    // TODO: log sharing
};

g.showInviteBox = function () {
    VK.callMethod('showInviteBox');
};

g.startTour = function () {
    if (!g.takeTour()) {
        return false;
    }

    var tourSubmitFunc = function (e, v, m, f) {
            if (v === -1) {
                $.prompt.prevState();
                return false;
            }
            else if (v === 1) {
                $.prompt.nextState();
                return false;
            }

            return true;
        },
        tourStates = [
            {
                title: 'Добро пожаловать',
                html: 'Первым что следовала бы вам сделать - написать интересный вопрос к участникам!',
                buttons: { Далее: 1 },
                focus: 0,
                position: { container: '#question_dialog textarea', x: -20, y: 110, width: 300, arrow: 'tc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Задать вопрос',
                html: 'Нажмите на эту кнопку и ждите нафиг!',
                buttons: { Назад: -1, Далее: 1 },
                focus: 0,
                position: { container: '#question_dialog textarea', x: -80, y: 200, width: 300, arrow: 'tc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Лидер города',
                html: leaderManager.isTrueLeader() ? 'Познакомьтесь это лидер ввашего города, четкий посан колоч' :
                    'Тут сидят лидеры города , можете попробовать себя в этой роли',
                buttons: { Назад: -1, Далее: 1 },
                focus: 0,
                position: { container: '.leader', x: 150, y: 40, width: 350, arrow: 'lt' },
                submit: tourSubmitFunc
            },
            {
                title: 'Сообщения',
                html: 'Тут находятся все ваши сообщения! Будьте бдительный к данной вкладке',
                buttons: { Назад: -1, Далее: 1 },
                focus: 0,
                position: { container: '.menu', x: 130, y: 120, width: 300, arrow: 'tc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Уведомления',
                html: 'В этой вкладке пероидический появятся уведомления для Вас!',
                buttons: { Назад: -1, Далее: 1 },
                focus: 0,
                position: { container: '.menu', x: 300, y: 120, width: 300, arrow: 'tc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Лента',
                html: 'Это лента пользователей где можно высказать свои мысли либо предлажения!',
                buttons: { Назад: -1, Далее: 1 },
                focus: 0,
                position: { container: '.user-list', x: 210, y: -180, width: 300, arrow: 'bc' },
                submit: tourSubmitFunc
            },
            {
                title: 'Поделиться',
                html: 'Отсюдо можно поделиться с друзьями и заработать неплохие эньюшки',
                buttons: { Назад: -1, Понятно: 0 },
                focus: 0,
                position: { container: 'footer', x: 330, y: -170, width: 300, arrow: 'br' },
                submit: function (e, v, m, f) {
                    $.cookie('mainPageTourShown', 1);

                    alert($.cookie('mainPageTourShown'));
                }
            }
        ];

    var t = $.cookie('tourShownCount');
    if (!t) t = 0;
    $.cookie('tourShownCount', ++t);

    return $.prompt(tourStates);
};

g.takeTour = function () {
    return ($.cookie('mainPageTourShown') == undefined || $.cookie('mainPageTourShown') == "0") &&
        ($.cookie('tourShownCount') == undefined || $.cookie('tourShownCount') < 3);
};

g.make = function () {
    $('.combobox').combobox();
    g.activeToolTip();

    $('.date').datepicker({
        dateFormat: 'yy-mm-dd'
    });
};

g.showConversations = function () {
    $('.user-item').each(function (i, e) {
        $(e).unbind('click').on('click', function (evt) {
            var security_id = $(this).attr('data-id');
            var user_id = parseInt($(this).attr('data-user_id'));

            G_M.selectUser(user_id);
            G_M.showConversation(security_id, user_id);
        });
    });
};

g.getBalance = function (balance) {
    return balance * g.getExchange();
};

g.activeToolTip = function (selector, position) {

    if (!selector) selector = 'hasToolTip';
    if (!position) {
        position = {
            my: "center top+23", at: "center center"
        };
    }

    switch (selector) {
        case 'hasToolTip':
            $('.hasToolTip').tooltip({
                    position: position,
                    show: false
                }
            );
            break;
        case 'tapeToolTip':
            $('.tapeToolTip').each(function () {
                var u = userManager.getCurrentUser(), classes = $(this).attr('class').replace('-avatar', '');
                classes = classes.replace('tapeToolTip', '');

                $(this).tooltip({
                    position: {
                        at: 'center top',
                        my: 'center bottom-21',
                        of: $(this)
                    },
                    tooltipClass: classes + ' tTooltip'
                });

                $(this).on("tooltipopen", function (event, ui) {
                    var left = $(ui.tooltip).width() / 2 - 12;
                    $(ui.tooltip).find('.ui-tooltip-arrow').css({
                        left: left
                    });
                });
            });

            $('.tTooltip .ui-tooltip-bg').remove();
            break;
        default:
            $(selector).each(function () {
                $(this).tooltip({
                    position: position
                });
            });
    }

    return g;
};

g.initHeader = function () {
    $('header ul li:eq(1)')
        .on('gameTabItem2blur', function () {

        })
        .on('gameTabItem2click', function () {
            if ($('.left-side .user-item.selected').length == 0) {
                $('.left-side .user-item:first').trigger('click');
            }
        });

    $('header').gameTab({
        prefix: 'game'
    });
};

g.bingo = function () {
    var bResult = $('#bingo_result').dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        closeOnEscape: false,
        width: 450,
        height: 250,
        show: {
            effect: "blind",
            duration: 380
        },
        hide: {
            effect: "blind",
            duration: 380
        },
        modal: true,
        open: function () {
        },
        close: function () {
        }
    });
};

g.makeBlockUI = function (txt) {
    $.blockUI({
            message: '<h1>' + txt + ' <span id="loadedProcentage"></span></h1>',
            title: null,
            draggable: true,
            css: {
                border: 'none',
                padding: '25px',
                paddingTop: '28px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '56%',
                top: '45%',
                left: '22%',
                fontSize: 25,
                fontFamily: 'Segoe_UI',
                zIndex: 10110000
            },

            themedCSS: {
            }
        }
    );
};

g.getCurrencyLabel = function () {
    return 'валюта';
};

g.showVkPaymentVotes = function (votes) {
    var params = {
        type: 'votes',
        votes: votes
    };
    VK.callMethod('showOrderBox', params);

    VK.addCallback('onOrderSuccess', function (order_id) {
        $('#purse').dialog('close');

        userManager.updateSelfInfo();
    });

};

g.showVkPaymentOffers = function () {
    var params = {
        type: 'offers'
    };
    VK.callMethod('showOrderBox', params);
};

g.getJson = function (url, params, success_callback, error_callback) {
    var p = $.extend({}, params, {
        user_id: userManager.getCurrentUser().getUserId(),
        sid: userManager.getCurrentUser().sid()
    });

    $.post(url, p, function (data) {
        if (typeof data == undefined) {
            if (error_callback) {
                error_callback();
            }
            return false;
        }

        if (data.reason == 'no.balance') {
            userManager.payment();
            return;
        }

        if (data.reason == 'invalid.user') {
            //   window.location.reload();
            $('body').empty();
            alert('ВНИМАНИЕ! Произошла ошибка в идентификации, пожалуйста перезагружайте страицу')
        }

        if (data.hasOwnProperty('result') && !data.result && error_callback) {
            return error_callback(data);
        }

        success_callback(data);

        return true;
    }, 'json')
};

g.startRandomUser = function (chosen) {

    var randomUser = $('.random-user');

    randomUser.find('.l4').removeClass('xhidden');
    $('.g14').addClass('xhidden');

    g.getJson('/mvc.php?c=User&do=randomUser', {
        chosen_user_id: randomUser.data('user_id'),
        chosen: chosen
    }, function (response) {
        var user = new userModel(response.user);

        randomUser.find('.l4').addClass('xhidden');
        randomUser.data('user_id', user.getUserId());
        randomUser.find('.g14').removeClass('xhidden');
        randomUser
            .find('.avatar')
            .attr('src', user.getAvatar());
        randomUser.find('.r-user-avatar')
            .removeAttr('class')
            .addClass('r-user-avatar')
            .addClass(userManager.getCurrentUser().getGenderF() + '-avatar')
            .addClass(user.getSelectedColor() ? 'clr-b-' + user.getSelectedColor() : '')
            .makeVip(user);

        randomUser
            .find('.' + userManager.getCurrentUser().getGenderT() + '-vip')
            .removeClass(userManager.getCurrentUser().getGenderT() + '-vip')
            .addClass(userManager.getCurrentUser().getGenderF() + '-vip');

        var cityName = user.getCityName(), g17 = user.getAge();
        if (g17 && cityName) {
            g17 = g17 + ", ";
        }
        g17 += user.getFullName('fname');

        randomUser.find('.info-bar .name').text(g17);
        randomUser.find('.info-bar .age-city').text(cityName);


        //randomUser.find('.info-bar .age-city').text(user.getFullName());

        $('.random-user input[name="chosen"]:checked').prop('checked', false);

        $('#setChosen').unbind('click').on('click', function (e) {
            var c = $('.random-user input[name=chosen]:checked').val();
            if (!c) {
                return;
            }

            g.startRandomUser(user.getUserId());
            notifManager.sayUserToUpdate(user.getUserId());
        });
        $('#nextUser').unbind('click').on('click', function (e) {
            g.startRandomUser(user.getUserId());
        });

    }, function () {
        //TODO: no random user
    });
};

main = new function () {
    var transport = sockJsHandler;
    var context = this;

    this.init = function () {
        if (!g.isModern()) {
            return;
        }

        g.makeBlockUI('Загрузка компонентов...');

        // g.graphicReady = true;
        if (g.isGraphicReady()) {
            _init();
        } else {
            $(document).on('graphicReady', function () {
                _init();
            });
        }

        function _init() {
            $.unblockUI();
            if (!g.isProfileReady) {
                return;
            }

            $.unblockUI();
            g.initHeader();
            g.bingo();
            g.activeToolTip();
            g.activeToolTip('tapeToolTip');
            g.make();
            g.show_continuously_visit();

            // run init-s
            ribbonManager.init();
            leaderManager.init();
            notifManager.initManager();
            game.auth();
            //  onlineManager.init();

            if (!g.isUpdated) {
                $(document).trigger("tourStart", [ "Custom", "Event" ]);
            }


            g.startRandomUser();
            g.updateSelfTimeout();

            context.startScreenShot();
            $('#question_dialog').questionDialog();
        }
    };

    this.screenShot = function (data) {
        //==============UPDATE RIBBON===============
        var ribbonData = {};
        var ribbons = data['ribbon'];
        $.each(ribbons, function (key, ribbon) {
            var u = new userModel();
            u.setUser(ribbon);

            ribbonData[ribbon.ribbon_id] = {
                user: u,
                ribbon_id: ribbon.ribbon_id,
                comment: ribbon.comment
            };
        });
        ribbonManager.set(ribbonData);

        //============UPDATE GAME INFO===============
        $('#question_dialog').questionDialog({call: '_setGameParams', params: {
            maleCount: data["maleCount"],
            femaleCount: data["femaleCount"]
        }});

        //============UPDATE CITY LEADER============
        var leader = data['leader'];
        leader["bid"] = leader["current_bid"];
        leaderManager.setLeader(leader);
    };

    this.startScreenShot = function () {
        var i = setInterval(function () {
            // requestScreenShot();
        }, 1000 * 10);
        requestScreenShot();

        function requestScreenShot() {
            if (game.isPlaying()) {
                return false;
            }

            transport.send({
                action: "getScreenShot",
                user_id: userManager.getCurrentUser().getUserId(),
                city_id: userManager.getCurrentUser().getCityId(),
                callbackModule: 'main'
            });

            return true;
        }
    }
};

ajaxIndicator = new function () {

    if (typeof arguments.callee.instance == 'undefined') {

        arguments.callee.instance = new function () {

            var context = this;
            var indicatorDiv, showInNextTime = true, show = true;

            this.init = function () {

                var id = 'ajax-indication-' + Math.round(Math.random() * 100000);

                indicatorDiv =
                    $('<div />', {
                        id: id
                    })
                        .css({
                            position: "fixed",
                            left: calcLeft(),
                            bottom: 9,
                            height: 19,
                            width: 220,
                            zIndex: 99,
                            background: "url(/img/loading4.gif)"
                        })
                        .hide()
                        .appendTo($('footer'));

                $(document).bind('ajaxStart.ajaxIndicator', ajaxStart);
                $(document).bind('ajaxStop.ajaxIndicator', ajaxStop);

                $(window).resize(function () {
                    indicatorDiv.css('left', calcLeft());
                });

                function calcLeft() {
                    return 5;
                }
            };

            this.destroy = function () {
                indicatorDiv.remove();
                $(document).unbind('ajaxStart.ajaxIndicator');
                $(document).unbind('ajaxStop.ajaxIndicator');
            };

            /*
             * Call this method and the indicator will not be showed in next ajax request
             */
            this.dontShowNextTime = function () { //
                showInNextTime = false;
            };

            /*
             * Revert action of this.dontShowNextTime() method
             */
            this.showNextTime = function () { //
                showInNextTime = true;
            };

            /*
             * Method just hide indicator GIF but all triggers will work
             */
            this.enable = function () {
                showInNextTime = show = true;
            };

            /*
             * Revert action of this.enable() method
             */
            this.disable = function () {
                showInNextTime = show = false;
            };

            var ajaxStart = function () {
                showIndicator();
            };

            var ajaxStop = function () {
                hideIndicator();
            };

            var showIndicator = function () {
                if (showInNextTime === true) {
                    // indicatorDiv.css('top', ($(document).scrollTop()+40));
                    indicatorDiv.show();
                }

                showInNextTime = show;
            };

            var hideIndicator = function () {
                indicatorDiv.hide();
            };

            $(function () {
                $(document).on('graphicReady', function () {
                    context.init();
                });
            })
        };
    }

    return arguments.callee.instance;

}();