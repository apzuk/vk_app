ribbonManager = new function () {

    var limit = 20, transport = sockJsHandler, context = this, ribbon, isFirst = true;

    this.init = function () {
        _initDialog();
        _initTextarea();

        $('.set-btn').on('click', function (evt) {
            ribbon.dialog('open');
        });
    };

    this.set = function (ribbonItems) {
        var array = $.map(ribbonItems,function (value, index) {
            return [value];
        }).reverse();

        _set(array, function () {
            g.activeToolTip('tapeToolTip');
        });

        $('.user:eq(' + limit + ')').nextAll().remove();
    };

    //=============================================================================================
    var _set = function (ribbonItems, callback) {
        var ribbon = ribbonItems.pop();

        _to(ribbon, function () {
            if (ribbonItems.length == 0) {
                if (callback) callback();
                /*
                 if ($('.game-tab1').is(':visible')) {
                 $('#tape .user:eq(0)').tooltip('open');
                 }

                 setTimeout(function () {
                 $('#tape .user:eq(0)').tooltip('close');
                 }, 3000);
                 */
                return false;
            }

            _set(ribbonItems, callback);

            return true;
        });
    };

    var _to = function (ribbon, callback) {
        if (ribbon == undefined) return;

        var all = $('.user-list .all');

        if ($('#rUser-' + ribbon.ribbon_id).length != 0) {
            callback();
            return;
        }

        $('<img />').load(function () {

            if ($('#rUser-' + ribbon.ribbon_id).length != 0) {
                return;
            }

            var hover = $('<div></div>');

            hover.addClass('hover');
            hover.addClass(ribbon.user.getGenderT());
            if (ribbon.user.isVip()) {
                hover.addClass(ribbon.user.getSelectedColor() ? 'clr-b-' + ribbon.user.getSelectedColor() + '-color' : '')
            } else {
                hover.addClass(ribbon.user.getGenderT() + '-bcolor');
            }

            $('<div>')
                .css('margin-left', '-90px')
                .addClass('user')
                .addClass(ribbon.user.getGenderT() + '-avatar')
                .addClass(ribbon.user.getSelectedColor() ? 'clr-b-' + ribbon.user.getSelectedColor() : '')
                .addClass(ribbon.user.getUserId() == userManager.getCurrentUser().getUserId() ? 'me' : 'm')
                .addClass('h')
                .attr('title', ribbon.comment)
                .addClass('tapeToolTip')
                .addClass(ribbon.user.getSelectedColor() ? 'tVip' : '')
                .attr('id', 'rUser-' + ribbon.ribbon_id)
                .append($('<img />').attr('src', ribbon.user.getOriginalAvatar(80)))
                .append(hover)
                .click(function () {
                    userManager.showProfile(ribbon.user.getUserId());
                })
                .prependTo(all);

            all.find('div:first').makeVip(ribbon.user).animate({
                marginLeft: 0
            }, 500, function () {
                callback();
            });
        }).attr('src', ribbon.user.getOriginalAvatar(80));
    };

    var _toTape = function () {
        $.post('/mvc.php?c=User&do=toRibbon', {
            comment: $('textarea', ribbon).val(),
            user_id: userManager.getCurrentUser().getUserId()
        }, function (data) {

            if (data.result) {
                var rItem = {
                    ribbon_id: data.id,
                    comment: $('textarea', ribbon).val(),
                    user: userManager.getCurrentUser()
                };

                transport.send({
                    action: 'update',
                    module: 'ribbonManager'
                });

                ribbon.dialog('close');
                context.set([rItem]);

                userManager.updateSelfInfo();
            }

            else {

                switch (data.reason) {
                    case 'empty.content':
                        $('textarea', ribbon).focus();
                        break;
                    default :
                        _vkPayment();
                }
            }

        }, 'json');
    };

    var _vkPayment = function () {
        userManager.payment();
    };

    var _initDialog = function () {
        var rbn = $('#ribbon');

        ribbon = rbn.dialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            width: 350,
            height: 240,
            show: {
                effect: "blind",
                duration: 380
            },
            hide: {
                effect: "blind"
            },
            modal: true,
            open: function () {
                $('#to_ribbon_tmpl').tmpl({
                    cost: g.getBalance(2),
                    currency_label: g.getCurrencyLabel()
                }).appendTo($(this).empty());

                $(this).find('textarea').limit(140, '#ribbon .charsLeft');
            },
            close: function () {
                $(this).find('textarea').trigger('blur');
            },

            buttons: {
                Сказать: function () {
                    _toTape();
                },
                Зкрыть: function () {
                    $(this).dialog('close');
                }
            }
        });

        //rbn.parent().find('.ui-dialog-buttonset a:first').hide();
    };

    var _initTextarea = function () {
        var ta = $('textarea', ribbon);

        ta.on('keyup', function (evt) {
            var regExp = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/;
        });
    }
};