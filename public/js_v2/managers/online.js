onlineManager = new function () {

    var transport = sockJsHandler;
    var context = this;
    var interval;

    this.init = function () {
        $('#tagCloud1, #tagCloud2').tagcanvas({
            textColour: '#000000',
            outlineColour: '#000000',
            maxSpeed: 0.003,
            minSpeed: 0.003,
            depth: 0.1,
            zoom: 1.2,
            wheelZoom: false,
            initial: [0, 0.7],
            dragThreshold: 5,
            shape: "hcylinder",
            reverse: true,
            lock: "x",
            freezeActive: false,
            dragControl: true,
            outlineOffset: 5
        });

        _call();

        interval = setInterval(function () {
            if (!$('.start-page').is(':visible')) {
                return;
            }

            _call();
        }, 1000 * 60);
    };

    var _call = function () {
        g.getJson('/mvc.php?do=getRandomUsers&c=User', {},
            function (users) {
                context.setOnlineUsers(users);
            }, function () {

            }
        );
    };

    this.setOnlineUsers = function (data) {
        _onUsersLoaded(data.users);

        $(document).on('userAvatarsLoaded', function (event) {
            _appendUsers(data.users);
        });
    };

    //-----------------------------------------------------------------------------------------------------------
    _appendUsers = function (users) {
        var ul1 = $('#tagCloud1 ul'), ul2 = $('#tagCloud2 ul');

        ul1.empty();
        ul2.empty();

        $(users).each(function (key, value) {
            var u = new userModel(value);

            value['class_name'] = u.getColorHex();

            if (key < 15) {
                $('#online_user_tmpl').tmpl(value).appendTo(ul1);
            }

            if (key >= 15 && key < 30) {
                $('#online_user_tmpl').tmpl(value).appendTo(ul2);
            }
        });

        $('#tagCloud1, #tagCloud2').tagcanvas("reload");
    };

    _onUsersLoaded = function (users) {
        var len = users.length, lenLoaded = 0;

        $(users).each(function (key, value) {
            var avatar = value.avatar;

            $('<img />').load(function () {
                if (++lenLoaded >= len - 2) {
                    $(document).trigger('userAvatarsLoaded', ['Custom', 'Event']);
                }
            }).attr('src', '/data/avatars/100/' + avatar);
        });
    }
};