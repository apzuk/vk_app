messageManager = new function () {

    /**
     * Socket transport
     * @type {sockJsHandler|*}
     */
    var transport = sockJsHandler;

    /**
     * Dialogs list
     */
    var messageBoxList;

    /**
     * Left side jQuery selector
     */
    var leftSide;

    /**
     * Game tab 2 jQuery selectors
     */
    var gameTab2;

    /**
     * messageManager object
     * @type {messageManager}
     */
    var context = this;

    /**
     * Message textarea jQuery selectors
     */
    var messageBox;

    /**
     * Top position scroll for start loading
     */
    var loadMessagesTop;

    /**
     * jQuery dialog
     */
    var attachImageDialog;

    /**
     * jQuery dialog
     */
    var takePhotoDialog;

    /**
     * jQuery dialog
     */
    var msgGraphicDialog;

    /**
     * Attached files to message
     * @type {{}}
     */
    var attachment = {};

    this.initial = function () {
        // Jquery selectors
        gameTab2 = $('.game-tab2');
        messageBoxList = $('.messages .message-box-list', gameTab2);
        leftSide = $('.left-side', gameTab2);
        messageBox = $('#message');

        // Constants
        loadMessagesTop = 50;

        // Animate left side .num-s
        animator.animate($('.num', leftSide));

        // Initial emojics
        emojic.initial();

        //active left side scroll
        leftSide.jScrollPane({
            autoReinitialise: true
        });

        /**
         * Dialog initial
         */
        var imageAttach = $('#attachImage');
        attachImageDialog = imageAttach.dialog({
            autoOpen: false,
            width: 407,
            height: 315,
            modal: true,
            draggable: false,
            resizable: false,
            closeOnEscape: false
        });

        imageAttach.styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            btn: '.close-photo-take',
            btnLabel: 'Закрыть',
            openDlg: false
        });

        imageAttach.modalDialog(function () {
            var g = userManager.getCurrentUser().getGenderT() ,
                color = userManager.getCurrentUser().getSelectedColor();

            imageAttach
                .find('.me-avatar')
                .attr('src', '/img/camera-' + g + '.png')
                .removeClass(g + '-avatar');

            $('.photo-from-pk')
                .unbind('click')
                .on('click', function () {
                    if ($('#photoAttach').attr('accept') != "images/*") {
                        $('#photoAttach').attr('accept', 'images/*');
                    }

                    $('#photoAttach').trigger('click');
                });
        });

        takePhotoDialog = $('#takePhoto').dialog({
            autoOpen: false,
            closeOnEscape: false,
            modal: true,
            draggable: false,
            resizable: false,
            width: 540,
            height: 450
        });

        $('#takePhoto').styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btns: [
                {
                    btn: '.close-tp',
                    btnLabel: 'Закрыть'
                },
                {
                    btn: '.take-screen',
                    btnLabel: 'Сделать снимок'
                }
            ]
        });

        $('#takePhoto').modalDialog(function (dlg) {
            var g = userManager.getCurrentUser().getGenderT() ,
                color = userManager.getCurrentUser().getSelectedColor();

            $(dlg)
                .find('.me-avatar')
                .attr('src', '/img/camera-' + g + '.png')
                .removeClass(g + '-avatar');

            context.turnCam();
        });

        $('#captureContainer').dialog({
            autoOpen: false,
            closeOnEscape: false,
            modal: true,
            draggable: false,
            resizable: false,
            width: 540,
            height: 450
        });

        $('#captureContainer').styledDialog({
            type: userManager.getCurrentUser().getGenderT(),
            openDlg: false,
            btns: [
                {
                    btn: '.close-a-image',
                    btnLabel: 'Закрыть'
                },
                {
                    btn: '.attach-image',
                    btnLabel: 'Прикрепить снимок'
                }
            ]
        });

        $('#captureContainer').modalDialog(function (dlg) {
            var g = userManager.getCurrentUser().getGenderT() ,
                color = userManager.getCurrentUser().getSelectedColor();

            $(dlg)
                .find('.me-avatar')
                .attr('src', '/img/camera-' + g + '.png')
                .removeClass(g + '-avatar');
        });

        msgGraphicDialog = $('#msgGraphic').dialog({
            autoOpen: false,
            closeOnEscape: false,
            modal: true,
            draggable: false,
            resizable: false,
            width: 650,
            height: 500,
            title: "Мои сообщения"
        });

        _initButtons();
    };

    this.selectDialog = function (user_id, context) {
        $('.myBlocked.active a').removeAttr('onclick');
        if ($(context).data('meBlocked')) {
            _enabledMessaheBox(false, 1);
            $('#sendMessage').unbind('click');
        }
        else if ($(context).data('myBlocked')) {
            _enabledMessaheBox(false, 2);
            $('#sendMessage').unbind('click');

            $('.myBlocked.active a').attr('onclick', 'userManager.unblockUser(' + user_id + ')');
        }
        else {
            _enabledMessaheBox(true);
        }

        // Make sure message tab selected
        _selectMessageTab();

        // Set selected item
        _setLeftSideItemSelected(context);

        // Selected dialog
        var dialog = $('#dialog_' + user_id);

        // Set focus to message box
        messageBox.focus();

        // Hide label
        $('.select-conv-label').hide();

        // Load messages for dialog
        if (!dialog.data('mLoaded')) {
            transport.send({
                u_id: user_id,
                action: 'getConversation',
                module: 'Messanger',
                page: 0
            });
        }
        else {
            _showDialog(user_id);
        }
    };

    this.writeToUser = function (user_id) {
        var uProfile = $('#userProfile');

        if (uProfile.data('lock-' + user_id)) {
            return;
        }

        //close all available dialogs
        uProfile.dialog('close');

        // Lock function for this user while _createLeftSide
        uProfile.data('lock-' + user_id, true);

        // Switch tab
        var tab2Item = $('header .menu li:eq(1)', $('#wrapper'));
        tab2Item.trigger('click');

        // Create dialog div if not exists
        _createDialog(user_id);

        // Create left side item
        _createLeftSide(user_id, function () {
            // Set focus to message box to write text
            messageBox.focus();

            // Activate send message btn
            _activateMessageSend(user_id);

            //unlock
            uProfile.data('lock-' + user_id, false);
        }, true);
    };

    this.messageShow = function (data) {
        // Create dialog div if not exists
        _createDialog(data.u_id);

        // Create left side item
        _createLeftSide(data.u_id, function () {
            // Append message to box
            _setMessages(data);
        }, false);
    };

    this.makeASRead = function (msg_id, receiver_user_id, sender_user_id, context) {
        // Has item unread status?!
        var classes = $(context).attr('class');
        if (classes.indexOf('unread') == -1 ||
            sender_user_id == userManager.getCurrentUser().getUserId()) {
            return;
        }

        // If already sent request, just ignore
        if ($(context).data('process')) {
            return;
        }

        // Busy
        $(context).data('process', 1);

        // Send request
        transport.send({
            id: msg_id,
            receiver_user_id: receiver_user_id,
            sender_user_id: sender_user_id,
            action: 'makeAsRead',
            module: 'Messanger'
        });
    };

    this.changeMessageStatus = function (data) {
        // Remove unread status
        $('#message_' + data.id)
            .removeClass('male-unread')
            .removeClass('female-unread');

        _downgradeBadge(data.with_user_id);
    };

    this.messagePrepend = function (data) {
        // Select dialog
        var dialog = $('#dialog_' + data.u_id);

        // Hide loading
        //_hideLoading(data.u_id);
        $(document).trigger('ajaxStop.ajaxIndicator');

        // Set current page
        dialog.data('page', data.page);

        // Set all messages loaded
        if (data.messages.length == 0) {
            dialog.data('page', -1);
        }

        // Set loaded messages
        var api = dialog.find('.d').data('jsp');
        $.each(data.messages, function (i, e) {
            var msg = new messageModel();
            msg.setMessage(e);

            var msgView = $('#msg_tmpl').tmpl(msg.getMessageView());
            var html = msgView.wrap('<div>').parent().html();

            api.getContentPane().prepend(html);
        });

        _reinitialiseScroll(data.u_id);

        // Set free status
        dialog.data('loadingInProcess', 0);
    };

    this.deleteMessage = function (message_id) {
        $('#confirmMessageRemoval').dialog({
            autoOpen: true,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 420,
            height: 'auto',
            buttons: {
                'Да': function () {
                    transport.send({
                        action: 'removeMessage',
                        module: 'Messanger',
                        message_id: message_id
                    });

                    $(this).dialog('close');
                },
                'Нет': function () {
                    $(this).dialog('close');
                }
            }
        });
    };

    this.messageRemoved = function (data) {
        $('#message_' + data.message_id).each(function (i, e) {
            $(e).fadeOut('normal', function () {
                var user_id = $('.item.selected', leftSide).attr('data-user-id');

                _reinitialiseScroll(user_id);
                $(this).remove();
            });
        });
    };

    this.attachImage = function () {
        if ($('.left-side .item.selected').length == 0) {
            return;
        }

        if (!userManager.getCurrentUser().isVip()) {
            userManager.vip();
            return;
        }

        if ($('.img-container').length > 0) {
            return;
        }

        if ($('#photoAttach').attr('accept') != "image/*") {
            $('#photoAttach').attr('accept', 'images/*');
        }

        // Open dialog
        attachImageDialog.dialog('open');

        $('#photoAttach').change(function () {
            var file = this.files ? this.files[0] : null;

            if (file) {
                if (!_validateSelectedFile(this.files[0])) {
                    return;
                }
            }
            $('#attachImage .loading').removeClass('xhidden');

            if (file) {
                $('#attachImage label').text(file.name);
            }

            $('#msgPhotoForm').submit(function () {
                $('#photoIFrame').attr('src', $(this).attr('action'));
            }).submit();
        });
    };

    this.photoUploaded = function (response, isError) {
        // Close dialog
        attachImageDialog.dialog('close');

        if (isError) {
            _setFileUploadError(response);
            return;
        } else {
            _setFileUploadError(false);
        }

        attachment = {1: response};

        $('#attachImage .loading').addClass('xhidden');

        $('.ico-photo-attach').addClass('ui-state-disabled');

        // Uploaded image
        var img = $('<img/>')
            .load(function () {
                _imgLoaded(this);
            })
            .attr('src', '/data/msg/cache/' + response);

        function _imgLoaded(image) {
            var ico = $('<a>')
                .attr('href', '#')
                .attr('onclick', 'messageManager.removeAttachment(this); return false;')
                .addClass('ico')
                .addClass('ico-img-close');

            // Append as html
            $('#msg-image')
                .append($('<div class="img-container">'))
                .find('div:last')
                .append(img);
            // ;

            setTimeout(function () {
                var width = $('.img-container').width();
                logger.log('width =' + width);
                $('.img-container')
                    .width(width + 20)
                    .append(ico);

                //$('.img-container').width($('.img-container').width() + 21);
                $('.ico-img-close').css({
                    right: -2,
                    top: -1,
                    position: 'absolute'
                });

            }, 150);

            $('.ico-photo')
                .addClass('ui-state-disabled');
        }
    };

    this.removeAttachment = function (obj) {
        // Remove image
        $(obj).parent().remove();

        // Empty attachment
        attachment = {};

        $('.ico-photo-attach').removeClass('ui-state-disabled');
    };

    this.showGraphic = function (img) {
        $('<img />')
            .attr("src", $(img).attr("src"))
            .load(function () {
                var image = this;
                msgGraphicDialog.dialog({
                    width: image.width + 40,
                    height: image.height + 84,
                    open: function () {
                        $(this).html(image);
                    }
                }).dialog('open');
            });
    };

    this.showCaptureDialog = function () {
        takePhotoDialog.dialog('open');
    };

    this.turnCam = function () {
        // Grab elements, create settings, etc.
        var canvas = document.getElementById("capturedImage"),
            c = canvas.getContext("2d"),
            video = document.getElementById("video"),
            videoCont = $('#videoCanvasContainer'),
            captureCont = $('#captureCanvasContainer'),
            s,
            videoObj = { "video": true },
            errBack = function (error) {
                $('<img />')
                    .attr('src', '/img/no-camera-' + userManager.getCurrentUser().getGenderT() + '.png')
                    .addClass('no-camera')
                    .appendTo($('#videoCanvasContainer').empty());

                $('<p>')
                    .addClass('no-cam-label')
                    .text('Камера не найдена')
                    .prependTo($('#takePhoto'));

                $('.take-screen')
                    .unbind('click')
                    .addClass('ui-state-disabled');
            };

        // Put video listeners into place
        if (navigator.getUserMedia) { // Standard
            navigator.getUserMedia(videoObj, function (stream) {
                s = stream;

                video.src = stream;
                video.play();
            }, errBack);
        } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
            navigator.webkitGetUserMedia(videoObj, function (stream) {
                s = stream;

                video.src = window.webkitURL.createObjectURL(stream);
                video.play();
            }, errBack);
        }
        else if (navigator.mozGetUserMedia) { // Firefox-prefixed
            navigator.mozGetUserMedia(videoObj, function (stream) {
                s = stream;

                video.src = window.URL.createObjectURL(stream);
                video.play();
            }, errBack);
        } else {
            errBack();
        }

        takePhotoDialog.dialog({
            close: function () {
                if (s) s.stop();
            }
        });

        $('.attach-image').unbind('click').on('click', function () {
            if ($(this).data('inProcess')) {
                return;
            }

            $('#captureContainer .loading').removeClass('xhidden');

            $(this).data('inProcess', 1);

            var dataURL = canvas.toDataURL();
            $.post('/mvc.php?c=Game&do=saveCapture', {
                photo: dataURL,
                user_id: userManager.getCurrentUser().getUserId()
            }, function (data) {
                takePhotoDialog.dialog('close');
                $('#captureContainer').dialog('close');

                context.photoUploaded(data.filename);
                $('#captureContainer .loading').addClass('xhidden');

            }, 'json');
        });

        $("#snap").unbind('click').on("click", function () {
            c.drawImage(video, 0, 0, 480, 360);
            $('#captureContainer').dialog('open');
        });
    };

    this.importLeftSideItem = function (user, convId, count, meBlocked, myBlocked) {
        var leftSideJSP = leftSide.data('jsp');
        $('#ls_ui_tmpl')
            .tmpl($.extend({}, {
                num: count,
                className: count == 0 ? 'single zero' : count < 10 ? 'single' : 'many'
            }, user.getUser()))
            .prependTo(leftSideJSP.getContentPane());

        $('.item:first', leftSide).data('conv-id', convId);
        $('.item:first', leftSide).data('meBlocked', meBlocked);
        $('.item:first', leftSide).data('myBlocked', myBlocked);

        animator.animate($('.item:first', leftSide).find('.num'));

        $('.has-no-conv')
            .removeClass('has-no-conv')
            .addClass('has-conv');
    };

    this.report = function (message_id, user_id) {
        $('#msg-report').dialog({
            autoOpen: true,
            width: 450,
            height: 180,
            draggable: false,
            resizable: false,
            modal: true,
            open: function () {
            },
            buttons: {
                Отправить: function () {
                    g.getJson('/mvc.php?c=Msg&do=report', {
                        msg_id: message_id,
                        type_id: $('.report_type').val()
                    }, function (data) {
                        $('#msg-report').dialog('close');
                        _removeDialog(user_id);

                        transport.send({
                            to_user_id: data.blocked_user_id,
                            module: 'userManager',
                            action: 'blocked'
                        });

                    }, function (data) {

                    });
                },
                Отменять: function () {
                    $(this).dialog('close');
                }
            }
        });
    };

    //============================================================================================================
    var _removeDialog = function (user_id) {
        $('.item[data-user-id="' + user_id + '"]').remove();
        $('#dialog_' + user_id).remove();

        $('#sendMessage').unbind('click');
    };

    var _selectMessageTab = function () {
        $('header li.message').trigger('click');
    };

    var _validateSelectedFile = function (file) {

        if (!file.type.match('image.*')) {
            _setFileUploadError('Выбранный файл не является картинкой');
            return false;
        }

        if (file.size > 3 * 1024 * 1024) {
            _setFileUploadError('Максимальный размер фото 3Мб');
            return false;
        }

        _setFileUploadError(false);
        return true;
    };

    var _setFileUploadError = function (error) {
        if (!error) {
            $('#imgUploadErrors').text('').addClass('xhidden');
            return true;
        }

        // Close dialog
        attachImageDialog.dialog('close');

        $('#imgUploadErrors').text(error).removeClass('xhidden');

        return true;
    };

    var _setMessages = function (data) {
        var dialog = $('#dialog_' + data.u_id);

        // If dialog not loaded yet
        if (data.is_new && !dialog.data('mLoaded')) {
            _upgradeBadge(data.user_id);

            // Set conversation id
            $('.item[data-user-id="' + data.u_id + '"]').data('conv-id', data.conv_id);

            return;
        }

        if (!data.is_new) {
            // Show dialog div
            _showDialog(data.u_id);
        }

        // Set dialog already loaded
        dialog.data('mLoaded', 1);

        // Append dialog messages
        var api = $('#dialog_' + data.u_id, gameTab2).find('.d').data('jsp');
        $.each(data.messages.reverse(), function (i, e) {
            var msg = new messageModel();
            msg.setMessage(e);

            var msgView = $('#msg_tmpl').tmpl(msg.getMessageView());
            var html = msgView.wrap('<div>').parent().html();

            api.getContentPane().append(html);

            if (data.is_new) {
                _upgradeBadge(data.user_id);
            }
        });

        // Set conversation id
        $('.item[data-user-id="' + data.u_id + '"]').data('conv-id', data.conv_id);

        setTimeout(function () {
            // Reinitial scroll
            _reinitialiseScroll(data.u_id);

            // Scroll to bottom
            api.scrollToBottom();
        }, 500);

        // Scrool to bottom
        var i = dialog.find('.msg-item .attachment');
        $('<img />')
            .attr('src', i.attr('src'))
            .load(function () {
                // Reinitial scroll
                _reinitialiseScroll(data.u_id);

                // Scroll to bottom
                api.scrollToBottom();
            });

        // Active tooltips
        g.activeToolTip();
    };

    var _reinitialiseScroll = function (user_id) {
        // Select dialog
        var dialog = $('#dialog_' + user_id);

        // Unbind scroll
        dialog.unbind('jsp-scroll-y');

        // Api
        var api = dialog.find('.d').data('jsp');

        // Reinitialise scroll
        api.reinitialise();

        // Bind scroll
        dialog.bind('jsp-scroll-y', function (event, scrollPositionY, isAtTop, isAtBottom) {
            if (scrollPositionY < loadMessagesTop) {
                var user_id = $(this).attr('id').split('_')[1];
                _loadMessages(user_id);
            }
        });
    };

    var _loadMessages = function (user_id) {
        // Is busy?!
        var dialog = $('#dialog_' + user_id);
        if (dialog.data('loadingInProcess')) {
            return;
        }

        // If all messages loaded
        if (dialog.data('page') == -1) {
            dialog.unbind('jsp-scroll-y');
            return;
        }

        // Set busy status
        dialog.data('loadingInProcess', 1);

        // Show loading progress
        // _showLoading(user_id);
        $(document).trigger('ajaxStart.ajaxIndicator');

        // Page
        var page = dialog.data('page');
        if (!page) page = 0;

        var conv_id = $('.item[data-user-id="' + user_id + '"]').data('conv-id');

        g.getJson('/mvc.php?c=Msg&do=getMessages', {
            u_id: user_id,
            conv_id: conv_id,
            offset: ++page
        }, function (response) {
            context.messagePrepend(response);
        });
        // Request!
        /*transport.send({
         u_id: user_id,
         type: 'load-old',
         action: 'getConversation',
         module: 'Messanger',
         page: ++page
         });*/


    };

    var _createDialog = function (user_id) {
        var dialog = $('#dialog_' + user_id);

        if (dialog.length != 0) {
            return;
        }

        $('<div>')
            .attr('id', 'dialog_' + user_id)
            .addClass('message-box')
            .appendTo(messageBoxList);

        $('<div>')
            .addClass('dialog_' + user_id + '_' + userManager.getCurrentUser().getUserId())
            .addClass('d')
            .appendTo($('.message-box:last', messageBoxList));

        _activateScroll(user_id);
    };

    var _upgradeBadge = function (user_id) {
        if (user_id == userManager.getCurrentUser().getUserId()) {
            return;
        }

        var badgeMenu = $('header ul.menu li:eq(1) .badge'),
            badgeSmall = $('.item[data-user-id="' + user_id + '"] .num');

        // Update top menu badge
        var badgeMenuNum = parseInt(badgeMenu.text().trim());
        badgeMenu.text(++badgeMenuNum).removeClass('zero');
        if (badgeMenuNum.toString().length > 1) {
            badgeMenu.removeClass('single').addClass('many');
        } else {
            badgeMenu.removeClass('many').addClass('single');
        }

        // Update left side badge
        var badgeSmallNum = parseInt(badgeSmall.text().trim());
        badgeSmall.text(++badgeSmallNum).removeClass('zero');

        if (badgeMenuNum == 0) {
            badgeSmall.removeClass('many').addClass('single').addClass('zero');
        }
        if (badgeSmallNum.toString().length > 1) {
            badgeSmall.removeClass('single').addClass('many');
        } else {
            badgeSmall.removeClass('many').addClass('single');
        }
    };

    var _downgradeBadge = function (user_id) {
        if (user_id == userManager.getCurrentUser().getUserId()) {
            return;
        }

        var badgeMenu = $('header ul.menu li:eq(1) .badge'),
            badgeSmall = $('.item[data-user-id="' + user_id + '"] .num');

        // Update top menu badge
        var badgeMenuNum = parseInt(badgeMenu.text().trim());
        badgeMenu.text(--badgeMenuNum < 0 ? 0 : badgeMenuNum);
        if (badgeMenuNum == 0) {
            badgeMenu.removeClass('many').addClass('single').addClass('zero');
        }
        if (badgeMenuNum.length > 1) {
            badgeMenu.removeClass('single').addClass('many');
        } else {
            badgeMenu.removeClass('many').addClass('single');
        }

        // Update left side badge
        var badgeSmallNum = parseInt(badgeSmall.text().trim());
        badgeSmall.text(--badgeSmallNum < 0 ? 0 : badgeSmallNum);
        if (badgeMenuNum == 0) {
            badgeSmall.removeClass('many').addClass('single').addClass('zero');
        }

        if (badgeMenuNum.length > 1) {
            badgeMenu.removeClass('single').addClass('many');
        } else {
            badgeMenu.removeClass('many').addClass('single');
        }
    };

    var _createLeftSide = function (user_id, callback, click) {
        var item = $('[data-user-id="' + user_id + '"]', leftSide);
        if (!item.length) {
            _appendToLeft(user_id, function () {

                if (callback) callback();

                if (click) {
                    item = $('.item[data-user-id="' + user_id + '"]', leftSide);
                    context.selectDialog(user_id, item);
                }
            });
        } else {
            if (callback) callback();

            if (click) {
                item = $('.item[data-user-id="' + user_id + '"]', leftSide);
                context.selectDialog(user_id, item);
            }
        }
    };

    var _appendToLeft = function (user_id, callback) {
        // If user wants to write himself
        if (user_id == userManager.getCurrentUser().getUserId()) {
            if (callback) callback();

            return true;
        }

        //request user to make new user block
        $.post('/mvc.php?c=User&do=getProfile', {
            ids: user_id,
            blocked_info: 1,
            user_id: userManager.getCurrentUser().getUserId(),
            sid: userManager.getCurrentUser().sid()
        }, function (data) {
            if (!data[0]) {
                return;
            }

            // Received user
            var user = new userModel(data[0]), d = $.extend({}, user.getUser(), {className: 'zero'});

            // Append to left side
            $('#ls_ui_tmpl').tmpl(d).prependTo(leftSide.data('jsp').getContentPane());

            $('.item:first', leftSide).data('meBlocked', data[0].meblocked);
            $('.item:first', leftSide).data('myBlocked', data[0].myblocked);

            // Remove label
            $('.has-no-conv')
                .removeClass('has-no-conv')
                .addClass('has-conv');

            if (callback) callback();

        }, 'json');

        return true;
    };

    var _showDialog = function (user_id) {
        $('.message-box.shown', messageBoxList)
            .removeClass('shown');

        $('#dialog_' + user_id).addClass('shown');
        _activateMessageSend(user_id);
    };

    var _setLeftSideItemSelected = function (item) {
        leftSide.find('.selected').removeClass('selected');
        $(item).addClass('selected');
    };

    var _scrollToBottom = function (user_id) {
        var api = $('#dialog_' + user_id).find('.d').data('jsp');
        api.scrollToBottom();
    };

    var _activateScroll = function (u_id, toBottom) {
        var gameTab2 = $('.game-tab2');
        var pane = $('#dialog_' + u_id, gameTab2).find('.d').jScrollPane({
            //autoReinitialise: true
        });
    };

    var _activateMessageSend = function (u_id) {
        $('#sendMessage').unbind('click').on('click', function (evt) {
            _sendMessage(u_id);
        });

        $('#message').unbind('keyup').on('keyup', function (evt) {
            if (evt.ctrlKey && evt.keyCode == 13) {
                _sendMessage(u_id);
            }
        });

        return messageBox.focus();
    };

    var _sendMessage = function (u_id) {
        var message = g.trimMessage(messageBox.html().trim()), leftSideItem = $('.item[data-user-id="' + u_id + '"]');

        var key, count = 0;
        for (key in attachment) {
            if (attachment.hasOwnProperty(key)) {
                count++;
            }
        }

        if (!message && count == 0) {
            return messageBox.focus();
        }

        transport.send({
            sender_user_id: userManager.getCurrentUser().getUserId(),
            receiver_user_id: parseInt(u_id),
            message: message,
            attachment: attachment,
            conv_id: leftSideItem.data('conv-id'),
            module: 'Messanger',
            action: count > 0 ? 'saveWithAttachment' : 'save'
        });

        leftSideItem.insertBefore($('.left-side .item:first-child'));

        attachment = {};
        $('#msg-image').empty();

        messageBox.html('').focus();
        return true;
    };

    var _showLoading = function (u_id) {
        var dialog = $('#dialog_' + u_id).find('.d');
        $('<div>')
            .addClass('loading')
            .addClass('loading4')
            .addClass('hasToolTip')
            .addClass('currently')
            .attr('title', 'Обновление сообщений')
            .append('<img src="/img/loading4.gif"/>')
            .prependTo(dialog);

        g.activeToolTip();
    };

    var _enabledMessaheBox = function (flag, type) {
        var $msg = $('#message'), className;

        if (type == 1) {
            className = 'mеBlocked';
        }
        if (type == 2) {
            className = 'myBlocked';
        }

        if (!flag) {
            $msg
                .removeAttr('contenteditable')
                .parent()
                .addClass('ui-state-disabled')
                .children()
                .each(function () {
                    $(this).click(function (e) {
                        e.stopPropagation();
                        return false;
                    })
                });

            $('.blocked').removeClass('active');
            $('.' + className)
                .addClass('active');

        } else {
            $msg
                .attr('contenteditable', true)
                .parent()
                .removeClass('ui-state-disabled');

            $('.blocked').removeClass('active');
            $('.' + className)
                .removeClass('active');
        }

    };

    var _hideLoading = function (u_id) {
        $('#dialog_' + u_id).find('.d').find('.loading').remove();
    };

    var _initButtons = function () {
        $('#denyCaturedImage').unbind('click').on('click', function () {
        });
    }
};

//################################### animator ########################################
var animator = new function () {
    /**
     * Jquery selector of .num
     */
    var numItems;

    /**
     * Opacity min value
     * @type {number}
     */
    var opacityMin = 0.4;

    /**
     * Opacity max value
     * @type {number}
     */
    var opacityMax = 1;

    this.animate = function (items) {
        numItems = items;
        //_startAnimation();
    };

    this.shake = function () {
        $('header .menu li:eq(1) .badge').removeClass('zero').effect("shake");
    };

    //============================================================================================================
    var _startAnimation = function () {
        numItems.each(function () {
            _animateItem(this, opacityMin);
        });
    };

    var _animateItem = function (obj, ocy) {
        $(obj).animate({
            opacity: ocy
        }, 350, function () {
            var tmp;

            if (ocy == opacityMax) {
                tmp = opacityMin;
            }
            if (ocy == opacityMin) {
                tmp = opacityMax;
            }

            _animateItem(obj, tmp);
        });
    }
};

//################################### emojic ########################################
var emojic = new function () {
    var msg;
    this.initial = function () {
        msg = jQuery('#message');

        msg
            // make sure br is always the lastChild of contenteditable
            .on("keyup mouseup", function () {
                if (!this.lastChild || this.lastChild.nodeName.toLowerCase() != "br") {
                    this.appendChild(document.createElement("br"));
                }
            })

            // use br instead of div div
            .on("keypress", function (e) {
                if (e.which == 13) {
                    if (window.getSelection) {
                        var selection = window.getSelection(),
                            range = selection.getRangeAt(0),
                            br = document.createElement("br");
                        range.deleteContents();
                        range.insertNode(br);
                        range.setStartAfter(br);
                        range.setEndAfter(br);
                        range.collapse(false);
                        selection.removeAllRanges();
                        selection.addRange(range);
                        return false;
                    }
                }
            });
    };

    this.showEmotics = function (emotic, obj) {
        if (msg.parent().hasClass('ui-state-disabled')) {
            return false;
        }

        msg.focus();

        var x = document.createElement('img');
        x.src = $(obj).attr('src');
        _insertNodeOverSelection(x, msg[0]);
    };
    //============================================================================================================
    /**
     * @param ancestor
     * @param descendant
     * @returns {boolean}
     * @private
     */
    var _isOrContainsNode = function (ancestor, descendant) {
        var node = descendant;
        while (node) {
            if (node === ancestor) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };

    /**
     * @param node
     * @param containerNode
     * @private
     */
    var _insertNodeOverSelection = function (node, containerNode) {
        var sel, range, html, str;

        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                if (_isOrContainsNode(containerNode, range.commonAncestorContainer)) {
                    range.deleteContents();
                    range.insertNode(node);
                } else {
                    containerNode.appendChild(node);
                }
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();
            if (_isOrContainsNode(containerNode, range.parentElement())) {
                html = (node.nodeType == 3) ? node.data : node.outerHTML;
                range.pasteHTML(html);
            } else {
                containerNode.appendChild(node);
            }
        }

        _placeCaretAtEnd(containerNode);
    };

    /**
     * @param el
     * @private
     */
    var _placeCaretAtEnd = function (el) {
        el.focus();
        if (window.getSelection) {
            if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);

                if (navigator.userAgent.indexOf("Firefox") != -1) {
                    range.setEndBefore($(el).find('br')[0]); //only for FF, fiddle does not have browser check
                }
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        }
    };
};

$(function () {
    $(document).on('currentUserReady', function () {
        messageManager.initial();
    });
});