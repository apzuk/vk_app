leaderManager = new function () {

    /**
     *
     * JQuery dialog
     *
     */
    var dialog;

    /**
     *
     * This object
     *
     * @type {leaderManager}
     */
    var context = this;

    /**
     *
     * leaderModel
     *
     * @type {null}
     */
    var leaderModel = null;

    /**
     *
     * Socket data transport
     *
     * @type {sockJsHandler|*}
     */
    var transport = sockJsHandler;

    /**
     *
     * Is Object initialized?!
     *
     */
    var inited;

    /**
     *
     * Opacity for fake leader
     *
     * @type {string}
     */
    var o = 'o04';

    // ################ public methods ###############

    this.init = function () {
        if (inited) return;

        //initialize dialogs
        _mainDialog();
        _becomeLeader();
        _controlDisplay();

        // Become leade button
        $('.become-leader').unbind('click').on('click', function (evt) {
            $('#leadership_dialog').dialog('open');
        });

        // Object initialized!
        inited = true;
    };

    this.setLeader = function (data) {
        leaderModel = new leader(data, data.bid, data.message);

        this.init();
        _setNewLeader();
    };

    this.getCityLeader = function () {
        return leaderModel.leader();
    };

    this.isTrueLeader = function () {
        return !$('.leader').find('.avatar').hasClass(o);
    };

    // ################## private methods ###################
    var _setNewLeader = function () {
        var wrapper = $('.leader'),
            bid = $('.bet-info .bet', wrapper),
            leader = leaderModel.leader(),
            clr = leader.getSelectedColor();

        if (leaderModel.message()) {
            if (leaderModel.message().length > 120) {
                $('.leader-text .content').height(70);
            }

            $('.content .txt').text(leaderModel.message())
                .parents('.leader-text')
                .fadeIn();

        } else {
            // TODO: hide
        }

        if (g.getBalance(leaderModel.bid()) != parseInt(bid.text().trim())) {
            bid.fadeOut('normal', function () {
                $(this).text(g.getBalance(leaderModel.bid())).fadeIn();
            });
        }

        wrapper.attr('id', 'leader_' + leader.getUserId());

        var img = $('img', wrapper),
            c = (leader.getUserId() == userManager.getCurrentUser) && context.isTrueLeader() ? 'me' : 'm';

        $('a', wrapper).attr('onclick', 'userManager.showProfile("' + leader.getUserId() + '"); return false;');
        img
            .attr('src', leader.getAvatar(100))
            .parent().parent()
            .removeClass('female-avatar')
            .removeClass('male-avatar')
            .addClass(c)
            .addClass(leader.getGenderT() + '-avatar')
            .addClass(clr ? 'clr-b-' + clr : '')
            .makeVip(leader);

        if (leaderModel.isFakeLeader()) {
            img
                .parent()
                .parent()
                .addClass(o)
                .addClass('no-leader');
        } else {
            img
                .parent()
                .parent()
                .removeClass(o)
                .removeClass('no-leader');
        }

        wrapper.parent()
            .removeClass('male')
            .removeClass('female')
            .addClass(leader.getGenderT());
    };

    var _mainDialog = function () {
        $('.leader').dialog({
            closeOnEscape: false,
            autoOpen: false,
            width: 156,
            height: 201,
            draggable: false,
            position: {
                my: "left top",
                at: "left+12 top+42",
                of: $("#content")
            }
        }).styledDialog({
                btn: '.become-leader',
                btnLabel: "Стать лидером",
                type: leaderModel.leader().getGenderT(),
                zIndex: 99
            });
    };

    var _becomeLeader = function () {
        var lsd = $('#leadership_dialog');
        dialog = lsd.dialog({
            autoOpen: false,
            resizable: false,
            closeOnEscape: false,
            modal: true,
            width: 385,
            height: 'auto',
            open: function () {
                $('#leadership_dialog .error').addClass('xhidden');

                var leader = $('.leader'), leaderId = leader.attr('id').split('_')[1], cUser = userManager.getCurrentUser();
                if (leaderId == cUser.getUserId() && !leader.find('.avatar').hasClass('no-leader')) {
                    $(this).dialog({
                        buttons: {
                            'закрыть': function () {
                                $('#leadership_dialog').dialog('close');
                            }
                        }
                    });
                    $('#already_leader_tmpl').tmpl({}).appendTo($(this));

                } else {
                    $('#become_leader_tmpl').tmpl({
                        city_name: cUser.getCityName(),
                        current_bid: g.getBalance(leaderModel.bid()),
                        bid_cost: g.getBalance(leaderModel.bid() + 1)
                    }).appendTo($(this));

                    var ta = $('textarea', lsd);
                    ta.limit(130, '#leadership_dialog .charsLeft');
                    ta.focus();
                }
            },
            close: function () {
                $(this).find('textarea').trigger('blur');
                $(this).empty();
            },

            buttons: {
                Да: function () {
                    var msg = $('#leadership_dialog textarea').val().trim();

                    if (!msg) {
                        $('#leadership_dialog .error')
                            .text('Введите текст')
                            .removeClass('xhidden');

                        $('#leadership_dialog textarea').focus();
                        return;
                    }

                    var params = {
                        user_id: userManager.getCurrentUser().getUserId(),
                        sid: userManager.getCurrentUser().sid(),
                        msg: msg
                    };

                    g.getJson('/mvc.php?c=User&do=makeMeLeader', params,
                        function (response) {
                            context.setLeader(response.user);
                            $('#leadership_dialog').dialog('close');

                            $('#leadership_dialog .error').empty().addClass('xhidden');

                            transport.send({
                                action: 'updateCityLeader',
                                module: 'leaderManager',
                                last_leader_id: response.last_leader_id,
                                city_id: userManager.getCurrentUser().getCityId()
                            });

                            userManager.updateSelfInfo();
                        },
                        function (response) {
                            if (response.reason == 'content.empty') {
                                $('#leadership_dialog textarea').focus();

                                $('#leadership_dialog .error')
                                    .text('Введите текст')
                                    .removeClass('xhidden');
                            }

                            if (response.reason == "link.contains") {
                                $('#leadership_dialog .error')
                                    .text('Ссылка запрещена')
                                    .removeClass('xhidden');
                            }
                        }
                    );
                },
                Нет: function () {
                    $(this).dialog('close');
                }
            }
        });
    };

    var _controlDisplay = function () {
        $('header ul li:eq(0)').
            on('gameTabItem1blur', function () {
                $('.leader').dialog('close');
            })
            .on('gameTabItem1click', function () {
                if (!game.isPlaying()) {
                    $('.leader').dialog('open');
                }
            });
    }
};