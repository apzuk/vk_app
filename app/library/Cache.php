<?php

namespace library;

class Cache
{
    private static $instance;

    private $memCached;

    public function __construct()
    {
        //return self::instance();
    }

    /**
     * @return Cache
     */
    public static function instance()
    {
        if (!self :: $instance) {
            self :: $instance = new self();
        }

        self :: $instance->init();

        return self :: $instance;
    }

    /**
     * Init memcache
     */
    public function init()
    {
        if ($this->memCached) {
            return;
        }

        $memCache = new \Memcache();
        $conn = $memCache->connect('127.0.0.1', 12875);

        $this->memCached = $memCache;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return trim($this->cache()->get($key));
    }

    /**
     * @param $key
     * @param $value
     * @param $expire
     * @return mixed
     */
    public function set($key, $value, $expire = 0)
    {
        return $this->cache()->set($key, $value, MEMCACHE_COMPRESSED, $expire);
    }

    /**
     * @return mixed
     */
    public function cache()
    {
        return $this->memCached;
    }
} 