<?php
class Loader {
    //----------------------------------------------------------------------
    public static function autoload($class_name) {
        $file = str_replace('\\', '/', $class_name);
        $file = str_replace('library/', '', $file);

        // try including the file.  PHP will search the include path to find it.
        $result = include_once $file . '.php';
        return $result;

    }

    //----------------------------------------------------------------------
    public static function register($path) {
        set_include_path($path);

        // register our autoloader
        spl_autoload_register(array (
            __CLASS__,
            'autoload'
        ), false, false);
    }

    //----------------------------------------------------------------------
}