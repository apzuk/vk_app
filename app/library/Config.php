<?php

namespace library;

use library\Debug;
use library\String;

class Config {
    // config data storage
    private static $CONFIGS = array ();

    //----------------------------------------------------------------------
    /**
     * Having this function strays from our loose coupling policy, but we allow
     * it because this function should only be called when testing code and
     * it'll be commented out before going live, usually.
     */
    public static function dump() {
        Debug :: dump(print_r(self :: $CONFIGS, true));
    }

    //----------------------------------------------------------------------
    public static function get($key, $default = '') {
        return isset (self :: $CONFIGS[$key]) ? self :: $CONFIGS[$key] : $default;
    }

    //----------------------------------------------------------------------
    private static function importConfig($ini_file) {
        // nothing to do if the file does not exist
        if (!file_exists($ini_file)) {
            return false;
        }

        // php loads INI files for us with it's built-in functions (use sections)
        $configs = (array) parse_ini_file($ini_file, true);

        // flatten nested arrays with lowercase and underscored naming convention
        $data = array ();
        foreach ($configs as $section => $block) {
            // push flat keys into block format
            if (!is_array($block)) {
                $block = array (
                    $section => $block
                );
                $section = '';
            }

            // push all blocks into our config collection while flattening keys
            foreach ($block as $key => $value) {
                $key = $section ? ($section . '.' . $key) : $key;
                $key = strtolower(preg_replace('/[\s_]+/', '_', $key));
                $data[$key] = $value;
            }
        }
        // merge the imported config data into our class configs
        self :: $CONFIGS = array_merge(self :: $CONFIGS, $data);
        return true;
    }

    //----------------------------------------------------------------------
    public static function init($init_path) {
        $data = array();

        // import the SERVER config settings
        self :: importConfig($init_path);

        // don't let our "core" values be overridden ... put them back in again
        self :: $CONFIGS = array_merge(self :: $CONFIGS, $data);

        // do macro replacements
        self :: replaceMacros();
    }

    //----------------------------------------------------------------------
    private static function replaceMacros($loop_count = 0) {
        // copy the class configs locally
        $configs = self :: $CONFIGS;
        $again = false;

        // loop through configs looking for macros, replace them if found
        foreach ($configs as $key => $value) {
            // find our macros and replace them if we have the values defined
            $match = array ();
            $new_value = preg_replace_callback('/{%(.*?)%}/', function ($match) use ($configs, $again) {
                // found the replacement
                if (isset ($configs[$match[1]])) {
                    $again = true;
                    return $configs[$match[1]];
                }

                // leave original tag as it was
                return '{%' . $match[1] . '%}';
            }, $value);

            // if the replaced string is not the same as the original, something changed and we need to run through the replacements all over again
            if ($value != $new_value) {
                $configs[$key] = $new_value;
                $again = true;
            }
        }

        // update class configs with processed values
        self :: $CONFIGS = $configs;

        // replace macros again (recursive macros)
        if ($again) {
            self :: replaceMacros($loop_count +1);
        }
    }

    //----------------------------------------------------------------------
}