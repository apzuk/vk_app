<?php

namespace library\Components\Minifier;

use library\Debug;
use library\Object;
use library\String;

class Minify
{
    /**
     * @var string
     */
    private $jsTemplate = '<script type="text/javascript" src="[jsPath]"></script>';

    /**
     * @var string
     */
    private $cssTemplate = '<link type="text/css" rel="stylesheet"href="[cssPath]">';

    /**
     *
     * Appended js files full path
     *
     * @var Array
     */
    private $_jsFiles = array();

    /**
     *
     * Appended css files full path
     *
     * @var Array
     */
    private $_cssFiles = array();

    /**
     *
     * Plugin configs
     *
     * @var \library\Object
     */
    private $_configs;

    /**
     *
     * Generated cache file full path
     *
     * @var String
     */
    private $_filename;

    /**
     *
     * Generated cache filename
     *
     * @var String
     */
    private $_fname;

    /**
     * @param $configs
     */
    public function __construct($configs)
    {
        $this->_configs = new Object($configs);

        $this->_configs->cache_dir = $this->_configs->public_dir . '/' . $this->_configs->cache_dir_public_path;

    }

    /**
     *
     * Add all js file under the passed directory bo be minified
     *
     * @param $folder
     * @param array $exclude
     * @return array
     */
    public function registerJS($folder, $exclude = array())
    {
        // Return found files under dir!
        $this->register($folder, $exclude, $this->_jsFiles);

        return $this;
    }

    /**
     *
     * Add all js file under the passed directory bo be minified
     *
     * @param $folder
     * @param array $exclude
     * @return array
     */
    public function registerCSS($folder, $exclude = array())
    {
        // Return found files under dir!
        $this->register($folder, $exclude, $this->_cssFiles);

        return $this;
    }

    /**
     *
     * Append js file
     *
     * @param $js
     * @return $this
     */
    public function appendJsFile($js)
    {
        // Full path to js file
        $jsPath = $this->_configs->public_dir . $js;

        if (file_exists($jsPath) && !is_dir($jsPath)) {
            $this->_jsFiles[] = $jsPath;
        }

        return $this;
    }

    /**
     *
     * Prepend js file
     *
     * @param $js
     * @return $this
     */
    public function prependJsFile($js)
    {
        // Full path to js file
        $jsPath = $this->_configs->public_dir . $js;

        if (file_exists($jsPath) && !is_dir($jsPath)) {
            array_unshift($this->_jsFiles[], $this->_configs->public_dir . $jsPath);
        }

        return $this;
    }

    /**
     *
     * Append css file
     *
     * @param $css
     * @return $this
     */
    public function appendCssFile($css)
    {
        // Full path to css file
        $cssPath = $this->_configs->public_dir . $css;

        if (file_exists($cssPath) && !is_dir($cssPath)) {
            $this->_cssFiles[] = $cssPath;
        }

        return $this;
    }

    /**
     *
     * Prepend css file
     *
     * @param $css
     * @return $this
     */
    public function prependCssFile($css)
    {
        // Full path to css file
        $cssPath = $this->_configs->public_dir . $css;

        if (file_exists($cssPath) && !is_dir($cssPath)) {
            array_unshift($this->_cssFiles[], $cssPath);
        }

        return $this;
    }

    /**
     * @param $folder
     * @param $exclude
     * @return array
     */
    private function register($folder, $exclude, &$f)
    {
        // Open passed folder for looping
        $di = new \DirectoryIterator($folder);

        // Loop it!
        foreach ($di as $fileInfo) {
            // Current file name
            $filename = $fileInfo->getFilename();

            // Is css ot js?!
            if (!in_array(pathinfo($filename, PATHINFO_EXTENSION), array('js', 'css'))) {
                continue;
            }

            // Ignore self and parent directories
            if ($filename == '.' || $filename == '..') continue;

            // If current item is dir parse files under it recursively
            if ($fileInfo->isDir()) {
                $this->register($fileInfo->getPathname(), $exclude, $f);
            } else {
                // Is exclude?!
                if (in_array($filename, $exclude)) {
                    continue;
                }

                // Push to files array
                $f[] = $fileInfo->getPathname();
            }
        }
    }

    /**
     * @param $files
     * @param $type
     * @return bool
     */
    public function isFilesModified($files, $type)
    {
        // If not files passed
        if (count($files) == 0) return false;

        // Get files hash
        $cache = $this->cacheHash($files);

        // Generated file name
        $this->_fname = 'cache-' . $cache . '-min' . $type;

        // Generate cache file name
        $this->_filename = $filename = $this->_configs->cache_dir . '/' . $this->_fname;

        // If file not exists one or several of files has been modified
        if (!file_exists($filename)) {
            // remove older caches
            $this->removeCaches($type);

            // Create new file
            fopen($filename, 'w');

            // Set content
            $this->appendContent($filename, $files);

            return true;
        }

        return false;
    }

    /**
     *
     * Remove older cache files from cache directory
     *
     * @param $type
     */
    public function removeCaches($type)
    {
        $di = new \DirectoryIterator($this->_configs->cache_dir);
        foreach ($di as $fileInfo) {

            $filename = $fileInfo->getFilename();
            if (preg_match('/cache-[\d\w]+-min' . $type . '/', $filename)) {
                //unlink($fileInfo->getPathname());
            }
        }
    }

    /**
     *
     * Calculate hash name! MD5(all files last modified date)
     *
     * @param $files
     * @return string
     */
    private function cacheHash($files)
    {
        // Hash string
        $hash = '';

        // Walk through files
        array_walk_recursive($files, function ($file, $file_index) use (&$hash) {
            $hash .= filemtime($file);
        });

        // return hashed string!
        return md5($hash);
    }

    /**
     * @param $filename
     * @param $files
     */
    private function appendContent($filename, $files)
    {
        array_walk_recursive($files, function ($file, $index) use (&$filename) {
            $content = file_get_contents($file) . "\n\r";
            file_put_contents($filename, $content, FILE_APPEND);
        });
    }

    public function __toString()
    {
        $returnData = '';

        if ($this->_configs->noMinify) {
            // css files
            foreach ($this->_cssFiles as $jsFile) {
                $public_path = str_replace($this->_configs->public_dir, '', $jsFile);

                $returnData .= String::merge($this->cssTemplate, array(
                        'cssPath' => $public_path
                    )) . "\n";
            };

            // Js files
            foreach ($this->_jsFiles as $jsFile) {
                $public_path = str_replace($this->_configs->public_dir, '', $jsFile);

                $returnData .= String::merge($this->jsTemplate, array(
                        'jsPath' => $public_path
                    )) . "\n";
            };
        } else {
            $this->isFilesModified($this->_cssFiles, 'css');
            $returnData .= String::merge($this->cssTemplate, array(
                    'cssPath' => '/' . $this->_configs->cache_dir_public_path . '/' . $this->_fname
                )) . "\n";

            $this->isFilesModified($this->_jsFiles, 'js');
            $returnData .= String::merge($this->jsTemplate, array(
                    'jsPath' => '/' . $this->_configs->cache_dir_public_path . '/' . $this->_fname
                )) . "\n";
        }
        return $returnData;
    }
}