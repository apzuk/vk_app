<?php

namespace library;

/**
 * Description of Object
 *
 * @author Aram
 */
class Object
{

    public $DATA = array();

    //----------------------------------------------------------------------
    public function __construct($data = null)
    {
        if (is_array($data)) {
            $this->DATA = $data;
        }
    }

    //----------------------------------------------------------------------
    public function __get($key)
    {
        return isset($this->DATA[$key]) ? $this->DATA[$key] : false;
    }

    //----------------------------------------------------------------------
    public function __set($key, $value)
    {
        $this->DATA[$key] = $value;
    }

    //----------------------------------------------------------------------
    public function clear()
    {
        $this->DATA = array();
    }

    //----------------------------------------------------------------------
    public function dump()
    {
        Debug :: dump($this->DATA);
    }

    //----------------------------------------------------------------------
    public function get($key, $default = '')
    {
        return isset($this->DATA[$key]) ? $this->DATA[$key] : $default;
    }

    //----------------------------------------------------------------------
    public function getArray()
    {
        return $this->DATA;
    }

    //----------------------------------------------------------------------
    public function getValues($fields)
    {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->get($field);
        }
        return $data;
    }

    //----------------------------------------------------------------------
    public function import($values)
    {
        // no array?, no import
        if (!is_array($values)) {
            return false;
        }

        // import array values into current object
        foreach ($values as $key => $value) {
            $this->set($key, $value);
        }

        // yeah, that worked
        return true;
    }

    //----------------------------------------------------------------------
    public function isEmpty()
    {
        return !count($this->DATA);
    }

    //----------------------------------------------------------------------
    public function keys()
    {
        return array_keys($this->DATA);
    }

    //----------------------------------------------------------------------
    public function set($key, $value)
    {
        $this->DATA[$key] = $value;
    }

    //----------------------------------------------------------------------
}

?>
