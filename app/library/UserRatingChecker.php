<?php

namespace library;

use library\Query\User;

class UserRatingChecker
{
    /**
     * @param $user
     * @param $rating
     * @param $type
     * @param $rType
     * @return bool|string
     */
    public static function setRating($user, $rating, $type, $rType)
    {
        if (!$user) {
            return false;
        }

        if (is_array($user)) {
            $user = new Object($user);
        }

        $result1 = User::userSetRating($user->user_id, $rating, $type, $rType);
        if ($result1) {
            $user->rating = $user->rating + $rating;
        }

        $result2 = true;

        if ($user->ref_id && $user->rating >= 300) {
            $result2 = self::setMoneyForReferral($user->ref_id, $user);
        }

        return $result1 && $result2;
    }

    /**
     * @param $user_id
     * @param $ref
     * @return bool
     */
    private static function setMoneyForReferral($user_id, $ref)
    {
        $votes = intval(Config::get('setting.vote_for_ref'));

        $r = User\Balance::balanceSetUser($user_id, $votes);

        if (!$r) {
            return false;
        }

        $data = array('count' => $votes * EXCHANGE);

        $r = User\Notification::notifSet(14, json_encode($data), $user_id, $ref->user_id, date('Y-m-d H:i:s'));
        if (!$r) {
            return false;
        }

        $r = User::userUnsetReferral($ref->user_id);
        if (!$r) {
            return false;
        }

        return true;
    }
}