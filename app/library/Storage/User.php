<?php

namespace library\Storage;

use library\Debug;

class User
{
    /**
     * @param $src
     * @param $dest
     * @return mixed
     */
    public static function moveVKUserImage100($src, $dest)
    {
        if (!file_exists($dest) && !is_dir($dest)) {
            mkdir($dest, 0775, true);
        }

        $parts = preg_split('/\//', $src);
        $filename = end($parts);

        if (!self::validate($filename)) {
            return false;
        }

        copy($src, $dest . $filename);
        return $filename;
    }

    /**
     * @param $src
     * @param $dest
     * @return mixed
     */
    public static function moveVKUserImageOriginal($src, $dest)
    {
        if (!file_exists($dest) && !is_dir($dest)) {
            mkdir($dest, 0775, true);
        }

        $parts = preg_split('/\//', $src);
        $filename = end($parts);

        copy($src, $dest . $filename);

        self :: makeCache($dest, $filename, 80);

        return $filename;
    }

    /**
     * @param $src
     * @param $filename
     * @param $size
     * @return bool
     */
    public static function makeCache($src, $filename, $size)
    {
        //folder path
        $sizePath = $src . 'cache/' . $size . '/';

        //make sure the folder exists
        if (!file_exists($sizePath) && !is_dir($sizePath)) {
            mkdir($sizePath, 0775, true);
        }

        //new imagick object
        $i = new \Imagick($src . $filename);

        //resize original image
        $i->resizeImage($size, $size, \Imagick::FILTER_LANCZOS, 1, true);

        //save it
        $i->writeimage($sizePath . $filename);

        //deallocat!
        $i->destroy();
    }

    /**
     * @param $filename
     * @return string
     */
    public static function getPath($filename)
    {
        return '/data/avatars/100/' . $filename;
    }

    /**
     * @param $filename
     * @return string
     */
    public static function getPathOriginal($filename)
    {
        return '/data/avatars/original/' . $filename;
    }

    /**
     * @param $avatarName
     * @return int
     */
    public static function validate($avatarName)
    {
        return !preg_match('/(deactivated|camera)/', $avatarName);
    }
}