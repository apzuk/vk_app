<?php

namespace library;

class Session
{
    private $namespace = null;

    public function __construct($namespace)
    {
        $this->namespace = $namespace;
    }

    public function set($prop, $val)
    {
        $_SESSION[$this->namespace][$prop] = $val;
    }

    public function get($prop, $default = null)
    {
        return isset($_SESSION[$this->namespace][$prop]) ? $_SESSION[$this->namespace][$prop] : $default;
    }
}