<?php

namespace library\View;

use library\Tag;

class Flags
{
    public static function render()
    {
        // Load xml file with flags data
        $flags = simplexml_load_file(PRIVATE_PATH . '/flags.xml');

        // Wrap div
        $tag = new Tag('div', 'class', 'flags');

        foreach ($flags as $flag) {
            $ico = '/img/flags/' . $flag->icon;
            $countryName = $flag->label;
            $id = $flag->id;

            if ($id) {
                $tag->append('img', 'src', $ico, 'class', 'p-flag hasToolTip', 'title',
                    $countryName, 'width', 24, 'onclick', 'userManager.selectCountry(' . $id . ')',
                    'id', 'country_' . $id);
            }
        }

        //close main
        $tag->pop();

        //
        return $tag;
    }
} 