<?php

namespace library\View;

use library\String;

class GraphicLoader
{
    /**
     * @var null
     */
    private static $_directories = null;

    /**
     * @var string
     */
    private static $_html = '';

    /**
     * @param $dirName
     */
    public static function register($dirName)
    {
        if (file_exists($dirName) && is_dir($dirName)) {
            self :: $_directories[] = $dirName;
        }
    }

    /**
     * @return string
     */
    public static function appendFiles()
    {
        foreach (self::$_directories as $dir) {
            self::appendDirFiles($dir);
        }

        return self::$_html;
    }

    public static function appendFile($filePath)
    {
        if (file_exists($filePath) || !is_dir($filePath)) {
            self :: $_html .= String::merge('<img src="[path]" />', array(
                'path' => str_replace('/var/www/html/vk_app/dev/vk_app/public/', '', $filePath)
            ));
        }
    }

    /**
     * @param $dir
     */
    private static function appendDirFiles($dir)
    {
        $dir = new \DirectoryIterator($dir);
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir()) {
                if ($fileinfo->getFilename() == '.' || $fileinfo->getFilename() == '..') {
                    continue;
                }

                self::appendDirFiles($fileinfo->getPathname());
            } else {
                self :: $_html .= String::merge('<img src="[path]" />', array(
                    'path' => str_replace('/var/www/html/vk_app/dev/vk_app/public/', '', $fileinfo->getPathname())
                ));
            }
        }
    }
}

