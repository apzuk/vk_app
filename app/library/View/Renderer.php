<?php

namespace library\View;

use library\String;

class Renderer
{
    private $controller = null;

    public function __construct($controller = null)
    {
        $this->controller = strtolower($controller);
    }

    /**
     * @param $filename
     * @param $params
     * @param bool $render
     * @return bool|string
     */
    public function render($filename, $params = array(), $render = false)
    {
        // Dynamically variables
        foreach ($params as $var_name => $var_value) {
            $$var_name = $var_value;
        }

        $include = String::merge('[base]/../app/mvc/Views/[controller][filename].php', array(
            'base' => BASE_DIR,
            'filename' => $filename,
            'controller' => $this->controller . '/'
        ));

        if (!file_exists($include) || is_dir($filename)) {
            return false;
        }

        ob_start();
        include($include);
        $content = ob_get_clean();

        if ($render) {
            echo $content;
            return '';
        }

        return $content;
    }

    /**
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return false;
    }

    /**
     * @param $property
     * @param $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }
} 