<?php

namespace library\View;

use library\String;

class ScriptOnRun
{
    private $_script = array();

    protected static $_instance;

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function appendScript($script)
    {
        $this->_script[] = $script;
    }

    public function output()
    {
        $sTmpl = "[script]";
        foreach ($this->_script as $s) {
            echo String::merge($sTmpl, array(
                'script' => $s
            ));
        }
    }
} 