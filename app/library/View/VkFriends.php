<?php

namespace library\View;

use library\Debug;
use library\Object;
use library\Tag;
use library\VkApiWrapper;

class VkFriends
{
    protected static $_instance;

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function renderFriends($user_id)
    {
        $offset = 0;
        $friends = array();

        do {
            // Get user all friends
            $friends = array_merge($friends, VkApiWrapper::apiGetFriends($user_id, $offset));
            $offset++;
        } while (count($friends) >= 1000);


        // No friend label
        $label = 'По какой то не понятной причине не смоли найти Ваших друзьей.';

        $tag = new Tag('div', 'class', 'f');

        if (!$friends) {
            $tag->push('span', 'class', 'no-friends-found', $label);
            $tag->pop();

            $friends['response'] = array('items' => array());
        }

        foreach ($friends['response']['items'] as $friend) {

            if (isset($friend['deactivated'])) {
                continue;
            }

            $gender = $friend['sex'] == 1 ? 'female' : 'male';

            $tag->push(
                'div',
                'data-username',
                $friend['first_name'] . ' ' . $friend['last_name'],
                'class',
                'friend ' . $gender . '-avatar',
                'onclick',
                'g.postOnFriendWall(' . $friend['id'] . ', 2)'
            );
            $tag->append('img', 'src', $friend['photo_50']);
            $tag->push('span', 'class', 'name', $friend['first_name']);
            $tag->pop();
            $tag->pop();
        }

        if (count($friends['response']['items']) == 0) {
            $tag->push('span', 'class', 'no-friends-found', $label);
            $tag->pop();
        }
        $tag->pop();

        // Friends view
        return $tag;
    }
}