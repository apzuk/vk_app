<?php

namespace library\View;

use library\Tag;

class VkIcons
{
    public static function icons()
    {
        $icons = simplexml_load_file(PRIVATE_PATH . '/icondef.xml');

        $tag = new Tag('div');

        foreach ($icons as $icon) {
            $title = $icon->text[0];
            $src = '/img/vk/' . $icon->object;

            $tag->push('a', 'href', '#', 'title', $title, 'class', 'vk-icon');
            $tag->append('img', 'src' , $src, 'onclick', 'emojic.showEmotics(\'' . $title . '\', this)');
            $tag->pop(); //close link
        }

        $tag->pop(); //close main

        return $tag;
    }
}