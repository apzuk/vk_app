<?php

namespace library\View;

use library\Config;
use library\String;

class StylesManager
{
    /**
     * @var string
     */
    private static $jsTemplate = '<script type="text/javascript" src="[jsPath]"></script>';

    /**
     * @var string
     */
    private static $cssTemplate = '<link rel="stylesheet" href="[cssPath]" type="text/css">';

    /**
     * @param $jsPath
     * @param bool $noCache
     * @return mixed
     */
    public static function appendJsFile($jsPath, $noCache = false)
    {
        if (!Config::get('layout.jsCache') || $noCache) {
            $jsPath .= '?' . rand(0, 1000);
        }

        return String::merge(self :: $jsTemplate, array(
            "jsPath" => $jsPath
        ));
    }

    /**
     * @param $cssPath
     * @param bool $noCache
     * @return mixed
     */
    public static function appendCssFile($cssPath, $noCache = false)
    {
        if (!Config::get('layout.cssCache') || $noCache) {
            $cssPath .= '?' . rand(0, 1000);
        }

        return String::merge(self :: $cssTemplate, array(
            "cssPath" => $cssPath
        ));
    }
}