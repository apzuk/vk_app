<?php

namespace library;

class Controller
{
    protected $renderer;

    public function __construct($renderer = null)
    {
        $this->renderer = $renderer;
    }

    /**
     * @param $data
     */
    public function response($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * Response no enough balance
     */
    public function noEnoughBalance()
    {
        $this->response(
            array(
                'result' => false,
                'reason' => 'no.balance'
            )
        );
    }

    /**
     * @param $user_id
     * @param bool $load_soc_id
     * @return Object
     */
    public function loadUser($user_id, $load_soc_id = false, $checkIsRead = true)
    {
        $user = \library\Query\User::userGetFull($user_id, null, false, false, $load_soc_id);
        $user = new Object($user);

        if (!$user->user_id || ($checkIsRead && !$user->is_ready)) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'invalid.user'
                )
            );
        }

        return $user;
    }

    /**
     * @param $user_id
     * @param $sid
     * @param bool $load_soc_id
     * @return Object
     */
    public function checkOwnership($user_id, $sid, $load_soc_id = false, $checkIsRead = true)
    {
        $user = $this->loadUser($user_id, $load_soc_id, $checkIsRead);

        if ($user->sid != $sid || !$sid || $user_id == Config::get('user.admin_id')) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'invalid.user'
                )
            );
        }

        return $user;
    }
} 