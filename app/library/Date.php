<?php

namespace library;

class Date
{
    public static function now()
    {
        return date('Y-m-d H:i:s');
    }

    public static function dateWithLabel($date, $format = '%d %B')
    {
        setlocale(LC_ALL, 'ru_RU.UTF8', 'rus_RUS.UTF8', 'Russian_Russia.UTF8');
        return strftime($format, strtotime($date));
    }
} 