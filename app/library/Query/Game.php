<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramp
 * Date: 8/12/13
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */

namespace library\Query;


class Game extends \Connection
{
    /**
     * @param $limit
     * @param $offset
     * @return array|bool
     */
    public static function gameGet($limit, $offset)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM game_history');
        $SQL->sql('LIMIT ? OFFSET ?')->setInt($limit)->setInt($offset);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $city_id
     * @return bool
     */
    public static function gameGetBid($city_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT current_bid AS result');
        $SQL->sql('FROM user_leader');
        $SQL->sql('WHERE city_id = ?')->setInt($city_id);

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @param $city_id
     * @return bool
     */
    public static function gameGetLeader($city_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT user_id');
        $SQL->sql('FROM user_leader');
        $SQL->sql('WHERE city_id = ?')->setInt($city_id);

        //run
        return self::selectValue($SQL, 'user_id');
    }

    /**
     * @param $user_id
     * @param $city_id
     * @param $bid
     * @param $message
     * @return bool|string
     */
    public static function gameSetLeader($user_id, $city_id, $bid, $message)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_leader SET');
        $SQL->sql('user_id = ?, city_id = ?, current_bid = ?, message = ?, update_date = NOW()');
        $SQL->setInt($user_id)->setInt($city_id)->setInt($bid)->set($message);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?, current_bid = ?, message = ?, update_date = NOW()');
        $SQL->setInt($user_id)->setInt($bid)->set($message);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @return bool|int
     */
    public static function gameCleanLeaderTable()
    {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_ribbon LIMIT 1');

        //run
        return self::queryDelete($SQL);
    }
}
