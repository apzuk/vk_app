<?php

namespace library\Query;

use library\Debug;
use library\Object;
use \Connection;

class User extends Connection
{
    /**
     * @param Object $user
     * @return bool|string
     */
    public static function userSet(Object $user)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_account SET');
        $SQL
            ->sql('username = ?,')
            ->sql('avatar=?,')
            ->sql('avatar_orig = ?,')
            ->sql('gender=?,')
            ->sql('soc_id=?,')
            ->sql('online=?,')
            ->sql('city_id=?,')
            ->sql('balance=?,')
            ->sql('birth_date = ?,')
            ->sql('is_ready = ?,')
            ->sql('hash = ?');
        $SQL
            ->set($user->username)
            ->set($user->avatar)
            ->set($user->avatar_orig)
            ->setn($user->gender)
            ->set($user->soc_id)
            ->setn($user->online)
            ->setInt($user->city_id)
            ->setInt($user->balance)
            ->setn($user->bdate)
            ->setInt($user->is_ready)
            ->set($user->hash);

        $SQL->sql('ON DUPLICATE KEY UPDATE');

        $SQL
            ->sql('username = ?,')
            ->sql('avatar=?,')
            ->sql('avatar_orig = ?,')
            ->sql('gender=?,')
            ->sql('online=?,')
            ->sql('city_id=?,')
            ->sql('birth_date = ?,')
            ->sql('is_ready = ?,')
            ->sql('hash = ?');
        $SQL
            ->set($user->username)
            ->set($user->avatar)
            ->set($user->avatar_orig)
            ->setn($user->gender)
            ->setn($user->online)
            ->setInt($user->city_id)
            ->setn($user->bdate)
            ->setInt($user->is_ready)
            ->set($user->hash);

        //run
        return self::queryInsert($SQL);
    }

    public static function userCheckSid($user_id, $sid)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM user_account');
        $SQL->sql('   WHERE user_id = ? AND sid = ?')->setInt($user_id)->set($sid);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @param $user_id
     * @param $hash
     * @return bool|int
     */
    public static function userSetHash($user_id, $hash)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('hash = ?')->set($hash);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $social_id
     * @return bool|mixed
     */
    public static function userGetBySocialId($social_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.*, g.user_id AS has_group, c.city AS city_name,');
        $SQL->sql('   s.rating, NOW() < a.vip_end AS isVip, vip_end, hash');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN user_has_group g');
        $SQL->sql('    ON g.user_id = a.user_id');
        $SQL->sql('LEFT JOIN sys_cities c');
        $SQL->sql('    ON c.id = a.city_id');
        $SQL->sql('LEFT JOIN user_statistic s');
        $SQL->sql('    ON s.user_id = a.user_id');
        $SQL->sql('WHERE soc_id = ?')->setInt($social_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function userGetShort($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT username, avatar, user_id, gender, NOW() < vip_end AS isVip');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function userGetFull(
        $user_id,
        $for = null,
        $many = false,
        $blocked_info = false,
        $load_soc_id = false
    ) {
        $SQL = self::statement();

        $SQL->sql('SELECT u.user_id, u.username, u.avatar, u.gender, u.online AS is_online, u.is_fake, u.is_ready,');
        if ($load_soc_id) {
            $SQL->sql('     u.soc_id,');
        }
        $SQL->sql('    u.balance, s.game_count, s.rating, s.likes, s.dislikes, u.birth_date, s.kiss,');
        $SQL->sql(
            '    ct.city, u.city_id, ct.country_id, u.avatar_orig, ll.id AS liked, us.sponsor_user_id AS sponsor,'
        );
        $SQL->sql('    u.show_profile, u.sid, NOW() < u.vip_end AS isVip, u.cost, u.vip_end,');
        $SQL->sql('    kiss.log_id AS kissed, s.retinue_count');

        if ($blocked_info && $for) {
            $SQL->sql('    ,myBlocked.id AS myBlocked, meBlocked.id AS meBlocked');
        }

        $SQL->sql('FROM user_account u');
        $SQL->sql('LEFT JOIN user_statistic s');
        $SQL->sql('    ON s.user_id = u.user_id');
        $SQL->sql('LEFT JOIN sys_cities ct');
        $SQL->sql('    ON ct.id = u.city_id');
        $SQL->sql('LEFT JOIN user_likes ll');
        $SQL->sql('    ON (ll.target_user_id = u.user_id AND ll.user_id = ?)')->setInt($for);
        $SQL->sql('LEFT JOIN user_sponsor us');
        if ($for) {
            $SQL->sql('    ON (us.target_user_id = u.user_id AND us.sponsor_user_id = ?)')->setInt($for);
        } else {
            $SQL->sql('    ON (us.target_user_id = u.user_id)');
        }

        if ($blocked_info && $for) {
            $SQL->sql('LEFT JOIN user_blacklist myBlocked');
            $SQL->sql('   ON myBlocked.user_id = ? AND u.user_id = myBlocked.blocked_user_id')->setInt($for);

            $SQL->sql('LEFT JOIN user_blacklist meBlocked');
            $SQL->sql('   ON meBlocked.blocked_user_id = ? AND meBlocked.user_id = u.user_id')->setInt($for);
        }

        $SQL->sql('LEFT JOIN log_user_kiss kiss');

        if ($for) {
            $SQL->sql('   ON kiss.kissed_user_id = u.user_id AND kiss.user_id = ?')->setInt($for);
        } else {
            $SQL->sql('   ON kiss.kissed_user_id = u.user_id');
        }

        $SQL->sql('WHERE u.user_id IN (%s)', $SQL->uniqueIntArray($user_id));

        //run
        if ($many) {
            return self::selectMany($SQL);
        } else {
            return self::selectRow($SQL);
        }
    }

    /**
     * @param $user_id
     * @return bool|string
     */
    public static function userSetStatisticEmpty($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_statistic SET');
        $SQL->sql('user_id = ?')->setInt($user_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE user_id = user_id');

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $user_target_id
     * @return bool|string
     */
    public static function userSetLikeLog($user_id, $target_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_likes SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('target_user_id = ?')->setInt($target_user_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE user_id = user_id');

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $target_user_id
     */
    public static function userLikeExists($user_id, $target_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS(');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM user_likes');
        $SQL->sql('   WHERE user_id = ? AND target_user_id = ?');
        $SQL->setInt($user_id)->setInt($target_user_id);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @param $user_id
     * @return bool|int
     */
    public static function userSetLike($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_statistic SET');
        $SQL->sql('likes = likes + 1');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function userGetLikes($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT likes AS result');
        $SQL->sql('FROM user_statistic');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @param $city_id
     * @return bool|mixed
     */
    public static function userGetLeader($city_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.*, l.current_bid, l.message, ');
        $SQL->sql('    NOW() < a.vip_end AS isVip');
        $SQL->sql('FROM user_leader l');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('    ON a.user_id = l.user_id');
        $SQL->sql('WHERE l.city_id = ?')->setInt($city_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id
     * @param $city_id
     * @return bool|int
     */
    public static function userDeleteLeader($user_id, $city_id)
    {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_leader');
        $SQL->sql('WHERE user_id = ? AND city_id = ?');
        $SQL->setInt($user_id)->setInt($city_id);

        //run
        return self::queryDelete($SQL);
    }

    /**
     * @param $online
     * @param $limit
     * @param $offset
     * @return array|bool
     */
    public static function userGet($online, $limit, $offset)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE online = ?')->setInt($online);
        $SQL->sql('LIMIT ? OFFSET ?')->setInt($limit)->setInt($offset);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $user_id
     * @param $balance
     * @return bool|int
     */
    public static function userUpgradeBalance($user_id, $balance)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('balance = balance + ?')->setInt($balance);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function userGetBalance($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT balance');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::selectValue($SQL, 'balance');
    }

    /**
     * @param $user_id
     * @param $sid
     * @return bool|int
     */
    public static function userSetSid($user_id, $sid)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('sid = ?')->set($sid);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @param $offset
     * @param $limit
     * @return array|bool
     */
    public static function userGetRating($user_id, $offset, $limit)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT h.*, l.label');
        $SQL->sql('FROM user_rating_history h');
        $SQL->sql('LEFT JOIN sys_rating_label l');
        $SQL->sql('   ON h.rating_type = l.short_name');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);
        $SQL->sql('ORDER BY id DESC');
        $SQL->sql('LIMIT ? OFFSET ?')->setInt($limit)->setInt($offset);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $user_id
     * @param $lastId
     * @param $sid
     * @return array|bool
     */
    public static function userGetRatingUnread($user_id, $lastId)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT h.*, l.label');
        $SQL->sql('FROM user_rating_history h');
        $SQL->sql('LEFT JOIN sys_rating_label l');
        $SQL->sql('   ON h.rating_type = l.short_name');
        $SQL->sql('WHERE user_id = ? AND h.id > ?')->setInt($user_id)->setInt($lastId);
        $SQL->sql('GROUP BY id');

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $user_id
     * @param $count
     * @param $type
     * @return bool|string
     */
    public static function userSetRating($user_id, $count, $type, $rType)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_rating_history SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('count = ?,')->setInt($count);
        $SQL->sql('type = ?,')->set($type);
        $SQL->sql('rating_type = ?')->set($rType);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @return bool|mixed
     */
    public static function userGetSponsor($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_sponsor s');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('   ON s.sponsor_user_id = a.user_id');
        $SQL->sql('WHERE s.target_user_id = ?')->setInt($user_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    public static function userGetRetinue($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_sponsor s');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('   ON s.target_user_id = a.user_id');
        $SQL->sql('WHERE s.sponsor_user_id = ?')->setInt($user_id);
        $SQL->sql('   AND s.target_user_id != ?')->setInt($user_id);
        $SQL->sql('ORDER BY id DESC');

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $user_id
     * @param $vipEnd
     * @return bool|int
     */
    public static function userMakeVip($user_id, $vipEnd)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('vip_end = ?')->set($vipEnd);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryDelete($SQL);
    }

    /**
     * @param $firstname
     * @param $lastname
     * @param $birth_date
     * @param $city_id
     * @param $profileStatus
     * @param $user_id
     * @param $sid
     * @return bool|int
     */
    public static function userUpdate($firstname, $lastname, $birth_date, $city_id, $profileStatus, $user_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('username = ?,')->set($firstname . ' ' . $lastname);
        $SQL->sql('birth_date = ?,')->set($birth_date);
        $SQL->sql('city_id = ?,')->setInt($city_id);
        $SQL->sql('show_profile = ?')->set($profileStatus);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @return bool|int
     */
    public static function userSetLastVisitDate($user_id, $visits, $hash = null)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('last_visit_date = NOW(),');
        if ($hash) {
            $SQL->sql('hash = ?,')->set($hash);
        }
        $SQL->sql('continuously_visit = ?,')->setInt($visits);
        $SQL->sql('is_fake = 0');

        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $color
     * @param $visibility
     * @param $user_id
     * @param $sid
     * @return bool|int
     */
    public static function userSetSetting($color, $visibility, $user_id, $sid)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('color = ?,')->setInt($color);
        $SQL->sql('visibility = ?')->setInt($visibility);
        $SQL->sql('WHERE user_id = ? AND sid = ?')->setInt($user_id)->setInt($sid);

        // run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_soc_id
     * @return bool
     */
    public static function userGetInvitationsFromSocId($user_soc_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT invitations, user_id, online');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE soc_id = ?')->set($user_soc_id);

        //run
        return self::selectRow($SQL, 'result');
    }

    /**
     * @param $user_soc_id
     * @param $invitation
     * @return bool|int
     */
    public static function userSetInvitations($user_soc_id, $invitation)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('invitations = ?')->setInt($invitation);
        $SQL->sql('WHERE soc_id = ?')->set($user_soc_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $hash
     * @return bool|mixed
     */
    public static function userGetByHash($hash)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.*, s.rating');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN user_statistic s');
        $SQL->sql('    ON s.user_id = a.user_id');
        $SQL->sql('WHERE hash = ?')->set($hash);

        // run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id_1
     * @param $user_id_2
     * @return bool|int
     */
    public static function userSetReferral($user_id_1, $user_id_2)
    {
        $SQL = self::statement();
        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('ref_id = ?')->setInt($user_id_2);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id_1);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @return bool|int
     */
    public static function userUnsetReferral($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('ref_id = null');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @param $params
     * @return bool|int
     */
    public static function userSetFromParams($user_id, $params)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');

        foreach ($params as $col_name => $col_value) {
            $semicolon = '';
            if (next($params)) {
                $semicolon = ',';
            }

            $SQL->sql($col_name . '=?' . $semicolon)->set($col_value);
        }

        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @param $blocked_user_id
     * @return bool|string
     */
    public static function userAddBlock($user_id, $blocked_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_blacklist SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('blocked_user_id = ?')->setInt($blocked_user_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $target_user_id
     * @return bool|string
     */
    public static function userBecomeSponsor($user_id, $target_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_sponsor SET');
        $SQL->sql('sponsor_user_id = ?,')->setInt($user_id);
        $SQL->sql('target_user_id = ?')->setInt($target_user_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('sponsor_user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $cost
     * @return bool|int
     */
    public static function userSetNewCost($user_id, $cost)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('cost = ?')->setInt($cost);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @param $unblock_user_id
     * @return bool|int
     */
    public static function userRemoveBlock($user_id, $unblock_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM user_blacklist');
        $SQL->sql('WHERE user_id = ? AND blocked_user_id = ?');
        $SQL->setInt($user_id)->setInt($unblock_user_id);

        //run
        return self::queryDelete($SQL);
    }

    /**
     * @param $gender
     * @param $city_id
     * @param $userIds
     * @return bool|mixed
     */
    public static function userGetRandomForChosen($gender, $city_id, $userIds)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *, NOW() < vip_end AS isVip, a.city_id, city AS city_name');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN sys_cities ON id=a.city_id');
        $SQL->sql('WHERE is_ready = 1 /*AND a.user_id = 2133 */ AND');
        if ($userIds) {
            $SQL->sql('     user_id NOT IN (%s) AND ', $SQL->uniqueIntArray($userIds));
        }
        $SQL->sql('         (gender = ?)')->setInt($gender);
        if ($city_id) {
            $SQL->sql('     AND (city_id = ?)')->setInt($city_id);
        }
        $SQL->sql('ORDER BY RAND() DESC');
        $SQL->sql('LIMIT 1');

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $avatar
     * @param $avatar_orig
     * @param $user_id
     * @return bool|int
     */
    public static function userSetAvatar($avatar, $avatar_orig, $user_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('avatar = ?,')->set($avatar);
        $SQL->sql('avatar_orig = ?')->set($avatar_orig);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //rnu
        return self::queryUpdate($SQL);

    }

    /**
     * @return array|bool
     */
    public static function userGetRandom()
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.user_id, a.username, avatar, avatar_orig, gender, birth_date, a.color, ');
        $SQL->sql('     NOW() < a.vip_end AS isVip, c.city AS city_name, c.id AS city_id, s.*');
        $SQL->sql('FROM user_account a');
        $SQL->sql('LEFT JOIN sys_cities c');
        $SQL->sql('   ON a.city_id = c.id');
        $SQL->sql('LEFT JOIN user_statistic s');
        $SQL->sql('    ON s.user_id = a.user_id');
        $SQL->sql('WHERE a.is_ready = 1');
        $SQL->sql('ORDER BY RAND() LIMIT 30');

        //run
        return self::selectMany($SQL);
    }
}