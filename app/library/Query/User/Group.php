<?php

namespace library\Query\User;

class Group extends \Connection
{
    /**
     * @param $group_id
     * @param $user_id
     * @return bool|mixed
     */
    public static function groupHas($group_id, $user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM user_has_group');
        $SQL->sql('WHERE group_id = ? AND')->set($group_id);
        $SQL->sql('   user_id = ?')->setInt($user_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $group_id
     * @param $user_id
     * @return bool|string
     */
    public static function groupSet($group_id, $user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_has_group SET');
        $SQL->sql('group_id = ?, user_id = ?');
        $SQL->set($group_id)->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

}