<?php

namespace library\Query\User;

class Notification extends \Connection
{
    /**
     * @param $user_id
     * @param $offset
     * @param $limit
     * @return array|bool
     */
    public static function notifGet($user_id, $offset, $limit)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.user_id AS u_id, a.gender, a.username, a.avatar, n.id AS notif_id, n.tmpl_id,');
        $SQL->sql('    n.data, n.create_ts, t.tmpl, t.name, n.status, n.owner_user_id, n.user_id');
        $SQL->sql('FROM notification n');
        $SQL->sql('LEFT JOIN notification_tmpl t');
        $SQL->sql('    ON t.tmpl_id = n.tmpl_id');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('    ON n.owner_user_id = a.user_id');
        $SQL->sql('WHERE n.user_id = ? AND n.create_ts <= NOW()')->setInt($user_id);
        $SQL->sql('ORDER BY id DESC');
        $SQL->sql('LIMIT ? OFFSET ?')->setInt($limit)->setInt($offset);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $notif_id
     * @param $user_id
     * @param $sid
     * @return bool|int
     */
    public static function notifDelete($notif_id, $user_id, $sid)
    {
        $SQL = self::statement();

        $SQL->sql('DELETE n.*');
        $SQL->sql('FROM notification n');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('   ON a.user_id = n.user_id');
        $SQL->sql('WHERE a.user_id = ? AND a.sid = ? AND n.id = ?');
        $SQL->setInt($user_id)->set($sid)->setInt($notif_id);

        //run
        return self::queryDelete($SQL);
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function notifGetUnreadCount($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM notification n');
        $SQL->sql('WHERE user_id = ? AND')->setInt($user_id);
        $SQL->sql('   status = "pending" AND n.create_ts <= NOW()');

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @param $user_id
     * @param $lastId
     * @param $sid
     * @return array|bool
     */
    public static function notifGetLastUnread($user_id, $lastId, $sid)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT a.user_id AS u_id, a.gender, a.username, a.avatar, n.id AS notif_id,');
        $SQL->sql('    n.data, n.create_ts, t.tmpl, t.name, n.status, n.owner_user_id, n.user_id');
        $SQL->sql('FROM notification n');
        $SQL->sql('LEFT JOIN notification_tmpl t');
        $SQL->sql('    ON t.tmpl_id = n.tmpl_id');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('    ON n.owner_user_id = a.user_id');
        $SQL->sql('WHERE n.user_id = ? AND id > ? AND n.create_ts <= NOW()')->setInt($user_id)->setInt($lastId);
        $SQL->sql('GROUP BY id');

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $tmpl_id
     * @param $data
     * @param $user_id
     * @param $owner_user_id
     * @param $create_ts
     * @return bool|string
     */
    public static function notifSet($tmpl_id, $data, $user_id, $owner_user_id, $create_ts)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO notification SET');
        $SQL->sql('tmpl_id = ?,')->setInt($tmpl_id);
        $SQL->sql('data = ?,')->set($data);
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('owner_user_id = ?,')->setInt($owner_user_id);
        $SQL->sql('create_ts = ?')->set($create_ts);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $tmpl_id
     * @param $user_id
     * @param $from_user_id
     * @return bool|mixed
     */
    public static function notifHasUser($tmpl_id, $user_id, $from_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('   SELECT id');
        $SQL->sql('   FROM notification');
        $SQL->sql('   WHERE tmpl_id = ? AND')->setInt($tmpl_id);
        $SQL->sql('     user_id = ? AND owner_user_id = ?')->setInt($user_id)->setInt($from_user_id);
        $SQL->sql(') AS result');

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $tmpl_id
     * @param $userId
     * @return bool|int
     */
    public static function notifDeletePending($tmpl_id, $userId)
    {
        $SQL = self::statement();

        $SQL->sql('DELETE FROM notification');
        $SQL->sql('WHERE tmpl_id = ? AND user_id = ? AND')->setInt($tmpl_id)->setInt($userId);
        $SQL->sql('   status="pending" AND DATE(create_ts) > DATE(NOW())');

        //run
        return self::queryDelete($SQL);
    }
}