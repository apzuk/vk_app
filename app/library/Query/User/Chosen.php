<?php

namespace library\Query\User;

use library\Cache;

class Chosen extends \Connection
{
    /**
     * @return array|bool
     */
    public static function chosenTypes()
    {
        if ($types = Cache::instance()->get('chosenTypes')) {
            return unserialize($types);
        }

        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM chosen_type');

        $types = self::selectNested($SQL, 'id');
        Cache::instance()->set('chosenTypes', serialize($types));

        //run
        return $types;
    }

    /**
     * @param $user_id
     * @param $chosen_user_id
     * @param $type_id
     * @return bool|string
     */
    public static function chosenSet($user_id, $chosen_user_id, $type_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO chosen SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('chosen_user_id = ?,')->setInt($chosen_user_id);
        $SQL->sql('chosen_type_id = ?')->setnInt($type_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    public static function chosenGetUser($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT chosen_user_id');
        $SQL->sql('FROM chosen');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::selectNested($SQL, 'chosen_user_id');
    }
} 