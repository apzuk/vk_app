<?php
namespace library\Query\User;

class Ribbon extends \Connection
{
    /**
     * @param $user_id
     * @param $comment
     * @return bool|string
     */
    public static function ribbonSet($user_id, $comment)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO user_ribbon SET');
        $SQL->sql('user_id = ?, comment = ?');
        $SQL->setInt($user_id)->set($comment);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $limit
     * @param $offset
     * @return bool|mixed
     */
    public static function ribbonGet($limit, $offset)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT r.comment, u.avatar, u.username, u.gender');
        $SQL->sql('FROM user_ribbon r');
        $SQL->sql('LEFT JOIN user_account u');
        $SQL->sql('    ON u.user_id = r.user_id');
        $SQL->sql('WHERE LIMIT ? OFFSET ?')->setInt($limit)->setInt($offset);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @return bool|int
     */
    public static function ribbonClean()
    {
        $SQL = self::statement();

        $SQL->sql('CALL clearRibbonTable();');

        //run
        return self::queryDelete($SQL);
    }
}