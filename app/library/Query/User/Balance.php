<?php

namespace library\Query\User;

class Balance extends \Connection
{
    /**
     * @param $user_id
     * @return bool
     */
    public static function balanceGetUser($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT balance');
        $SQL->sql('FROM user_account');
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::selectValue($SQL, 'balance');
    }

    /**
     * @param $user_id
     * @return bool|int
     */
    public static function balanceSetUser($user_id, $balance)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('balance = balance+?')->setInt($balance);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $user_id
     * @param $balance
     * @return bool|int
     */
    public static function balanceWithdraw($user_id, $balance)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE user_account SET');
        $SQL->sql('balance = balance - ?')->setInt($balance);
        $SQL->sql('WHERE user_id = ?')->setInt($user_id);
        $SQL->sql('    AND balance >= ?')->setInt($balance);

        //run
        return self::queryUpdate($SQL);
    }
}