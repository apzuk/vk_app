<?php

namespace library\Query\App;

class Notif extends \Connection
{
    /**
     * @return array|bool
     */
    public static function getNotifs()
    {
        $SQL = self::statement();

        $SQL->sql('SELECT log_id,a.soc_id');
        $SQL->sql('FROM log_vk_notif n');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('    ON a.user_id = n.user_id');
        $SQL->sql('WHERE `date` = DATE(NOW()) AND n.status="new"');
        $SQL->sql('GROUP BY n.user_id, n.`date`');
        $SQL->sql('LIMIT 100');

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $log_id
     * @return bool|int
     */
    public static function makeSent($log_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE log_vk_notif SET');
        $SQL->sql('status="sent"');
        $SQL->sql('WHERE log_id <= ? AND status="new"')->setInt($log_id);

        //run
        return self::queryUpdate($SQL);
    }

} 