<?php

namespace library\Query;

class IM extends \Connection
{
    /**
     * @param $user_id
     * @param array $block_conversations
     * @return array|bool
     */
    public static function imGetUserConversations(
        $user_id,
        $block_conversations = array()
    ) {
        $SQL = self::statement();

        $SQL->sql('SELECT im.*, sender.username AS senderName, sender.avatar AS senderAvatar,');
        $SQL->sql('    sender.user_id AS senderId, receiver.username AS receiverName,');
        $SQL->sql('    receiver.avatar AS receiverAvatar, receiver.user_id AS receiverId,');
        $SQL->sql('    sender.gender AS senderGender, receiver.gender AS receiverGender,');
        $SQL->sql('    sc.city AS senderCityName, rc.city AS receiverCityName,');
        $SQL->sql('    sender.birth_date AS senderAge, receiver.birth_date AS receiverAge,');
        $SQL->sql('    (SELECT COUNT(*) FROM im_message WHERE receiver_user_id = ?');
        $SQL->sql('         AND sender_user_id = im.sender_user_id AND receiver_status="unread") AS count')->setInt(
            $user_id
        );
        $SQL->sql('FROM (SELECT *, max(message_id) FROM im_message GROUP BY message_id DESC) im');
        $SQL->sql('LEFT JOIN user_account sender');
        $SQL->sql('   ON sender.user_id = im.sender_user_id');
        $SQL->sql('LEFT JOIN user_account receiver');
        $SQL->sql('   ON receiver.user_id = im.receiver_user_id');
        $SQL->sql('LEFT JOIN sys_cities  sc');
        $SQL->sql('   ON sc.id = sender.city_id');
        $SQL->sql('LEFT JOIN sys_cities rc');
        $SQL->sql('   ON rc.id = receiver.city_id');
        $SQL->sql('WHERE (sender_user_id = ?')->setInt($user_id);
        $SQL->sql('   OR receiver_user_id = ?)')->setInt($user_id);

        if ($block_conversations) {
            $SQL->sql('AND conv_id NOT IN (%s)', $SQL->uniqueArray($block_conversations));
        }

        $SQL->sql('GROUP BY conv_id');
        $SQL->sql('ORDER BY create_ts');
        $SQL->sql('LIMIT 10');

        //run
        return self::selectMany($SQL);
    }


    /**
     * @param $user_id
     * @return int
     */
    public static function imGetUserConversationsUnreadCount(
        $user_id,
        $block_conversations = array()
    ) {
        $SQL = self::statement();

        $SQL->sql('SELECT COUNT(*) AS result');
        $SQL->sql('FROM im_message');
        $SQL->sql('WHERE receiver_user_id = ? AND')->setInt($user_id);
        $SQL->sql('   receiver_status = "unread"');

        if ($block_conversations) {
            $SQL->sql('AND conv_id NOT IN (%s)', $SQL->uniqueArray($block_conversations));
        }

        //run
        return self::selectValue($SQL, 'result');
    }


    /**
     * @param $user_id
     * @param $msg_id
     * @param $type_id
     * @return bool|string
     */
    public static function imReport(
        $user_id,
        $msg_id,
        $type_id
    ) {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO im_report SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('message_id = ?,')->setInt($msg_id);
        $SQL->sql('type_id = ?')->setInt($type_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $im_id
     * @return bool
     */
    public static function imGetMessage($im_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM im_message');
        $SQL->sql('WHERE message_id = ?')->setInt($im_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    public static function imGetBlockedConversations($user_id)
    {
        $SQL = self::statement();

        $SQL->sql('(SELECT conv_id, "my" AS type');
        $SQL->sql('FROM im_message im');
        $SQL->sql('WHERE sender_user_id IN');
        $SQL->sql('      (SELECT blocked_user_id FROM user_blacklist WHERE user_id = ?))')->setInt($user_id);
        $SQL->sql('UNION');
        $SQL->sql('(SELECT conv_id, "me" AS type');
        $SQL->sql('FROM im_message im');
        $SQL->sql('WHERE sender_user_id IN');
        $SQL->sql('      (SELECT user_id FROM user_blacklist WHERE blocked_user_id = ?))')->setInt($user_id);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $conv_id
     * @param $receiver_user_id
     * @return bool|int
     */
    public static function imSetAllRead($conv_id, $receiver_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE im_message SET');
        $SQL->sql('receiver_status = "read"');
        $SQL->sql('WHERE receiver_user_id = ?')->setInt($receiver_user_id);
        $SQL->sql('    AND conv_id = ?')->set($conv_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $conv_id
     * @param $user_id
     * @param $offset
     * @return array|bool
     */
    public static function imGetMessages($conv_id, $user_id, $offset)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *, m.create_ts AS msg_date');
        $SQL->sql('FROM im_message m');
        $SQL->sql('LEFT JOIN user_account a');
        $SQL->sql('   ON a.user_id = m.sender_user_id');
        $SQL->sql('WHERE (conv_id = ?) AND ')->set($conv_id);
        $SQL->sql('  ((sender_user_id = ? AND sender_status != "remove") OR')->setInt($user_id);
        $SQL->sql('  (receiver_user_id = ? AND receiver_status != "remove"))')->setInt($user_id);
        $SQL->sql('ORDER BY message_id DESC');
        $SQL->sql('LIMIT 20 OFFSET ?')->setInt(20 * $offset);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $conv_id
     * @param $user_id_1
     * @param $user_id_2
     * @return bool|mixed
     */
    public static function imCheckConvId($conv_id, $user_id_1, $user_id_2)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM im_message');
        $SQL->sql('WHERE conv_id = ? AND')->set($conv_id);
        $SQL->sql('   ((sender_user_id = ? AND receiver_user_id = ?) OR')->setInt($user_id_1)->setInt($user_id_2);
        $SQL->sql('   (sender_user_id = ? AND receiver_user_id = ?))')->setInt($user_id_2)->setInt($user_id_1);

        //run
        return self::selectRow($SQL);
    }
}