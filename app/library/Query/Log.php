<?php

namespace library\Query;

class Log extends \Connection
{
    /**
     * @param $user_id
     * @param $city_id
     * @param $bid
     * @param $message
     * @return bool|string
     */
    public static function logSetLeader($user_id, $city_id, $bid, $message)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_leader SET');
        $SQL->sql('user_id = ?, city_id = ?, current_bid = ?, message = ?, update_date = NOW()');
        $SQL->setInt($user_id)->setInt($city_id)->setInt($bid)->set($message);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $duration
     * @param $vipCost
     * @param $user_id
     * @return bool|string
     */
    public static function logSetVip($duration, $vipCost, $user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_user_vip SET');
        $SQL->sql('duration = ?,')->setInt($duration);
        $SQL->sql('cost = ?,')->setInt($vipCost);
        $SQL->sql('user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    public static function logSetGiftLog($user_id_1, $user_id_2, $guid)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_user_gift SET');
        $SQL->sql('user_id = ?,')->setInt($user_id_1);
        $SQL->sql('gitf_from = ?,')->setInt($user_id_2);
        $SQL->sql('`date` = DATE(NOW()),');
        $SQL->sql('guid = ?')->set($guid);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->setInt($user_id_1);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $receiver_id
     * @param $votes
     * @param $bonus
     * @return bool|string
     */
    public static function logSetUserPayment($receiver_id, $votes, $bonus)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_payment SET');
        $SQL->sql('user_id = ?,')->setInt($receiver_id);
        $SQL->sql('votes = ?,')->setInt($votes);
        $SQL->sql('bonus = ?')->setInt($bonus);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $viewer_id
     * @return bool|string
     */
    public static function logSetInvitation($user_id, $viewer_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_user_invitation SET');
        $SQL->sql('user_id = ?,')->set($user_id);
        $SQL->sql('invited_user_id = ?')->set($viewer_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->set($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $user_ref_id
     * @return bool|string
     */
    public static function logSetReferral($user_id, $user_ref_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_user_ref SET');
        $SQL->sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('ref_user_id = ?')->setInt($user_ref_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->setInt($user_id);

        //run
        return self::queryInsert($SQL);
    }

    /**
     * @param $user_id
     * @param $target_user_id
     * @return bool|string
     */
    public static function logSetKiss($user_id, $target_user_id)
    {
        $SQL = self::statement();

        $SQL->sql('INSERT INTO log_user_kiss SET');
        $SQL->Sql('user_id = ?,')->setInt($user_id);
        $SQL->sql('kissed_user_id = ?')->setInt($target_user_id);
        $SQL->sql('ON DUPLICATE KEY UPDATE');
        $SQL->sql('user_id = ?')->set($user_id);

        //run
        return self::queryInsert($SQL);
    }
}