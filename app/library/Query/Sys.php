<?php

namespace library\Query;

class Sys extends \Connection
{
    /**
     * @param $name
     * @return bool
     */
    public static function sysGetCityIdByName($name)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT id');
        $SQL->sql('FROM sys_cities');
        $SQL->sql('WHERE LOWER(city) = LOWER(?)')->set($name);

        //run
        return self::selectValue($SQL, 'id');
    }

    /**
     * @param $country_id
     * @return array|bool
     */
    public static function sysGetCitiesByCountry($country_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM sys_cities');
        $SQL->sql('WHERE country_id = ? AND ISNULL(rawoffset)')->setInt($country_id);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $term
     * @param int $limit
     * @return array|bool
     */
    public static function sysGetCities($country_id, $term, $limit = 18)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT c.id AS city_id, c.city, c.region, x.name AS country_name');
        $SQL->sql('FROM sys_countries x');
        $SQL->sql('LEFT JOIN sys_cities c');
        $SQL->sql('    ON x.country_id = c.country_id');
        $SQL->sql('WHERE x.country_id = ?')->setInt($country_id);
        $SQL->sql('     AND c.city LIKE CONCAT("%", ? ,"%")')->set($term);
        $SQL->sql('ORDER BY c.important=1 DESC');
        $SQL->sql('LIMIT ?')->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $term
     * @param int $limit
     * @return array|bool
     */
    public static function sysFindCities($term, $limit = 5)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT c.id AS city_id, c.city, c.region, x.name AS country_name');
        $SQL->sql('FROM sys_countries x');
        $SQL->sql('LEFT JOIN sys_cities c');
        $SQL->sql('    ON x.country_id = c.country_id');
        $SQL->sql('WHERE (c.city LIKE CONCAT("%", ?, "%") OR FALSE = ?)')->set($term)->setBoolean($term);
        $SQL->sql('ORDER BY c.important');
        $SQL->sql('LIMIT ?')->setInt($limit);

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $cty_id
     * @return bool
     */
    public static function sysIsValidCity($cty_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT EXISTS (');
        $SQL->sql('   SELECT *');
        $SQL->sql('   FROM sys_cities');
        $SQL->sql('   WHERE id = ?')->setInt($cty_id);
        $SQL->sql(') AS result');

        //run
        return self::selectValue($SQL, 'result');
    }

    /**
     * @return array|bool
     */
    public static function sysGetCountries()
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM vk_country');

        //run
        return self::selectMany($SQL);
    }

    /**
     * @param $city_id
     * @param $offset
     * @return bool|int
     */
    public static function sysSetCityTimezone($city_id, $offset)
    {
        $SQL = self::statement();

        $SQL->sql('UPDATE sys_cities SET');
        $SQL->sql('rawoffset = ?')->setInt($offset);
        $SQL->sql('WHERE id = ?')->setInt($city_id);

        //run
        return self::queryUpdate($SQL);
    }

    /**
     * @param $short
     * @return bool|mixed
     */
    public static function sysGetCountryByShort($short)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM sys_countries');
        $SQL->sql('WHERE short = ?')->set($short);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @param $country_id
     * @return bool|mixed
     */
    public static function sysGetCountry($country_id)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT *');
        $SQL->sql('FROM sys_countries');
        $SQL->sql('WHERE id = ?')->set($country_id);

        //run
        return self::selectRow($SQL);
    }

    /**
     * @return bool
     */
    public static function sysGetRandomQuestion()
    {
        $SQL = self::statement();

        $SQL->sql('SELECT question');
        $SQL->sql('FROM sys_questions');
        $SQL->sql('ORDER BY RAND()');

        //run
        return self::selectValue($SQL, 'question');
    }

    /**
     * @param $user_id
     * @param $key
     * @return bool
     */
    public static function sysGetValueByKey($user_id, $key)
    {
        $SQL = self::statement();

        $SQL->sql('SELECT value');
        $SQL->sql('FROM sys_tmpl_key');
        $SQL->sql('WHERE user_id = ? AND `key`=?');
        $SQL->setInt($user_id)->set($key);

        //run
        return self::selectValue($SQL, 'value');
    }
}