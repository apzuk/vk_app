<?php

namespace library;

class String
{
    /**
     * @param $string
     * @param $replacements
     * @return mixed
     */
    public static function merge($string, $replacements)
    {
        return preg_replace_callback(
            '/\[([^\]]+)\]/',
            function ($match) use ($replacements) {
                // tag being replaced
                $tag = $match[1];

                // matched tag is in our replacement array, use it
                if (isset ($replacements[$tag])) {
                    return $replacements[$tag];
                }

                // no match in replacement array, leave tag as-is
                return '[' . $tag . ']';
            },
            $string
        );
    }

    /**
     * @param $string
     * @param $len
     * @return string
     */
    public static function cutUtf8($string, $len)
    {
        return mb_substr($string, 0, $len, mb_detect_encoding($string));
    }
}