<?php

namespace library;

use library\Query\User\Notification;
use library\Query\User;

class Chosen
{
    private static $types;

    public static function init()
    {

        if (self::$types) {
            return self::$types;
        }

        return self::$types = \library\Query\User\Chosen::chosenTypes();
    }

    /**
     * @param $user_id
     * @param $chosen_user_id
     * @param $chosen
     * @param $flag
     * @return bool
     */
    public static function setChosen($user_id, $chosen_user_id, $chosen)
    {
        self :: init();

        $result = \library\Query\User\Chosen::chosenSet($user_id, $chosen_user_id, $chosen);
        if (!$result) {
            return false;
        }

        if ($chosen) {
            // Set notification
            $type = self::types($chosen);
            $data = array(
                'label' => $type['title'],
                'appeal' => ''
            );
            Notification::notifSet(18, json_encode($data), $chosen_user_id, $user_id, Date::now());
        }

        // Update cache
        self::appendChosenUserId($user_id, $chosen_user_id);

        return true;
    }

    /**
     * @param null $key
     * @return mixed
     */
    public static function types($key = null)
    {
        self::init();

        if ($key && isset(self::$types[$key])) {
            return self::$types[$key];
        }

        return self::$types;
    }

    /*
    public static function appeal($type_id)
    {

    }
*/

    /**
     * @param $u
     * @return bool|Object|mixed
     */
    public static function getRandomUser($u)
    {
        $cache_key = 'chosen_user_ids_for_' . $u->user_id;

        if (!($users_ids = Cache::instance()->get($cache_key))) {
            $chosen = User\Chosen::chosenGetUser($u->user_id);
            $users_ids = array_keys($chosen);
            Cache::instance()->set($cache_key, serialize($users_ids), 60 * 60 * 3);
            $fromCache = false;
        } else {
            $users_ids = unserialize($users_ids);
            $fromCache = true;
        }

        $found = User::userGetRandomForChosen(($u->gender == 1 ? 2 : 1), $u->city_id, $users_ids);
        if (!$found) {
            $found = User::userGetRandomForChosen(($u->gender == 1 ? 2 : 1), null, $users_ids);
        }
        if (!$found) {
            $found = User::userGetRandomForChosen(($u->gender == 1 ? 2 : 1), null, null);
        }

        if (!$found) {
            return array();
        }

        // Found user!
        return $user = array(
            'user_id' => (int)$found['user_id'],
            'avatar' => $found['avatar'],
            'username' => $found['username'],
            'vip' => (int)$found['isvip'],
            'color' => (int) $found['color'],
            'city_id' => (int) $found['city_id'],
            'city_name' => $found['city_name'],
            'birth_date' => $found['birth_date'],
            'fromCache' => $fromCache
        );
    }

    /**
     * @param $user_id
     * @return bool|mixed
     */
    private static function appendChosenUserId($user_id, $chosen_user_id)
    {
        $cache_key = 'chosen_user_ids_for_' . $user_id;

        $users_ids = Cache::instance()->get($cache_key);

        $users_ids = unserialize($users_ids);
        $users_ids[] = $chosen_user_id;

        return Cache::instance()->set($cache_key, serialize($users_ids), 60 * 60 * 3);
    }
}