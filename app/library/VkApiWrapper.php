<?php

namespace library;

class VkApiWrapper
{
    public static $vk_id = 3761544;
    public static $vk_secret = 'mtiFi4PQUs6mN4cNrTne';

    /**
     * @param $user_id
     * @return Object
     */
    public static function apiGetUser($user_id)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $params = array(
            'user_ids' => $user_id,
            'fields' => 'nickname, screen_name, sex, bdate, city, country, timezone, photo_100, photo_200_orig, city'
        );

        $uData = $vk->api('users.get', $params);
        return new Object($uData['response'][0]);
    }

    /**
     * @param $group_id
     * @param $user_id
     * @return bool
     */
    public static function apiHasUserGroup($group_id, $user_id)
    {
        //TODO: make api call!
        return true;
    }

    /**
     * @param $city_id
     * @return bool|Object
     */
    public static function apiGetCityById($city_id)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $params = array(
            'city_ids' => $city_id
        );

        $uData = $vk->api('database.getCitiesById', $params);
        return isset($uData['response'][0]) ? new Object($uData['response'][0]) : false;
    }

    /**
     * @param $c_id
     * @return mixed
     */
    public static function getImportantCities($c_id)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $params = array(
            'country_id' => $c_id,
            'count' => 1000,
            'need_all' => 0
        );

        $uData = $vk->api('database.getCities', $params);
        return $uData;
    }

    /**
     * @param $ids
     * @param $msg
     */
    public static function apiNotify($ids, $msg)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $accessToken = $vk->accessToken();

        $data = $vk->api(
            'secure.sendNotification',
            array(
                'user_ids' => $ids,
                'message' => $msg,
                'client_secret' => $accessToken
            )
        );
    }

    /**
     * @param $result
     * @return mixed
     */
    public static function apiSavePhoto($result)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $uData = $vk->api('photos.saveProfilePhoto', $result);
        return $uData;
    }

    /**
     * @param $user_id
     * @param $offset
     * @param string $term
     * @return mixed
     */
    public static function apiGetFriends($user_id, $offset, $term = '')
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $uData = $vk->api(
            'friends.get',
            array(
                'user_id' => $user_id,
                'offset' => $offset * 1000,
                'fields' => 'user_id, photo_50, sex, deactivated'
            )
        );

        return $uData;
    }

    /**
     * @param $user_id
     * @param $offset
     * @return mixed
     */
    public static function apiGetAppFriends($user_id, $offset)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        $uData = $vk->api(
            'friends.get',
            array(
                'user_id' => $user_id,
                'offset' => $offset * 1000,
                'fields' => 'user_id, photo_50'
            )
        );

        return $uData;
    }
}