<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramp
 * Date: 7/12/13
 * Time: 10:44 PM
 * To change this template use File | Settings | File Templates.
 */

namespace library;

class IFrameParams
{
    public static function get($key, $default = null)
    {
        return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
    }
}