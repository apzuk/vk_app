<?php

namespace library;

class Request
{
    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public static function get($key, $default = null)
    {
        return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
    }

    /**
     * @param $key
     * @param bool $default
     * @return bool
     */
    public static function getbool($key, $default = false)
    {
        $v = self :: get($key, $default);

        // we can only test scalar values
        if (!is_scalar($v)) {
            return (boolean)$v;
        }

        // case-insensitive checking
        $v = strtolower($v);

        // clearly it is TRUE
        if ($v == 'yes' || $v == 'true' || $v == 't' || $v == 'on' || $v === true) {
            return true;
        }

        // clearly it is FALSE
        if ($v == 'no' || $v == 'false' || $v == 'f' || $v == 'off' || $v === false) {
            return false;
        }

        // its not clear, just cast it and be done with it
        return (boolean)$v;
    }
}