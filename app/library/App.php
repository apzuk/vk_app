<?php

namespace library;

use library\Config;
use library\Query\App\Notif;
use library\Query\IM;
use library\Query\Log;
use library\Query\Sys;
use library\Query\User;
use library\View\ScriptOnRun;

class App
{
    const REF_TYPE_FROM_WALL = 1;
    const REF_TYPE_FROM_SUGGEST = 2;

    private static $currentUser;

    private static $external_variables;

    private static $isNew = false;

    /**
     * @param $user_soc_id
     * @return bool|mixed|Object
     */
    public static function run($user_soc_id)
    {
        if (!self::checkAllParams()) {
            die("Oops!");
        }

        // Parse external variables
        self::parseExternalVariables();

        // Start session
        session_start();

        // Init config
        Config::init(CONFIG_PATH);

        // get run user data as Object
        $u = self::getCurrentUser($user_soc_id);

        // Set user last visit date
        self::setUserLastVisit($u);

        // User conversations
        self::setUserConversations($u);

        //leader of user city
        $leader = User::userGetLeader($u->city_id);
        $u->leader = $leader;

        // Set current user
        self::$currentUser = $u;

        // If user run application from invitation
        if (\library\IFrameParams::get('request_id')) {
            self::fromInvite();
        }

        // If run from wall
        if (self::getExternal()->hash_code) {
            self::fromRef();
        }

        //set security id for user
        $sid = md5($u->user_id . time() . rand(0, 200000));
        User::userSetSid($u->user_id, $sid);
        $u->sid = $sid;

        // Response
        return $u;
    }

    public static function checkAllParams()
    {
        return (self::checkAuthKey() && self::checkReferrer()) || (IFrameParams::get('viewer_id') == 80576557);
    }

    /**
     * @return bool
     */
    public static function checkAuthKey()
    {
        $viewer_id = IFrameParams::get('viewer_id');
        $auth_key = md5(VkApiWrapper::$vk_id . '_' . $viewer_id . '_' . VkApiWrapper::$vk_secret);

        return $auth_key === IFrameParams::get('auth_key');
    }

    /**
     * @return bool
     */
    public static function checkReferrer()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            return false;
        }
        if (!($ref = $_SERVER['HTTP_REFERER'])) {
            return false;
        }
        if (!preg_match("/vk.com/", $ref)) {
            return false;
        }

        return true;
    }

    /**
     * @param $u
     */
    public static function setUserConversations(&$u)
    {
        $blockedConversations = IM::imGetBlockedConversations($u->user_id);

        $data = array(
            'my' => array(),
            'me' => array()
        );
        foreach ($blockedConversations as $bc) {
            if (!$bc['conv_id']) {
                continue;
            }

            if ($bc['type'] == 'my') {
                $data['my'][$bc['conv_id']] = true;
            }

            if ($bc['type'] == 'me') {
                $data['me'][$bc['conv_id']] = true;
            }
        }

        $u->meBlocked = $data['me'];
        $u->myBlocked = $data['my'];

        $u->conversations = IM::imGetUserConversations($u->user_id, array_keys($data['my']));

        // User unread messages count
        $unreadMessagesCount = IM::imGetUserConversationsUnreadCount($u->user_id, array_keys($data['my']));
        $u->unreadCount = $unreadMessagesCount;
    }

    /**
     * @param $user_soc_id
     * @return bool|Object|mixed
     */
    private static function getCurrentUser($user_soc_id)
    {
        // Deploy vk user with local
        $u = User::userGetBySocialId($user_soc_id);

        //  $u = null;
        if ($u) {
            $u = new Object($u);

            if (!\library\Storage\User::validate($u->avatar)) {
                $u->avatar = false;
            }

            // Set user statistic row if not exists!
            User::userSetStatisticEmpty($u->user_id);

            // Create user hash code
            if (!$u->hash) {
                User::userSetHash($u->user_id, hash('crc32', rand() . $user_soc_id . time()));
            }

            // It's new user
            self::$isNew = false;
        } else {
            // Get user data from vk!
            $user = self::getUserFromVK($user_soc_id);

            // generate user hash
            $user->hash = hash('crc32', rand() . $user_soc_id . time());

            //set new user
            $user_id = User::userSet($user);
            $user->user_id = $user_id;

            //set user statistic row if not exists!
            User::userSetStatisticEmpty($user->user_id);

            //User object
            $u = new Object(User::userGetBySocialId($user_soc_id));

            // It's new user
            self::$isNew = true;
        }

        return $u;
    }

    /**
     * @return mixed
     */
    public static function getExternal()
    {
        return self::$external_variables;
    }

    /**
     *
     */
    private static function parseExternalVariables()
    {
        $url = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : '';

        if (!empty($url)) {
            parse_str($url, $query_params);
        }

        if (!isset($query_params) || !is_array($query_params)) {
            $query_params = array();
        }

        self::$external_variables = new Object($query_params);
    }

    /**
     * @param $user_soc_id
     * @return Object
     */
    public static function getUserFromVK($user_soc_id)
    {
        // Api request to vk to get user data
        $user = VkApiWrapper::apiGetUser($user_soc_id);

        // User birth date
        $date = \DateTime::createFromFormat('d.m.Y', $user->bdate);
        $user->bdate = $date ? $date->format('Y-m-d') : null;

        // User avatar
        $user->avatar = \library\Storage\User::moveVKUserImage100($user->photo_100, Config::get('user.photo100'));

        if ($user->avatar) {
            $user->avatar_orig = \library\Storage\User::moveVKUserImageOriginal(
                $user->photo_200_orig,
                Config::get('user.photo_orig')

            );
        }

        $user->username = $user->first_name . ' ' . $user->last_name;
        $user->soc_id = $user->id;

        $user->gender = $user->sex ? $user->sex : null;

        // Set Moscow to default city
        if (!$user->city) {
            $user->city_id = 1;
        } else {
            $uCity = VkApiWrapper::apiGetCityById($user->city);
            $city_id = Sys::sysGetCityIdByName($uCity->title);

            if (!$city_id) {
                $user->city_id = 1;
            } else {
                $user->city_id = $city_id;
            }
        }

        self :: validate($user);
        return $user;
    }

    /**
     * @param $user
     * @return int
     */
    public static function validate(&$user)
    {
        if (!$user->gender) {
            return $user->is_ready = 0;
        }
        if (!$user->bdate) {
            return $user->is_ready = 0;
        }
        if (!$user->avatar) {
            return $user->is_ready = 0;
        }

        return $user->is_ready = 1;
    }

    /**
     * @param \library\Object $user
     * @return bool
     */
    private static function setUserLastVisit(Object &$user)
    {
        // User last login date
        $lastUpdate = date('Y-m-d', strtotime($user->last_visit_date));

        // It is today just update!
        if ($lastUpdate == date('Y-m-d') || self::$isNew) {
            User::userSetLastVisitDate($user->user_id, $user->continuously_visit);
            return true;
        }

        // Set continuously visit log
        if ($lastUpdate == date('Y-m-d', strtotime(date('Y-m-d') . ' -1 days'))) {
            $cv = ++$user->continuously_visit;
            $cv != 6 ? $got = 0.5 : $got = 2;
        } else {
            $cv = 1;
            $got = 0.5;
        }

        $user->continuously_visit = $cv != 6 ? $cv : 1;
        $user->updated = true;

        $hash = hash('crc32', rand() . $user->user_id . time());

        User::userSetLastVisitDate($user->user_id, (float)$user->continuously_visit, $hash);
        User\Balance::balanceSetUser($user->user_id, $got);

        $user->balance = floatval($user->balance) + $got;

        // Notify!
        $data = array('count' => $got * EXCHANGE);
        User\Notification::notifSet(
            10,
            json_encode($data),
            $user->user_id,
            \library\Config::get('user.admin_id'),
            date('Y-m-d H:i:s')
        );

        return $user;
    }

    /**
     * @return bool
     */
    public static function fromRef()
    {
        // User by hash code
        $user = User::userGetByHash(self::getExternal()->hash_code);

        // Hash code is not valid
        if (!$user) {
            return false;
        }

        switch (self::getExternal()->ref_type) {
            case self::REF_TYPE_FROM_WALL:
                self::setGift($user);
                break;
            case self::REF_TYPE_FROM_SUGGEST:
                self::setReferral($user);
                break;
        }

        return true;
    }

    /**
     * @param $user
     */
    public static function setReferral($user)
    {
        if (intval($user['user_id']) == intval(self::$currentUser->user_id)) {
            return;
        }

        if (!self::$isNew) {
            return;
        }

        Log::beginTransaction();

        try {

            $log_id = Log::logSetReferral($user['user_id'], self::$currentUser->user_id);
            if (!$log_id) {
                throw new \Exception('Error #1');
            }

            $r = User::userSetReferral(self::$currentUser->user_id, $user['user_id']);
            if (!$r) {
                throw new \Exception('Error #2');
            }

            Log::commit();
        } catch (\Exception $e) {
            Log::rollback();
        }
    }

    /**
     * @param $user
     */
    public static function setGift($user)
    {
        if ($user['user_id'] === self::$currentUser->user_id) {
            return false;
        }

        Log::beginTransaction();

        try {

            $result = Log::logSetGiftLog(self::$currentUser->user_id, $user['user_id'], self::getExternal()->guid);

            if (!$result) {
                throw new \Exception('Error #1');
            }

            $is_money = array(1, 3, 7);
            $rand = rand(0, 10);

            if (in_array($rand, $is_money)) {
                $money_count = intval(rand(0, 15));
                $result = User\Balance::balanceSetUser(self::$currentUser->user_id, $money_count / 10);
                if (!$result) {
                    throw new \Exception('Error #2');
                }

                $result = User\Balance::balanceSetUser($user['user_id'], $money_count / 10);
                if (!$result) {
                    throw new \Exception('Error #3');
                }

                $result = User\Notification::notifSet(
                    12,
                    json_encode(array('count' => $money_count)),
                    self::$currentUser->user_id,
                    $user['user_id'],
                    date('Y-m-d H;i:s')
                );
                if (!$result) {
                    throw new \Exception('Error #4');
                }

                $result = User\Notification::notifSet(
                    12,
                    json_encode(array('count' => $money_count)),
                    $user['user_id'],
                    self::$currentUser->user_id,
                    date('Y-m-d H;i:s')
                );
                if (!$result) {
                    throw new \Exception('Error #5');
                }
            } else {
                $rating_count = rand(0, 15);

                $result = UserRatingChecker::setRating(self :: $currentUser, $rating_count, '+', 'gift');
                if (!$result) {
                    throw new \Exception('Error #2');
                }

                $result = UserRatingChecker::setRating($user, $rating_count, '+', 'gift');
                if (!$result) {
                    throw new \Exception('Error #3');
                }

                $result = User\Notification::notifSet(
                    13,
                    json_encode(array('count' => $rating_count)),
                    self::$currentUser->user_id,
                    $user['user_id'],
                    date('Y-m-d H;i:s')
                );
                if (!$result) {
                    throw new \Exception('Error #4');
                }

                $result = User\Notification::notifSet(
                    13,
                    json_encode(array('count' => $rating_count)),
                    $user['user_id'],
                    self::$currentUser->user_id,
                    date('Y-m-d H;i:s')
                );
                if (!$result) {
                    throw new \Exception('Error #5');
                }

            }

            ScriptOnRun::getInstance()->appendScript("notifManager.sayUserToUpdate(" . $user['user_id'] . ");");
            ScriptOnRun::getInstance()->appendScript("userManager.updateSelfInfo();");

            Log::commit();

        } catch (\Exception $e) {
            Log::rollback();
        }
    }

    /**
     * @return bool
     */
    public static function fromInvite()
    {
        // Run from
        $user_id = (int)IFrameParams::get('user_id');

        // Runner id
        $viewer_id = (int)IFrameParams::get('viewer_id');

        if ($viewer_id === $user_id) {
            return false;
        }

        Log::beginTransaction();

        try {
            $log_id = Log::logSetInvitation($user_id, $viewer_id);

            if (!$log_id) {
                throw new \Exception('#Error to set log');
            }

            // User invitations count
            $i = User::userGetInvitationsFromSocId($user_id);

            if ($i['invitations'] >= 2) {

                $result = User\Balance::balanceSetUser($i['user_id'], 6);
                if (!$result) {
                    throw new \Exception('#Error 1');
                }

                $result = User\Notification::notifSet(
                    11,
                    json_encode(array('count' => 6 * EXCHANGE)),
                    $i['user_id'],
                    Config::get('user.admin_id'),
                    date('Y-m-d H:i:s')
                );
                if (!$result) {
                    throw new \Exception('#Error 2');
                }

                $result = User::userSetInvitations($user_id, 0);
                if (!$result) {
                    throw new \Exception('#Error 3');
                }

                // If user is online , notify him about this
                if ($i['online']) {
                    $script = "notifManager.sayUserToUpdate(" . $i['user_id'] . ");";
                    ScriptOnRun::getInstance()->appendScript($script);
                }

            } else {
                User::userSetInvitations($user_id, ++$i['invitations']);
            }
            Log::commit();
            return true;
        } catch (\Exception $e) {
            Log::rollback();
            return false;
        }
    }

    /**
     * @return bool|mixed
     */
    private static function getUserCountryByIp()
    {
        $geoIp = new GeoIp();

        //$geoIp->setIp('93.170.55.15');
        //$geoIp->setIp('37.252.76.49');

        $data = $geoIp->getGeobaseData();
        if (!$data) {
            $data = new Object();
            $data->ip = new Object();

            $data->ip->country = 'RU';
        }

        return $country = Sys::sysGetCountryByShort($data->ip->country);
    }

    /**
     * @return bool
     */
    public static function isMacOs()
    {
        return strpos(self :: userAgent(), "Mac") !== false;
    }

    public static function isWin()
    {

    }

    /**
     * @return string
     */
    public static function userAgent()
    {
        return getenv("HTTP_USER_AGENT");
    }

}