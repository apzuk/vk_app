<?php

class Game extends \library\Controller
{
    public function tapeAction()
    {
        $this->renderer->render('tape', array(), true);
    }

    public function testAction()
    {
        $this->renderer->render('test', array(), true);
    }

    public function game3Action()
    {
        $this->renderer->render('game3', array(), true);
    }

    public function game3screen1Action()
    {
        $this->renderer->render('game3screen1', array(), true);
    }

    public function game3screen2Action()
    {
        $this->renderer->render('game3screen2', array(), true);
    }

    public function game3screen3Action()
    {
        $this->renderer->render('game3screen3', array(), true);
    }

    public function game3screen4Action()
    {
        $this->renderer->render('game3screen4', array(), true);
    }

    public function canvasAction()
    {
        $this->renderer->render('canvas', array(), true);
    }

    public function vkAction()
    {
        $this->renderer->render('vk', array(), true);
    }

    public function saveCaptureAction()
    {
        $rawData = $_POST['photo'];
        $filteredData = explode(',', $rawData);

        $unencoded = base64_decode($filteredData[1]);

        // Current user id
        $user_id = \library\Request::get('user_id');

        // Generate filename
        $filename = \library\String::merge(
            '[filename].[ext]',
            array(
                'filename' => md5(rand(0, 100000) . time() . $user_id),
                'ext' => 'png'
            )
        );

        //Create the image
        $fp = fopen(\library\Config::get('msg.img_cache') . '/' . $filename, 'w');
        fwrite($fp, $unencoded);
        fclose($fp);

        // Response
        header('Content-Type: application/json');
        echo json_encode(array('filename' => $filename));
    }

    public function uploadMessageImageAction()
    {
        // Current user id
        $user_id = \library\Request::get('user_id');
        $user = $this->loadUser($user_id);

        // Is vip?!
        if (!$user->isvip) {
            die("<script>parent.userManager.vip();</script>");
        }

        // Attached photo
        $photo = isset($_FILES['photo']) ? $_FILES['photo'] : false;

        // if photo is not passed stop script execution
        if (!$photo) {
            die("<script>parent.messageManager.photoUploaded('Критическая ошибка', true);</script>");
        }

        // Make object
        $photo = new \library\Object($photo);

        // Check size
        if ($photo->size > 3 * 1024 * 1024) {
            die("<script>parent.messageManager.photoUploaded('Максимальный размер фото 3Мб', true);</script>");
        }

        // Generate filename
        $filename = \library\String::merge(
            '[filename].[ext]',
            array(
                'filename' => md5(rand(0, 100000) . time() . $user_id),
                'ext' => end(explode('.', $photo->name))
            )
        );

        // Upload full path
        $fullpath = \library\Config::get('msg.img_original') . '/' . $filename;

        // Cache file full path
        $cachefullpath = \library\Config::get('msg.img_cache') . '/' . $filename;

        // Is success moved
        $isMoved = move_uploaded_file($photo->tmp_name, $fullpath);

        if ($isMoved) {
            $i = new Imagick();
            try {
                $i->pingImage($fullpath);
                $i = new Imagick($fullpath);
            } catch (ImagickException $e) {
                unset($fullpath);
                die("<script>parent.messageManager.photoUploaded('Выбранный файл не является картинкой', true);</script>");
            }

            $w = \library\Config::get('msg.cache_size_w');
            $h = \library\Config::get('msg.cache_size_h');

            $i->resizeimage($w, $h, Imagick::FILTER_LANCZOS, 1, true);
            $i->writeimage($cachefullpath);

            echo "<script>parent.messageManager.photoUploaded('" . $filename . "');</script>";
            unset($fullpath);
        }
    }

    public function uploadPostPhotoToProfileAction()
    {
        // user id
        $user_id = \library\Request::get('user_id');

        // user sid
        $sid = \library\Request::get('sid');

        // Check sid
        $user = \library\Query\User::userGetFull($user_id);

        if (!$user) {
            // Response
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => 'invalid.user'));
            exit;
        }

        if ($user['sid'] !== $sid) {
            // Response
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => 'invalid.sid'));
            exit;
        }

        // Server url to upload
        $upload_url = \library\Request::get('upload_url');
        if (!$upload_url) {
            // Response
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => 'invalid.upload.server'));
            exit;
        }

        $type = \library\Request::get('type');
        if ($type == 1) {
            $screenshot = BASE_DIR . '/../public/img/banner/ban1.png';
        } else {
            $screenshot = BASE_DIR . '/../public/img/banner/ban2.png';
        }

        $post['photo'] = '@' . $screenshot;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_URL, $upload_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        header('Content-Type: application/json');
        echo $result;
    }

    public function runUsersAction()
    {
        $users = \library\Query\User::userGet(0, 1000, 0);

        $byGender = array(
            'male' => array(),
            'female' => array()
        );

        foreach ($users as $user) {
            if ($user['gender'] == 1) {
                $byGender['female'][] = $user;
            }
            if ($user['gender'] == 2) {
                $byGender['male'][] = $user;
            }
        }

        $this->renderer->render('list', array('users' => $byGender), true);
    }

    public function cssAction()
    {
        // Required filename
        $filename = $user_id = \library\Request::get('file');

        // fullpath
        $fullPath = BASE_DIR . '/../public/data/cache/cache-' . $filename . '-mincss';

        // Send css file
        header("Content-type: text/css");
        echo file_get_contents($fullPath);
    }
}