<?php

use \library\Session;

class Sys extends \library\Controller
{
    /**
     *  ================================================================================================
     */
    public function getCitiesAction()
    {
        // Selected country id
        $country_id = \library\Request::get('id');

        // Search string
        $term = \library\Request::get('term');

        // Cities list
        $data = \library\Query\Sys::sysGetCities($country_id, $term);

        // Prepare cities data
        foreach ($data as $d) {
            $city['value'] = $d['city_id'];
            $city['label'] = $d['city'];
            $city['desc'] = $d['region'];
            $city['additional'] = $d['country_name'];

            $cities['data'][] = $city;
        }

        // Success status
        $cities['status'] = true;

        // Response
        header('Content-Type: application/json');
        echo json_encode($cities);
    }

    /**
     *  ================================================================================================
     */
    public function getRandomQuestionAction()
    {
        $question = \library\Query\Sys::sysGetRandomQuestion();

        header('Content-Type: application/json');
        echo json_encode(array('question' => $question));
    }
}


