<?php

class App extends \library\Controller
{
    private $secret_key = 'mtiFi4PQUs6mN4cNrTne';

    private $notif_key = 'bu0aMX*cfdOAT%OCwCNqOaeDt4eowA';

    private $bonus = array(
        'bonus' => array(5 => 1, 10 => 3, 15 => 5, 20 => 9, 25 => 12, 30 => 15, 50 => 30),
        'rating' => array(5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30, 50 => 50)
    );

    public function paymentAction()
    {
        header("Content-Type: application/json; encoding=utf-8");

        $input = $_POST;

        // Проверка подписи
        $sig = $input['sig'];
        unset($input['sig']);
        ksort($input);
        $str = '';
        foreach ($input as $k => $v) {
            $str .= $k . '=' . $v;
        }

        if ($sig != md5($str . $this->secret_key)) {
            $response['error'] = array(
                'error_code' => 10,
                'error_msg' => 'Несовпадение вычисленной и переданной подписи запроса.',
                'critical' => true
            );
        } else {
            // Подпись правильная
            switch ($input['notification_type']) {
                case 'get_item':
                    // Получение информации о товаре
                    $item = $input['item']; // наименование товара

                    if ($item == 'item1') {
                        $response['response'] = array(
                            'item_id' => 25,
                            'title' => '300 золотых монет',
                            'photo_url' => 'http://somesite/images/coin.jpg',
                            'price' => 5
                        );
                    } elseif ($item == 'item2') {
                        $response['response'] = array(
                            'item_id' => 27,
                            'title' => '500 золотых монет',
                            'photo_url' => 'http://somesite/images/coin.jpg',
                            'price' => 10
                        );
                    } else {
                        $response['error'] = array(
                            'error_code' => 20,
                            'error_msg' => 'Товара не существует.',
                            'critical' => true
                        );
                    }
                    break;

                case 'get_item_test':
                    // Получение информации о товаре в тестовом режиме
                    $item = $input['item'];
                    if ($item == 'item1') {
                        $response['response'] = array(
                            'item_id' => 125,
                            'title' => '300 золотых монет (тестовый режим)',
                            'photo_url' => 'http://somesite/images/coin.jpg',
                            'price' => 5
                        );
                    } elseif ($item == 'item2') {
                        $response['response'] = array(
                            'item_id' => 127,
                            'title' => '500 золотых монет (тестовый режим)',
                            'photo_url' => 'http://somesite/images/coin.jpg',
                            'price' => 10
                        );
                    } else {
                        $response['error'] = array(
                            'error_code' => 20,
                            'error_msg' => 'Товара не существует.',
                            'critical' => true
                        );
                    }
                    break;

                case 'order_status_change':
                    // Изменение статуса заказа
                    if ($input['status'] == 'chargeable') {
                        $order_id = intval($input['order_id']);

                        // Код проверки товара, включая его стоимость
                        $app_order_id = 1; // Получающийся у вас идентификатор заказа.

                        $response['response'] = array(
                            'order_id' => $order_id,
                            'app_order_id' => $app_order_id,
                        );
                    } else {
                        $response['error'] = array(
                            'error_code' => 100,
                            'error_msg' => 'Передано непонятно что вместо chargeable.',
                            'critical' => true
                        );
                    }
                    break;

                case 'order_status_change_test':
                    // Изменение статуса заказа в тестовом режиме
                    if ($input['status'] == 'chargeable') {
                        $order_id = intval($input['order_id']);

                        // Receiver id
                        $receiver_id = intval($input['receiver_id']);

                        // VK Votes
                        $votes = intval($input['item_price']);
                        $bonus = isset($this->bonus['bonus'][$votes]) ? $this->bonus['bonus'][$votes] : 0;

                        // Rating from refill
                        $rating = isset($this->bonus['rating'][$votes]) ? $this->bonus['rating'][$votes] : 0;

                        $totalBalance = $votes + $bonus;
                        $u = \library\Query\User::userGetBySocialId($receiver_id);

                        if (!$u) {
                            $response['error'] = array(
                                'error_code' => 22,
                                'critical' => true
                            );

                            break;
                        }

                        $u = new \library\Object($u);

                        \library\Query\Log::beginTransaction();
                        try {
                            $app_order_id = \library\Query\Log::logSetUserPayment($u->user_id, $votes, $bonus);
                            if (!$app_order_id) {
                                throw new Exception();
                            }

                            $balanceChange = \library\Query\User\Balance::balanceSetUser($u->user_id, $totalBalance);
                            if (!$balanceChange) {
                                throw new Exception();
                            }

                            if ($rating) {
                                $r = \library\UserRatingChecker::setRating($u, $rating, '+', 'refill');
                                if (!$r) {
                                    throw new Exception();
                                }
                            }

                            $response['response'] = array(
                                'order_id' => $order_id,
                                'app_order_id' => $app_order_id,
                            );

                            \library\Query\Log::commit();
                        } catch (Exception $e) {
                            $response['error'] = array(
                                'error_code' => 101,
                                'error_msg' => 'Ошибка транзакции',
                                'critical' => true
                            );

                            \library\Query\Log::rollback();
                        }
                    } else {
                        $response['error'] = array(
                            'error_code' => 100,
                            'error_msg' => 'Передано непонятно что вместо chargeable.',
                            'critical' => true
                        );
                    }
                    break;
            }
        }

        echo json_encode($response);
    }

    public function notifyAction()
    {
        // Run screen long as we need
        set_time_limit(0);

        // secret key
        $sKey = \library\Request::get('sKey');

        // Check key
        if ($sKey !== $this->notif_key) exit;

        do {
            // Get notification list
            $notifs = \library\Query\App\Notif::getNotifs();

            // Get user social ids
            $ids = array();
            foreach ($notifs as $notif) {
                $ids[] = (int)$notif['soc_id'];
            }

            \library\Debug::dump($notifs);

            // Last row
            $last = end($notifs);

            // Notifications rows
            $count = count($notifs);

            // Make selected notifications as sent
            \library\Query\App\Notif::makeSent($last['log_id']);

            // Sent!
            \library\VkApiWrapper::apiNotify(join(',', $ids), "У вас есть новое событие в приложений!");

        } while ($count >= 100);
    }
} 