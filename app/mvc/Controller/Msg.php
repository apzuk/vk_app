<?php

class Msg extends \library\Controller
{
    public function reportAction()
    {
        $msg_id = \library\Request::get('msg_id');
        $type_id = \library\Request::get('type_id');

        $user_id = \library\Request::get('user_id');
        $sid = \library\Request::get('sid');

        $msg = \library\Query\IM::imGetMessage($msg_id);

        if (!$msg || $msg['receiver_user_id'] != $user_id) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'message.invalid'
                )
            );
        }

        $msg = new \library\Object($msg);

        $this->checkOwnership($user_id, $sid);

        \library\Query\IM::beginTransaction();

        try {
            \library\Query\IM::imReport($user_id, $msg_id, $type_id);
            \library\Query\IM::imSetAllRead($msg->conv_id, $user_id);

            $result = \library\Query\User::userAddBlock($user_id, $msg->sender_user_id);
            if (!$result) {
                throw new Exception('Error #2');
            }

            if (!\library\Query\User\Notification::notifHasUser(15, $msg->sender_user_id, $user_id)) {
                $result = \library\Query\User\Notification::notifSet(
                    15,
                    json_encode(array()),
                    $msg->sender_user_id,
                    $user_id,
                    \library\Date::now()
                );

                if (!$result) {
                    throw new Exception('Error #3');
                }
            }

            $response = array(
                'result' => true,
                'blocked_user_id' => $msg->sender_user_id
            );

            \library\Query\IM::commit();

        } catch (\Exception $e) {

            echo $e->getMessage();
            \library\Query\IM::rollback();

            $response = array(
                'result' => false,
            );
        }


        $this->response($response);
    }

    public function unBlockAction()
    {
        $unblock_user_id = \library\Request::get('unblock_user_id');

        $user_id = \library\Request::get('user_id');

        $sid = \library\Request::get('sid');

        $this->checkOwnership($user_id, $sid);

        \library\Query\IM::beginTransaction();

        try {

            $result = \library\Query\User::userRemoveBlock($user_id, $unblock_user_id);
            if (!$result) {
                throw new Exception('#Error 1');
            }

            if (!\library\Query\User\Notification::notifHasUser(15, $unblock_user_id, $user_id)) {

                $result = \library\Query\User\Notification::notifSet(
                    16,
                    json_encode(array()),
                    $unblock_user_id,
                    $user_id,
                    \library\Date::now()
                );
                if (!$result) {
                    throw new Exception('#Error 2');
                }
            }

            $response = array('result' => true);

            \library\Query\IM::commit();
        } catch (Exception $e) {
            $response = array('result' => false);
            \library\Query\IM::rollback();
        }

        $this->response($response);
    }

    public function getMessagesAction()
    {
        // Owner user id
        $user_id = \library\Request::get('user_id');

        // User sid
        $sid = \library\Request::get('sid');

        // Offset
        $offset = \library\Request::get('offset');

        // Check sid
        $u = $this->checkOwnership($user_id, $sid);

        $u_id = \library\Request::get('u_id');

        $conv_id = \library\Request::get('conv_id');

        if (!\library\Query\IM::imCheckConvId($conv_id, $u->user_id, $u_id)) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'invalid.conversation'
                )
            );
        }

        $messages = \library\Query\IM::imGetMessages($conv_id, $user_id, $offset);
        $this->response(
            array(
                'messages' => $messages,
                'security_id' => $conv_id,
                'u_id' => (int)$u_id,
                'user_id' => (int)$user_id,
                'page' => (int)$offset
            )
        );
    }
} 