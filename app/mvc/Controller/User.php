<?php

use library\Request;
use library\Query\User as UserQuery;

class User extends \library\Controller
{
    private $durations = array(
        'day' => 1,
        'week' => 7,
        'month' => 30
    );

    public function timezoneAction()
    {
        $countries = \library\Query\Sys::sysGetCountries();

        $f = array_shift($countries);

        $cities = \library\Query\Sys::sysGetCitiesByCountry($f['country_id']);

        foreach ($cities as $city) {
            $response = file_get_contents(
                'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($city['city']) . '&sensor=false'
            );
            $result = json_decode($response, true);

            if (!isset($result['results']) || !$result['results']) {
                continue;
            }

            $positions = $result['results'][0]['geometry']['location'];

            $response = file_get_contents(
                'https://maps.googleapis.com/maps/api/timezone/json?location=' . $positions['lat'] . ',' . $positions['lng'] . '&timestamp=1331161200&sensor=false'
            );
            $result = json_decode($response, true);

            echo $city['id'] . '_' . $result['rawOffset'] . '<br />';

            ob_flush();
            flush();

            \library\Query\Sys::sysSetCityTimezone($city['id'], $result['rawOffset']);
        }
    }

    /**
     *  ================================================================================================
     */
    public function selectCountryAction()
    {
        // selected country
        $country_id = Request::get('country_id');

        // Get country data
        $country = \library\Query\Sys::sysGetCountry($country_id);
    }

    /**
     *  ================================================================================================
     */
    public function getVkLinkAction()
    {
        // Current user id
        $user_id = Request::get('user_id');

        // User sid
        $sid = Request::get('sid');

        // Key
        $key = Request::get('key');

        // Get stored value
        $value = \library\Query\Sys::sysGetValueByKey($user_id, $key);

        if (!$value) {
            $response = array('result' => false);
        } else {
            $response = array('result' => true, 'vk.com/' . $value);
        }

        // Response
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /**
     *  ================================================================================================
     */
    public function getUserAction()
    {
        $user_id = Request::get('user_id');
        $u = UserQuery::userGetShort($user_id);

        if ($u) {
            $u['gender'] = $u['gender'] == 1 ? 'female' : 'male';
        }

        // Response
        header('Content-Type: application/json');
        echo json_encode($u);
    }

    /**
     *  ================================================================================================
     */
    public function vkLinkAction()
    {
        // Security id
        $sid = Request::get('sid');

        // Current user id
        $user_id = Request::get('user_id');

        // Check sid
        $user = $this->checkOwnership($user_id, $sid);

        // Get users
        $u = UserQuery::userGetFull(Request::get('target_user_id'), null, null, null, true);

        if ($u['show_profile'] == "all") {
            $u['link'] = "http://vk.com/id" . $u['soc_id'];
        } else {
            if ($u['show_profile'] == "vip" && $user->isvip) {
                $u['link'] = "http://vk.com/id" . $u['soc_id'];
            } else {
                unset($u['link']);
            }
        }

        $this->response(
            array(
                'result' => isset($u['link']) ? true : false,
                'link' => isset($u['link']) ? $u['link'] : ''
            )
        );

    }

    /**
     *  ================================================================================================
     */
    public function getProfileAction()
    {
        // Get ids separated by semicolon
        $ids = Request::get('ids');
        $ids = preg_split('/(\s+)?,(\s+)?/', $ids, -1, PREG_SPLIT_NO_EMPTY);

        // Current user id
        $user_id = Request::get('user_id');
        if (!$ids) {
            $ids = array($user_id);
        }

        // Select blocked info
        $blocked_info = Request::get('blocked_info');

        // Security id
        $sid = Request::get('sid');

        // Check sid
        $user = $this->checkOwnership($user_id, $sid);

        // Get users
        $users = UserQuery::userGetFull($ids, $user_id, true, $blocked_info, true);

        // Process
        foreach ($users as $k => &$u) {
            $u['game_count'] = (int)$u['game_count'];
            $u['dislikes'] = (int)$u['dislikes'];
            $u['likes'] = (int)$u['likes'];
            $u['liked'] = (boolean)$u['liked'];
            $u['rating'] = (int)$u['rating'];
            $u['kissed'] = (int)$u['kissed'];
            $u['cost'] = (int)$u['cost'];
            $u['city_name'] = $u['city'];
            $u['retinue_count'] = (int)$u['retinue_count'];
            $u['isvip'] = (int)$u['isvip'];
            $u['country_id'] = (int)$u['country_id'];
            $u['city_id'] = (int)$u['city_id'];
            $u['is_ready'] = (int)$u['is_ready'];
            $u['is_online'] = (int)$u['is_online'];
            $u['gender'] = (int)$u['gender'];
            $u['user_id'] = (int)$u['user_id'];

            if ($u['is_fake']) {
                $u['show_profile'] = 'nobody';
            }

            if ($u['show_profile'] == "all") {
                $u['link'] = "http://vk.com/id" . $u['soc_id'];
            } else {
                if ($u['show_profile'] == "vip" && $user->isvip) {
                    $u['link'] = "http://vk.com/id" . $u['soc_id'];
                } else {
                    unset($u['link']);
                }
            }

            if ($u['is_fake']) {
                $u['online'] = 1;
            }

            unset($u['balance']);
            unset($u['sid']);
            unset($u['dislikes']);
            unset($u['is_fake']);

            if ($u['user_id'] == $user_id) {
                $u['self'] = 1;
            } else {
                $u['self'] = 0;
            }
        }

        // Response
        header('Content-Type: application/json');
        echo json_encode($users);
    }

    /**
     *  ================================================================================================
     */
    public function likeAction()
    {
        // Owner id
        $user_id = Request::get('user_id');

        // Set like to user
        $target_user_id = Request::get('target_user_id');

        // Check owner sid
        $user = $this->checkOwnership($user_id, Request::get('sid'));

        // Is target user exists?!
        $target_user = $this->loadUser($target_user_id);

        if ($user->balance < 0.5) {
            $this->noEnoughBalance();
        }

        if (UserQuery::userLikeExists($user_id, $target_user_id)) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'already.liked'
                )
            );
        }

        UserQuery::beginTransaction();

        try {
            $result = UserQuery::userSetLike($target_user_id);
            if (!$result) {
                throw new Exception('Error #1');
            }

            $result = UserQuery::userSetLikeLog($user_id, $target_user_id);
            if (!$result) {
                throw new Exception('Error #2');
            }

            // Withdraw money
            $result = UserQuery\Balance::balanceWithdraw($user_id, 0.5);
            if (!$result) {
                throw new Exception('Error #3');
            }

            $result = UserQuery\Notification::notifSet(
                2,
                json_encode(array()),
                $target_user_id,
                $user_id,
                date('Y-m-d')
            );
            if (!$result) {
                throw new Exception('Error #4');
            }

            \library\UserRatingChecker::setRating($target_user, 1, '+', 'like');

            UserQuery::commit();

            $this->response(
                array(
                    'result' => true,
                    'likes' => \library\Query\User::userGetLikes($target_user_id)
                )
            );
        } catch (Exception $e) {
            UserQuery::rollback();

            $this->response(
                array(
                    'result' => false,
                    'msg' => $e->getMessage()
                )
            );
        }
    }

    /**
     *  ================================================================================================
     */
    public function becomeSponsorAction()
    {
        // Owner
        $sponsor_user_id = Request::get('user_id');;

        // target user id
        $target_user_id = Request::get('target_user_id');

        // Check user sid
        $user = $this->checkOwnership($sponsor_user_id, Request::get('sid'));

        // Validate target user
        $target = $target_user_id == $sponsor_user_id ? $user : $this->loadUser($target_user_id);

        // Is already sponsor?!
        if ($target->sponsor == $user->user_id) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'already.sponsor'
                )
            );
        }

        $new_cost = $target->cost + 1;
        if ($user->balance < $new_cost) {
            $this->noEnoughBalance();
        }

        \library\Query\User::beginTransaction();

        try {
            if (!UserQuery::userBecomeSponsor($user->user_id, $target->user_id)) {
                throw new Exception ('#Error 1');
            }

            if (!UserQuery\Balance::balanceWithdraw($user->user_id, $new_cost)) {
                throw new Exception ('#Error 2');
            }

            if (!UserQuery::userSetNewCost($target->user_id, $new_cost)) {
                throw new Exception ('#Error 3');
            }

            // Set notifications
            if ($user->user_id != $target->user_id) {
                UserQuery\Notification::notifSet(
                    3,
                    json_encode(array()),
                    $target->user_id,
                    $user->user_id,
                    date('Y-m-d H:i:s')
                );

                \library\UserRatingChecker::setRating($user, $new_cost, '+', 'retinue');
                \library\UserRatingChecker::setRating($target, $new_cost, '+', 'patron');

                if ($target->sponsor && $target->sponsor != $target->user_id) {
                    UserQuery\Notification::notifSet(
                        4,
                        json_encode(
                            array(
                                'username' => $target->username,
                                'user_id' => $target->user_id
                            )
                        ),
                        $user->user_id,
                        $target->sponsor,
                        date('Y-m-d H:i:s')
                    );
                }
            } else {
                UserQuery\Notification::notifSet(
                    5,
                    json_encode(array()),
                    $user->sponsor,
                    $target->user_id,
                    date('Y-m-d H:i:s')
                );
            }

            UserQuery::commit();

            $this->response(
                array(
                    'result' => true,
                    'ownerId' => (int)$user->user_id,
                    'targetId' => (int)$target->user_id,
                    'patronId' => (int)$target->sponsor
                )
            );
        } catch (Exception $e) {
            UserQuery::rollback();
            $this->response(
                array(
                    'result' => false,
                    'reason' => $e->getMessage()
                )
            );
        }
    }

    /**
     *  ================================================================================================
     */
    public function generateAction()
    {
        $public = dirname(__FILE__) . '/../../../public/data/avatars/';

        $genders = array('female', 'male');

        $soc_id = 1;
        foreach ($genders as $gender) {

            if ($handle = opendir($public . $gender)) {

                while (false !== ($entry = readdir($handle))) {

                    if (in_array($entry, array('.', '..'))) {
                        continue;
                    }

                    $parts = explode('.', $entry);
                    $username = $parts[0];
                    $avatar = $entry;

                    $u = new \library\Object(array(
                        'username' => $username,
                        'avatar' => $avatar,
                        'soc_id' => $soc_id,
                        'city_id' => 1,
                        'gender' => $gender == 'male' ? 2 : 1
                    ));

                    \library\Query\User::userSet($u);

                    $soc_id++;
                }

                closedir($handle);
            }
        }
    }

    /**
     *  ================================================================================================
     */
    public function checkSubscriptionAction()
    {
        $user_id = $_SESSION['user_id'];

        //if already subscribed , do nothing!
        $hasSubscription = UserQuery\Group::groupHas('public57284494', $user_id);
        if ($hasSubscription) {
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => 'already.subscribed'));
            exit;
        }

        UserQuery\Group::beginTransaction();

        try {
            $result = UserQuery\Group::groupSet('public57284494', $user_id);
            if (!$result) {
                throw new Exception('#Error 1');
            }

            $result = UserQuery\Balance::balanceSetUser($user_id, 5);
            if (!$result) {
                throw new Exception('#Error 2');
            }

            if (!\library\VkApiWrapper::apiHasUserGroup('public57284494', $user_id)) {
                throw new Exception('#Error 3');
            }

            \library\Query\User::commit();

            header('Content-Type: application/json');
            echo json_encode(array('result' => true, 'balance' => \library\Query\User::userGetBalance($user_id)));

        } catch (Exception $e) {
            \library\Query\User::rollback();

            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function makeMeLeaderAction()
    {
        // Current user_id to become leader
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // Leader message
        $msg = Request::get('msg');
        $msg = \library\String::cutUtf8(trim($msg), 130);
        $msg = htmlspecialchars($msg);

        if (filter_var($msg, FILTER_VALIDATE_URL)) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'link.contains'
                )
            );
        }

        if (empty($msg)) {
            $this->response(
                array(
                    'result' => false,
                    'reason' => 'content.empty'
                )
            );
        }

        $user = $this->checkOwnership($user_id, $sid);

        UserQuery\Balance::beginTransaction();

        try {
            // Check user balance!
            $balance = $user->balance;
            $current_bid = (int)\library\Query\Game::gameGetBid($user->city_id);

            $bid = $current_bid + 1;

            if ($balance < $bid) {
                throw new Exception('no.balance');
            }

            // Reserve last leader id for notification
            $last_leader_id = \library\Query\Game::gameGetLeader($user->city_id);

            // Set notification for last leader
            if ($last_leader_id && $last_leader_id != $user->user_id) {
                UserQuery\Notification::notifSet(
                    7,
                    '{}',
                    $last_leader_id,
                    $user->user_id,
                    \library\Date::now()
                );
            }

            // Withdraw from user balance
            $result = UserQuery\Balance::balanceWithdraw($user->user_id, $bid);
            if (!$result) {
                throw new Exception('transaction.error');
            }

            // Make user leader of his city
            $result = \library\Query\Game::gameSetLeader($user->user_id, $user->city_id, $bid, $msg);
            if (!$result) {
                throw new Exception('transaction.error');
            }

            // remove first row
            \library\Query\Game::gameCleanLeaderTable();

            // Set log for admin
            \library\Query\Log::logSetLeader($user->user_id, $user->city_id, $bid, $msg);

            // Commit changes
            \library\Query\Log::commit();

            $u = $user->getArray();
            $u['bid'] = $bid;
            $u['vip'] = (int)$user->isvip;
            $u['message'] = $msg;

            // Response
            header('Content-Type: application/json');
            echo json_encode(array('result' => true, 'user' => $u, 'last_leader_id' => $last_leader_id));

        } catch (Exception $e) {
            UserQuery\Balance::rollback();

            // Response
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function toRibbonAction()
    {
        // current user id
        $user_id = Request::get('user_id');

        //comment
        $comment = trim(Request::get('comment'));
        $comment = htmlspecialchars($comment);

        $u = \library\Query\User::userGetFull($user_id);
        if (!$u) {
            exit;
        }

        $user = new \library\Object($u);

        UserQuery\Balance::beginTransaction();

        try {

            $balance = $user->balance;

            if ($balance < 2) {
                throw new Exception('no.balance');
            }

            $result = UserQuery\Balance::balanceWithdraw($user->user_id, 2);
            if (!$result) {
                throw new Exception('transaction.error');
            }

            $result = UserQuery\Ribbon::ribbonSet($user_id, $comment);
            if (!$result) {
                throw new Exception('transaction.error');
            }

            UserQuery\Ribbon::ribbonClean();

            UserQuery\Balance::commit();
            header('Content-Type: application/json');
            echo json_encode(array('result' => true, 'id' => $result));

        } catch (Exception $e) {
            UserQuery\Balance::rollback();

            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function getNotificationAction()
    {
        //data limit
        $limit = 20;

        //notification offset
        $notif_offset = Request::get('n_offset');

        //rating offset
        $rating_offset = Request::get('r_offset');

        //current user
        $user_id = Request::get('user_id');

        // To load
        $is_notif = Request::getbool('is_notif');
        $is_rating = Request::getbool('is_rating');

        // Response data
        $data = array();

        // Load notifications
        if ($is_notif) {
            //user notification
            $tmpl = UserQuery\Notification::notifGet($user_id, $notif_offset * $limit, $limit);

            foreach ($tmpl as &$t) {
                $params = json_decode($t['data'], true);

                $t['isAdmin'] = (int)($t['owner_user_id'] == \library\Config::get('user.admin_id'));

                $t['notif'] = \library\String::merge(
                    $t['tmpl'],
                    array_merge(
                        array(
                            'end' => $t['gender'] == 1 ? 'а' : '',
                            'sender_user_id' => $t['owner_user_id'],
                        ),
                        is_array($params) ? $params : array()
                    )
                );

                $t['viewed_status'] = $t['status'] == 'pending' ? 'new' : '';
            }
            $data['notif'] = array(
                'data' => $tmpl,
                'unreadCount' => UserQuery\Notification::notifGetUnreadCount($user_id)
            );
        }

        // Load ratings
        if ($is_rating) {
            //user rating
            $data['rating'] = array(
                'data' => UserQuery::userGetRating($user_id, $rating_offset * $limit, $limit)
            );

            foreach ($data['rating']['data'] as &$r) {
                $r['create_ts'] = date('d.m.Y', strtotime($r['create_ts']));
            }
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     *  ================================================================================================
     */
    public function getNotificationUnreadAction()
    {
        // Last loaded notif id
        $lastId = (int)Request::get('lastId');

        // Current user id
        $user_id = (int)Request::get('user_id');

        // Security id
        $sid = Request::get('sid');

        $tmpl = UserQuery\Notification::notifGetLastUnread($user_id, $lastId, $sid);
        foreach ($tmpl as &$t) {
            $params = json_decode($t['data'], true);

            $t['isAdmin'] = (int)($t['owner_user_id'] == \library\Config::get('user.admin_id'));

            $t['notif'] = \library\String::merge(
                $t['tmpl'],
                array_merge(
                    array(
                        'end' => $t['gender'] == 1 ? 'а' : '',
                        'sender_user_id' => $t['owner_user_id']
                    ),
                    $params
                )
            );

            $t['viewed_status'] = $t['status'] == 'pending' ? 'new' : '';
        }

        $data['notif'] = array(
            'data' => $tmpl,
            'unreadCount' => UserQuery\Notification::notifGetUnreadCount($user_id)
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    /**
     *  ================================================================================================
     */
    public function getRatingsUnreadAction()
    {
        // Last loaded notif id
        $lastId = (int)Request::get('lastId');

        // Current user id
        $user_id = (int)Request::get('user_id');

        // Security id
        $sid = Request::get('sid');

        $tmpl = UserQuery::userGetRatingUnread($user_id, $lastId);
        foreach ($tmpl as &$t) {
            $t['create_ts'] = date('d.m.Y', strtotime($t['create_ts']));
        }

        $data['rating'] = array(
            'data' => $tmpl
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     *  ================================================================================================
     */
    public function getSponsorInfoAction()
    {
        //owner user id
        $user_id = Request::get('user_id');

        //user sponsor
        $user_sponsor = UserQuery::userGetSponsor($user_id);

        unset($user_sponsor['balance']);
        unset($user_sponsor['sid']);

        //user sweetie list
        $user_retinue = UserQuery::userGetRetinue($user_id);

        foreach ($user_retinue as $k => &$u) {
            $u['show_profile'] = (int)$u['show_profile'];
            unset($u['balance']);
            unset($u['sid']);
        }

        header('Content-Type: application/json');
        echo json_encode(
            array(
                'sponsor' => $user_sponsor,
                'retinue' => $user_retinue
            )
        );
    }

    /**
     *  ================================================================================================
     */
    public function becomeVipAction()
    {
        // Current user_id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // Check ownership
        $user = $this->checkOwnership($user_id, $sid);

        // Vip status duration
        $duration = Request::get('duration');

        // Buy vip for user
        $to_user_id = Request::get('to_user_id', $user_id);
        $tUser = $to_user_id ? $this->loadUser($to_user_id) : false;

        $u = $tUser ? : $user;

        // Calculate vip status cost!
        switch ($duration) {
            case 'day':
                $duration_label = 'на день';
                $vipCost = 3;
                break;
            case 'week':
                $duration_label = 'на неделю';
                $vipCost = 10;
                break;
            case 'month':
                $vipCost = 25;
                $duration_label = 'на месяц';
                break;
            default:
                $duration_label = 'на день';
                $vipCost = 5;
        }

        // Make user vip transaction!
        UserQuery\Balance::beginTransaction();

        try {
            // Check user balance
            $balance = $user->balance;
            if ($balance < $vipCost) {
                throw new Exception('no.balance');
            }

            // Calculate vip status end date
            $start = date("Y-m-d", strtotime($u->vip_end));
            $start = strtotime($start) > strtotime(date("Y-m-d")) ? $u->vip_end : \library\Date::now();
            $vipEnd = date('Y-m-d H:i:s', strtotime($start . '+' . $this->durations[$duration] . ' days'));

            // Set user vip
            $result = \library\Query\User::userMakeVip($u->user_id, $vipEnd);
            if (!$result) {
                throw new Exception('transaction.error');
            }

            // Withdraw money
            $result = UserQuery\Balance::balanceWithdraw($user->user_id, $vipCost);
            if (!$result) {
                throw new Exception('withdraw.error');
            }

            // Set log
            $result = \library\Query\Log::logSetVip($duration, $vipCost, $user->user_id);
            if (!$result) {
                throw new Exception('log.error');
            }

            // Set vip expired notification
            UserQuery\Notification::notifDeletePending(6, $u->user_id);
            UserQuery\Notification::notifSet(6, '{}', $u->user_id, \library\Config::get('user.admin_id'), $vipEnd);
            if ($to_user_id != $user->user_id) {
                $data = array('duration' => $duration_label);
                UserQuery\Notification::notifSet(
                    8,
                    json_encode($data),
                    $to_user_id,
                    $user->user_id,
                    date('Y-m-d H:i:s')
                );
            }

            // Commit
            UserQuery\Balance::commit();
            $this->response(array('result' => true, 'vip_end' => $vipEnd, 'to_user_id' => $to_user_id));

        } catch (Exception $e) {
            UserQuery\Balance::rollback();
            $this->response(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function sendMoneyAction()
    {
        // Current user id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // Check ownership
        $user = $this->checkOwnership($user_id, $sid);

        // Send money to user id
        $to_user_id = Request::get('to_user_id');

        // Count to send
        $count = Request::get('count');

        if ($count < 10) {
            $this->response(array('result' => false, 'reason' => 'min.value'));
        }

        // To VK votes
        $count = (float)((float)$count / EXCHANGE);

        // Target user
        $toUser = $this->loadUser($to_user_id);

        \library\Query\User::beginTransaction();

        try {
            // Check balance
            if ($user->balance < $count) {
                throw new Exception('no.balance');
            }

            // Withdraw from sender
            UserQuery\Balance::balanceWithdraw($user->user_id, $count);

            // Set to receiver
            UserQuery\Balance::balanceSetUser($toUser->user_id, $count);

            // Set notification for receiver
            $data = array('count' => $count * EXCHANGE);
            UserQuery\Notification::notifSet(
                9,
                json_encode($data),
                $toUser->user_id,
                $user->user_id,
                date('Y-m-d H:i:s')
            );

            // Response
            UserQuery\Balance::commit();
            $this->response(array('result' => true));
        } catch (Exception $e) {
            UserQuery\Balance::rollback();
            $this->response(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function removeUserNotifAction()
    {
        // Notification id to be removed
        $notif_id = Request::get('id');

        // Notification owner id
        $user_id = Request::get('user_id');

        // Notification owner sid
        $sid = Request::get('sid');

        // Remove notification for user
        $result = UserQuery\Notification::notifDelete($notif_id, $user_id, $sid);

        // Response
        header('Content-Type: application/json');
        echo json_encode(array('result' => $result));
    }

    /**
     *  ================================================================================================
     */
    public function completeAction()
    {
        // user id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // User form data
        $serializeData = Request::get('data');

        // Form data
        $form = urldecode($serializeData);
        parse_str($form, $formArray);

        $user = \library\Query\User::userGetFull($user_id);
        if (!$user && $user['sid'] != $sid) {
            echo json_encode(array('result' => false, 'reason' => 'invalid.user'));
            exit;
        }

        $user = new \library\Object($user);

        $params = array();

        try {
            if (!$user->gender) {

                $gender = isset($formArray['gender']) ? $formArray['gender'] : null;
                $check = $gender && in_array($gender, array(1, 2));

                if (!$check) {
                    throw new \Exception('Неправильный пол');
                }
                $params['gender'] = $user->gender = (int)$gender;
            }

            if (!$user->birth_date) {
                $bd = isset($formArray['bdate']) ? $formArray['bdate'] : '---';
                $bd = explode('-', $bd);

                $day = isset($bd[2]) ? (int)trim($bd[2]) : 0;
                $month = isset($bd[1]) ? (int)trim($bd[1]) : 0;
                $year = isset($bd[0]) ? (int)trim($bd[0]) : 0;

                $birth_day = \library\String::merge(
                    '[year]-[month]-[day]',
                    array(
                        'day' => $day,
                        'month' => $month,
                        'year' => $year
                    )
                );

                if (!checkdate($day, $month, $year)) {
                    throw new Exception('Неправильная дата');
                }
                $params['birth_date'] = $user->birth_date = $birth_day;
            }

            if ($user->gender && $user->birth_date && $user->avatar) {
                $params['is_ready'] = 1;
            }

            if (!$params) {
                throw new Exception("No changes");
            }

            \library\Query\User::userSetFromParams($user->user_id, $params);

            header('Content-Type: application/json');
            echo json_encode(array('result' => true));

        } catch (Exception $e) {
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function saveAction()
    {
        // user id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // Current user
        $user = $this->checkOwnership($user_id, $sid);

        // User form data
        $serializeData = Request::get('data');

        // Form data
        $form = urldecode($serializeData);
        parse_str($form, $formArray);


        try {
            // User firstname , lastname parser
            $name = isset($formArray['name']) ? trim($formArray['name']) : '';
            if (empty($name)) {
                throw new Exception('Имя не может быть пустым');
            }

            $fullName = preg_split('/\s+/', $name);
            $firstname = array_shift($fullName);
            $lastname = array_shift($fullName);

            // User birth day builder
            $day = isset($formArray['day']) ? (int)trim($formArray['day']) : 0;
            $month = isset($formArray['month']) ? (int)trim($formArray['month']) : 0;
            $year = isset($formArray['year']) ? (int)trim($formArray['year']) : 0;

            $birth_day = \library\String::merge(
                '[year]-[month]-[day]',
                array(
                    'day' => $day,
                    'month' => $month,
                    'year' => $year
                )
            );

            if (!checkdate($day, $month, $year)) {
                throw new Exception('Не правильная дата');
            }

            // City validator
            $city_id = (int)(isset($formArray['city_id']) ? trim($formArray['city_id']) : 0);
            if (!\library\Query\Sys::sysIsValidCity($city_id)) {
                throw new Exception('Город не найден в базе');
            }

            if ($city_id != $user->city_id) {
                \library\Query\User::userDeleteLeader($user->user_id, $user->city_id);
            }

            // Profile visibility
            $profileStatus = isset($formArray['show_profile']) && trim($formArray['show_profile']) ? "all" : false;

            if (!$profileStatus) {
                $profileStatus = isset($formArray['show_profile_only_vip']) && trim(
                    $formArray['show_profile_only_vip']
                ) ? "vip" : false;
            }

            if (!$profileStatus) {
                $profileStatus = "nobody";
            }

            $result = \library\Query\User::userUpdate(
                $firstname,
                $lastname,
                $birth_day,
                $city_id,
                $profileStatus,
                $user_id
            );

            header('Content-Type: application/json');
            echo json_encode(array('result' => true));

        } catch (Exception $e) {
            header('Content-Type: application/json');
            echo json_encode(array('result' => false, 'reason' => $e->getMessage()));
        }
    }

    /**
     *  ================================================================================================
     */
    public function getShortInfoAction()
    {
        // User id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // Get user info
        $u = $this->checkOwnership($user_id, $sid);

        $response = array(
            'result' => true,
            'user' => array(
                'balance' => $u->balance,
                'rating' => $u->rating,
                'vip' => (int)$u->isvip,
                'vip_end' => intval($u->isvip) ? \library\Date::dateWithlabel($u->vip_end) : null
            )
        );

        // Header response json encoded!
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /**
     *  ================================================================================================
     */
    public function setSettingsAction()
    {
        // User id
        $user_id = Request::get('user_id');

        // User security id
        $sid = Request::get('sid');

        // color
        $color = Request::get('color');

        // Visibility
        $visibility = (int)Request::getbool('visibility');

        // Save settings
        \library\Query\User::userSetSetting($color, $visibility, $user_id, $sid);

        // Header response json encoded!
        header('Content-Type: application/json');
        echo json_encode(
            array(
                'result' => true,
                'params' => array(
                    'color' => $color,
                    'visibility' => $visibility
                )
            )
        );
    }

    /**
     *  ================================================================================================
     */
    public function kissAction()
    {
        // Owner user id
        $user_id = Request::get('user_id');

        // Owner sid
        $sid = Request::get('sid');

        // Check ownership
        $user = $this->checkOwnership($user_id, $sid);

        $target_user_id = Request::get('target_user_id');

        // Check balance
        if ($user->balance < 0.5) {
            $this->noEnoughBalance();
        }

        \library\Query\Log::beginTransaction();

        try {
            $result = \library\Query\Log::logSetKiss($user_id, $target_user_id);
            if (!$result) {
                throw new Exception('#Error 1');
            }

            $result = UserQuery\Balance::balanceWithdraw($user_id, 0.5);
            if (!$result) {
                throw new Exception('#Error 2');
            }

            $result = UserQuery\Notification::notifSet(
                17,
                json_encode(array()),
                $target_user_id,
                $user_id,
                \library\Date::now()
            );
            if (!$result) {
                throw new Exception('#Error 3');
            }

            UserQuery\Balance::commit();

            $this->response(
                array(
                    'result' => $result
                )
            );

        } catch (Exception $e) {
            \library\Query\Log::rollback();

            $this->response(
                array(
                    'result' => false
                )
            );
        }
    }

    /**
     *  ================================================================================================
     */
    public function randomUserAction()
    {
        // Owner user id
        $user_id = Request::get('user_id');

        // Owner sid
        $sid = Request::get('sid');

        // Check owner sid
        $u = $this->checkOwnership($user_id, $sid);

        // Target user id
        $chosen_user_id = Request::get('chosen_user_id');

        // Chosen type id
        $chosen = Request::get('chosen');

        if ($chosen_user_id) {
            \library\Chosen::setChosen($user_id, $chosen_user_id, $chosen);
        }

        $randomUser = \library\Chosen::getRandomUser($u);

        $this->response(
            array(
                'result' => (count($randomUser) > 0 ? true : false),
                'user' => $randomUser,
                'cache' => \library\Cache::instance()->get('chosen_user_ids_for_' . $user_id)
            )
        );
    }

    /**
     *  ================================================================================================
     */
    public function getAvatarAction()
    {
        // Owner user id
        $user_id = Request::get('user_id');

        // Owner sid
        $sid = Request::get('sid');

        // Check owner sid
        $u = $this->checkOwnership($user_id, $sid, true, false);

        $vkUser = \library\VkApiWrapper::apiGetUser($u->soc_id);

        // User avatar
        $vkUser->avatar = \library\Storage\User::moveVKUserImage100(
            $vkUser->photo_100,
            \library\Config::get('user.photo100')
        );
        if (!$vkUser->avatar) {
            $this->response(
                array(
                    'result' => false
                )
            );
        }


        $vkUser->avatar_orig = \library\Storage\User::moveVKUserImageOriginal(
            $vkUser->photo_200_orig,
            \library\Config::get('user.photo_orig')
        );

        \library\Query\User::userSetAvatar($vkUser->avatar, $vkUser->avatar_orig, $user_id);

        $this->response(
            array(
                'result' => true,
                'avatar' => \library\Storage\User::getPath($vkUser->avatar)
            )
        );
    }

    /**
     *  ================================================================================================
     */
    public function getRandomUsersAction()
    {
        // Owner user id
        $user_id = Request::get('user_id');

        // Owner sid
        $sid = Request::get('sid');

        // Check owner sid
        $u = $this->checkOwnership($user_id, $sid, true);

        if ($row = \library\Cache::instance()->get('randomUsers')) {
            $rows = unserialize($row);
        } else {
            $rows = \library\Query\User::userGetRandom();

            if ($rows) {
                \library\Cache::instance()->set('randomUsers', serialize($rows), 60);
            }
        }


        foreach ($rows as &$row) {
            $row['color'] = (int)$row['color'];
            $row['vip'] = (int)$row['isvip'];

            unset($row['isvip']);
        }

        $this->response(
            array(
                'result' => true,
                'users' => $rows
            )
        );
    }
}