<?php

class User
{
    private $user_id;
    private $username;
    private $avatar;
    private $avatar_orig;
    private $gender;
    private $birth_date;
    private $soc_id;
    private $online;
    private $city_id;
    private $city_name;
    private $balance;
    private $sid;
    private $color;
    private $visibility;
    private $cost;
    private $show_profile;
    private $vip_end;
    private $create_ts;
    private $last_visit_date;
    private $continuously_visit;
    private $rating;
    private $is_ready;
    private $invitations;
    private $hash;

    public function setIsReady($is_ready)
    {
        $this->is_ready = $is_ready;
        return $this;
    }

    public function isReady()
    {
        return $this->is_ready;
    }

    public function setInvitations($i)
    {
        $this->invitations = $i;
        return $this;
    }

    public function getInvitations()
    {
        return $this->invitations;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getHash()
    {
        return $this->hash;
    }
}