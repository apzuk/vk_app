<?php

class User
{
    private $user_id;
    private $username;
    private $avatar;
    private $avatar_orig;
    private $gender;
    private $birth_date;
    private $soc_id;
    private $online;
    private $city_id;
    private $city_name;
    private $balance;
    private $sid;
    private $color;
    private $visibility;
    private $cost;
    private $show_profile;
    private $vip_end;
    private $create_ts;
    private $last_visit_date;
    private $continuously_visit;
    private $rating;
    private $is_ready;
    private $invitations;
    private $hash;

    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setShowProfile($sp)
    {
        $this->show_profile = $sp;
        return $this;
    }

    public function getSowProfile()
    {
        return $this->show_profile;
    }

    public function setVipEnd($ve)
    {
        $this->vip_end = $ve;
        return $this;
    }

    public function getVipEnd()
    {
        $this->vip_end;
    }

    public function setCreateTs($create_ts)
    {
        $this->create_ts = $create_ts;
        return $this;
    }

    public function getCreateTs()
    {
        return $this->create_ts;
    }

    public function setLastVisitDate($lvd)
    {
        $this->last_visit_date = $lvd;
        return $this;
    }

    public function getLastVisitDate()
    {
        return $this->last_visit_date;
    }

    public function setContinuouslyVisit($cv)
    {
        $this->continuously_visit = $cv;
        return $this;
    }

    public function getContinuouslyVisit()
    {
        return $this->continuously_visit;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setIsReady($is_ready)
    {
        $this->is_ready = $is_ready;
        return $this;
    }

    public function isReady()
    {
        return $this->is_ready;
    }

    public function setInvitations($i)
    {
        $this->invitations = $i;
        return $this;
    }

    public function getInvitations()
    {
        return $this->invitations;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function getHash()
    {
        return $this->hash;
    }
}