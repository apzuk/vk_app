<div class="app-graphic">
    <img src="img/icons.png">
    <img src="img/header/backgrounds/passed/heart.png">
    <img src="img/header/backgrounds/passed/notification.png">
    <img src="img/header/backgrounds/passed/message.png">
    <img src="img/header/backgrounds/normal/heart.png">
    <img src="img/header/backgrounds/normal/notification.png">
    <img src="img/header/backgrounds/normal/message.png">
    <img src="img/header/backgrounds/hover/heart.png">
    <img src="img/header/backgrounds/hover/notification.png">
    <img src="img/header/backgrounds/hover/message.png">
    <img src="img/messages/female-mess-bg-selected.png">
    <img src="img/messages/female-mess-bg.png">
    <img src="img/messages/male-mess-bg-selected.png">
    <img src="img/messages/mess-bg-male-unread.png">
    <img src="img/messages/male-mess-bg.png">
    <img src="img/messages/box-area.png">
    <img src="img/messages/mess-bg.png">
    <img src="img/messages/mess-bg-female-unread.png">
    <img src="img/game/r-leftBg.png">
    <img src="img/game/bingoMaleTopBg.png">
    <img src="img/game/sd-female.png">
    <img src="img/game/sideBg.png">
    <img src="img/game/icons.png">
    <img src="img/game/female-pntik-right.png">
    <img src="img/game/female-pntik.png">
    <img src="img/game/bingoMaleBg.png">
    <img src="img/game/female-pntik-left.png">
    <img src="img/game/bingoFemaleLeftBg.png">
    <img src="img/game/male-pntik-right.png">
    <img src="img/game/bingoFemaleBg.png">
    <img src="img/game/bingoFemaleRightBg.png">
    <img src="img/game/bingoMaleRightBg.png">
    <img src="img/game/maleBg.png">
    <img src="img/game/bingoBg.png">
    <img src="img/game/male-pntik-left.png">
    <img src="img/game/male-pntik.png">
    <img src="img/game/femaleBg.png">
    <img src="img/game/bingoFemaleTopBg.png">
    <img src="img/game/l-rightBg.png">
    <img src="img/game/sd-male.png">
    <img src="img/game/bingoMaleLeftBg.png">
    <img src="img/content/sd-female-bg.png">
    <img src="img/content/styled-dialog/female/btn-bg.png">
    <img src="img/content/styled-dialog/female/btn-passed-bg.png">
    <img src="img/content/styled-dialog/female/btn-over-bg.png">
    <img src="img/content/styled-dialog/male/btn-bg.png">
    <img src="img/content/styled-dialog/male/btn-passed-bg.png">
    <img src="img/content/styled-dialog/male/btn-over-bg.png">
    <img src="img/content/sd-male-bg.png">
    <img src="img/content/buttonpane.png">
    <img src="img/content/main-bg.png">
    <img src="img/content/leader-label.png">
    <img src="img/content/tape/btn/btn-hover.png">
    <img src="img/content/tape/btn/btn-passed.png">
    <img src="img/content/tape/btn/btn.png">
    <img src="img/content/footer-bg.png">
    <img src="img/notif/male-new-bg.png">
    <img src="img/notif/notif-male-right.png">
    <img src="img/notif/rating-bg.png">
    <img src="img/notif/notif-female-right.png">
    <img src="img/notif/notif.png">
    <img src="img/notif/female-new-bg.png">
</div>

<script>
    $(function () {
        var imgs = $('.app-graphic img'), imgsLen = imgs.length, loaded = 0;

        imgs.each(function () {
            $(this).load(function () {
                loaded++;

                console.log(loaded + '|' + imgsLen);
            });
        });
    })
</script>