<!DOCTYPE HTML>
<html>
<head>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }
    </style>
</head>
<body>
<canvas id="myCanvas" width="578" height="200"></canvas>
<script>
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
    var x = 80;
    var y = 110;

    context.font = '60pt Segoe_UI';

    // stroke color
    context.fillStyle = 'black';
    context.strokeStyle = 'white';
    context.strokeText('Маргарита Васильевна', x, y);
</script>
</body>
</html>