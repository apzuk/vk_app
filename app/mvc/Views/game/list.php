<style>
    div.row {
        border: 1px solid #ccc;
        padding: 3px;
        float: left;
    }

    table {
        float: left
    }

    #iframes {
        clear: both;
        width: 1614px;
        margin: 0 auto;
    }

    #iframes iframe {
        float: left;
    }

    #users {
        display: none;
    }
</style>

<script src="/js_v2/standard/jquery-2.0.3.min.js"></script>

<div id="users">
    <div>
        <?php foreach ($users['male'] as $user) : ?>
            <div class="row" gender="<?= $user['gender'] ?>" id="<?= $user['user_id'] ?>">
                <?= array_shift(explode(' ', $user['username'])); ?>
            </div>
        <?php endforeach ?>
    </div>

    <div style="clear: both"></div>

    <div>
        <?php foreach ($users['female'] as $user) : ?>
            <div class="row" gender="<?= $user['gender'] ?>" id="<?= $user['user_id'] ?>">
                <?= array_shift(explode(' ', $user['username'])); ?>
            </div>
        <?php endforeach ?>
    </div>
</div>
<div style="text-align: center; clear: both;">
    <a href="#" id="show" onclick="return false">Показать</a>
</div>


<div id="iframes">

</div>

<script>
    $(function () {
        $('.row').each(function () {

            $(this).click(function () {
                var id = $(this).attr('id'), gender = $(this).attr('gender'),
                    src = "http://dev.vkapp-lubof.ru/game_v2.php?viewer_id=" + id + "&gender=" + gender;
                $('#iframes').append('<iframe border="0" style="width: 800px;height: 700px;" src="' + src + '"></iframe>');

                $(this).remove();
            });
        });

        $('#show').on('click', function () {
            $('#users').slideDown();
        });
    });
</script>