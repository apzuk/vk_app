<!DOCTYPE html>
<html>
<head>
    <!-------  css libs ------->
    <link rel="stylesheet" href="/css/jquery-ui-1.10.3.custom.css?15" type="text/css">
    <link rel="stylesheet" href="/css/jquery.jscrollpane.css?415" type="text/css">
    <link rel="stylesheet" href="/css/main.css?774" type="text/css">
    <link rel="stylesheet" href="/css/macos_trimming.css?178" type="text/css">
    <!------- external js libs ------->
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?98"></script>
    <script src="https://vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>

    <!------- standard js libs ------->
    <script type="text/javascript" src="/js_v2/standard/jquery-2.0.3.min.js?938"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery-ui-1.10.3.custom.min.js?686"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery-tmpl-1.0.0.min.js?308"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery.mousewheel.js?594"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery.jscrollpane.min.js?367"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery.tagcanvas.js?650"></script>
    <script type="text/javascript" src="/js_v2/standard/sockjs.js?487"></script>
    <script type="text/javascript" src="/js_v2/standard/jquery.blockUI.js?462"></script>
    <!------- js models libs ------->
    <script type="text/javascript" src="/js_v2/models/user.js?153"></script>
    <script type="text/javascript" src="/js_v2/models/message.js?584"></script>
    <!------- js managers libs ------->
    <script type="text/javascript" src="/js_v2/managers/transport.js?624"></script>
    <script type="text/javascript" src="/js_v2/managers/main.js?134"></script>
    <script type="text/javascript" src="/js_v2/managers/logger.js?476"></script>
    <script type="text/javascript" src="/js_v2/managers/notif.js?721"></script>
    <script type="text/javascript" src="/js_v2/managers/user.js?715"></script>
    <script type="text/javascript" src="/js_v2/managers/ribbon.js?1"></script>
    <script type="text/javascript" src="/js_v2/managers/online.js?443"></script>
    <script type="text/javascript" src="/js_v2/managers/message.js?201"></script>
    <script type="text/javascript" src="/js_v2/managers/game.js?830"></script>
    <script type="text/javascript" src="/js_v2/managers/leader.js?899"></script>
    <!------- js plugins libs ------->
    <script type="text/javascript" src="/js_v2/plugins/game.make.vip.js?555"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.styled.dialog.js?227"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.question.dialog.js?294"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.tab.js?792"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.counter.down.js?533"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.help.text.js?10"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.combobox.js?110"></script>
    <script type="text/javascript" src="/js_v2/plugins/game.checkbox.js?548"></script>
</head>
<body>

<div id="wrapper">
    <header>
        <ul class="menu">
            <li class="heart active">
                <div class="content">
                    <div class="heart-ico"></div>
                    <span class="label">Игра</span>
                </div>
            </li>
            <li class="message">
                <div class="content">
                    <div class="msg-ico">

                        <div class="badge zero single">
                            0
                        </div>
                    </div>
                    <span class="label">Сообщения</span>
                </div>
            </li>
            <li class="notification">
                <div class="content">
                    <div class="notif-ico">
                        <div class="badge single zero">0</div>
                    </div>
                    <span class="label">Уведомления</span>
                </div>
            </li>
        </ul>
    </header>
</div>
<div id="content">
    <div class="game-tab1">
        <?php // $renderer->render('game-tab1'); ?>

        <div class="gamePage">
            <div class="side g-left">
                <div class="right-border"></div>

                <div class="users">
                    <div class="female1 female-item">
                        <div class="ico ico-wait hasToolTip" title="Отвечает на вопросы"></div>

                        <div class="female-avatar">
                            <img src="/img/data/avatars/100/2.jpg"/>
                        </div>

                        <span>Маргарита, 25</span>
                    </div>
                    <div class="female2 female-item">
                        <div class="ico ico-ok hasToolTip" title="Ответил на все вопросы"></div>

                        <div class="female-avatar">
                            <img src="/img/data/avatars/100/1.jpg"/>
                        </div>

                        <span>Елизавета, 28</span>
                    </div>
                    <div class="female3 female-item">
                        <div class="ico ico-ok hasToolTip" title="Ответил на все вопросы"></div>

                        <div class="female-avatar">
                            <img src="/img/data/avatars/100/2.jpg"/>
                        </div>
                        <span>Лизавета, 18</span>
                    </div>
                </div>
            </div>
            <div class="g-center">

                <div class="help">Отвечайте на все вопросы и подождите участников</div>

                <div class="dlg">
                    <div class="female-pntik-right"></div>

                    <div class="c c2">
                        <div class="header">
                            <b class="question-label">Сергей Анатольевич отвечает...</b>
                            <span class="time">осталось 98</span>
                        </div>

                        <div class="items">
                            <div class="item">
                                <div class="q">
                                    <div class="question-label">Вопрос 1</div>
                                    <div class="question">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit udin, lorem quis.
                                    </div>
                                </div>

                                <div class="a">
                                    <div class="answer-label">Ответ</div>
                                    <div class="answer">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit
                                    </div>
                                </div>
                                <hr/>
                            </div>
                            <div class="item">
                                <div class="q">
                                    <div class="question-label">Вопрос 1</div>
                                    <div class="question">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit udin, lorem quis.
                                    </div>
                                </div>

                                <div class="a">
                                    <div class="answer-label">Ответ</div>
                                    <div class="answer">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit
                                    </div>
                                </div>
                                <hr/>
                            </div>
                            <div class="item">
                                <div class="q">
                                    <div class="question-label">Вопрос 1</div>
                                    <div class="question">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit udin, lorem quis.
                                    </div>
                                </div>

                                <div class="a">
                                    <div class="answer-label">Ответ</div>
                                    <div class="answer">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh
                                        vel
                                        velit auctor aliquet. Aenean sollicit
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="estimate">
                            <a href="#" class="ico ico-star hasToolTip" title="Ваша оценка: 1 рейтинг"
                               onclick="return false;"></a>
                            <a href="#" class="ico ico-star hasToolTip" title="Ваша оценка: 2 рейтинг"
                               onclick="return false;"></a>
                            <a href="#" class="ico ico-star hasToolTip" title="Ваша оценка: 3 рейтинг"
                               onclick="return false;"></a>
                            <a href="#" class="ico ico-star hasToolTip" title="Ваша оценка: 4 рейтинг"
                               onclick="return false;"></a>
                            <a href="#" class="ico ico-star hasToolTip" title="Ваша оценка: 5 рейтинг"
                               onclick="return false;"></a>
                        </div>

                        <div class="footer">
                            <div class="comment-label">Ваш комментарий</div>
                            <textarea></textarea>
                        </div>
                    </div>

                    <button class="dlg-btn"></button>
                </div>
            </div>
            <div class="side g-right">
                <div class="left-border"></div>

                <div class="users">
                    <div class="male1 male-item">
                        <div class="ico ico-wait hasToolTip" title="Отвечает на вопросы"></div>

                        <div class="male-avatar">
                            <img src="/img/data/avatars/100/100.jpg"/>
                        </div>

                        <span>Маргарита, 25</span>
                    </div>
                    <div class="male2 male-item">
                        <div class="ico ico-ok hasToolTip" title="Ответил на все вопросы"></div>

                        <div class="male-avatar">
                            <img src="/img/data/avatars/100/101.jpg"/>
                        </div>

                        <span>Елизавета, 28</span>
                    </div>
                    <div class="male3 male-item">
                        <div class="ico ico-ok hasToolTip" title="Ответил на все вопросы"></div>

                        <div class="male-avatar">
                            <img src="/img/data/avatars/100/102.jpg"/>
                        </div>
                        <span>Лизавета, 18</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="game-tab2 xhidden">
        <?php // $renderer->render('game-tab2', array('conversations' => $uData->conversations, 'user_id' => $uData->user_id)); ?>
    </div>

    <div class="game-tab3 xhidden">
        <?php // $renderer->render('game-tab3'); ?>
    </div>
</div>

<footer>
    <ul class="menu">
        <li>
            <div class="ico ico-share"></div>
        </li>
        <li>
            <div class="ico ico-faq"></div>
        </li>
        <li>
            <div class="ico ico-privacy"></div>
        </li>
    </ul>
</footer>
</body>

<script>
    $(function () {
        $('.g-center .dlg').dialog({
            autoOpen: true,
            width: 350,
            height: 'auto',
            resizable: false,
            draggable: false,
            position: {
                my: 'center top',
                at: 'center top+30',
                of: $('.gamePage .g-center')
            },
            open: function () {
                // $(this).dialog('option', 'height', $(this).find('.c').height() + $(this).find('.sd-btn').height());
            }
        }).styledDialog({
                type: 'female',
                btn: '.dlg-btn',
                btnLabel: 'Ответить'
            });

        $('.items').jScrollPane();

        $('.estimate .ico-star').each(function () {
            $(this).mousemove(function () {
                $(this).addClass('ico-star-selected').prevAll('.ico-star').addClass('ico-star-selected');
                $(this).nextAll('.ico-star').removeClass('ico-star-selected');
            });

            $(this).mouseout(function () {
                if ($(this).is(':last-child')) return;
                $(this).removeClass('ico-star-selected');
            });
        });
    });
</script>

</html>