<!DOCTYPE html>
<html>
<head>
    <!-------  css libs ------->
    <link rel="stylesheet" href="/css/jquery-ui-1.10.3.custom.css?15" type="text/css">
    <link rel="stylesheet" href="/css/jquery.jscrollpane.css?415" type="text/css">
    <link rel="stylesheet" href="/css/main.css?774" type="text/css">
    <link rel="stylesheet" href="/css/macos_trimming.css?178" type="text/css">

    <style>

        .hover{
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            z-index: 9;
            border: 6px solid transparent;
            -webkit-transition: all 700ms ease;
            -moz-transition: all 700ms ease;
            -ms-transition: all 700ms ease;
            -o-transition: all 700ms ease;
            transition: all 700ms ease;
            content: "";
        }

        div.hover:hover .hover{
            border-color: #fee505;
            -webkit-transition: all 50ms ease;
            -moz-transition: all 50ms ease;
            -ms-transition: all 50ms ease;
            -o-transition: all 50ms ease;
            transition: all 50ms ease;
        }

    </style>
</head>
<body>

<div class="tape" id="tape">
    <div class="set-btn">
        <span class="label"></span>
    </div>
    <div class="user-list">
        <div class="all">
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-279" style="margin-left: 0px;">
                <img src="/data/avatars/original/cache/80/62Z31FnfbVI.jpg">

                <div class="hover"></div>
            </div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-278" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_3b2c7288.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-277" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_5d910d77.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-276" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_8a1136a3.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-275" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_3882455c.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-274" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_e758036a.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-273" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_3c62f94d.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-272" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/w_R7DcnZ-Gg.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-271" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_a469e8fe.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-270" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/1Yhj41WX54M.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-269" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/e8nY67K0VFI.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-268" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_f7d96022.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-267" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_72cb9ce0.jpg"></div>
            <div class="user female-avatar m tapeToolTip" title="" id="rUser-266" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_73f2da36.jpg"></div>
            <div class="user male-avatar m tapeToolTip" title="" id="rUser-265" style="margin-left: 0px;"><img
                    src="/data/avatars/original/cache/80/a_3854ad4a.jpg"></div>
        </div>
    </div>
</div>

</body>
</html>