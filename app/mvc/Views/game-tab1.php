<div class="start-page">
    <div class="top" id="top_content">
        <div>
            <div id="leader">
                <div class="leader-label"></div>

                <div class="leader xhidden">
                    <div class="avatar">
                        <a href="#">
                            <img src=""/>
                        </a>
                    </div>

                    <div class="bet-info">
                        <span class="bet-label">Ставка:</span>
                        <span class="bet"></span>
                    </div>

                    <button class="become-leader"></button>
                </div>
            </div>

            <div class="leader-text xhidden">
                <div class="bg l-top"></div>
                <div class="content">
                    <span class="txt"></span>
                </div>
                <div class="bg l-bottom"></div>
            </div>
        </div>
    </div>

    <div class="online-user">
        <?= $renderer->render('canvas1', array(), true) ?>
    </div>

    <div class="random-user <?= $gender == 1 ? 'male' : 'female'; ?>">
        <div class="xhidden g14">
            <div class="xleft">
                <div class="r-user-avatar">
                    <a href="#" onclick="return false;">
                        <img class="avatar" src="">
                    </a>
                </div>

                <div class="info-bar">
                    <div class="name"></div>
                    <div class="age-city"></div>
                </div>
            </div>

            <div class="select-items">
                <table class="choose">
                    <?php foreach ($chosenTypes as $id => $type) : ?>
                        <tr>
                            <td>
                                <div class="chose-type chosen-type-<?= $id ?>"
                                     onclick="g.startRandomUser(<?= (int)$id ?>)">
                                    <span class="rIcon"></span>
                                    <span class="rTitle"><?= $type['title']; ?></span>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>
                            <div class="chose-type chosen-type-none"
                                 onclick="g.startRandomUser(null);">
                                <span class="rIcon"></span>
                                <span class="rTitle">Пропустить</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <?php /*
            <div>
                <button id="setChosen">Отправить выбор</button>
                <button id="nextUser">Пропустить</button>
            </div> */
            ?>
        </div>

        <div class="l4"></div>
    </div>

    <div class="tape" id="tape">
        <div class="set-btn">
            <span class="label"></span>
        </div>
        <div class="user-list">
            <div class="all"></div>
        </div>
    </div>
</div>

<?= $renderer->render('game-page', array(), true); ?>