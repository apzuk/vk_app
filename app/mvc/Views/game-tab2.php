<div class="messages">
    <div class="left-side"></div>
    <article class="<?= count($conversations) == 0 ? 'has-no-conv' : 'has-conv'; ?>">
        <div class="no-conv-label">Нет диалогов.</div>
        <div class="select-conv-label">Не выбран диалог.</div>

        <div class="message-box-list">
        </div>

        <div class="myBlocked blocked">
            Вы заблакировали этого пользователя
            <a href="#" onclick="userManager.unBlock('[user_id]')">Разблакировать</a>
        </div>
        <div class="mеBlocked blocked">
            Этот пользователь заблакировал Вас.
        </div>

        <div class="textarea-box">
            <div id="message" class="enbl-selection" contenteditable="true"></div>

            <div id="smiles">
                <?= \library\View\VkIcons::icons(); ?>
            </div>
            <a class="button" href="#" onclick="return false;" id="sendMessage"></a>

            <div class="vipMessage">
                <a href="#" class="ico ico-photo-attach" onclick="messageManager.attachImage();"></a>

                <div id="msg-image"></div>
                <span id="imgUploadErrors" class="xhidden">Размер картинки привышает допустимый максимум 5 мб</span>
            </div>
        </div>
    </article>
</div>
<?php if (count($conversations) > 0) : ?>
    <script>
        $(function () {
            var u = new userModel();

            <?php foreach ($conversations as $conversation) : ?>
            <?php $type = $conversation['sender_user_id'] == $user_id ? 'receiver' : 'sender'; ?>

            $(document).on('socketConnectionReady', function () {
                u.setUser({
                    gender: "<?= $conversation[$type . 'gender'] ?>",
                    user_id:  <?= (int) $conversation[$type . '_user_id']; ?>,
                    avatar: "<?= $conversation[$type . 'avatar'] ?>",
                    username: "<?= $conversation[$type . 'name']; ?>",
                    birth_date: "<?= $conversation[$type . 'age'] ?>",
                    city: "<?= $conversation[$type . 'cityname'] ?>",
                });

                messageManager.importLeftSideItem(
                    u,
                    "<?= $conversation['conv_id']; ?>",
                    <?= (int) $conversation['count'] ?>,
                    <?= (int) isset($meBlocked[$conversation['conv_id']]) ?>,
                    <?= (int) isset($myBlocked[$conversation['conv_id']]) ?>
                );
            });

            <?php endforeach; ?>
        })
    </script>
<?php endif; ?>