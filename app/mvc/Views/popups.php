<div id="all-popups">
<div id="userProfile" title="Профиль пользователя"></div>
<div id="bingo_result" title="БИНГО!"></div>
<div id="question_dialog" class="question_dialog" title="Задайте Ваш вопрос...">
    <div class="left">

        <img src="<?= \library\Storage\User::getPath($uData->avatar); ?>" class="circleBase me">

        <div class="icons">
            <div class="females">
                <div title="Женщины онлайн"></div>
                <div class="count">2587</div>
            </div>
            <div class="males">
                <div title="Мужчины онлайн"></div>
                <div class="count">1891</div>
            </div>
        </div>
    </div>
    <div class="right">
        <textarea placeholder="Ваш вопрос..."></textarea>

        <div class="vipPanel">
            <a href="#" class="ico ico-random hasToolTip" title="Случайный вопрос"
               onclick="game.setRandomQuestion(); return false;"></a>

            <div class="play-unlimit <?= !$uData->isvip ? 'xhidden' : 'panel' ?>">
                <input type="checkbox" name="play-unlimit" id="play-unlimit">
                <label for="play-unlimit">Играть без очереди</label>
            </div>
        </div>
    </div>

    <button class="ask-question"></button>
</div>
<div id="leadership_dialog" title="Стать лидером Вашей страны"></div>
<div id="ribbon" title="Попасть в ленту пользователей"></div>
<div id="confirmMessageRemoval" title="Удалить сообщение">
    Вы действительно хотите удалитъ данное сообщение?!
</div>
<div id="confirmKiss" title="Поцеловать">
    Данное действие стоит 5 монет, хотите продолжать?!
</div>
<div id="confirmNotifRemoval" title="Удалить сообщение">
    Вы действительно хотите удалить уведомление?<br/>
</div>

<div id="confirmBecomeSponsor" title="Стать покравителем">
</div>

<div class="xhidden app-graphic">
    <?= \library\View\GraphicLoader::appendFiles(); ?>
</div>

<div id="guests">
    <div class="list">
        <div class="no-guest">Никто еще не просмотрел Ваш профиль.</div>
    </div>

    <script id="guest_tmpl" type="text/template">

        <div class="${gender} guest">
            <div class="${gender}-avatar">
                <a href="#" onclick="userManager.showProfile(${user_id})">
                    <img src="${avatar}"/>
                </a>
            </div>
            <div class="c">
                <span class="name">${username}</span>
                <span class="label">посмотрел${end} ваш профиль</span>
            </div>

            <time>${time}</time>
        </div>

    </script>
</div>
<div id="purse" class="toFront">
    <div class="header">
        <a href="#" onclick="return false;" class="ico ico-wallet"></a>

        <div class="wallet-info">
            <label>Ваш кашелек</label>

            <p class="value">7 360 валюта</p>
        </div>
    </div>

    <div class="tariffs">
        <div class="row">
            <div class="left-side">
                <div>
                    <div class="ico ico-tariff"><p>10</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(1); return false;">Купить за 1 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+0</div>
            </div>
            <div class="right-side">
                <div>
                    <div class="ico ico-tariff"><p>30</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(3); return false;">Купить за 3 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+0</div>
            </div>
        </div>
        <div class="row">
            <div class="left-side">
                <div>
                    <div class="ico ico-tariff"><p>50</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(5); return false;">Купить за 5 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+5</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>10 бонус</span>
                </div>
            </div>
            <div class="right-side">
                <div>
                    <div class="ico ico-tariff"><p>100</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(10); return false;">Купить за 10 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+10</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>30 бонус</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="left-side">
                <div>
                    <div class="ico ico-tariff"><p>150</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(15); return false;">Купить за 15 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+15</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>50 бонус</span>
                </div>
            </div>
            <div class="right-side">
                <div>
                    <div class="ico ico-tariff"><p>200</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(20); return false;">Купить за 20 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+20</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>90 бонус</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="left-side">
                <div>
                    <div class="ico ico-tariff"><p>250</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(25); return false;">Купить за 25 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+25</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>120 бонус</span>
                </div>
            </div>
            <div class="right-side">
                <div>
                    <div class="ico ico-tariff"><p>300</p></div>
                    <div class="cost">
                        <a href="#" onclick="g.showVkPaymentVotes(30); return false;">Купить за 30 голос</a>
                    </div>
                </div>

                <div class="ico ico-purse-heart hasToolTip" title="рейтинг">+30</div>
                <div class="purse-bonus">
                    <a href="#" class="ico ico-purse-bonus"></a>
                    <span>150 бонус</span>
                </div>
            </div>
        </div>
    </div>

    <div class="akcia">
        <div class="akcia-cost">
            <div class="ico ico-akcia-tariff">
                <a href="#" onclick="g.showVkPaymentVotes(50); return false;">500</a>
            </div>

            <p class="label">Суперпредложение! + 300 рейтинг +300 бонус за 50 голос</p>

            <a href="#" onclick="g.showVkPaymentVotes(50); return false;" class="ico ico-akcia-arrow"></a>
        </div>
    </div>

    <div class="earn-money">
        <a href="#" onclick="g.showVkPaymentOffers();return false;">Получить бесплатные валюты</a>
    </div>

    <button class="close-purse"></button>
</div>
<div id="vip">
    <b class="label">Стать vip</b>

    <div class="items">
        <div class="item">
            <table>
                <tr>
                    <td>
                        <a href="#" class="vip-ico ico-no-turn"></a>
                    </td>
                    <td>
                        Игра без учереди
                    </td>
                </tr>
            </table>
        </div>
        <div class="item">
            <table>
                <tr>
                    <td>
                        <a href="#" class="vip-ico ico-color"></a>
                    </td>
                    <td>
                        Выбор цвета и ярлыка
                    </td>
                </tr>
            </table>
        </div>
        <div class="item">
            <table>
                <tr>
                    <td>
                        <a href="#" class="vip-ico ico-photo"></a>
                    </td>
                    <td>
                        Добавление фотографии
                        в сообщениях и возможность
                        делать снимки
                    </td>
                </tr>
            </table>
        </div>
        <div class="item">
            <table>
                <tr>
                    <td>
                        <a href="#" class="vip-ico ico-invisible"></a>
                    </td>
                    <td>
                        Режим невидимки
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="buttons">
        <a href="#" class="vip-buy-button" onclick="userManager.selectVipRate('day')">На день за <b>30 валют</b></a>
        <a href="#" class="vip-buy-button" onclick="userManager.selectVipRate('week')">На неделю <b>100 валют</b></a>
        <a href="#" class="vip-buy-button" onclick="userManager.selectVipRate('month')">На месяц <b>250 валют</b></a>

        <div class="l4 l"></div>
    </div>

    <button class="close-vip"></button>
</div>
<div id="takePhoto">
    <div id="videoCanvasContainer">
        <video id="video" width="600" height="360" autoplay></video>
    </div>
    <div id="captureCanvasContainer">
        <canvas id="canvas" width="480" height="360"></canvas>

        <div class="buttons">
            <a href="#" id="attachCaturedImage">Прикрепить</a>
            <a href="#" id="denyCaturedImage">отменить</a>
        </div>
    </div>

    <span class="ico ico-cam"></span>

    <button class="take-screen" id="snap"></button>
    <button class="close-tp"></button>
</div>

<div id="captureContainer" title="Прикрепить снимок">

    <canvas width="480" height="360" id="capturedImage"></canvas>

    <button class="attach-image" id="attachCaturedImage"></button>
    <button class="close-a-image"></button>

    <div class="loading xhidden"></div>
</div>

<div id="attachImage">
    <div class="loading xhidden"></div>

    <div class="photo-buttons">
        <a href="#" class="vip-buy-button photo-from-pk">Фото с компьютера</a>
        <label>Файл не выбран</label>

        <a href="#" onclick="messageManager.showCaptureDialog(); return false;" class="vip-buy-button">Снимок с
            веб-камеры</a>
    </div>

    <div class="xhidden">
        <form action="/mvc.php?c=Game&do=uploadMessageImage"
              target="photoIFrame" id="msgPhotoForm"
              method="post" enctype="multipart/form-data">
            <input type="file" name="photo" id="photoAttach" accept="image/*"/>
            <input type="hidden" name="user_id" value="<?= $uData->user_id; ?>"/>
        </form>
        <iframe id="photoIFrame"></iframe>

    </div>
    <button class="close-photo-take"></button>
</div>
<div id="msgGraphic"></div>
<div id="vipCapabilities">
    <div class="row">
        <span>Выберите цвет</span>

        <div class="colors">
            <div class="clr1 clr"></div>
            <div class="clr2 clr selected"></div>
            <div class="clr3 clr"></div>
            <div class="clr4 clr"></div>
            <div class="clr5 clr"></div>
        </div>
    </div>

    <div class="row">
        <span>Невидимый режим: <b>включен</b></span>
        <input type="checkbox" name="visible-mode" class="xhidden">

        <div class="ico ico-mode-turn-on"></div>
    </div>

    <div class="preview">
        <?php $gender = $uData->gender == 1 ? 'female' : 'male' ?>
        <?php $modeClass = $uData->isvip & $uData->color ? ' clr-b-' . $uData->color : ''; ?>
        <div class="avatar <?= $gender ?>-avatar <?= $modeClass ?>">
            <a href="#"><img src="<?= \library\Storage\User::getPath($uData->avatar); ?>"/></a>

            <div class="vip <?= $gender ?>-vip"></div>
        </div>

        <span class="tapeToolTip <?= $gender ?>" title="Привет всем!"></span>
    </div>

    <button class="save"></button>
    <button class="close-vc"></button>
</div>
<div id="donateMoney">
    <div class="info-bar">
        <div class="ico ico-share-2"></div>
        <span>Вы собираетесь передать данному пользователю валюты.</span><br/>
    </div>
    <input type="number" name="count" min="10" value="10" class="count"/>

    <button class="send-money-btn"></button>
    <button class="close-sm-btn"></button>
</div>

<?php if ($uData->updated) : ?>
    <div id="continuously_visit" title="Награждение от ежедненого посещения!">
        <div class="g">
            <p>Награждение за ежедневное посещение</p>

            <div class="days">
                <div class="ico ico-day">5</div>
                <div class="ico ico-day">5</div>
                <div class="ico ico-day">5</div>
                <div class="ico ico-day">5</div>
                <div class="ico ico-day">20</div>
            </div>

            <p class="second">
                Посещайте каждый день и получайте 5 монет. <br/>
                После 5 дней посещений получайте 20 монет.
            </p>
            <button class="close-cv-btn"></button>
        </div>
    </div>
<?php endif; ?>

<div id="share-with-friends" title="Поделиться с друзьями">
    <div class="share-menu">
        <div class="row first" onclick="g.postOnWall(<?= $uData->soc_id ?> , 1);return false;">
            <div class="ico ico-share-1"></div>
            <div class="label label-m1">Попросить у друга <b>подарок</b></div>

            <div class="loading xhidden"></div>
        </div>
        <div class="row second" onclick="g.showInviteBox(); return false;">
            <div class="ico ico-share-2"></div>
            <div class="label label-m2">
                Пригласить друзей.<br/>
                Вы получите <b>30 монет</b> от каждого<br/>
                3-го вошедшего
            </div>
        </div>
        <div class="row" onclick="g.friendList();return false;">
            <div class="ico ico-share-3"></div>
            <div class="label label-m3">
                Пригласить друзей. <br/>
                Вы и ваш друг получите <b>100 монет</b><br/>
                при достижении другом рейтинга 300
            </div>
        </div>
    </div>

    <button class="close-share"></button>
</div>

<div id="friends-list" title="Мои друзья">
    <div class="search">
        <input type="text" name="search" value="" autocomplete="off" placeholder="Имя фамилия искомого друга"/>
    </div>

    <div class="friends" data-offset="1">
        <?= \library\View\VkFriends::getInstance()->renderFriends($uData->soc_id); ?>
    </div>

    <div class="l4 xhidden"></div>

    <button class="close-friends"></button>
</div>

<?php if (!$uData->is_ready) : ?>
    <div id="complete-dlg" title="Заполнить данные">
        <form>
            <?php if (!$uData->avatar) : ?>
                <div class="form-row">
                    <b>Аватар</b><br/>

                    <input type="button" name="avatar" value="Поставить аватар ВК" onclick="userManager.vkAvatar()"/>

                    <div class="mosaic-backdrop avatar" style="display: block;">
                        <img width="80" height="80"/>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!$uData->gender) : ?>
                <hr/>
                <div class="form-row">
                    <b>Ваш пол?</b><br/>

                    <label for="g_male">
                        Мужской
                        <input type="radio" name="gender" value="2" id="g_male"/>
                    </label>
                    <label for="g_female">
                        Мужской
                        <input type="radio" name="gender" value="1" id="g_female"/>
                    </label>
                </div>
            <?php endif; ?>

            <?php if (!$uData->birth_date) : ?>
                <hr/>
                <div class="form-row">
                    <b>Дата рождения <span>(yyyy-mm-dd)</span></b><br/>

                    <input type="text" name="bdate" placeholder="Например: 1988-02-02"/>
                </div>
            <?php endif; ?>
        </form>
    </div>
<?php endif; ?>

<div id="confirm-user-like" title="Проявить симпатию">
</div>

<div id="msg-report" title="Пожаловаться на сообщение">
    <span class="attention">ВНИМАНИЕ! Отправляя жалобу вы внесете данного пользователя в черный список</span> <br/><br/>

    <label id="report_type">Пожалуйста выберите причину
        <select class="report_type" name="report_type" id="report_type">
            <option value="1">Оскорбление</option>
            <option value="2">Спам письмо</option>
            <option value="3">Пропаганда наркотиков</option>
            <option value="4">Детская порнография</option>
            <option value="5">Насилие / экстремизм</option>
        </select>
    </label>
</div>

</div>