<script id="msg_tmpl" type="text/template">
    <div class="msg-item ${gender}-${receiver_status}" id="message_${message_id}"
         onmousemove="messageManager.makeASRead(${message_id}, ${receiver_user_id}, ${sender_user_id}, this)">
        <div class="avatar-box ${gender}">
            <a href="#" onclick="userManager.showProfile(${sender_user_id}); return false;">
                <img src="${avatar}"/>
            </a>
        </div>

        <div class="header">
            <p class="name">${username}</p>
            <time>${date}</time>
        </div>
        <p class="msg">
            {{html message}}

            {{if hasAttachment}}
            <img src="${img}" class="attachment" onclick="messageManager.showGraphic(this);"/>
            {{/if}}
        </p>

        <div class="xclean"></div>
        <div class="footer">
            <?php /*
                <div class="ico ico-mess-edit hasToolTip" title="Редактировать"></div>
            */ ?>

            {{if !self}}
            <div class="ico ico-mess-spam hasToolTip" onclick="messageManager.report(${message_id}, ${sender_user_id});"
                 title="Пожаловаться"></div>
            {{/if}}

            <div class="ico ico-mess-delete hasToolTip" onclick="messageManager.deleteMessage(${message_id})"
                 title="Удалить"></div>
        </div>
    </div>
</script>

<script id="ls_ui_tmpl" type="text/template">
    <div class="item ${genderT}"
         data-user-id="${user_id}"
         onclick="messageManager.selectDialog('${user_id}', this); return false;">

        <div class="content">
            <img src="${avatar}"/>

            <div class="info">
                <div class="name">${username}</div>
                <div class="details">${age} {{if age!=''}},{{/if}} ${city_name}</div>
            </div>
        </div>
        <div class="num ${className}">
            {{if num}}
            ${num}
            {{else}}
            0
            {{/if}}
        </div>
    </div>

</script>

<script id="become_sponsor_tmpl" type="text/template">
    Вы действительно хотите стать спонсором ?
    Из Вашего счета будет списан <b>${cost}</b> ${currency_label}.
</script>

<script id="my_profile_tmpl" type="text/template">
    <div class="profile">
        <form id="user-profile-form">
            <div class="leftSide ${genderT}">
                <div class="mosaic-block bar">
                    <div class="mosaic-backdrop avatar ${genderT}-avatar">
                        <a href="#"><img src="${avatar_orig}"/></a>

                        <div class="status-ico ico-${status} hasToolTip" title="${status_title}"></div>
                        <div class="ico-kiss-div hasToolTip" title="Поцелуи">
                            <a href="#" class="ico-kiss-small" onclick="return false;"></a>
                            <span>${kiss}</span>
                        </div>
                    </div>

                    <div class="mosaic-overlay">
                        <div class="details">
                            <span class="ico ico-edit-ico"></span>
                            <a href="#" onclick="userManager.openForm(); return false;">
                                Редактировать
                            </a>
                            <a href="#" onclick="userManager.vkAvatar(this); return false;">
                                Поставить аватарку ВК
                            </a>
                        </div>
                    </div>
                </div>

                <ul class="menu-icons"></ul>
            </div>
            <div class="rightSide ${genderT}">
                <div class="no-error">
                    <p></p>
                </div>

                <div class="block form">
                    <div class="formRow">
                        <label class="label">Имя и фамилия</label>

                        <div class="profile name">
                            <input type="text" name="name" value="Арам Петросян" id="name"/>
                        </div>
                    </div>
                    <div class="formRow">
                        <label class="label">Дата рождения(день/месяц/год)</label>

                        <div class="xclean"></div>

                        <div class="profile day">
                            <input type="text" name="day" value="02" id="day"/>
                        </div>
                        <div class="profile month">
                            <input type="text" name="month" value="02" id="month"/>
                        </div>
                        <div class="profile year">
                            <input type="text" name="year" value="1988" id="year"/>
                        </div>

                        <div class="xclean"></div>
                    </div>

                    <div class="formRow">
                        <label class="label">Страна</label>

                        <?= library\View\Flags::render(); ?>
                    </div>

                    <div class="formRow">
                        <label class="label">Город</label>

                        <div class="profile city">
                            <select class="combobox" data-href="/mvc.php?c=Sys&do=getCities" data-id="${country_id}"
                                    name="city_id" id="user_city_id">
                                <option value="${city_id}" selected>${city_name}</option>
                            </select>
                        </div>
                    </div>
                    <div class="formRow">
                        <div>
                            <input type="checkbox" name="show_profile" id="show_profile" value="1"/>

                            <div class="label-wrapper">
                                <label for="show_profile" id="show-profile-label" class="label">
                                    Показать ссылку на профиль ВК всем пользователей
                                </label>
                            </div>
                        </div>
                        <div class="xclean"></div>

                        <div>
                            <input type="checkbox" name="show_profile_only_vip" id="show_profile_only_vip" value="2"/>

                            <div class="label-wrapper">
                                <label for="show_profile_only_vip" id="show-profile-label" class="label">
                                    Показать только vip пользователям
                                </label>
                            </div>
                        </div>
                        <div class="xclean"></div>

                    </div>
                    <div class="formRow">
                        <a href="#" class="save-btn"></a>
                        <a href="#" class="close" onclick="userManager.closeForm(); return false;">Закрыть</a>
                    </div>

                    <div id="cityChangeAttention" class="xhidden"><p></p></div>
                </div>
                <div class="block sponsor active">
                    <div class="patron-wrap"></div>
                    <div class="retinue-wrap">
                        <h1></h1>

                        <div class="retinue-about">
                            Свите это человек который - бал бла бла , написать тескт кто такой свита!
                        </div>
                    </div>
                </div>
                <div class="block guests"></div>
            </div>
        </form>
    </div>
</script>

<script id="profile_tmpl" type="text/template">
    <div class="profile">
        <form id="user-profile-form">
            <div class="leftSide ${genderT}">
                <div class="avatar ${genderT}-avatar">
                    <img src="${avatar_orig}"/>

                    <div class="status-ico ico-${status} hasToolTip" title="${status_title}"></div>

                    <div class="ico-kiss-div hasToolTip" title="Поцелуи" style="position: absolute; top:-100000px;">
                        <a href="#" class="ico-kiss-small" style="position: absolute; top:-100000px;"
                           onclick="return false;"></a>
                        <span>${kiss}</span>
                    </div>
                    <div class="ico-kiss-div hasToolTip" title="Поцелуи">
                        <a href="#" class="ico-kiss-small" onclick="return false;"></a>
                        <span>${kiss}</span>
                    </div>
                </div>

                <div class="sympathy" style="position: absolute; top: -1000000px;">
                    <a href="#" class=" ico ico-like-to hasToolTip" title=""></a>
                </div>

                <div id="operations">
                    <div class="sympathy item o07">
                        <a href="#" class="ico ico-mess-to hasToolTip"
                           title="Написать сообщение"
                           onclick="messageManager.writeToUser(${user_id}); return false;"></a>
                    </div>

                    <div class="sympathy item {{if !liked}}o07{{/if}} {{if liked}}o05 dsbl{{/if}}">
                        <a href="#" class="ico ico-like-to hasToolTip"
                           title="{{if liked}}Вы уже проявили симпатию{{else}}Проявить симпатию{{/if}}"
                           onclick="userManager.like(${user_id}, '${username}', this); return false;"></a>
                    </div>

                    <div class="kiss item {{if !kissed}}o07{{/if}} {{if kissed}}o05 dsbl{{/if}}">
                        <a href="#" class="ico ico-kiss-to hasToolTip"
                           title="{{if kissed}}Вы уже поцеловали{{else}}Поцеловать{{/if}}"
                           onclick="userManager.kiss(${user_id}, this); return false;"> </a>
                    </div>
                </div>

                <ul class="menu-icons"></ul>
            </div>
            <div class="rightSide ${genderT}">
                <div class="block sponsor active" tabindex="1">
                    <div class="patron-wrap no-patron">
                    </div>

                    <div class="retinue-wrap">
                        <h1></h1>

                        <div class="retinue-about">
                            Свите это человек который - бал бла бла , написать тескт кто такой свита!
                        </div>
                    </div>
                </div>

                <table class="profile-icons">
                    <tr>
                        <td>
                            <a href="#" class="send-vip" onclick="userManager.vip();"></a>
                        </td>
                        <td>
                            <a href="#" class="send-money" onclick="userManager.sendMoney(${user_id});"></a>
                        </td>
                        <td>
                            {{if show_profile == "all"}}
                            <a href="${link}" class="vk-profile" target="_blank">
                            </a>
                            {{/if}}

                            {{if show_profile == "vip" && vip == 1}}
                            <a href="${link}" class="vk-profile" target="_blank">
                            </a>
                            {{/if}}
                            {{if show_profile == "vip" && vip == 0}}
                            <a href="#" onclick="userManager.needVip();return false;"
                               class="o04 vkLinkFromProfile vk-profile"
                               target="_blank">
                            </a>
                            {{/if}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="label">Отправить vip</div>
                        </td>
                        <td>
                            <div class="label">Передать валюту</div>
                        </td>
                        <td>
                            <div class="label">Профиль ВК</div>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</script>

<script id="profile_menu_tmpl" type="text/template">
    <li>
        <a href="#" class="ico ico-like"></a>

        <p class="label">Симпатий</p>

        <p class="value">${likes}</p>
    </li>
    <li>
        <a href="#" class="ico ico-patron-to"></a>

        <p class="label">Свиты</p>

        <p class="value">${retinue_count}</p>
    </li>
    <li>
        <a href="#" class="ico ico-menu-rating"></a>

        <p class="label">Рейтинг</p>

        <p class="value">${rating}</p>
    </li>
    <li>
        <a href="#" class="ico ico-game"></a>

        <p class="label">Кол-во игр</p>

        <p class="value">${game_count}</p>
    </li>
</script>

<script id="patron_tmpl" type="text/template">
    <div class="patron">
        {{if has_patron == 1}}
        <span>
            <a href="#" onclick="userManager.showProfile(${patron_id})">
                <img src="${avatar}" title="${username}" class="hasToolTip pw"/>
            </a>
        </span>
        {{/if}}
        {{if has_patron == 0}}
        <span>
            <img src="/img/no-patron.png" title="У Вас нет покровителя" class="hasToolTip pw no-patron"/>
        </span>
        {{/if}}

        <div class="patron-text xleft">
            <h1></h1>

            <div class="cost-info">
                <div class="cost">
                    <a href="#" onclick="return false;" class="ico ico-currency hasToolTip" title="Стоимость"></a>
                    <label>${cost}</label>
                </div>
                <div class="rating">
                    <a href="#" onclick="return false;" class="ico ico-patron-rating hasToolTip"
                       title="Получаемый рейтинг"></a>
                    <label>${rating}</label>
                </div>
            </div>

            <p>Покравитель это человек который - бла бла бла , написать текст описывающий покорвителя :)</p>

            {{if self == 1}}
            {{if has_patron == 1}}
            <a href="#" class="buy-button" onclick="userManager.becomeSponsor(${user_id}, this); return false;">
                Выкупить себя
            </a>
            {{/if}}

            {{/if}}

            {{if self == 0}}
            {{if selfPatron == 0}}
            <a href="#" class="buy-button" onclick="userManager.becomeSponsor(${user_id}, this); return false;">
                Стать покровителем
            </a>
            {{/if}}
            {{/if}}
        </div>
    </div>
</script>

<script id="retinue_tmpl" type="text/template">
    <div class="retinue">
        <span>
            <a href="#" onclick="userManager.showProfile(${user_id});" class="hasToolTip rw" title="${username}">
                <img src="${avatar}"/>
            </a>
        </span>
    </div>
</script>

<script id="like_user_tmpl" type="text/template">
    Вы хотите проявить симпатию ${name} , стоимость услуги ${cost}
</script>

<script id="user_item_tmpl" type="text/template">
    <img class="user-avatar" src="${avatar}"/>
    <span>
        <a href="#" onclick="userManager.showProfile(${user_id}); return false;">
            ${uName}
        </a>

        {{if gender == 'female'}}
            <a href="#" class="icons icon-clock xleft hasToolTip" title="Отвечает на вопросы..."
               onclick="return false"></a>
        {{/if}}

        {{if gender == 'male'}}
            <a href="#" class="icons icon-clock xright hasToolTip" title="Отвечает на вопросы..."
               onclick="return false"></a>
        {{/if}}
    </span>

</script>

<script id="notification_profile_view_tmpl" type="text/template">
    <div class="line">
        <div class="avatar xleft">
            <img src="/data/avatars/100/${avatar}" width="25"/>
        </div>
        <div class="xleft notification-text">
            <a href="#" onclick="userManager.showProfile(${user_id}); return false;>">${username}</a>
            посмотрел${genderEnd} Ваш профиль
        </div>
    </div>

</script>

<script id="vk_public_tmpl" type="text/template">
    <div id="vk_groups">
        <b class="xcenter"></b>

        <div id="vk_group_content"></div>
    </div>

</script>

<script id="become_leader_tmpl" type="text/template">
    Вы хотите стать лидером <b>${city_name}</b>. Текующая ставка <b>${current_bid}</b>.
    С вашего счета будет зачислено <b>${bid_cost}</b> мАнэт.

    <textarea placeholder="Ваш комментарий..."></textarea>
    <div class="charsLeft">0</div>
    <span class="error xhidden"></span>
</script>

<script id="already_leader_tmpl" type="text/template">
    Поздравляем вы уже являетесь лидером Вашего города
</script>

<script id="to_ribbon_tmpl" type="text/template">
    <p>Из Вашего счета будет вычеслено ${cost} ${currency_label}</p>
    <textarea placeholder="Ваш комментарий..."></textarea>
    <div class="charsLeft">0</div>
</script>

<script id="online_user_tmpl" type="text/template">
    <li>
        <a href='#' onclick='userManager.showProfile(${user_id}); return false;'>
            <img src="/data/avatars/100/${avatar}" width="45" height="45" class="${class_name} xleft"/>
        </a>
    </li>

</script>

<script id="rating_log_item_tmpl" type="text/template">
    <div class="item ${type}" id="rating_${rating_id}">
        <div class="icon"></div>
        <div class="c">
            <div class="value">${t}${value}</div>
            <div class="txt">${label}</div>
            <time>${create_ts}</time>
        </div>
        <div class="end"></div>
    </div>
</script>

<script id="notif_tmpl" type="text/template">
    <div class="notif-item ${genderT} ${viewed_status}"
         onmousemove="notifManager.makeAsGot(${notif_id},this); return false;" id="notif_${notif_id}">
        <div class="avatar {{if isAdmin}}admin{{/if}} xleft">
            <a href="#" onclick="userManager.showProfile(${owner_user_id});return false;">
                <img src="${avatar}"/>
            </a>
        </div>

        <div class="info">
            <div class="name">${username}</div>
            <div class="msg">{{html notif}}</div>
        </div>
        <div class="ico ico-notif-delete" onclick="notifManager.confirmRemoveNotif(${notif_id}); return false;"></div>

        <time>${create_ts}</time>
        <div class="right-border"></div>
    </div>
</script>

<script id="sponsor-sweetie" type="text/template">
    <fieldset>
        <legend>Покравитель</legend>

        <div class="sponsor-profile">
            <div class="avatar">
                <img src="${avatar}"/>
            </div>
            <div>
                <div class="name">${username}</div>
                <div class="info">${age}, ${username}</div>
            </div>
        </div>
    </fieldset>
</script>