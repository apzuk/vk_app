<div class="gamePage xhidden"></div>
<div id="bingo"></div>

<script id="bingo_tmpl" type="text/template">
    <div class="bg">
        <div class="bingo-heart-left"></div>
        <div class="bingo-heart-right"></div>

        <div class="person first ${genderT}-avatar">
            <div class="left"></div>
            <div class="right"></div>
            <div class="top"></div>

            <div class="${genderT}-avatar">
                <img src="${my_avatar}"/>
            </div>

            <a href="#" class="bingo-btn dsbl" onclick="return false;">Написать сообщение</a>
            <a href="#" class="bingo-btn dsbl" onclick="return false;">Профиль ВКонтакте</a>
        </div>
        <div class="person second ${genderF}-avatar">
            <div class="left"></div>
            <div class="right"></div>
            <div class="top"></div>

            <div class="${genderF}-avatar">
                <img src="${target_avatar}"/>
            </div>

            <a href="#" class="bingo-btn" onclick="messageManager.writeToUser(${target_user_id});return false;">Написать
                сообщение</a>
            <a href="#" class="bingo-btn vk">Профиль ВКонтакте</a>
        </div>
    </div>
</script>

<script id="gamePage_tmpl" type="text/template">
    <div class="side g-left">
        <div class="right-border"></div>

        <div class="users">
            <div class="u1 ${genderT}-item u" id="u_${user_id_1_1}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>
                <div class="${genderT}-avatar ${clr_1_1}">
                    <a href="#" onclick="userManager.showProfile(${user_id_1_1})">
                        <img src="${avatar_1_1}"/>
                    </a>
                </div>

                <span>${name_1_1} {{if age_1_1}}, ${age_1_1} {{/if}}</span>
            </div>
            <div class="u2 ${genderT}-item u" id="u_${user_id_1_2}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>
                <div class="${genderT}-avatar ${clr_1_2}">
                    <a href="#" onclick="userManager.showProfile(${user_id_1_2})">
                        <img src="${avatar_1_2}"/>
                    </a>
                </div>

                <span>${name_1_2}{{if age_1_2}}, ${age_1_2} {{/if}}</span>
            </div>
            <div class="u3 ${genderT}-item u" id="u_${user_id_1_3}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>
                <div class="${genderT}-avatar ${clr_1_3}">
                    <a href="#" onclick="userManager.showProfile(${user_id_1_3})">
                        <img src="${avatar_1_3}"/>
                    </a>
                </div>
                <span>${name_1_3}{{if age_1_3}}, ${age_1_3} {{/if}}</span>
            </div>
        </div>
    </div>
    <div class="g-center">
        <div class="help"></div>
    </div>
    <div class="side g-right">
        <div class="left-border"></div>

        <div class="users">
            <div class="u1 ${genderF}-item u" id="u_${user_id_2_1}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>

                <div class="${genderF}-avatar ${clr_2_1}">
                    <a href="#" onclick="userManager.showProfile(${user_id_2_1})">
                        <img src="${avatar_2_1}"/>
                    </a>
                </div>
                <span>${name_2_1}{{if age_2_1}}, ${age_2_1} {{/if}}</span>
            </div>
            <div class="u2 ${genderF}-item u" id="u_${user_id_2_2}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>

                <div class="${genderF}-avatar ${clr_2_2}">
                    <a href="#" onclick="userManager.showProfile(${user_id_2_2})">
                        <img src="${avatar_2_2}"/>
                    </a>
                </div>
                <span>${name_2_2}{{if age_2_2}}, ${age_2_2} {{/if}}</span>
            </div>
            <div class="u3 ${genderF}-item u" id="u_${user_id_2_3}">
                <div class="ico ico-wait hasToolTip" title="Ожидание"></div>

                <div class="${genderF}-avatar  ${clr_2_3}">
                    <a href="#" onclick="userManager.showProfile(${user_id_2_3})">
                        <img src="${avatar_2_3}"/>
                    </a>
                </div>
                <span>${name_2_3}{{if age_2_3}}, ${age_2_3} {{/if}}</span>
            </div>
        </div>
    </div>
</script>

<script id="gameStep1_tmpl" type="text/template">
    <div class="dlg">
        <div class="${genderF}-pntik-right"></div>

        <div class="c c1">
            <div class="header">
                <b class="question-label">Вопрос 1</b>
                <span class="time">осталось <b></b></span>
            </div>

            <div class="question"></div>

            <textarea></textarea>
        </div>

        <button class="dlg-btn"></button>
    </div>
</script>

<script id="gameStep2_tmpl" type="text/template">
    <div class="dlg">
        <div class="${genderF}-pntik-right"></div>

        <div class="c c2">
            <div class="header">
                <b class="question-label"></b>
                <span class="time">осталось <b></b></span>
            </div>
        </div>

        <button class="dlg-btn"></button>
    </div>
</script>

<script id="gameStep2Content" type="text/template">
    {{if hasAnswers == 1}}
    <div class="items">
        {{if has_answer_1 == 1}}
        <div class="item">
            <div class="q xleft">
                <div class="question-label xleft">${username_1}:</div>
                <div class="question ph-5">${question_1}</div>
            </div>
            <div class="a xleft">
                <div class="answer-label xleft">Ответ:</div>
                <div class="answer ph-5">${answer_1}</div>
            </div>
            <hr class="xclean"/>
        </div>
        {{/if}}

        {{if has_answer_2 == 1}}
        <div class="item">
            <div class="q xleft">
                <div class="question-label xleft">${username_2}:</div>
                <div class="question ph-5">${question_2}</div>
            </div>
            <div class="a xleft">
                <div class="answer-label xleft">Ответ:</div>
                <div class="answer ph-5">${answer_2}</div>
            </div>
            <hr class="xclean"/>
        </div>
        {{/if}}

        {{if has_answer_3 == 1}}
        <div class="item">
            <div class="q xleft">
                <div class="question-label xleft">${username_3}:</div>
                <div class="question ph-5">${question_3}</div>
            </div>
            <div class="a xleft">
                <div class="answer-label xleft">Ответ:</div>
                <div class="answer ph-5">${answer_3}</div>
            </div>
            <hr class="xclean"/>
        </div>
        {{/if}}
    </div>
    {{/if}}

    {{if hasAnswers == 0}}
    <div class="no-answers">
        Не ответил${end} на вопросы

        <img src="/img/no-answer.png?54" />
    </div>
    {{/if}}
    <div class="estimate">
        <a href="#" class="ico ico1 ico-star hasToolTip" title="Ваша оценка: 1 рейтинг" onclick="return false;"></a>
        <a href="#" class="ico ico2 ico-star hasToolTip" title="Ваша оценка: 2 рейтинг" onclick="return false;"></a>
        <a href="#" class="ico ico3 ico-star hasToolTip" title="Ваша оценка: 3 рейтинг" onclick="return false;"></a>
        <a href="#" class="ico ico4 ico-star hasToolTip" title="Ваша оценка: 4 рейтинг" onclick="return false;"></a>
        <a href="#" class="ico ico5 ico-star hasToolTip" title="Ваша оценка: 5 рейтинг" onclick="return false;"></a>
    </div>

    <div class="footer">
        <div class="comment-label">Ваш комментарий</div>
        <textarea></textarea>
    </div>
</script>

<script id="gameStep3_tmpl" type="text/template">
    <div class="dlg">
        <div class="c c3">
            <div class="header">
                <span class="time">осталось <b></b></span>
            </div>

            <div class="xclean"></div>

            <div class="comments">
                {{if hasComment_1 == 1}}
                <div class="comment">
                    <b>Комментарий от ${username_1}:</b>

                    <div>${comment_1}</div>
                </div>
                {{/if}}

                {{if hasComment_2 == 1}}
                <div class="comment">
                    <b>Комментарий от ${username_2}:</b>

                    <div>${comment_2}</div>
                </div>
                {{/if}}

                {{if hasComment_3 == 1}}
                <div class="comment">
                    <b>Комментарий от ${username_3}:</b>

                    <div>${comment_3}</div>
                </div>
                {{/if}}

                {{if !hasComments}}
                <div class="no-comments">У вас нет комментарий.</div>
                {{/if}}
            </div>
        </div>
    </div>

    <div class="participants t">
        <div class="person-item" id="cuId_${user_id_1}">
            <div class="person ${genderF}">
                <div class="heart"></div>
            </div>
            <img src="${avatar_1}"/>
        </div>
        <div class="person-item" id="cuId_${user_id_2}">
            <div class="person ${genderF}">
                <div class="heart"></div>
            </div>
            <img src="${avatar_2}"/>
        </div>
        <div class="person-item" id="cuId_${user_id_3}">
            <div class="person ${genderF}">
                <div class="heart"></div>
            </div>
            <img src="${avatar_3}"/>
        </div>
    </div>
</script>

<script id="gameStep4" type="text/template">
    <div class="chosen left chosen1">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderF}">
                    <div class="heart {{if has_left_1==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_left_1}"/>
            </div>
        </div>
    </div>
    <div class="chosen left chosen2">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderF}">
                    <div class="heart {{if has_left_2==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_left_2}"/>
            </div>
        </div>
    </div>
    <div class="chosen left chosen3">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderF}">
                    <div class="heart {{if has_left_3==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_left_3}"/>
            </div>
        </div>
    </div>

    <div class="chosen right chosen1">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderT}">
                    <div class="heart {{if has_right_1==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_right_1}"/>
            </div>
        </div>
    </div>
    <div class="chosen right chosen2">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderT}">
                    <div class="heart {{if has_right_2==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_right_2}"/>
            </div>
        </div>
    </div>
    <div class="chosen right chosen3">
        <div class="points">
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
            <div class="ico ico-heart-point"></div>
        </div>

        <div class="participants r">
            <div class="person-item">
                <div class="person ${genderT}">
                    <div class="heart {{if has_right_3==1}}active{{/if}}"></div>
                </div>
                <img src="${avatar_right_3}"/>
            </div>
        </div>
    </div>
</script>