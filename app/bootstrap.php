<?php

$basedir = dirname(__FILE__);

// Private dir path
defined('PRIVATE_PATH') || define('PRIVATE_PATH', $privatePath = $basedir . '/../private');

// Config path
defined('CONFIG_PATH') || define('CONFIG_PATH', PRIVATE_PATH . '/app.ini');

//Base dir
defined('BASE_DIR') || define('BASE_DIR', $basedir);

// Exchange difference
defined('EXCHANGE') || define('EXCHANGE', 10);

// Set timezone
date_default_timezone_set('Europe/Moscow');