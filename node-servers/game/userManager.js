function userManager() {

    /**
     *
     * This object
     *
     * @type {userManager}
     */
    var context = this;

    /**
     *
     * Store online users data
     *
     * @type {{}}
     */
    var users = {};

    /**
     *
     * Add new user to list
     *
     * @param user
     */
    this.userAdd = function (user) {
        users[user.user_id] = user;
    };

    this.userRemove = function (user_id) {
        delete users[user_id];
    };

    this.userGetById = function () {

    };

    /**
     * User liked another user
     * @param data
     */
    this.likeUser = function (data) {
        _response(data.likedUserId, {}, 'updateNotif', 'notifManager');
    };

    /**
     * Donate vip status for user
     * * @param data
     */
    this.donateVIP = function (data) {
        _response(data.to_user_id, {}, 'updateProfileInfo', 'userManager');
    };

    /**
     * Send money
     * @param data
     */
    this.sendMoney = function (data) {
        _response(data.to_user_id, {}, 'updateProfileInfo', 'userManager');
    };

    /**
     * User becomes sponsor for another user
     * @param data
     */
    this.becomeSponsor = function (data) {
        log(data);
        var sponsor_user_id = data.user_id;
        var target_user_id = data.target_user_id;
        var patron_id = parseInt(data.patron_id);

        if (sponsor_user_id != target_user_id) {
            _response(target_user_id, {
            }, 'updateNotif', 'notifManager');

            if (target_user_id != patron_id) {
                _response(patron_id, {
                }, 'updateNotif', 'notifManager');
            }
        }
        else {
            _response(patron_id, {
            }, 'updateNotif', 'notifManager');
        }
    };

    /**
     *
     * Block user
     *
     * @param data
     */
    this.blocked = function (data) {
        _response(data.to_user_id, {from_user_id: data.user_id}, 'blocked', 'userManager');
    };

    /**
     *
     * Unblocked user
     *
     * @param data
     */
    this.unBlocked = function (data) {
        _response(data.to_user_id, {from_user_id: data.user_id}, 'unBlocked', 'userManager');
    };

    /**
     *
     * @param ref_id
     * @param rating
     * @param user_id
     * @returns {boolean}
     */
    this.setMoneyForReferral = function (ref_id, rating, user_id) {
        if (!ref_id) return false;

        if (rating < 300) return false;

        var votes = 100;

        queryManager.userSetBalance(ref_id, votes, function () {
            notificationManager.fromReferral({
                sender_user_id: user_id,
                receiver_user_id: ref_id,
                count: votes
            }, function () {
                queryManager.userUnsetReferral(user_id, function () {
                    _response(ref_id, {}, 'updateNotif', 'notifManager');
                });
            });

        });

        return true;
    };

    this.updateMe = function (data) {
    };

    /* ====================================================================================================== */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'userManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/

}

module.exports = userManager;
