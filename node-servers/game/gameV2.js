function gameV2() {

    /**
     *
     * This object
     *
     * @type {game}
     */
    var context = this;

    /**
     *
     * All games
     *
     * @type {{}}
     */
    var tables = {};

    /**
     * Game users
     * @type {{male: {}, female: {}}}
     */
    var users = {
        male: {},
        female: {}
    };

    /**
     *
     * Tables cache by type
     *
     * @type {{}}
     */
    var tablesCache = {
        vip: [],
        standard: []};

    /**
     * Rating values
     * @type {number}
     */
    var RATING_FOR_GAME = 5;
    var RATING_FOR_CHOSEN = 5;

    /**
     *
     * User authenticate when run game, one time
     *
     * @param data
     * @param sockConnection
     * @param successCallback
     * @returns {boolean}
     */
    this.auth = function (data, sockConnection, successCallback) {
        if (!data.user_id) {
            return false;
        }

        if (typeof(sockConnection) == "undefined") {
            return false;
        }

        var session = sessionManager.getSessionByUserId(data.user_id);
        var $new = false;

        if (session) {
            if (session.sessionId != sockConnection.id) {
                session.sessionId = sockConnection.id;
                session.connection = sockConnection;
                sessionManager.update(session.sessionId, session);

                if (successCallback) {
                    successCallback();
                }
            }
            $new = false;
        }
        else {
            var session = {};
            session.sessionId = sockConnection.id;
            session.userId = data.user_id;
            session.connection = sockConnection;
            sessionManager.add(session);

            $new = true;
        }

        if ($new) {
            var game = _userHasGame(data.user_id);
            if (game) {
                game.checkTableTime(null, function () {
                    _response(data.user_id, game.model(), 'gameResponse');
                });
            } else {
                var params = {result: true};
                _response(data.user_id, params, 'authenticated');
            }

            pool.getConnection(function (error, connection) {
                var sql = [];
                sql.push('UPDATE user_account SET');
                sql.push('online = 1');
                sql.push('WHERE user_id = ?');

                connection.query(sql.join(" "), [data.user_id], function () {
                    connection.end();

                    queryManager.userGet(data.user_id, function (user) {
                        if (user == undefined) return;

                        var gender = user.gender == 1 ? 'female' : 'male';
                        users[gender][user.user_id] = user;
                    });
                });
            });
        }

        if (successCallback) {
            successCallback();
        }

        return false;
    };

    /**
     *
     * Play without quque , check is vip user
     *
     * @param data
     */
    this.playWithoutQueue = function (data) {
        queryManager.isVip(data.user_id, function () {
            context.play(data, true);
        }, function () {
            context.play(data, false);
        });
    };

    /**
     *
     * User request for playing
     *
     * @param data
     * @param withoutQueue
     * @returns {boolean}
     */
    this.play = function (data, withoutQueue) {
        // User gender
        var gender = _getVariable(data.gender);

        // Current user_id
        var user_id = _getVariable(data.user_id == 1000 ? data.userId : data.user_id);

        // Validate vars
        if ((!gender || !user_id)) {
            return false;
        }

        // Check if user already has game, response game model!
        var found;
        if (found = _userHasGame(user_id)) {
            if (data.user_id == 1000) {
                _response(data.user_id, {result: true, iterator: found.iterator(), userId: user_id}, 'placed');
            }
            return false;
        }

        // Get required user gender
        gender = _genderGetSecure(gender);

        // User question for game
        var userQuestion = _getVariable(data.question);
        if (!userQuestion) {
            return false;
        }

        // Get free table for this user
        var game = _getTableFreeFor(gender, withoutQueue);

        // add new player to this game
        game.addPlayer({
            user_id: user_id,
            gender: gender
        }, userQuestion);

        // Check if table is ready send to all players else wait to others
        game.isReady(function (self) {
            self.users().forEach(function (user_id) {
                _response(user_id, game.model(), 'gameResponse');
            });
        }, function (self) {
            _response(data.user_id, {result: true, iterator: self.iterator(), userId: user_id}, 'placed');
        });
        return true;
    };

    /**
     *
     * User cancel playing
     *
     * @param data
     * @returns {boolean}
     */
    this.undoPlay = function (data) {
        // Required user id
        var user_id = _getVariable(data.user_id);

        // User gender
        var gender = _getVariable(data.gender);

        if (!gender || !user_id) {
            return false;
        }

        // Get current user gender security
        gender = _genderGetSecure(gender);

        // Get user game model
        var game = _tableGetSecure(data.iterator);

        console.log(game.model());
        if (!game) {
            return false;
        }

        // Remove player from game
        game.removePlayer({
            gender: gender,
            user_id: user_id
        });

        // No no user in the table , remove it!
        if (game.isEmpty()) {
            _deleteGame(game);
        }

        // Send user unplace response
        _response(user_id, {}, 'unPlaced');
        return true;
    };

    /**
     *
     * Check if step time expired
     *
     * @param data
     * @returns {boolean}
     */
    this.checkStepTimeExpired = function (data) {
        // Get game by iterator
        var game = _tableGetSecure(data.iterator);
        if (!game) {
            return false;
        }

        game.checkTableTime(data.step, function (currentStep) {
            // If is not end
            if (currentStep != 3) {
                game.users().forEach(function (user_id) {
                    _response(user_id, game.model(), 'gameResponse');
                });
            }

            // If game end finish game!
            if (currentStep == 3) {
                game.finishGame();

                _finishGame(game);
            }

        }, function () {
            // step not checked!
        });

        return true;
    };

    /**
     *
     * Set users answers
     *
     * @param data
     * @returns {boolean}
     */
    this.setAnswerToQuestion = function (data) {
        if (!data.gender || !data.iterator || !data.to || !data.user_id) {
            return false;
        }
        // Get table iterator
        var game = _tableGetSecure(data.iterator);
        if (!game) {
            return;
        }

        // Get current user
        var gender = _genderGetSecure(data.gender);

        // User id
        var user_id = data.user_id;

        // Set answer to user
        var user_to_id = data.to;

        // User answer
        var answer = _getVariable(data.answer);

        // user answer model
        var answerData = {
            user_id: user_id,
            answer: answer,
            user_to_id: user_to_id,
            gender: gender
        };

        // Check is it user table?!
        if (!game.hasUser(user_id)) {
            return false;
        }

        game.setAnswer(answerData);

        // If user set answers for all user , set done status
        if (game.hasUserAnswerAll(user_id)) {
            game.users().forEach(function (user_id) {
                _response(user_id, {
                    user_id: data.user_id
                }, 'setDoneStatus');
            });
        }

        // If all answers set , notify all user
        if (game.allAnswersDone()) {
            // Send response to users
            game.users().forEach(function (user_id) {
                _response(user_id, table, 'gameResponse');
            });
        }

        return true;
    };

    /**
     *
     * Set comments to answers
     *
     * @param data
     * @returns {boolean}
     */
    this.setLastAnswer = function (data) {
        if (!data.gender || !data.iterator) {
            return false;
        }

        // Get current gender
        var gender = _genderGetSecure(data.gender);

        // Owner user id
        var user_id = data.user_id;

        // Set comment to user
        var to_user_id = _getVariable(data.to);
        if (!to_user_id) {
            return false;
        }

        // Comment
        var lastAnswer = _getVariable(data.lastAnswer);

        // Get table by iterator
        var game = _tableGetSecure(data.iterator);
        if (!game) {
            return false;
        }

        // Estimate
        var estimate = data.estimate ? data.estimate : 0;

        // Comment data
        var comment = {
            user_id: user_id,
            answer: lastAnswer,
            estimate: estimate,
            gender: gender,
            to: to_user_id
        };

        // Check if user game player
        if (!game.hasUser(data.user_id)) {
            return false;
        }

        // Set comments
        game.setComment(comment);

        // Notify all users
        if (game.hasUserAllComments(user_id)) {
            game.users().forEach(function (user_id) {
                _response(user_id, {
                    user_id: data.user_id
                }, 'setDoneStatus');
            });
        }

        if (game.allCommentsDone()) {
            game.users().forEach(function (user_id) {
                _response(user_id, table, 'gameResponse');
            });
        }


        return true;
    };

    /**
     *
     * Set user chosen
     *
     * @param data
     */
    this.setChosen = function (data) {
        var game = _tableGetSecure(data.iterator);
        if (!game) {
            return;
        }

        if (!game.hasUser(data.user_id)) {
            return;
        }

        var d = {
            user_id: data.user_id,
            chosen: data.chosen
        };
        game.setChosen(d);

        game.users().forEach(function (user_id) {
            _response(user_id, {
                user_id: data.user_id
            }, 'chosenDone');
        });


        if (game.finished()) {
            // Finish game
            game.finishGame();

            _finishGame(game);
        }
    };

    /**
     *
     * Logout user from game
     *
     * @param user_id
     */
    this.logout = function (user_id) {
        if (typeof users['male'][user_id]) {
            delete users['male'][user_id];
        }

        if (typeof users['female'][user_id]) {
            delete users['female'][user_id];
        }
    };

    /**
     *
     * Return game binar screenshot
     *
     * @param data
     */
    this.getScreenShot = function (data) {
        var response = {
            maleCount: Object.keys(users['male']).length,
            femaleCount: Object.keys(users['female']).length
        };
        response['leader'] = leaderManager.getLeaderSecurity(data.city_id);
        response['ribbon'] = ribbonManager.get();

        _response(data.user_id, response, 'screenShot', data.callbackModule);
    };

    /**
     *
     * Set profile viewed
     *
     * @param data
     */
    this.setViewProfile = function (data) {
        queryManager.userGet(data.user_id, function (result) {
            if (result && (data.viewed_id != data.user_id)) {
                _response(data.viewed_id, result, 'showViewed', 'userManager');
            }
        });
    };

    /**
     *
     * Return random online users
     *
     * @param data
     */
    this.getRandomOnlineUsers = function (data) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT a.*, c.*');
            sql.push('FROM user_account a');
            sql.push('LEFT JOIN sys_cities c');
            sql.push('   ON a.city_id = c.id');
            sql.push('ORDER BY RAND() LIMIT ?');

            connection.query(sql.join(' '), [data.onlineUserLimit], function (result, rows) {
                connection.end();
                _response(data.user_id, {users: rows}, 'setOnlineUsers', 'onlineManager');
            });
        });
    };

    /**
     *
     * Return all games
     *
     * @returns {{}}
     */
    this.getTables = function () {
        return tables;
    };

    //-------------------------- PRIVATE METHODS -----------------------
    /**
     *
     * Delete game
     *
     * @param game
     * @private
     */
    var _deleteGame = function (game) {
        var cacheKey = game.isVipTable() ? 'vip' : 'standard';
        var index = tablesCache[cacheKey].indexOf(game.iterator());

        if (index != -1) {
            tablesCache[cacheKey].splice(index, 1);
        }

        delete tables[game.iterator()];
    };

    /**
     *
     * Finish game
     *
     * @param game
     * @private
     */
    var _finishGame = function (game) {
        // Send users result
        game.users().forEach(function (user_id) {
            var overlap = game.hasOverlap(user_id);

            if (overlap) {
                queryManager.getUserVKLink(overlap, function (vk_id) {
                    _response(user_id, extend(false, {}, game.model(), {vk_id: vk_id}), 'gameResponse');
                });
            } else {
                _response(user_id, game.model(), 'gameResponse');
            }
        });

        _deleteGame(game);
    };

    /**
     *
     * Check has user some game already
     *
     * @param user_id
     * @returns {*}
     * @private
     */
    var _userHasGame = function (user_id) {
        // There is some situation when "tables" is undefined. LOL :)
        if (tables == undefined) {
            return false;
        }

        // Foreach all games
        for (var key in tables) {
            var game = tables[key];

            console.log(key + '=>' + game.hasUser(user_id));

            if (game.hasUser(user_id)) {
                return game;
            }
        }

        return false;
    };

    /**
     *
     * Return valid gender
     *
     * @param gender
     * @returns {*}
     * @private
     */
    var _genderGetSecure = function (gender) {
        if (gender == 'male' || gender == 'female') {
            return gender;
        }

        if (gender == 1) {
            return 'female';
        }

        if (gender == 2) {
            return 'male';
        }

        return -1;
        //TODO: Fuck out this user!
    };

    /**
     *
     * Find table for gender
     *
     *
     * @param gender
     * @param withoutQueue
     * @returns {boolean}
     * @private
     */
    var _getTableFreeFor = function (gender, withoutQueue) {
        var keysVip = tablesCache['vip'],
            keysStandard = tablesCache['standard'];

        // Find in vip tables
        var found = _findTableInKeys(keysVip, gender);
        if (!found && !withoutQueue) {
            found = _findTableInKeys(keysStandard, gender);
        }

        // If not empty table generate new
        if (found === false) {
            // Create new game
            found = new gameModel(withoutQueue);

            // Set game iterator
            found.setIterator(_guid());

            // Cache game info
            var cacheKey = withoutQueue ? 'vip' : 'standard';
            tablesCache[cacheKey].push(found.iterator());

            // Add to list
            tables[found.iterator()] = found;
        }

        // TODO: immediately update gender spot in found game
        return found;
    };

    /**
     *
     * Find free table
     *
     * @param keys
     * @param gender
     * @returns {boolean}
     * @private
     */
    var _findTableInKeys = function (keys, gender) {
        var found = false;

        // Iterate from end
        keys.reverse();

        // Loop through tables
        keys.forEach(function (key) {
            // Current game
            var game = tables[key];

            // Ignore if is already playing
            if (game.isPlaying() || game.isLocked()) return false;

            // Check is there free spot
            if (game.checkGenderSpot(gender)) {
                return (found = game);
            }

            return false;
        });

        return found;
    };

    /**
     *
     * Return trimmed variable
     *
     * @param variable
     * @returns {string}
     * @private
     */
    var _getVariable = function (variable) {
        return variable ? variable.toString().trim() : '';
    };

    /**
     *
     * Return game by iterator if iterator is valid
     *
     * @param iterator
     * @returns {*}
     * @private
     */
    var _tableGetSecure = function (iterator) {
        if (tables == undefined) return false;

        if (tables.hasOwnProperty(iterator)) {
            return tables[iterator]
        }

        return false;
    };

    /* ====================================================================================================== */

    /**
     * @param userId
     * @param responseData
     * @param action
     * @param module
     * @private
     */
    var _response = function (userId, responseData, action, module) {
        responseData['module'] = module ? module : 'game';
        appManager.response(userId, responseData, action);
    };

    /**
     *
     * Generate unique id
     *
     * @returns {string}
     * @private
     */
    var _guid = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };

    /**********************************************************************************************************/
}

module.exports = gameV2;