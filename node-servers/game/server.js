var express = require('express')();
var sockjs = require('sockjs');
var http = require('http');
var mysql = require('mysql');
var util = require('util');
var req = require('request');
var merge = require('array-merge')

extend = require('extend');
md5 = require('MD5');
gameV2 = require('./gameV2');
leaderManager = require('./leaderManager');
messenger = require('./messenger');
sessionManager = require('./sessionManager');
queryManager = require('./queryManager');
ribbonManager = require('./ribbonManager');
appManager = require('./appManager');
notificationManager = require('./notificationManager');
userManager = require('./userManager');
robotLenta = require('./robots/lenta.js');
robotLider = require('./robots/lider.js');
apiManager = require('./apiManager.js');
gameModel = require('./models/gameModel.js');
dateFormat = require('dateformat');

var isLocal = require('./config/isLocal');
if (isLocal === true) {
    config = require('./config/localConfig');
} else {
    config = require('./config/remoteConfig');
}

log = function (m, e) {
    console.log(util.inspect(m, false, null));
};

var broadcast = {};
var sjs_broadcast = sockjs.createServer(config.server_opts);

connection = mysql.createConnection(config.db);

connection.connect(function (err) {
    if (err) {
        throw "Mysql connection error: " + err;
    }
});

pool = mysql.createPool(config.db);

sessionManager = new sessionManager();
gameV2 = new gameV2();
messenger = new messenger();
queryManager = new queryManager();
leaderManager = new leaderManager();
ribbonManager = new ribbonManager();
notificationManager = new notificationManager();
userManager = new userManager();
apiManager = new apiManager();
appManager = new appManager();

// Run system robots
robotLenta = new robotLenta();
/*robotLider = new robotLider();
 */
leaderManager.init();
ribbonManager.update();
notificationManager.init();

sjs_broadcast.on('connection', function (conn) {
    conn.on('close', function () {
        var userId = sessionManager.getUserIdBySessionId(conn.id);

        pool.getConnection(function (err, connection) {
            var sql = [];
            sql.push('UPDATE user_account SET');
            sql.push('online = 0');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [userId], function () {
                connection.end();
                gameV2.logout(userId);
            });
        });

        sessionManager.remove(conn.id);
    });

    conn.on('data', function (m) {
        log(m);
        var data;

        try {
            data = JSON.parse(m);
            log(data);
        } catch (e) {
            log('Request parsing error: ', e);
        }


        var module = getModule(data.module);
        if (!module) {
            conn.close();
            sessionManager.remove(conn.id);

            return;
        }
        module = eval(module);

        if (typeof(data.action) != 'undefined' && data.action) {
            gameV2.auth(data, conn, function () {
                if (typeof (module[data.action]) != 'undefined') {
                    module[data.action](data);
                } else {
                    conn.close();
                    sessionManager.remove(conn.id);
                }
            });
        }
    });
});

var getModule = function (module) {
    module = (module == undefined) ? "game" : module;

    switch (module) {
        case "game":
            module = "gameV2";
            break;
        case "gameV2":
            module = "gameV2";
            break;
        case "Messanger":
            module = "messenger";
            break;
        case "messanger":
            module = "messenger";
            break;
        case "leaderManager":
            module = "leaderManager";
            break;
        case "ribbonManager":
            module = "ribbonManager";
            break;
        case "userManager":
            module = "userManager";
            break;
        case "notificationManager":
            module = "notificationManager";
            break;
        case "apiManager":
            module = "apiManager";
            break;
        default:
            module = false;
    }

    return module;
};

setInterval(function () {
    log('stay ssh connection alive');
}, 20000);

var server = http.createServer();
sjs_broadcast.installHandlers(server, {
    prefix: '/broadcast',
    disconnect_delay: 60000
});

server.listen(config.port, config.host);