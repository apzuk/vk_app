function notificationManager() {

    var context = this, notifications;

    var NEW_MESSAGE_NOTIFICATION = 1;
    var NEW_LIKE_NOTIFICATION = 2;
    var BECOME_SPONSOR = 3;
    var RETINUE_LOST_NOTIFICATION = 4;
    var BOUGHT_SELF = 5;
    var FROM_REFERRAL = 14;

    /**
     * @param data
     * @param success_callback
     * @param error_callback
     */
    this.fromReferral = function (data, success_callback, error_callback) {
        var sender_user_id = data.sender_user_id;
        var receiver_user_id = data.receiver_user_id;

        var notif = {
            tmpl_id: FROM_REFERRAL,
            receiver_user_id: receiver_user_id,
            sender_user_id: sender_user_id,
            data: {
                count: data.count
            }
        };

        context.set(notif, success_callback, error_callback);
    };

    /**
     * @param data
     */
    this.newMessage = function (data) {
        var sender_user_id = data.sender_user_id;
        var receiver_user_id = data.receiver_user_id;
        var message_id = data.message_id;

        var notif = {
            tmpl_id: NEW_MESSAGE_NOTIFICATION,
            receiver_user_id: receiver_user_id,
            sender_user_id: sender_user_id,
            data: {
                message_id: message_id
            }
        };

        context.set(notif);
    };

    /**
     * @param data
     * @param success_callback
     * @param error_callback
     */
    this.newLike = function (data, success_callback, error_callback) {
        var user_id = data.user_id;
        var target_user_id = data.target_user_id;

        var notif = {
            tmpl_id: NEW_LIKE_NOTIFICATION,
            receiver_user_id: target_user_id,
            sender_user_id: user_id,
            data: {
                end: ''
            }
        };

        context.set(notif, success_callback, error_callback);
    };

    /**
     * @param sender_u_id
     * @param uName
     * @param uId
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.lostRetinue = function (sender_u_id, uName, uId, user_id, success_callback, error_callback) {
        var notif = {
            tmpl_id: RETINUE_LOST_NOTIFICATION,
            receiver_user_id: user_id,
            sender_user_id: sender_u_id,
            data: {
                user_id: uId,
                username: uName
            }
        };

        context.set(notif, success_callback, error_callback);
    };

    /**
     * @param sender_user_id
     * @param receiver_user_id
     * @param success_callback
     * @param error_callback
     */
    this.becomeSponsor = function (sender_user_id, receiver_user_id, success_callback, error_callback) {

        var notif = {
            tmpl_id: BECOME_SPONSOR,
            sender_user_id: sender_user_id,
            receiver_user_id: receiver_user_id,
            data: {

            }
        };

        context.set(notif, success_callback, error_callback);
    };

    /**
     *
     * @param data
     */
    this.sayUserToUpdateNotifications = function (data) {
        _response(data.to_user_id, {}, 'updateProfileInfo', 'userManager');
    };

    /**
     * @param sender_user_id
     * @param receiver_user_id
     * @param success_callback
     * @param error_callback
     */
    this.boughtSelf = function (sender_user_id, receiver_user_id, success_callback, error_callback) {
        var notif = {
            tmpl_id: BOUGHT_SELF,
            sender_user_id: sender_user_id,
            receiver_user_id: receiver_user_id,
            data: {

            }
        };

        context.set(notif, success_callback, error_callback);
    };

    /**
     * @param data
     * @param success_callback
     * @param error_callback
     * @returns {boolean}
     */
    this.set = function (data, success_callback, error_callback) {
        context.setNewNotification(data, success_callback, error_callback);
        return true;
    };

    this.init = function () {
        notifications = {};
        this.loadNotificationTypes(function (rows) {
            rows.forEach(function (row) {
                notifications[row.tmpl_id] = row;
            });
        });
    };

    /**
     * @param data
     */
    this.makeASRead = function (data) {
        this.setNotificationGotStatus(data.id, data.user_id, function (result) {
            _response(data.user_id, {id: data.id}, 'changeNotifStatus');
        });

        this.removeVkNotif(data.id);
    };

    //========================mysql part========================//
    this.loadNotificationTypes = function (callback) {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                return;
            }

            var sql = [];
            sql.push('SELECT *');
            sql.push('FROM notification_tmpl');

            connection.query(sql.join(" "), [], function (error, result) {
                connection.end();

                if (callback) {
                    callback(result);
                }
            });
        });
    };

    this.setNotificationGotStatus = function (id, user_id, callback) {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                return;
            }

            var sql = [];
            sql.push('UPDATE notification SET');
            sql.push('status="got"');
            sql.push('WHERE id=? AND user_id=?');

            connection.query(sql.join(" "), [id, user_id], function (error, result) {
                connection.end();

                if (callback) {
                    callback(result);
                }
            });
        });
    };

    this.removeVkNotif = function (notif_id) {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                return;
            }

            var sql = [];
            sql.push('DELETE FROM log_vk_notif');
            sql.push('WHERE notif_id=?');

            connection.query(sql.join(" "), [notif_id], function (error, result) {
                connection.end();
            });
        });
    };

    /**
     * @param data
     * @param success_callback
     * @param error_callback
     */
    this.setNewNotification = function (data, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {
            if (error) {
                if (error_callback) error_callback();
                return;
            }

            var sql = [];
            sql.push('INSERT INTO notification SET');
            sql.push('tmpl_id = ?,');
            sql.push('data = ?,');
            sql.push('user_id = ?,');
            sql.push('owner_user_id = ?');

            var d = [data.tmpl_id, JSON.stringify(data.data).toString("utf8"), data.receiver_user_id, data.sender_user_id];
            connection.query(sql.join(" "), d, function (error, result) {
                connection.end();

                if (success_callback) {
                    success_callback(result);
                }

                if (error_callback) {
                    error_callback();
                }
            });
        });
    };

    /* ====================================================================================================== */

    /**
     *
     * Response to use
     *
     * @param userId
     * @param message
     * @param action
     * @param module
     * @private
     */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'notifManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/
}
module.exports = notificationManager;
