function gameModel(isVip) {

    /**
     *
     * This object
     *
     * @type {gameModel}
     */
    var context = this;

    /**
     * Step durations
     * @type {{step1: number, step2: number, step3: number}}
     */
    var stepDuration = {
        step1: 90,
        step2: 60,
        step3: 25
    };

    /**
     * Rating value for playing
     * @type {number}
     */
    var RATING_FOR_GAME = 5;

    /**
     *
     * Rating value for chosen
     *
     * @type {number}
     */
    var RATING_FOR_CHOSEN = 5;

    /**
     *
     * Game status
     *
     * @type {boolean}
     */
    var finished = false;

    /**
     *
     * Is vip table, playing without turn
     *
     * @type {boolean}
     */
    var isVipTable = false;

    /**
     *
     * game model
     *
     * @type {{}}
     */
    var model = null;

    /**
     *
     * Current game robot
     *
     * @type {null}
     */
    var robot = null;

    /**
     *
     * Table is locked
     *
     * @type {null}
     */
    var locked = null;

    if (!model) {
        model = {
            step: 0,
            male: {},
            female: {},
            chosen: {},
            answersReport: {},
            commentReport: {},
            estimate: {},
            users: [],
            maleCount: 0,
            femaleCount: 0,
            playing: false,
            answerCount: 0,
            lastAnswerCount: 0,
            step1checked: false,
            step2checked: false,
            step3checked: false,
            step4checked: false,
            keys: {},
            stepDuration: stepDuration
        };

        context.robot = new gameRobot(context);

        if (isVip) {
            isVipTable = true;
        }
    }

    //###############################################################
    /**
     *
     * Set self iterator
     *
     * @param iterator
     */
    this.setIterator = function (iterator) {
        if (model['iterator'] == undefined) {
            model['iterator'] = iterator;
        }
    };

    /**
     *
     * Add new player to game
     *
     * @param user
     * @param question
     */
    this.addPlayer = function (user, question) {
        if (context.hasUser(user.user_id)) {
            return false;
        }

        var c = ++model[user.gender + 'Count'];
        model[user.gender][user.user_id] = {
            user_id: user.user_id,
            question: question,
            gameIterator: c
        };
        model['users'].push(parseInt(user.user_id));
        model['answersReport'][user.user_id] = 0;
        model['commentReport'][user.user_id] = 0;

        log(model);

        // success
        return true;
    };

    /**
     *
     * Remove user from game
     *
     * @param user
     */
    this.removePlayer = function (user) {
        // Unset user from user list
        if (model[user.gender].hasOwnProperty(user.user_id)) {
            delete model[user.gender][user.user_id];
        }

        if (model['answersReport'].hasOwnProperty(user.user_id)) {
            delete model['answersReport'][user.user_id];
        }

        if (model['commentReport'].hasOwnProperty(user.user_id)) {
            delete model['commentReport'][user.user_id];
        }

        // User user from his table
        var pos = model['users'].indexOf(parseInt(user.user_id));
        if (pos >= 0) {
            model['users'].splice(pos, 1);
        }

        // Set user count
        if (model[user.gender + 'Count'] > 0) {
            --model[user.gender + 'Count'];
        }
    };

    /**
     *
     * Is there any players?
     *
     * @returns {boolean}
     */
    this.isEmpty = function () {
        return context.users().length == 0;
    };

    /**
     *
     * Check if passed user playing in this game?!
     *
     * @param user_id
     * @returns {boolean}
     */
    this.hasUser = function (user_id) {
        if (model['users'] == undefined) {
            return false;
        }

        return model['users'].indexOf(parseInt(user_id)) != -1;
    };

    /**
     * Is vip table
     * @returns {boolean}
     */
    this.isVipTable = function () {
        return isVipTable;
    };

    /**
     *
     * Check if game already starts
     *
     * @returns boolean
     */
    this.isPlaying = function () {
        return model['isPlaying'];
    };

    /**
     *
     * Check is there spot for gender
     *
     * @param gender
     * @return boolean
     */
    this.checkGenderSpot = function (gender) {
        return model[gender + 'Count'] <= 2;
    };

    /**
     *
     * Is table locked
     *
     * @returns {null}
     */
    this.isLocked = function () {
        return locked;
    };

    /**
     *
     * Get self iterator
     *
     * @return string
     */
    this.iterator = function () {
        return model.iterator;
    };

    /**
     *
     * Check if game is ready for start
     *
     * @param ready
     * @param waiting
     */
    this.isReady = function (ready, waiting) {
        var check = model.femaleCount == 3 && model.maleCount == 3;

        if (check) {
            // Otherwise select players data from DB!
            context.loadPlayersData(function () {
                // Game start
                model['playing'] = true;

                ready(context);

                model.dateUpdate = new Date();
            });
        }
        else {
            waiting(context);
        }
    };

    /**
     *
     * Get game players
     *
     * @returns {*|users|users|users|Function}
     */
    this.users = function () {
        return model.users;
    };

    /**
     *
     * Load players data from db
     *
     */
    this.loadPlayersData = function (callback) {

        pool.getConnection(function (err, connection) {
            var sql = [];
            sql.push('SELECT a.*, NOW() < vip_end AS isVip, s.rating');
            sql.push('FROM user_account a');
            sql.push('LEFT JOIN user_statistic s');
            sql.push('     ON s.user_id = a.user_id');
            sql.push('WHERE a.user_id IN (' + context.users().join(',') + ')');

            connection.query(
                sql.join(" "),
                [],
                function (error, result) {
                    connection.end();

                    if (error) {
                        return log(error);
                    }

                    result.forEach(function (e) {
                        var gender = e['gender'] == 1 ? 'female' : 'male';
                        var uId = e['user_id'];

                        model[gender][uId]['username'] = e['username'];
                        model[gender][uId]['avatar'] = e['avatar'];
                        model[gender][uId]['birth_date'] = e['birth_date'];
                        model[gender][uId]['vip'] = e['isVip'];
                        model[gender][uId]['color'] = e['color'];
                        model[gender][uId]['rating'] = e['rating'];
                        model[gender][uId]['ref_id'] = e['ref_id'];
                    });
                    model['dateUpdate'] = new Date();

                    if (callback) {
                        callback();
                    }
                }
            );
        });
    };

    /**
     *
     * Check if current step time expired
     *
     * @param step
     * @param expired
     * @param notExpired
     * @returns {boolean}
     */
    this.checkTableTime = function (step, expired, notExpired) {
        if (!step) step = model.step;

        // game step
        var currentStep = parseInt(step) + 1;

        // If already checked ignore request!
        if (model['step' + currentStep + 'checked']) {
            return false;
        }

        // Step already done
        model['step' + currentStep + 'checked'] = true;

        // Now!
        var t2 = new Date(), diff = t2.getTime() - model.dateUpdate.getTime();

        // Diif of now and last update date
        var Seconds_Between_Dates = Math.abs(diff / 1000);

        console.log(Seconds_Between_Dates + '|' + stepDuration['step' + currentStep] + '|' + ('step' + currentStep));
        // If time expired send to all users for next step
        if (Seconds_Between_Dates >= stepDuration['step' + currentStep]) {
            // Step up!
            model.step++;

            // Set last update date to now
            model.dateUpdate = new Date();

            if (expired) expired(currentStep);
        }
        else {
            if (notExpired) notExpired();
            model['step' + currentStep + 'checked'] = false;
        }
        return true;
    };

    /**
     *
     * Set user answers qo questions
     *
     * @param data
     */
    this.setAnswer = function (data) {
        // If not exists index create it!
        if (!model[data.gender].hasOwnProperty(data.user_to_id)) {
            model[data.gender][data.user_to_id] = {};
        }

        // If not exists index create it!
        if (!model[data.gender][data.user_to_id].hasOwnProperty('answer')) {
            model[data.gender][data.user_to_id]['answer'] = {};
        }

        // Answers count in table
        model['answerCount']++;

        // Set answer model
        model[data.gender][data.user_to_id]['answer'][data.user_id] = {
            user_id: data.user_id,
            answer: data.answer
        };

        // Answers count for user
        model['answersReport'][data.user_id]++;
    };

    /**
     *
     * Set comment for user answer
     *
     * @param comment
     * @returns {boolean}
     */
    this.setComment = function (comment) {
        // is the game player?!
        if (!context.hasUser(comment.to)) {
            return false;
        }

        // Create needed keys
        if (!model[comment.gender][comment.to].hasOwnProperty('lastAnswer')) {
            model[comment.gender][comment.to]['lastAnswer'] = {};
        }

        // set answers reports
        model.lastAnswerCount++;
        model[comment.gender][comment.to]['lastAnswer'][comment.user_id] = comment;
        model["commentReport"][comment.user_id]++;

        // Init estimate
        if (!model['estimate'][comment.to]) {
            model['estimate'][comment.to] = {};
        }

        // Set estimate
        model['estimate'][comment.to][comment.user_id] = comment.estimate;

        return true;
    };

    /**
     *
     * Is user set comments to all users
     *
     * @param user_id
     * @returns {boolean}
     */
    this.hasUserAllComments = function (user_id) {
        return model['commentReport'][user_id] == 3;
    };

    /**
     *
     * If all users set comments fo all answers
     *
     * @returns {boolean}
     */
    this.allCommentsDone = function () {
        if (model['lastAnswerCount'] != 18) {
            return false;
        }

        model['step'] = 2;
        model['dateUpdate'] = new Date();

        return true;
    };

    /**
     *
     * Set user chosen
     *
     * @param chosen
     */
    this.setChosen = function (chosen) {
        model['chosen'][chosen.user_id] = chosen.chosen;
    };

    this.finishGame = function () {
        // Set finished status
        finished = true;

        model.step = 3;
        model.dateUpdate = new Date();

        context.users().forEach(function (user_id) {
            var rating = RATING_FOR_GAME + _userChosenCount(user_id) * RATING_FOR_CHOSEN;

            // Add estimate
            rating += _userGetRatingFromEstimate(user_id);

            var u = _getUser(user_id),
                ref_id = u && u['ref_id'] ? u['ref_id'] : null,
                u_r = u && u['rating'] ? u['rating'] + rating : null;

            queryManager.userUpdateGameCount(user_id);
            queryManager.userSetRatingHistory(user_id, rating, '+', 'game', ref_id, u_r, false);
        });

        delete context.robot;
    };

    /**
     *
     * Is game finished?!
     *
     * @returns {boolean}
     */
    this.finished = function () {
        return Object.keys(model['chosen']).length == 6 && !finished;
    };

    /**
     *
     * If current user answers all questions?!
     *
     * @param user_id
     * @returns {boolean}
     */
    this.hasUserAnswerAll = function (user_id) {
        return model['answersReport'][user_id] == 3;
    };

    /**
     *
     * Check if all user set answers to all questions?!
     *
     * @returns {boolean}
     */
    this.allAnswersDone = function () {
        if (model['answerCount'] != 18) {
            return false;
        }

        // Set game step
        model['step'] = 1;

        // Last update date to now!
        model['dateUpdate'] = new Date();

        return true;
    };

    /**
     *
     * Check if there is overlap for user in the game
     *
     * @param table
     * @param user_id
     * @returns {*}
     */
    this.hasOverlap = function (user_id) {
        var userChose = model['chosen'][user_id], overlap = false;

        if (model['chosen'][userChose] != undefined &&
            model['chosen'][userChose] == user_id) {
            return userChose;
        }

        return false;
    };

    /**
     *
     * return model
     *
     * @returns {{}}
     */
    this.model = function () {
        return model;
    };

    // ===============================================================
    /**
     *
     * Get how many chosen has user
     *
     * @param user_id
     * @returns {number}
     * @private
     */
    var _userChosenCount = function (user_id) {
        var chosen = model['chosen'], count = 0;

        for (var key in chosen) {
            if (chosen[key] == user_id) {
                count++;
            }
        }

        return count;
    };

    /**
     *
     * Calculate and get user rating from estimate
     *
     * @param user_id
     * @param table
     * @returns {number}
     */
    var _userGetRatingFromEstimate = function (user_id) {
        var rating = 0;

        // There is no estimate in game!
        if (!model['estimate']) {
            return rating;
        }

        // There is no estimate for this user
        if (!model['estimate'][user_id]) {
            return rating;
        }

        // Loop estimates
        for (var estimate_from in model['estimate'][user_id]) {
            var value = model['estimate'][user_id][estimate_from];

            if (value == null || parseInt(value) == "NaN") continue;

            rating += parseInt(value);
        }

        // Response
        return rating;
    };

    var _getUser = function (user_id) {
        for (var key in model['female']) {
            var u = model['female'][key];

            if (key == user_id) return u;
        }

        for (var key in model['male']) {
            var u = model['male'][key];

            if (key == user_id) return u;
        }

        return false;
    };
}

var gameRobot = function (gModel) {
    var fakeStep = 0;

    var intervals = [
        1.2 * 60 * 1000,
        1.2 * 60 * 1000,
        1.2 * 60 * 1000
    ];

    var context = this;

    this.start = function () {
        setTimeout(function () {
            log('aaaa1');
            if (gModel.isPlaying()) {
                return;
            }

            log('aaaa2');

            if (gModel.model().femaleCount < 3) {
                var player = appManager.randomPlayer('female');

            } else if (gModel.model().maleCount < 3) {
                fakeStep = gModel.model().maleCount - 1;
                var player = appManager.randomPlayer('male');
            }
            log('aaaa3');

            if (!player || player == undefined) {
                return;
            }

            fakeStep++;
            if (fakeStep > 2) fakeStep = 2;
            gModel.addPlayer(player.user, player.question);
            log('aaaa4');

            gModel.isReady(function (self) {
                self.users().forEach(function (user_id) {
                    _response_(user_id, gModel.model(), 'gameResponse');
                });
            }, function (self) {
                context.start();
            });

        }, intervals[fakeStep]);
    };
    this.start();

    /* ====================================================================================================== */
    var _response = function (userId, message, action, module) {
        module = module ? module : 'game';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/
};

module.exports = gameModel;