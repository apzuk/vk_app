function userModel(data) {

    /**
     *
     * User data container
     *
     * @type {{}}
     */
    var model = null;

    if (data) {
        model = data;
    }

    //##############################################################################
    this.load = function () {
        queryManager.getUser(model.user_id, function (user) {
            model = user
        });
    }
}

module.export = userModel;