function Messenger() {
    var context = this, data;

    /**
     * @param data
     */
    this.saveWithAttachment = function (data) {
        queryManager.isVip(data.sender_user_id, function () {
            context.save(data, true);
        });
    };

    this.save = function (data, isVip) {
        pool.getConnection(function (err, connection) {
            var sql = [];

            sql.push('SELECT *');
            sql.push('FROM user_blacklist');
            sql.push('WHERE (user_id = ? AND blocked_user_id = ?)');
            sql.push('  OR (user_id = ? AND blocked_user_id = ?)');

            log(sql.join(" "));
            connection.query(
                sql.join(" "),
                [data.sender_user_id, data.receiver_user_id, data.receiver_user_id, data.sender_user_id],
                function (error, result) {
                    connection.end();

                    log(error);

                    if (result.length > 0) {
                        log(result);
                    } else {
                        log('bbb');
                        context.saveMessage(data, true, isVip)
                    }
                }
            )
        });
    };

    /**
     *
     * @param data
     * @param isInternalCall
     * @param isVip
     */
    this.saveMessage = function (data, isInternalCall, isVip) {
        if (!isInternalCall) {
            return;
        }

        pool.getConnection(function (err, connection) {

            var sql = [];
            sql.push('INSERT INTO im_message SET');
            sql.push('sender_user_id = ?,');
            sql.push('receiver_user_id = ?,');
            sql.push('message = ?,');
            sql.push('sender_status = "read",');
            sql.push('conv_id = ?,');
            sql.push('attachment = ?');

            if (!data.conv_id) {
                data.conv_id = md5(data.receiver_user_id + '_prch123_tran_qaqlan_' + data.sender_user_id);
            }

            var params = [data.sender_user_id, data.receiver_user_id, data.message , data.conv_id, isVip && data.attachment ? JSON.stringify(data.attachment) : '' ];

            connection.query(
                sql.join(" "),
                params,
                function (error, result) {
                    connection.end();

                    if (error) {
                        log(error);
                        return false;
                    }

                    var lastId = result.insertId;

                    pool.getConnection(function (err, connection) {
                        connection.end();

                        if (err) {
                            return false;
                        }

                        var sql = [];
                        sql.push('SELECT im.*, u1.*, u2.online AS is_online, im.create_ts AS msg_date, im.attachment');
                        sql.push('FROM im_message im');
                        sql.push('LEFT JOIN user_account u1');
                        sql.push('    ON u1.user_id = im.sender_user_id');
                        sql.push('LEFT JOIN user_account u2');
                        sql.push('    ON u2.user_id = im.receiver_user_id');
                        sql.push('WHERE message_id = ?');

                        connection.query(sql.join(" "), [lastId], function (error, result) {
                            connection.end();

                            result.forEach(function (value, index) {
                                var d = value.msg_date;

                                var month = d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth();
                                var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
                                var hours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
                                var minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
                                var seconds = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
                                value.msg_date = d.getFullYear() + '-' + month + '-' + day + ' ' +
                                    hours + ':' + minutes + ':' + seconds;
                            });

                            _response(data.sender_user_id, {
                                messages: result,
                                u_id: data.receiver_user_id,
                                user_id: data.user_id,
                                is_new: 1,
                                conv_id: data.conv_id
                            }, 'messageShow');

                            if (result[0]['is_online']) {
                                _response(data.receiver_user_id, {
                                    messages: result,
                                    u_id: data.sender_user_id,
                                    is_new: 1,
                                    user_id: data.user_id,
                                    conv_id: data.conv_id
                                }, 'messageShow');
                                /*
                                 notificationManager.newMessage({
                                 sender_user_id: result[0]['sender_user_id'],
                                 receiver_user_id: result[0]['receiver_user_id'],
                                 message_id: result[0]['message_id']
                                 });

                                 context.sendMessageToUser(data.receiver_user_id, {
                                 }, 'updateNotif', 'notifManager');*/
                            }
                            else {
                                notificationManager.newMessage({
                                    sender_user_id: result[0]['sender_user_id'],
                                    receiver_user_id: result[0]['receiver_user_id'],
                                    message_id: result[0]['message_id']
                                });
                            }
                        });
                    });
                }
            );
        });
    };

    this.getConversation = function (data) {
        pool.getConnection(function (error, connection) {
            var offset = (20 * parseInt(data.page));

            var sql = [];
            sql.push('SELECT *, m.create_ts AS msg_date');
            sql.push('FROM im_message m');
            sql.push('LEFT JOIN user_account a');
            sql.push('   ON a.user_id = m.sender_user_id');
            sql.push('WHERE ((sender_user_id = ? AND receiver_user_id = ?) OR (sender_user_id = ? AND receiver_user_id = ?) ) AND ');
            sql.push('  ((sender_user_id = ? AND sender_status != "remove") OR');
            sql.push('  (receiver_user_id = ? AND receiver_status != "remove"))');
            sql.push('ORDER BY message_id DESC');
            sql.push('LIMIT 20 OFFSET ?');

            var queryParams = [data.user_id, data.u_id, data.u_id, data.user_id, data.user_id, data.user_id, offset];
            connection.query(sql.join(" "), queryParams, function (e, result) {
                connection.end();

                var method = data.type && data.type == "load-old" ? 'messagePrepend' : 'messageShow';

                result.forEach(function (value, index) {
                    var d = value.msg_date;

                    var month = d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth();
                    var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate();
                    var hours = d.getHours() < 10 ? '0' + d.getHours() : d.getHours();
                    var minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
                    var seconds = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
                    value.msg_date = d.getFullYear() + '-' + month + '-' + day + ' ' +
                        hours + ':' + minutes + ':' + seconds;
                });

                _response(data.user_id, {
                    messages: result,
                    security_id: data.security_id,
                    u_id: data.u_id,
                    user_id: data.user_id,
                    page: data.page
                }, method);
            });
        });
    };

    this.removeMessage = function (data) {
        queryManager.checkUserSid(data.user_id, data.sid, function () {
            queryManager.messageRemove(data.message_id, data.user_id, function () {
                _response(data.user_id, {message_id: data.message_id}, 'messageRemoved');
            });
        }, function () {
        });
    };

    this.makeAsRead = function (data) {
        queryManager.makeMessageAsRead(data.id, data.receiver_user_id, function () {
            _response(data.receiver_user_id, {id: data.id, with_user_id: data.sender_user_id}, 'changeMessageStatus');
            _response(data.sender_user_id, {id: data.id, with_user_id: data.receiver_user_id}, 'changeMessageStatus');
        });
    };

    /* ====================================================================================================== */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'messageManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/
}

module.exports = Messenger;