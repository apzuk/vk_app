function leaderManager() {

    var leaders = {};

    var context = this;

    this.init = function () {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT *, NOW() < a.vip_end AS vip');
            sql.push('FROM user_leader l');
            sql.push('LEFT JOIN user_account a');
            sql.push('   ON a.user_id = l.user_id');

            connection.query(sql.join(" "), [], function (error, rows) {
                connection.end();

                rows.forEach(function (row) {
                    leaders[row['city_id']] = row;
                });

                context.checkLeaders();
                setInterval(context.checkLeaders, 1000 * 60);
            });
        });
    };

    /**
     * Check and update leader cost
     */
    this.checkLeaders = function () {
        for (var city_id in leaders) {
            var leader = context.getLeaderSecurity(city_id);

            if (!leader) continue;

            var day = new Date(dateFormat(leader.update_date, "yyyy-mm-dd H:MM:ss")),
                now = new Date(),
                minutes = Math.round((now.getTime() - day.getTime()) / (1000 * 60));

            if (parseInt(minutes) >= 5 && leader['current_bid'] > 1) {

                leader['current_bid'] = --leader['current_bid'];
                leader['update_date'] = dateFormat(new Date(), "yyyy-mm-dd H:MM:ss");

                //update in db
                pool.getConnection(function (error, connection) {
                    var sql = [];
                    sql.push('UPDATE user_leader SET');
                    sql.push('current_bid = ?,');
                    sql.push('update_date = NOW()');
                    sql.push('WHERE city_id = ?');

                    connection.query(sql.join(" "), [leader['current_bid'], leader['city_id']], function (error, rows) {
                        connection.end();
                    });
                });
            }
        }
    };

    /**
     *
     * Check if leader is valid
     *
     * @param city_id
     * @returns {*}
     */
    this.getLeaderSecurity = function (city_id) {
        if (leaders.hasOwnProperty(city_id) && leaders[city_id]) {
            return leaders[city_id];
        }

        return false;
    };

    /**
     *
     * Update city leader
     *
     * @param data
     */
    this.updateCityLeader = function (data) {

        if (!data['city_id']) {
            return;
        }

        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT *, NOW() < a.vip_end AS vip');
            sql.push('FROM user_leader l');
            sql.push('LEFT JOIN user_account a');
            sql.push('   ON a.user_id = l.user_id');
            sql.push('WHERE l.city_id = ?');

            connection.query(sql.join(" "), [data['city_id']], function (error, rows) {
                connection.end();

                if (!rows) return;
                leaders[data['city_id']] = rows[0];
            });
        });

        if (data.last_leader_id) {
            queryManager.userGet(data.user_id, function (user) {
                var l = context.getLeaderSecurity(data.city_id);
                if (l) {
                    user['bid'] = l['current_bid'];
                    user['message'] = l['message'];
                    user['vip'] = l['vip'];
                    user['color'] = l['color'];
                }

                // notify last leader
                _response(data.last_leader_id, user, 'lostLeaderness', 'userManager');
            });
        }
    };

    /* ====================================================================================================== */
    /**
     *
     * Response to user
     *
     * @param userId
     * @param message
     * @param action
     * @param module
     * @private
     */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'leaderManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/
}
module.exports = leaderManager;
