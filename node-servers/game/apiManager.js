function apiManager() {

    var context = this;

    this.gameGetTables = function (data) {
        var tables = gameV2.getTables();

        var d = [];

        for (var key in tables) {
            d.push(tables[key].model());
        }

        _response(data.user_id, {tables: d}, 'apiResponse');
    };

    /* ====================================================================================================== */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'apiManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/

}

module.exports = apiManager;