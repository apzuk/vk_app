function appManager() {
    /**
     *
     * All system question
     *
     * @type {{all: Array, female: Array, male: Array}}
     */
    var questions = {
        all: [],
        female: [],
        male: []
    };

    /**
     *
     * All fake users
     *
     * @type {{male: Array, female: Array}}
     */
    var fakeUsers = {
        male: [],
        female: []
    };

    /**
     *
     * appManager
     *
     * @type {appManager}
     */
    var context = this;

    this.randomQuestion = function (gender) {
        if (gender == 1) gender = 'female';
        if (gender == 2) gender = 'male';

        if (gender != "male" && gender != "female") {
            return '';
        }

        var questionsMerged = questions['all'].concat(questions[gender]);
        return questionsMerged[_getRandomArbitrary(0, questionsMerged.length - 1)];
    };

    this.randomPlayer = function (gender) {
        var array = fakeUsers[gender],
            random = _getRandomArbitrary(0, array.length - 1);

        return {
            user: array[random],
            question: context.randomQuestion(gender)
        };
    };

    this.response = function (userId, dataResponse, action) {
        var session = sessionManager.getSessionByUserId(userId);

        dataResponse['action'] = action;
        dataResponse['serverTime'] = new Date();

        log(dataResponse);

        if (session) {
            session.connection.write(JSON.stringify(dataResponse));
        }
    };

    /* ================================================================================================ */
    var _getRandomArbitrary = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var _init = function () {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT *');
            sql.push('FROM sys_questions');

            connection.query(sql.join(" "), [], function (error, result) {
                connection.end();

                var g;
                result.forEach(function (value, index) {
                    switch (value.gender) {
                        case 0:
                            g = "all";
                            break;
                        case 1:
                            g = "female";
                            break;
                        case 2:
                            g = "male";
                            break;
                    }

                    questions[g].push(value.question);
                });

                context.randomQuestion('male');
                context.randomQuestion('female');
            });
        });
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT user_id, gender');
            sql.push('FROM user_account');
            sql.push('WHERE is_ready = 1 AND is_fake = 1');

            connection.query(sql.join(" "), [], function (error, result) {
                connection.end();

                var g;
                result.forEach(function (value, index) {
                    switch (value.gender) {
                        case 1:
                            g = "female";
                            break;
                        case 2:
                            g = "male";
                            break;
                    }

                    value.gender = g;
                    fakeUsers[g].push(value);
                });
            });
        });
    };

    _init();
}

module.exports = appManager;