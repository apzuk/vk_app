function sessionManager() {

    var sessions = [], context = this;

    this.log = function () {
        log(sessions);
    };

    this.indexOf = function (sessionId) {
        for (var i in sessions) {
            if (sessions[i].sessionId == sessionId)
                return i;
        }

        return null;
    },

        this.indexOfUser = function (userId) {
            for (var i in sessions) {
                if (sessions[i].userId == userId)
                    return i;
            }

            return null;
        },

        this.add = function (sessionData) {
            sessions.push(sessionData);
        },

        this.update = function (sessionId, session) {
            context.remove(sessionId);
            context.add(session);
        },

        this.remove = function (sessionId) {
            var index = this.indexOf(sessionId);
            if (index != null) {
                sessions.splice(index, 1);
            } else {
                return null;
            }
        },

        this.removeByUserId = function (userId) {
            var index = this.indexOf(userId);
            if (index != null) {
                sessions.splice(index, 1);
            } else {
                return null;
            }
        },

        this.getSessionById = function (sessionId) {
            var index = this.indexOf(sessionId);
            if (index != null) {
                return sessions[index];
            } else {
                return null;
            }
        },

        this.getSessionByUserId = function (userId) {
            var index = this.indexOfUser(userId);
            if (index != null) {
                return sessions[index];
            } else {
                return null;
            }
        }

    this.getUserIdBySessionId = function (sessId) {
        var t = this.getSessionById(sessId);
        if (t) {
            return t['userId'];
        }

        return null;
    }
};

module.exports = sessionManager;
