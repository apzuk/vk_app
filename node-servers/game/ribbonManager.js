function ribbonManager() {

    var ribbon = [];

    var context = this;

    var lastUpdate;

    this.update = function () {
        // Fix last update
        lastUpdate = new Date();

        // Empty list
        ribbon.length = 0;

        // Select all list from db!
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT r.id AS ribbon_id,r.comment, a.*,');
            sql.push('   NOW() < a.vip_end AS vip');
            sql.push('FROM user_ribbon r');
            sql.push('LEFT JOIN user_account a');
            sql.push('   ON a.user_id = r.user_id');
            sql.push('ORDER BY id DESC');
            sql.push('LIMIT 0, 15');

            connection.query(sql.join(' '), [], function (result, rows) {
                ribbon = rows;
            });
        });
    };

    this.get = function () {
        return ribbon;
    };

    this.lastUpdate = function (date) {
        if (date) {
            lastUpdate = date;
        }

        return lastUpdate;
    };

    this.insertItem = function (item, isInternalCall) {
        if (!isInternalCall) {
            return false;
        }

        ribbon.unshift(item);
        ribbon.pop();

        lastUpdate = new Date();

        return true;
    }

    /* ====================================================================================================== */
    var _response = function (userId, message, action, module) {
        message['module'] = module ? module : 'ribbonManager';
        appManager.response(userId, message, action);
    };
    /**********************************************************************************************************/

}

module.exports = ribbonManager;