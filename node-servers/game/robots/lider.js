function liderRobot() {

    /**
     *
     * Important cities list
     *
     * @type {Array}
     */
    var cities = [];

    function init() {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT id AS city_id, country_id, city');
            sql.push('FROM sys_cities');
            sql.push('WHERE `important` = 1');

            connection.query(sql.join(" "), [], function (error, rows) {
                connection.end();

                cities = rows;
            });
        });
    }

    init();
}

module.exports = liderRobot;