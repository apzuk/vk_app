function lentaRobot() {

    /**
     *
     * Fake user list
     *
     * @type {Array}
     */
    var usersList;

    /**
     *
     * Is loading users?!
     *
     * @type {boolean}
     */
    var isLoading = false;

    /**
     *
     * @type {boolean}
     */
    var isInProcess = false;

    var loadFakeUsers = function (success_callback) {
        if (usersList != undefined && usersList.length != 0) {
            if (success_callback) success_callback();
            return false;
        }

        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                return;
            }

            isLoading = true;

            var sql = [];
            sql.push('SELECT *, NOW() < vip_end AS vip');
            sql.push('FROM user_account r');
            sql.push('WHERE is_ready = 1 AND is_fake = 1');
            sql.push('ORDER BY RAND()');
            sql.push('LIMIT 150');

            connection.query(sql.join(" "), [], function (result, rows) {
                connection.end();

                usersList = rows;
                isLoading = false;

                if (success_callback && usersList.length > 0) success_callback();
            });
        });

        return true;
    };

    var startListener = function () {

        if (isInProcess) return;

        loadFakeUsers(function () {

            log('called');

            var d1 = ribbonManager.lastUpdate(), d2 = new Date(),
                seconds = Math.round((d2.getTime() - d1.getTime()) / (1000));

            if (seconds >= 30) {
                log('called 1');

                var rand = getRandomArbitrary(0, usersList.length),
                    user = usersList[rand],
                    comment = "";

                isInProcess = true;

                set(user['user_id'], comment, function (ribbon_id) {
                    log('called 2');
                    var item = extend({}, user, {
                        ribbon_id: ribbon_id,
                        comment: comment
                    });

                    ribbonManager.insertItem(item, true);
                    isInProcess = false;

                    usersList.splice(rand, 1);
                });
            }
        });
    };

    var set = function (user_id, comment, success_callback) {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                isInProcess = false;
                return;
            }

            var sql = [];
            sql.push("INSERT INTO user_ribbon SET");
            sql.push("user_id = ?,");
            sql.push("comment = ?");

            connection.query(sql.join(" "), [user_id, comment], function (result, rows) {

                connection.end();

                if (rows && Object.keys(rows).length > 0) {
                    success_callback(rows['insertId']);
                    cleanTable();
                }

            })
        });
    };

    var getRandomArbitrary = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var cleanTable = function () {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
                return;
            }

            var sql = [];
            sql.push("CALL clearRibbonTable();");

            connection.query(sql.join(" "), [], function (result, rows) {
                connection.end();
            })
        });
    };

   // setInterval(startListener, 1000 * 30);
   // startListener();

}


module.exports = lentaRobot;