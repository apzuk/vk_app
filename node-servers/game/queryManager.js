function queryManager() {
    var context = this;

    /******************USER**********************/

    /**
     * @param userId
     * @param success_callback
     */
    this.userIsOnline = function (userId, success_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT online AS is_online');
            sql.push('FROM user_account');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [userId], function () {
                connection.end();

                if (success_callback) {
                    success_callback();
                }
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.isVip = function (user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('SELECT EXISTS(');
            sql.push('   SELECT NOW() < vip_end');
            sql.push('   FROM user_account');
            sql.push('   WHERE user_id = ?');
            sql.push(') AS result');

            connection.query(sql.join(" "), [user_id], function (error, result) {
                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                if (success_callback) {
                    success_callback(result);
                }
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     */
    this.userUpdateGameCount = function (user_id, success_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];

            sql.push('UPDATE user_statistic SET');
            sql.push('game_count = game_count+1');
            sql.push('WHERE user_id = ?');

            if (!success_callback) {
                success_callback = function () {
                    connection.end();
                }
            }

            connection.query(sql.join(" "), [user_id], function () {
                connection.end();

                if (success_callback) {
                    success_callback();
                }
            });
        });
    };

    /**
     * @param user_id
     * @param count
     * @param type
     * @param rType
     * @param ref_id
     * @param rating
     * @param loadUser
     */
    this.userSetRatingHistory = function (user_id, count, type, rType, ref_id, rating, loadUser) {
        pool.getConnection(function (error, connection) {

            if (error) {
                return;
            }

            var sql = [];

            sql.push('INSERT INTO user_rating_history SET');
            sql.push('user_id = ?,');
            sql.push('count = ?,');
            sql.push('type = ?,');
            sql.push('rating_type = ?');

            connection.query(sql.join(" "), [user_id, count, type, rType], function () {
                connection.end();

                if (ref_id) {
                    userManager.setMoneyForReferral(ref_id, rating, user_id)
                } else {
                    if (loadUser) {
                        queryManager.userGet(user_id, function (user) {
                            userManager.setMoneyForReferral(user['ref_id'], user['rating'], user_id);
                        });
                    }
                }
            });
        });
    };

    /**
     * @param user_id
     * @param callback
     */
    this.userGet = function (user_id, callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];

            sql.push('SELECT gender, user_id, username, avatar, sid');
            sql.push('FROM user_account');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {
                connection.end();

                if (callback) {
                    callback(result[0]);
                }
            });
        });
    };

    /**
     * @param user_id
     * @param sid
     * @param success
     * @param fail
     */
    this.checkUserSid = function (user_id, sid, success, fail) {
        context.userGet(user_id, function (user) {
            if (sid == user.sid) {
                if (success) success();
            }
            else {
                if (fail) fail();
            }
        });
    };

    /**
     * @param user_id
     * @param target_user_id
     * @param callback
     */
    this.hasUserLiked = function (user_id, target_user_id, callback) {

        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('SELECT id');
            sql.push('FROM user_likes');
            sql.push('WHERE user_id = ? AND target_user_id = ?');

            connection.query(sql.join(" "), [user_id, target_user_id], function (error, result) {

                connection.end();

                if (error) {
                    log(error);
                }

                if (callback) {
                    callback(result[0]);
                }
            });
        });
    };

    /**
     * @param user_id
     * @param liked_user_id
     * @param success_callback
     * @param error_callback
     */
    this.userLikeUser = function (user_id, liked_user_id, success_callback, error_callback) {

        context.userLikeLog(user_id, liked_user_id, function () {

            context.userGetLikes(liked_user_id, function (userLikeCount) {

                success_callback(userLikeCount);

            });

        }, function () {

            log('Error set like log');

        });
    };

    /**
     * @param user_id
     * @param unliked_user_id
     * @param success_callback
     * @param error_callback
     */
    this.userUnlikeUser = function (user_id, unliked_user_id, success_callback, error_callback) {

        context.userUnsetLikeLog(user_od, unliked_user_id, function () {

            context.userGetLikes(unliked_user_id, function (userLikeCount) {

                success_callback(userLikeCount);

            });
        });

    };

    /**
     * @param user_id
     * @param success_callback
     */
    this.userGetLikes = function (user_id, success_callback) {
        pool.getConnection(function (error, connection) {
            if (error) {
                log(error);
            }

            var sql = [];
            sql.push('SELECT likes');
            sql.push('FROM user_statistic');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {

                connection.end();

                if (success_callback) {
                    success_callback(result[0]['likes']);
                }
            });
        });
    };

    /**
     * @param user_id
     * @param unliked_user_id
     * @param success_callback
     * @param error_callback
     */
    this.userUnsetLikeLog = function (user_id, unliked_user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];

            sql.push('DELETE FROM user_likes');
            sql.push('WHERE user_id = ? AND target_user_id = ?');

            connection.query(sql.join(" "), [user_id, liked_user_id], function (error, result) {

                connection.end();

                if (error) {
                    log(error);
                }

                if (success_callback) {
                    success_callback();
                }
            });
        });
    };

    /**
     * @param user_id
     * @param liked_user_id
     * @param success_callback
     * @param error_callback
     */
    this.userLikeLog = function (user_id, liked_user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('INSERT INTO user_likes SET');
            sql.push('user_id = ?,');
            sql.push('target_user_id = ?');

            connection.query(sql.join(" "), [user_id, liked_user_id], function (error, result) {

                connection.end();

                if (error) {
                    log(error);
                }

                if (success_callback) {
                    success_callback();
                }
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     */
    this.userSetLike = function (user_id, success_callback) {
        pool.getConnection(function (error, connection) {

            if (error) {
                log(error);
            }

            var sql = [];
            sql.push('UPDATE user_statistic SET');
            sql.push('likes = likes + 1');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {
                connection.end();

                success_callback();
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.userGetCost = function (user_id, success_callback, error_callback) {

        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
            }

            var sql = [];
            sql.push('SELECT username, cost, sponsor_user_id');
            sql.push('FROM user_account a');
            sql.push('LEFT JOIN user_sponsor us');
            sql.push('   ON us.target_user_id = a.user_id');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {

                connection.end();
                if (error && error_callback) {
                    error_callback(error);
                }

                if (result[0]) {
                    success_callback(
                        result[0]['cost'],
                        result[0]['username'],
                        result[0]['sponsor_user_id']
                    );
                }

            });

        });

    };

    /**
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.userGetBalance = function (user_id, success_callback, error_callback) {

        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('SELECT balance');
            sql.push('FROM user_account');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                if (result[0]) {

                    success_callback(result[0]['balance']);

                }
            });
        });
    };

    /**
     * @param user_id
     * @param balance
     * @param success_callback
     * @param error_callback
     */
    this.userSetBalance = function (user_id, balance, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('UPDATE user_account SET');
            sql.push('balance = balance + ?');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [balance, user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                success_callback();
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.userUnsetReferral = function (user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('UPDATE user_account SET');
            sql.push('ref_id = null');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                success_callback();
            });
        });
    };

    /**
     * @param user_id
     * @param success_callback
     * @param error_callback
     */
    this.getUserVKLink = function (user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {
            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('SELECT soc_id');
            sql.push('FROM user_account');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                if (result[0]) {
                    success_callback(result[0]);
                }
            });
        });
    };

    /**
     * @param user_id
     * @param balance
     * @param success_callback
     * @param error_callback
     */
    this.userWithdrawBalance = function (user_id, balance, success_callback, error_callback) {

        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            } else {
                log(error);
            }

            var sql = [];
            sql.push('UPDATE user_account SET');
            sql.push('balance = balance-?');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [balance, user_id], function (error, result) {

                connection.end();
                if (error && error_callback) {
                    error_callback(error);
                    return;
                } else {
                    log(error);
                }

                success_callback(result);

            });

        });

    };

    /**
     * @param sponsor_user_id
     * @param target_user_id
     * @param success_callback
     * @param error_callback
     */
    this.userBecomeSponsor = function (sponsor_user_id, target_user_id, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('INSERT INTO user_sponsor SET');
            sql.push('sponsor_user_id = ?,');
            sql.push('target_user_id = ?');
            sql.push('ON DUPLICATE KEY UPDATE');
            sql.push('sponsor_user_id = ?');


            connection.query(sql.join(" "), [sponsor_user_id, target_user_id, sponsor_user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                success_callback(result);

            });

        });

    };

    /**
     * @param user_id
     * @param cost
     * @param success_callback
     * @param error_callback
     */
    this.userSetNewCost = function (user_id, cost, success_callback, error_callback) {
        pool.getConnection(function (error, connection) {

            if (error && error_callback) {
                error_callback(error);
                return;
            }

            var sql = [];
            sql.push('UPDATE user_account SET');
            sql.push('cost = ?');
            sql.push('WHERE user_id = ?');

            connection.query(sql.join(" "), [cost, user_id], function (error, result) {

                connection.end();

                if (error && error_callback) {
                    error_callback(error);
                    return;
                }

                if (success_callback) success_callback(result);

            });

        });
    };
    /****************USER END********************/

    /*****************GAME***********************/
    /**
     * @param jsonGame
     * @param callback
     */
    this.gameSave = function (jsonGame, callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('INSERT INTO game_history SET');
            sql.push('game = "' + encodeURIComponent(jsonGame) + '";');

            if (!callback) {
                callback = function () {
                    connection.end();
                }
            }

            connection.query(sql.join(" "), [], function () {
                connection.end();
            });
        });
    };
    /****************GAME END********************/

    /*******************MESSAGE******************/
    /**
     * @param message_id
     * @param user_id
     * @param success_callback
     * @param fail_callback
     */
    this.messageRemove = function (message_id, user_id, success_callback, fail_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('UPDATE im_message SET');
            sql.push('  sender_status = IF(sender_user_id = ?,"remove",sender_status),');
            sql.push('  receiver_status = IF(receiver_user_id = ?,"remove",receiver_status)');
            sql.push('WHERE message_id = ?');

            connection.query(sql.join(" "), [user_id, user_id, message_id], function (error, result) {
                connection.end();

                if (error) log(error);
                if (success_callback) success_callback();
            });
        });
    };

    /**
     * @param id
     * @param receiver_user_id
     * @param success_callback
     * @param fail_callback
     */
    this.makeMessageAsRead = function (id, receiver_user_id, success_callback, fail_callback) {
        pool.getConnection(function (error, connection) {
            var sql = [];
            sql.push('UPDATE im_message SET');
            sql.push('receiver_status = "read"');
            sql.push('WHERE message_id = ? AND receiver_user_id=?');

            connection.query(sql.join(" "), [id, receiver_user_id], function (error, result) {
                if (error) {
                    console.log(error);
                }

                connection.end();
                if (success_callback) success_callback();
            });
        });
    };
    /****************END MESSAGE*****************/


    /*******************SYSTEM******************/
    this.setNewKey = function (user_id, value, success_callback, fail_callback) {
        pool.getConnection(function (error, connection) {
            var key = md5(new Date() + '|||' + user_id + '|||' + value);

            var sql = [];
            sql.push('INSERT INTO sys_tmpl_key SET');
            sql.push('user_id = ?,');
            sql.push('`key` = ?,');
            sql.push('value = ?');

            log(sql.join(" "));
            connection.query(sql.join(" "), [user_id, key, value], function (error, result) {
                if (error) {
                    log(error);
                }

                connection.end();
                if (success_callback) success_callback(key);
            });
        });
    };
    /****************END SYSTEM*****************/
}

module.exports = queryManager;
