CREATE  TABLE `im` (
  `im_id` INT NOT NULL ,
  `creator_user_id` INT NULL ,
  `create_ts` DATETIME NULL ,
  PRIMARY KEY (`im_id`) );

ALTER TABLE im ENGINE = InnoDB;

ALTER TABLE `im`
ADD CONSTRAINT `IM_creator_user_id_FK_user_id`
FOREIGN KEY (`creator_user_id` )
REFERENCES `user_account` (`user_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `IM_creator_user_id_FK_user_id` (`creator_user_id` ASC) ;


ALTER TABLE `im` DROP FOREIGN KEY `IM_creator_user_id_FK_user_id` ;
ALTER TABLE `im` ADD COLUMN `user_2_id` INT NULL  AFTER `user_1_id` , CHANGE COLUMN `creator_user_id` `user_1_id` INT(11) NULL DEFAULT NULL  ,
ADD CONSTRAINT `IM_creator_user_id_FK_user_id`
FOREIGN KEY (`user_1_id` )
REFERENCES `user_account` (`user_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `im` DROP FOREIGN KEY `IM_creator_user_id_FK_user_id` ;
ALTER TABLE `im`
ADD CONSTRAINT `IM_user_1_id_FK_user_id`
FOREIGN KEY (`user_1_id` )
REFERENCES `user_account` (`user_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `IM_user_2_id_FK_user_id`
FOREIGN KEY (`user_2_id` )
REFERENCES `user_account` (`user_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, DROP INDEX `IM_creator_user_id_FK_user_id`
, ADD INDEX `IM_user_1_id_FK_user_id` (`user_1_id` ASC)
, ADD INDEX `IM_user_2_id_FK_user_id` (`user_2_id` ASC) ;


CREATE  TABLE `im_message` (
  `message_id` INT NOT NULL AUTO_INCREMENT ,
  `sender_user_id` INT NULL ,
  `receiver_user_id` INT NULL ,
  `message` TEXT NULL ,
  `sender_status` ENUM('read','unread') NULL ,
  `receiver_status` ENUM('read','unread') NULL ,
  `create_ts` TIMESTAMP NULL ,
  PRIMARY KEY (`message_id`) );


ALTER TABLE im_message ENGINE = InnoDB;

ALTER TABLE `im_message`
ADD CONSTRAINT `IM_sender_user_id_FK_user_id`
FOREIGN KEY (`sender_user_id` )
REFERENCES `im_message` (`message_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `IM_receiver_user_id_FK_user_id`
FOREIGN KEY (`receiver_user_id` )
REFERENCES `user_account` (`user_id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `IM_sender_user_id_FK_user_id` (`sender_user_id` ASC)
, ADD INDEX `IM_receiver_user_id_FK_user_id` (`receiver_user_id` ASC);

ALTER TABLE `im` CHANGE COLUMN `create_ts` `create_ts` TIMESTAMP NULL DEFAULT NULL;

ALTER TABLE `user_account` ADD COLUMN `soc_id` VARCHAR(255) NULL  AFTER `gender`;

/**********************************************************************************************************************/

UPDATE `vkApp`.`user_account` SET `soc_id`='80576550' WHERE `user_id`='1';
UPDATE `vkApp`.`user_account` SET `soc_id`='80576551' WHERE `user_id`='2';
UPDATE `vkApp`.`user_account` SET `soc_id`='80576552' WHERE `user_id`='3';
UPDATE `vkApp`.`user_account` SET `soc_id`='80576553' WHERE `user_id`='100';
UPDATE `vkApp`.`user_account` SET `soc_id`='80576554' WHERE `user_id`='101';
UPDATE `vkApp`.`user_account` SET `soc_id`='80576555' WHERE `user_id`='102';
